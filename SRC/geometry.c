/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

/***************************************************************************
                                  geometry.c
                                  ----------
 This is geometry: here the external grid is read and preprocessed to store
       and compute all the usefull informations for the computation
                             -------------------
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include "common.h"

extern int initial_state, btableimax, btablejmax, blending, NN, grad_reco;
extern double gridsize, cut_off_U;
extern double **btablex, **btabley, **btableb;
extern void (*set_numerical_integration) (void);
extern const int size;
extern struct node_struct *node;
double dump;

void read_spatial_grid(void);
void element_preprocessing(void);
void boundary_preprocessing(void);
void ref_grid(void);
void scale_grid(void);
void coeff_qlsgr(void);
void install_probes(void);
void weight_coefficients(void);
void correction_matrices(void);
void moments(void);
void compute_bases(void);

void geometry(void)
{
	/* read external grid */
	printf("          *************************************\n");
	printf("          **     Reading the gridfile.....   **\n");
	printf("          *************************************\n");
	printf("\n");
	read_spatial_grid();
	scale_grid();	

	printf("          *************************************\n");
	printf("          **    Numerical integration...     **\n");
	printf("          *************************************\n");
	printf("\n");
	set_numerical_integration();

	/* geometry pre-processing */
	printf("          *************************************\n");
	printf("          **  Processing element geometry... **\n");
	//if (initial_state == 24 || initial_state == 25) {
	if (initial_state == 25 || initial_state ==84 || initial_state == 92) {
		FILE *bedfile;
		if (24 == initial_state)
			bedfile = fopen("../input/bathy_shelf_ideal.dat", "r");
		else if (25 == initial_state)
			bedfile = fopen("../input/bathy_okushiri.dat", "r");
		else if (84 == initial_state)
			bedfile = fopen("../input/bathy_oregon.dat", "r");
		else if (92 == initial_state)
			bedfile = fopen("../input/biarritz.sol", "r");
		if (!bedfile) {
			printf("ERROR: inputfile-exp.txt not found!\n");
			exit(EXIT_FAILURE);
		}

		printf("limits : imax = %d, jmax = %d\n", btableimax, btablejmax);

		if(initial_state ==92) {
			//for (int i=0; i<1; i++)
			//	fscanf(bedfile, "\n");
			fscanf(bedfile,"MeshVersionFormatted 2\n");
			fscanf(bedfile,"\n");
			fscanf(bedfile,"Dimension 2\n");
			fscanf(bedfile,"\n");
			fscanf(bedfile,"SolAtVertices\n");
			fscanf(bedfile,"%le\n", &dump);
			fscanf(bedfile,"1 1\n");
			fscanf(bedfile,"\n");
			for (int i = 0; i < btableimax; i++){
				fscanf(bedfile, " %le\n",  &btableb[i][0]);
			}
		}else{
			for (int i = 0; i < btableimax; i++)
				for (int j = 0; j < btablejmax; j++)
					fscanf(bedfile, "%le %le %le\n", &btablex[i][j], &btabley[i][j], &btableb[i][j]);
		}
		fclose(bedfile);
	
	}

	boundary_preprocessing();
	ref_grid();
	element_preprocessing();
	install_probes();


	printf("          **     Grid size: %le     **\n", gridsize);
	printf("          **     Cut off U: %le     **\n", cut_off_U);
	printf("          *************************************\n");
	printf("\n");
}
