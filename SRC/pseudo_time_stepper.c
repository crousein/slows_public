/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/


/***************************************************************************
pseudo_time_stepper.c
---------------------
    This is pseudo_time_stepper : it drives an explicit pseudo time loop
	to convergence through the computation of the nodal residuals and
	the nodal updates
-------------------
***************************************************************************/
#include <stdio.h>
#include "common.h"
#include "math.h"
#include "time.h"
#ifdef WITH_NHT
#include "nht.h"
extern int model_dispersion;
#endif

clock_t start, end;
double cpu_time_used,Time_rk;

extern const int size, initial_state, time_discretization;
extern int NBF, NBF2, NBF3,grad_reco;
extern int iteration, iteration_max, info, NN, NE, vertex, was_negative,  first_cell, scheme, numerics_friction_explicit, blending, time_step;
extern double first_time, residual_treshold, residual_norm, ref_norm, dt, dt1, dt2, Time, cut_off_H, HHref, a_RK[3][4];
double int_Bdry_new, int_Bdry_old, wb_mass_loss, remap_mass_loss, abdf[6];
extern double int_H_new, int_H_old, int_B_new, int_B_old, shoreline_flux, coarse_cut_off, cut_off_U, t_max;
extern struct node_struct *node;
extern struct element_struct *element;
extern struct boundary_nodes_struct *b_nodes;
extern struct boundary_nodes2_struct *b_nodes2;
extern void (*gradient_reconstruction) (int, int, int);

extern double *nht_h;
extern double *nht_u;
extern double *nht_v;
extern double *nht_eta;
extern double *nht_bed;
extern double *nht_phi;
extern double *nht_k;
extern double *nht_e;
extern int *nht_bb;
extern double *nht_dxh;
extern double *nht_dxhu;
extern double *nht_dxhv;
extern double *nht_dxeta;
extern double *nht_dyh;
extern double *nht_dyhu;
extern double *nht_dyhv;
extern double *nht_dyeta;
extern int model_breaking;

void compute_norm(void);
void nodal_residuals(void);
void boundary_conditions(void);
void P_2_Z(double *, double *, double);
void Z_2_P(double *, double *, double);
void Z_2_K(double *, double *, double, double);
void friction(int, int);
void compute_reference_values(void);
void sponge_bc(int);
void compute_tide_amplitude(void);
void compute_INOUT_elev(void);
void node_reconstruction( void );
void compute_rhs_ex(double);
void pass_the_values( void );
void get_PHI_bar( void );
double reconstructed_fun(double*, int , int );

void pseudo_time_stepper(void)
{
	/* starting the pseudo time loop */
	for (int n = 0; n < NN; n++) {
		if (!(node[n].P[2][0] > cut_off_H * node[n].cut_off_ratio)) {
			node[n].dry_flag2 = 1;
			node[n].P[2][0] = node[n].Z[2][0] = node[n].K[2][0] = 0.;
			node[n].P[2][1] = node[n].Z[2][1] = node[n].K[2][1] = 0.;
			node[n].P[2][2] = node[n].Z[2][2] = node[n].K[2][2] = 0.;
		} else {
			node[n].dry_flag2 = 0;
		}

		if (!(node[n].P[1][0] > cut_off_H * node[n].cut_off_ratio)) {
			node[n].dry_flag1 = 1;
			node[n].P[1][0] = node[n].Z[1][0] = node[n].K[1][0] = 0.;
			node[n].P[1][1] = node[n].Z[1][1] = node[n].K[1][1] = 0.;
			node[n].P[1][2] = node[n].Z[1][2] = node[n].K[1][2] = 0.;
		} else {
			node[n].dry_flag1 = 0;
		}

		if (!(node[n].P[0][0] > cut_off_H * node[n].cut_off_ratio)) {
			node[n].dry_flag0 = 1;
			node[n].P[0][0] = node[n].Z[0][0] = node[n].K[0][0] = 0.;
			node[n].P[0][1] = node[n].Z[0][1] = node[n].K[0][1] = 0.;
			node[n].P[0][2] = node[n].Z[0][2] = node[n].K[0][2] = 0.;
		} else {
			node[n].dry_flag0 = 0;
		}
	}

	iteration = 0;
	int prj_flag = 0;
	do {
		int itr = iteration;
                if(itr == 0) Time_rk =Time - dt;
                if(itr == 1) Time_rk =Time - dt + dt;
                if(itr == 2) Time_rk =Time - dt + 0.5*dt;

		iteration += 1;

		compute_reference_values();


		if (NULL != gradient_reconstruction)
			gradient_reconstruction(size, itr, prj_flag);


		nodal_residuals();

		if (83 == initial_state){ // adds the integral over the volume of the analytic function for the manufactured solution, for the SWeq
#ifdef WITH_NHT
			if(0 == model_dispersion) {
#endif
				compute_rhs_ex(Time_rk);
				for ( int n = 0 ; n < NN ; n ++ ){
					node[n].Res[1] -= dt*node[n].rhs[0]*node[n].volume;
				}
#ifdef WITH_NHT
			}
#endif
		}

		if (85 == initial_state) // channel
			compute_INOUT_elev();

        
		for ( int n = 0 ; n < NN ; n++ )
			node[n].flag = 0 ;
		
        
		boundary_conditions();

//		start=clock();
#ifdef WITH_NHT
		if (1 == model_dispersion) {
			//if (2 <= blending)
			node_reconstruction(); // nodal value reconstruction for FV/FE interface
			pass_the_values(); //copy the values of the unknowns to nonHydroInput
			if ( 2 == model_breaking ) {
				/* accounts for the breaking nodes. 1 if the node is breaking 0 if it is not */
				/* energy for the GN equations */
				/* kinetic energy for the breaking model */
				if ( EXIT_SUCCESS != nht_phi_bb_k_e( nht_h, nht_u, nht_v, nht_eta, nht_dxh, nht_dxhu, nht_dxhv, nht_dxeta, nht_dyh, nht_dyhu, nht_dyhv, nht_dyeta, NN, Time_rk, nht_phi,nht_bb, nht_e, nht_k ) ) {
					printf( "error initializing non hydrodynamic terms lib. Exiting\n" );
					exit( EXIT_FAILURE );
				}
			} else {
				/* energy for the GN equations */
				/* accounts for the breaking nodes. 1 if the node is breaking 0 if it is not */
				if ( EXIT_SUCCESS != nht_phi_bb_e( nht_h, nht_u, nht_v, nht_eta, nht_dxh, nht_dxhu, nht_dxhv, nht_dxeta, nht_dyh, nht_dyhu, nht_dyhv, nht_dyeta, NN, Time_rk, nht_phi, nht_bb, nht_e ) ) {
					printf( "error initializing non hydrodynamic terms lib. Exiting\n" );
					exit( EXIT_FAILURE );
				}
			}
			get_PHI_bar(); //constructs PHI_bar as to add in the equations
			for ( int n = 0 ; n < NN ; n ++ ){
				for ( int i = 0 ; i < size ; i++ ){
					//node[n].Res[i] -= node[n].PHI_bar[1][i];//  if you don't do mass lumping on FVM
					node[n].Res[i] -= node[n].PHI_bar[1][i]*node[n].volume;  //if you do mass lumping on FVM

				}
			}
		}
#endif
//		exit(0);
//		end=clock();
//      	cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
//		printf("The time for the library call is %le\n", cpu_time_used);
 
		if (79 == initial_state) //gironde+garonne+dordogne
                        compute_tide_amplitude();



		wb_mass_loss = 0.;
		HHref = 0.;
		int_H_new = 0.;
		int_H_old = 0.;
		int_B_new = 0.;
		int_B_old = 0.;
		int_Bdry_new = 0.;
		int_Bdry_old = 0.;
		double A_wet = 0.;
		double M_eps = 0.;

		for (int n = 0; n < NN; n++) {
			node[n].h1 = node[n].h2;
			node[n].h2 = 100000.;

			// nodes belonging to wet cells: update in water height
			// nodes belonging to dry cells: update in water depth
			if (!node[n].fully_dry) {
				node[n].P[0][0] += node[n].bed[0];
				if ( iteration != 1 )
					node[n].P[2][0] += node[n].bed[2];
				else
					node[n].P[2][0] += node[n].bed[0];
			}

			// update
			for (int i = 0; i < size; i++) {
				if (1 == numerics_friction_explicit){
					if (2>blending){ // Update for RD schemes
						node[n].P[2][i] = node[n].P[0][i] - node[n].Res[i]/node[n].mod_volume;
					}else{ // Update for FV schemes
						if(i==0)
							node[n].P[2][i] = a_RK[itr][0]*node[n].P[0][i] + a_RK[itr][1]*(node[n].P[2][i] - node[n].Res[i]/node[n].mod_volume) ;//+ dt*node[n].wg);
						else
							node[n].P[2][i] = a_RK[itr][0]*node[n].P[0][i] + a_RK[itr][1]*(node[n].P[2][i] - node[n].Res[i]/node[n].mod_volume);
					}
				} else { /* Semi-implicit treatment of friction terms (Kurganov) */
					if (2>blending) // Update for RD schemes
						node[n].P[2][i] = node[n].P[0][i] - node[n].Res[i]/node[n].mod_volume;
					else { // Update for FV schemes
						friction( n, 2);
						double fr = node[n].volume/node[n].mod_volume*node[n].friction[2][i];

						if (iteration==iteration_max+1) {
							node[n].P[2][i] = ( node[n].P[2][i] + dt*node[n].Res[i]/node[n].mod_volume * fr )/( 1 - dt*dt*fr*fr );
						} else {
							node[n].P[2][i] = a_RK[itr][0]*node[n].P[0][i] + a_RK[itr][1]*( node[n].P[2][i] - node[n].Res[i]/node[n].mod_volume) / ( 1 + dt*fr );
						}
					}
				}
			}

			// Well-Balanced correction for moving grids at shoreline
			if (!node[n].fully_dry) {
				// node is moving from D->W
				node[n].P[2][0] -= (node[n].bed[0] - node[n].max_Htot0);
				double temp_val = node[n].P[2][0] - node[n].bed[2];

				// node is moving from W->D
				node[n].P[0][0] = MAX(0., node[n].P[0][0] - node[n].bed[0]);
				node[n].P[2][0] = MAX(0., node[n].P[2][0] - node[n].bed[2]);

				// mass loss for wb corrections
				wb_mass_loss += node[n].old_volume * (node[n].max_Htot0 - node[n].bed[0]) + node[n].mod_volume * (node[n].P[2][0] - temp_val);
			} else {
				int_Bdry_new += node[n].mod_volume * node[n].bed[2];
				int_Bdry_old += node[n].old_volume * node[n].bed[0];
			}

			// Semi-implicit friction treatment for RD schemes:
			if ( (2 > blending) && (0 == numerics_friction_explicit)) {
				P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio);
				friction(n, 2);
				for (int l = 0; l < size; l++)
					node[n].P[2][l] /= (1. + dt * node[n].volume / node[n].mod_volume * node[n].friction[2][l]);
			}

			// Wetted area and mass correction
			if (node[n].P[2][0] >= cut_off_H * node[n].cut_off_ratio)
				A_wet += node[n].mod_volume;
			else
				M_eps += node[n].P[2][0] * node[n].mod_volume;

		}

		// mass loss in the remap step: B_{DRY}^{n+1} - B_{DRY}^{n} + \int_{DRY} div(b\sigma)
		remap_mass_loss = int_Bdry_new - int_Bdry_old + shoreline_flux;
		//double tot_mass_loss = wb_mass_loss + remap_mass_loss;


		/* Dry node detection - mass correction */
		for (int n = 0; n < NN; n++) {
			node[n].dry_flag2 = 0;
			node[n].dry_flag3 = 0;
			node[n].dry_flag4 = 0;
		}
		for (int n = 0; n < NN; n++) {

			if (node[n].P[2][0] >= cut_off_H * node[n].cut_off_ratio) {
				node[n].dry_flag2 = 0;
				node[n].P[2][0] += (M_eps ) / A_wet;  // TODO ANDREA have commented this line because this causes problems with the periodic bc. in the Volker reef case.
			} else {
				node[n].dry_flag2 = 1;
				node[n].P[2][0] = 0.;
				node[n].P[2][1] = 0.;
				node[n].P[2][2] = 0.;
			}

			if (node[n].P[0][0] >= cut_off_H * node[n].cut_off_ratio) {
				node[n].dry_flag0 = 0;
			} else {
				node[n].dry_flag0 = 1;
				node[n].P[0][0] = 0.;
				node[n].P[0][1] = 0.;
				node[n].P[0][2] = 0.;
			}


			if(node[n].dry_flag2==1){ // if a node is dry put to his neighbors dry_flag3=1
				for(int i=0; i<node[n].num_neigh; i++){
					int m=node[n].neigh[i];
					node[m].dry_flag3=1;
				}
			}

			// HHref: max water depth
			if (node[n].P[2][0] > HHref)
				HHref = node[n].P[2][0];

			// Warning if Negative water depth
			if (node[n].P[2][0] < -cut_off_H) {
				was_negative = 1;
				printf("Node %d is negative, cut off : %le , height: %le, ds: %le, res: %le\n",
				       n, -cut_off_H, node[n].P[2][0], node[n].dtau, node[n].Res[0]);

				node[n].P[2][0] = -1.;

				if (first_time < 0) {
					first_time = Time;
					first_cell = n;
				}
			}

			/* Sponge layer correction */
			//TODO: ADD A TEXTINPUT SWITCH
			sponge_bc(n);

			// P --> Z
			P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio);
	
			
			//start--- For velocity limiting!!!
			//if (  node[n].u_norm < cut_off_U ) printf(" WARNING WARNING!!!!!!!   %le \n",node[n].u_norm) ;
             
             	/*	double  vel_norm  = sqrt( node[n].Z[2][1]*node[n].Z[2][1] + node[n].Z[2][2]*node[n].Z[2][2]);
             		double u_limit = vel_norm/node[n].u_norm ;
            		int enable = 0;//0;//1 ; //VELOCITY LIMITING ENABLED if positive !!!!
             
             		if ( ( u_limit  >  1.0  ) && ( enable  > 0 ) ){
                 		node[n].Z[2][1] /= u_limit ;
                 		node[n].Z[2][2] /= u_limit ;
             		}
			//end--- For velocity limiting!!!
		*/	

			// Z --> P (* important to have compatibilty between Z and P)
			Z_2_P(node[n].Z[2], node[n].P[2], node[n].cut_off_ratio);

			if (1 == numerics_friction_explicit)
				friction( n, 2 );

			// computing integrals
			int_H_new += node[n].mod_volume * node[n].P[2][0];
			int_B_new += node[n].mod_volume * node[n].bed[2];
			int_H_old += node[n].old_volume * node[n].P[0][0];
			int_B_old += node[n].old_volume * node[n].bed[0];

		}

		for (int n = 0; n < NN; n++) {

			if(node[n].dry_flag3==1){
				for(int i=0; i<node[n].num_neigh; i++){
					int m=node[n].neigh[i];
					node[m].dry_flag4=1;
				}
			}

			if (scheme > 0) {
				Z_2_K(node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio);
			} else {
				for (int i = 0; i < size; i++)
					node[n].K[2][i] = node[n].Z[2][i];
			}
		}
		compute_norm();


		if (info) {
			printf("************************************************************************************\n");
			printf("************************************************************************************\n");
			printf("ITERATION %d|| REF %le|| NORM %le|| TARGET DROP %le\n", iteration, ref_norm, residual_norm, residual_treshold);
			printf("************************************************************************************\n");
		}
	} while (iteration < (iteration_max + ( 1 == numerics_friction_explicit ? 0 : 1 )) ); // non explicit treatment of friction requires one more iteration: Kurganov
}
