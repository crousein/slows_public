/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

/***************************************************************************
 MUSCL_distribution.c
 ----------------
 This is MUSCL_distribution: here the distribution of the cell fluctuation is
 performed through  distribution function allowing to recover 
 the Finite Volume MUSCL scheme
 -------------------
 ***************************************************************************/
#include <math.h>
#include "common.h"

extern const int size, vertex, initial_state;
extern int iteration, numerics_friction_explicit, blending, model_dispersion, face_q_pts,grad_reco;
extern double gm, dt, gridsize, cut_off_H, temp_scalar;
extern double *work_vector0, *work_vector1, temp_normal[2+1], *FLUX, *FV_sigma_barx, *FV_sigma_bary;
extern double sigma_barx, sigma_bary, ubar0, vbar0, height0, speed_of_sound0;
extern double *FV_ubar0, *FV_vbar0, *FV_height0, *FV_speed_of_sound0, *FV_sigma_barx, *FV_sigma_bary;
extern double UUinf, VVinf, L_ref, gridsize;
extern double **W, **PHIN_d, ***K_i_p1, ***sK_i_p1, ***fK_i_p1, **Right, **Left, *Lambda;
extern struct node_struct *node;
extern int *nht_bb;
extern struct element_struct *element;
extern struct numerical_int_struct numerical_int;

void ad_flux_P(double *, double *, double);
void Eigenvectors(double *);
void Waves(double *);
void P_2_Z(double *, double *, double);
void Z_2_P(double *, double *, double);
double face_value( double, double, int, int ,int, int);
double reconstructed_fun(double *, int , int );
double base1( double *, int);
double base2( double *, int);
double base3( double *, int);
double base4( double *, int);
double base5( double *, int);

/*  Van Albada-Van Leer slope limiter */
double limiter(double a, double b)
{
	const double EPS = 1e-15;
	double result;
	if (a * b > 0.)
		result = ((a * a + EPS) * b + (b * b + EPS) * a) / (a * a + b * b + 2. * EPS);
	else
		result = 0.;
	return result;
}

double limiter2(double a, double b)
{
	const double EPS=1e-15 ;
	double result;
	if (a * b > 0.)
		result = (a*b + fabs(a*b) +pow(EPS,2) )/( a*a+b*b +pow(EPS,2) ) ;
	else
		result = 0.;
	return result;
}

/* DISTRIBUTE SHALLOW WATER FLUCTUATION */
void distribute_FV_fluctuation_V3(int e)
{
	int itr = iteration - 1;
	double max_Htot;
	if (itr == 0)
		max_Htot = -10.;
	else
		max_Htot = 0.;
	/* Storing nodal values with WB correction */
	//double W0[vertex][size];
	for (int v = 0 ; v < 3 ; v++ ) {
		int vv = element[e].node[v];
		for (int i = 0 ; i < size ; i++ ) {
			W[v][i] = node[vv].P[2][i];
			//W0[v][i] = node[vv].P[2][i];
			if (itr > 0)
				W[v][i] = 0.5 * node[vv].P[0][i] + 0.5 * node[vv].P[2][i];
			PHIN_d[v][i] = 0.0;
		}
		if ( !node[vv].dry_flag2 )
			if ( ( node[vv].P[2][0] + node[vv].bed[2] ) >  max_Htot ){
				max_Htot = node[vv].P[2][0] + node[vv].bed[2];
			}
	}
	// Well-Balanced correction at shoreline: cancelling artificial fluxes	
	for (int v = 0; v < 3; v++) {
		int vv = element[e].node[v];
		if ((node[vv].dry_flag2) && ((node[vv].bed[2] + cut_off_H * node[vv].cut_off_ratio) > max_Htot))
			W[v][0] += max_Htot;
		else
			W[v][0] +=  node[vv].bed[2];

		//if (  ((node[vv].dry_flag0) && ((node[vv].bed[0] + cut_off_H * node[vv].cut_off_ratio) > max_Htot))
		//    ||((node[vv].dry_flag2) && ((node[vv].bed[2] + cut_off_H * node[vv].cut_off_ratio) > max_Htot)) )
		//	 	W0[v][0]=W[v][0]; 
	}

	int num_q_pts = 1;

	/* FV scheme with Roe approximate solver */
	int k = 0 ;
	double lim[num_q_pts][vertex][size], L[num_q_pts][vertex][size], limbed[num_q_pts][vertex] ;
	for (int i = 0 ; i < (vertex-1) ; i++ ) {
		for (int j = i+1 ; j < vertex ; j++ ) {	
	      		temp_normal[0] = element[e].FVnormal[k][0] ;
		      	temp_normal[1] = element[e].FVnormal[k][1] ;		
		      	int ni = element[e].node[i] ;	
	      		int nj = element[e].node[j] ;

			double xGauss[2], yGauss[2];
			double lengthx_niG[2],lengthy_niG[2];
			double lengthx_Gnj[2],lengthy_Gnj[2];
			for (int f = 0; f < num_q_pts; f++) {
				xGauss[f] = numerical_int.face_coordinate[f][0] * element[e].mp[k][0] + numerical_int.face_coordinate[f][1] * element[e].cg[0];
				yGauss[f] = numerical_int.face_coordinate[f][0] * element[e].mp[k][1] + numerical_int.face_coordinate[f][1] * element[e].cg[1];
				if (num_q_pts==2){
			      		lengthx_niG[f] = xGauss[f] - node[ni].coordinate[0] ; 
			      		lengthy_niG[f] = yGauss[f] - node[ni].coordinate[1] ;     	
			      		lengthx_Gnj[f] = -(xGauss[f] - node[nj].coordinate[0]) ; 
			      		lengthy_Gnj[f] = -(yGauss[f] - node[nj].coordinate[1]) ;     	
				} else {
					double rig[2] = {0};
					double rjg[2] = {0}; 
					/***********************************************/
					// For the conservation of the mean:
					/***********************************************/
					rig[0] = node[ni].coordinate[0] - node[ni].DcC[0]; 
					rig[1] = node[ni].coordinate[1] - node[ni].DcC[1]; 
					rjg[0] = node[nj].coordinate[0] - node[nj].DcC[0]; 
					rjg[1] = node[nj].coordinate[1] - node[nj].DcC[1]; 
					/***********************************************/
			      		lengthx_niG[f] = 0.5*(node[nj].coordinate[0] - node[ni].coordinate[0]) + rig[0]; 
			      		lengthy_niG[f] = 0.5*(node[nj].coordinate[1] - node[ni].coordinate[1]) + rig[1];     	
		      			lengthx_Gnj[f] = 0.5*(node[nj].coordinate[0] - node[ni].coordinate[0]) + rjg[0]; 
		      			lengthy_Gnj[f] = 0.5*(node[nj].coordinate[1] - node[ni].coordinate[1]) + rjg[1];     	
				}
			}

			if ( node[ni].dry_flag2 || node[nj].dry_flag2 ) {
				for (int f = 0; f < num_q_pts; f++) {
					for ( int l = 0 ; l < size ; l++ ) {
						lim[f][i][l] = 0.0;
						lim[f][j][l] = 0.0;
						L[f][i][l] = 0.0;
						L[f][j][l] = 0.0;
				   	}
				  	limbed[f][i] = 0.0;
			  		limbed[f][j] = 0.0;
				}
			} else { 
	      			// Slope Limiter for reconstructing gradients at interface	
				for (int f = 0; f < num_q_pts; f++) {
			      		for ( int l = 0 ; l < size ; l++ ) {
						double cc = node[nj].P[2][l] - node[ni].P[2][l] ;
//						if (model_dispersion==0 || nonHydro.bb[ni]==1 || nonHydro.bb[nj]==1){ // if SW or one of the two nodes is flagged as breaking node, apply the limiter
//							double aa = 2.*( node[ni].dxP2[0][l]*lengthx_niG[f] + node[ni].dyP2[0][l]*lengthy_niG[f] ) - cc ;
//							double bb = 2.*( node[nj].dxP2[0][l]*lengthx_Gnj[f] + node[nj].dyP2[0][l]*lengthy_Gnj[f] ) - cc ;
//							lim[f][i][l] = limiter2( aa , cc )*(1./3. *aa + 2./3.*cc) ; 
//							lim[f][j][l] = limiter2( bb , cc )*(1./3. *bb + 2./3.*cc) ;		
//						} else {
							double aa = node[ni].dxP2[0][l]*lengthx_niG[f] + node[ni].dyP2[0][l]*lengthy_niG[f] ;
//							double hh = node[ni].dxxP2[0][l]*lengthx_niG[f]*lengthx_niG[f] + node[ni].dyyP2[0][l]*lengthy_niG[f]*lengthy_niG[f] + 2.*node[ni].dxyP2[0][l]*lengthx_niG[f]*lengthy_niG[f];
							double bb = node[nj].dxP2[0][l]*lengthx_Gnj[f] + node[nj].dyP2[0][l]*lengthy_Gnj[f] ;
//							double kk = node[nj].dxxP2[0][l]*lengthx_Gnj[f]*lengthx_Gnj[f] + node[nj].dyyP2[0][l]*lengthy_Gnj[f]*lengthy_Gnj[f] + 2.*node[nj].dxyP2[0][l]*lengthx_Gnj[f]*lengthy_Gnj[f];
//							lim[f][i][l] = aa + 0.5*hh;
//							lim[f][j][l] = bb - 0.5*kk;		
							lim[f][i][l] = (2./3.*aa + 0.5*1./3.*cc);
							lim[f][j][l] = (2./3.*bb + 0.5*1./3.*cc);
//						}
						L[f][i][l] = lim[f][i][l] ;
						L[f][j][l] = lim[f][j][l] ;		
					}
		
					double cc = node[nj].bed[2] - node[ni].bed[2] ;
					double aa = node[ni].dxbed[0]*lengthx_niG[f] + node[ni].dybed[0]*lengthy_niG[f] ;
					double bb = node[nj].dxbed[0]*lengthx_Gnj[f] + node[nj].dybed[0]*lengthy_Gnj[f] ;
//					limbed[f][i] = aa;
//					limbed[f][j] = bb;		
					limbed[f][i] = (2./3.*aa + 0.5*1./3.*cc);
					limbed[f][j] = (2./3.*bb + 0.5*1./3.*cc);		
					
					L[f][i][0] += limbed[f][i] ;
					L[f][j][0] += limbed[f][j] ;		

				}
			}


			/* Numerical Treatment of artificial velocities */
			double eta_node_i, eta_node_j;
			double bed2_i, bed2_j;
			if ( ( node[ni].dry_flag2 ) && ( ( node[ni].bed[0]+cut_off_H*node[ni].cut_off_ratio ) > max_Htot ) ) { 
				bed2_i = max_Htot; 
			} else { 
				bed2_i = node[ni].bed[2]; 
			}

  	
			if ( ( node[nj].dry_flag2 ) && ( ( node[nj].bed[0]+cut_off_H*node[nj].cut_off_ratio ) > max_Htot ) ) { 
				bed2_j = max_Htot; 
			} else { 
				bed2_j = node[nj].bed[2] ; 
			}	

			double bed_i[num_q_pts], eta_i[num_q_pts];
			double bed_j[num_q_pts], eta_j[num_q_pts];
			double rr = sqrt( temp_normal[0]*temp_normal[0] + temp_normal[1]*temp_normal[1] )/gridsize ;
	
			/* Centered Flux F_i */
			for (int f = 0; f < num_q_pts; f++) {
				bed_i[f] = bed2_i + limbed[f][i];
				eta_node_i = node[ni].P[2][0] + bed2_i;
				eta_i[f] = eta_node_i + lim[f][i][0] + limbed[f][i] ;
				bed_j[f] = bed2_j - limbed[f][j];
				eta_node_j = node[nj].P[2][0] + bed2_j;
				eta_j[f] = eta_node_j - lim[f][j][0] - limbed[f][j] ;
			}

			for (int f = 0; f < num_q_pts; f++) {
				for (int l = 0 ; l < size ; l++ ){
					work_vector0[l] = node[ni].P[2][l] ;
					work_vector1[l] = node[ni].P[2][l] + lim[f][i][l] ; 
				} // reconstructed variables 

				ad_flux_P( temp_normal, work_vector1, rr ) ;
				double pressure = 0.5*gm*( eta_i[f]*eta_i[f] - 2.0*eta_i[f]*bed_i[f] ) ;		

				FLUX[0] += 0.; 
				FLUX[1] += pressure*temp_normal[0];
				FLUX[2] += pressure*temp_normal[1]; 	       
	
				double source[size];
				source[0] = 0.;
				source[1] = 0.5* gm* (eta_i[f]+eta_j[f]) *bed_i[f]*temp_normal[0];// can be improved for high order well-balancedness
				source[2] = 0.5* gm* (eta_i[f]+eta_j[f]) *bed_i[f]*temp_normal[1];

				/* Higher order reconstruction of the source term + Boundary flux term*/
				double source_cor[size];
				source_cor[0] = 0.;
				source_cor[1] = 0.5*gm* (eta_i[f]+eta_node_i) *(bed_i[f] - bed2_i) *temp_normal[0];// can be improved for high order well-balancedness
				source_cor[2] = 0.5*gm* (eta_i[f]+eta_node_i) *(bed_i[f] - bed2_i) *temp_normal[1];

				for (int l = 0 ; l < size ; l++ ) {
					if (num_q_pts==2){
				  		PHIN_d[i][l] += dt * 0.5 * numerical_int.face_weight[f] * ( FLUX[l] - source[l] + 2.0*source_cor[l]);
				  		PHIN_d[j][l] -= dt * 0.5 * numerical_int.face_weight[f] * ( FLUX[l] + source[l] );
					} else {
					  	PHIN_d[i][l] += dt * 0.5 * ( FLUX[l] - source[l] );
				  		PHIN_d[j][l] -= dt * 0.5 * ( FLUX[l] + source[l] );
					}
				}
			}
			

			ad_flux_P( temp_normal, work_vector0, rr );
			work_vector0[0] += bed2_i;
			double pressure = 0.5*gm*( work_vector0[0]*work_vector0[0] - 2.0*work_vector0[0]*bed2_i );
			FLUX[0] += 0.; 
			FLUX[1] += pressure*temp_normal[0];
			FLUX[2] += pressure*temp_normal[1]; 	      

			for (int l = 0 ; l < size ; l++ ) 
		  		PHIN_d[i][l] -= dt*2.0*0.5* (FLUX[l]);

			/* Centered Flux F_j */
			for (int f = 0; f < num_q_pts; f++) {
				for (int l = 0 ; l < size ; l++ ) {
					work_vector0[l] = node[nj].P[2][l] ;
					work_vector1[l] = node[nj].P[2][l] - lim[f][j][l] ; 
				}
		
				ad_flux_P( temp_normal, work_vector1, rr ) ;
		
				double pressure = 0.5*gm*( eta_j[f]*eta_j[f] - 2.0*eta_j[f]*bed_j[f] ) ;		
				FLUX[0] += 0. ; 
				FLUX[1] += pressure*temp_normal[0] ;
				FLUX[2] += pressure*temp_normal[1] ;

				double source[size];
				source[0] = 0. ;
				source[1] = 0.5* gm* (eta_i[f]+eta_j[f]) *bed_j[f]*temp_normal[0] ;
				source[2] = 0.5* gm* (eta_i[f]+eta_j[f]) *bed_j[f]*temp_normal[1] ;

				/* Higher order reconstruction of the source term + Boundary flux term*/
				double source_cor[size];
				source_cor[0] = 0.;
				source_cor[1] = 0.5* gm* (eta_j[f]+eta_node_j) *(bed_j[f] - bed2_j) *temp_normal[0];// can be imprived for high order well-balancedness
				source_cor[2] = 0.5* gm* (eta_j[f]+eta_node_j) *(bed_j[f] - bed2_j) *temp_normal[1];

				for (int l = 0 ; l < size ; l++ ) {
					if (num_q_pts==2){
				  		PHIN_d[i][l] += dt * 0.5 * numerical_int.face_weight[f] * ( FLUX[l] + source[l] );
				  		PHIN_d[j][l] -= dt * 0.5 * numerical_int.face_weight[f] * ( FLUX[l] - source[l] + 2.0*source_cor[l]);
					} else {
					  	PHIN_d[i][l] += dt * 0.5 * ( FLUX[l] + source[l] );
				  		PHIN_d[j][l] -= dt * 0.5 * ( FLUX[l] - source[l] );
					}
				}
			}


			ad_flux_P( temp_normal, work_vector0, rr ) ;
			work_vector0[0] += bed2_j ;
			 pressure = 0.5*gm*( work_vector0[0]*work_vector0[0] - 2.0*work_vector0[0]*bed2_j ) ;		

			FLUX[0] += 0. ; 
			FLUX[1] += pressure*temp_normal[0] ;
			FLUX[2] += pressure*temp_normal[1] ; 	      

			for (int l = 0 ; l < size ; l++ ) 			
				PHIN_d[j][l] += dt*2.0*0.5*(FLUX[l]);

			/* Dissipation */
			for ( int l = 0 ; l < size ; l++ ) {
				double diss = 0.0 ;
				for ( int m = 0 ; m < size ; m++ ){
					for (int f = 0; f < num_q_pts; f++) {
						if (num_q_pts==2)
							diss += K_i_p1[k][l][m]* numerical_int.face_weight[f] *( W[j][m] - L[f][j][m] - W[i][m] - L[f][i][m] );
						else
							diss += K_i_p1[k][l][m]*( W[j][m] - L[f][j][m] - W[i][m] - L[f][i][m] );
					}
				}
				PHIN_d[i][l] -= dt*diss; 
				PHIN_d[j][l] += dt*diss;
			}
			k+= 1 ; // Counter on the elemental interfaces
		}
	}
	
	/* Explicit friction contribution */
	if (1 == numerics_friction_explicit){
		for (int v = 0 ; v < vertex ; v++) {
			int vv = element[e].node[v];
			for (int l = 0 ; l < size ; l++)
				PHIN_d[v][l] += dt*element[e].volume/3.*node[vv].friction[2][l];
		}
	}
	
	// Wave Generator
	if (75 == initial_state || 78 == initial_state || 81 == initial_state || 82 == initial_state || 88 == initial_state) {
		for (int v = 0 ; v < vertex ; v++) {
			int vv = element[e].node[v];
			PHIN_d[v][0] += dt *element[e].volume/3.*node[vv].wg ;
		}
	}
}

/* DISTRIBUTE SHALLOW WATER FLUCTUATION */
void distribute_FV_fluctuation(int e)
{
  double kappa=0.0;
  double delta=0.0;
  double kappa3=-1.0+delta;
  int itr = iteration - 1;
  double max_Htot;
  if (itr == 0)
    max_Htot = -10.;
  else
    max_Htot = 0.;

  /* Storing nodal values with WB correction */
  for (int v = 0 ; v < vertex ; v++ ) {
    int vv = element[e].node[v];
    for (int i = 0 ; i < size ; i++ ) {
      PHIN_d[v][i] = 0.0;
    }
    if (  !node[vv].dry_flag2 )
      if ( ( node[vv].P[2][0] + node[vv].bed[2] ) >  max_Htot ){
        max_Htot = node[vv].P[2][0] + node[vv].bed[2];
      }
  }

  /* FV scheme with Roe approximate solver */
  double lim[vertex][size], limbed[vertex] ;
  for (int i = 0 ; i < (vertex-1) ; i++ ) {
    for (int j = i+1 ; j < vertex ; j++ ) {
      int index = i+j-1; //Represent the 3 interfaces inside an element
      //  i  j  index
      //  0  1  0
      //  0  2  1
      //  1  2  2
      temp_normal[0] = element[e].FVnormal[index][0] ;
      temp_normal[1] = element[e].FVnormal[index][1] ;      
      int ni = element[e].node[i] ;	
      int nj = element[e].node[j] ;

      if (!node[ni].dry_flag2 || !node[nj].dry_flag2) { // Numerical fluxes are computed only if one of the two nodes is wet!
        double rr = sqrt( temp_normal[0]*temp_normal[0] + temp_normal[1]*temp_normal[1] )/gridsize ;
        double lengthx = node[nj].coordinate[0] - node[ni].coordinate[0] ; // The length of the egde ij
        double lengthy = node[nj].coordinate[1] - node[ni].coordinate[1] ;
	double XM[2]={0., 0.};
	XM[0] = (node[ni].coordinate[0] + node[nj].coordinate[0])/2.0;
	XM[1] = (node[ni].coordinate[1] + node[nj].coordinate[1])/2.0;

        // Slope Limiter for reconstructing gradients at interface	
        for ( int l = 0 ; l < size ; l++ ) {
          double cc = node[nj].P[2][l] - node[ni].P[2][l];
          if (blending == 2) {
            // second order reconstruction with limiter:
            double aa = 2.*( node[ni].dxP[0][l]*lengthx + node[ni].dyP[0][l]*lengthy ) - cc;
            double bb = 2.*( node[nj].dxP[0][l]*lengthx + node[nj].dyP[0][l]*lengthy ) - cc;
            lim[i][l] = limiter( aa , cc );
            lim[j][l] = limiter( bb , cc );

            // second order reconstruction without limiter:
            //double aa = node[ni].dxP[0][l]*lengthx + node[ni].dyP[0][l]*lengthy ;
            //double bb = node[nj].dxP[0][l]*lengthx + node[nj].dyP[0][l]*lengthy ;
            //lim[i][l] = aa;
            //lim[j][l] = bb;  
          } else {
            // third order reconstruction with limiter:  ATTENTION! the function limiter2() is not doing anything for the moment!!
            if (model_dispersion==0 || nht_bb[ni]==1 || nht_bb[nj]==1){
            // if SW or one of the two nodes is flagged as breaking node, apply the limiter
          	double aa = 2.*( node[ni].dxP[0][l]*lengthx + node[ni].dyP[0][l]*lengthy ) - cc ;
            	double bb = 2.*( node[nj].dxP[0][l]*lengthx + node[nj].dyP[0][l]*lengthy ) - cc ;
		//lim[i][l] = (1./3. *aa + 2./3.*cc) ; 
             	//lim[j][l] = (1./3. *bb + 2./3.*cc) ;		
		lim[i][l] = limiter2( aa , cc )*(1./3. *aa + 2./3.*cc) ; 
             	lim[j][l] = limiter2( bb , cc )*(1./3. *bb + 2./3.*cc) ;		
            } else {
		//k=1/3 Method
             	double aa = node[ni].dxP[0][l]*lengthx + node[ni].dyP[0][l]*lengthy ;
              	double bb = node[nj].dxP[0][l]*lengthx + node[nj].dyP[0][l]*lengthy ;
              	lim[i][l] = (1./3.*aa + 2./3.*cc);
              	lim[j][l] = (1./3.*bb + 2./3.*cc);		
            }
          }
        }

        double cc = node[nj].bed[2] - node[ni].bed[2] ;
        if (blending == 2) {
          // second order reconstruction with limiter:
          	double aa = 2.*( node[ni].dxbed[0]*lengthx + node[ni].dybed[0]*lengthy ) - cc;
          	double bb = 2.*( node[nj].dxbed[0]*lengthx + node[nj].dybed[0]*lengthy ) - cc;
          	limbed[i] = limiter( aa , cc );   //second order reconstruction for the bed
          	limbed[j] = limiter( bb , cc ); 

          // second order reconstruction without limiter:
          	//double aa = node[ni].dxbed[0]*lengthx + node[ni].dybed[0]*lengthy ;
          	//double bb = node[nj].dxbed[0]*lengthx + node[nj].dybed[0]*lengthy ;
          	//limbed[i] = aa;
          	//limbed[j] = bb;
        } else {
          //third order reconstruction with limiter
          	double aa = 2.*( node[ni].dxbed[0]*lengthx + node[ni].dybed[0]*lengthy ) - cc ;
          	double bb = 2.*( node[nj].dxbed[0]*lengthx + node[nj].dybed[0]*lengthy ) - cc ;
          	if (model_dispersion==0 || nht_bb[ni]==1 || nht_bb[nj]==1 ){
            	// if SW or one of the two nodes is flagged as breaking node, apply the limiter
       			double aa = 2.*( node[ni].dxbed[0]*lengthx + node[ni].dybed[0]*lengthy ) - cc ;
         		double bb = 2.*( node[nj].dxbed[0]*lengthx + node[nj].dybed[0]*lengthy ) - cc ;
            		//limbed[i] = (1./3. *aa + 2./3.*cc) ; 
            		//limbed[j] = (1./3. *bb + 2./3.*cc) ;		
            		limbed[i] = limiter2( aa , cc )*(1./3. *aa + 2./3.*cc) ; 
            		limbed[j] = limiter2( bb , cc )*(1./3. *bb + 2./3.*cc) ;		
          	} else {
            		aa = node[ni].dxbed[0]*lengthx + node[ni].dybed[0]*lengthy ;
          		bb = node[nj].dxbed[0]*lengthx + node[nj].dybed[0]*lengthy ;
            		limbed[i] = (1./3.*aa + 2./3.*cc);
           		limbed[j] = (1./3.*bb + 2./3.*cc);		

          }	
        }

        // No vars reconstruction for dry-wet edges
       	if ( node[ni].dry_flag2 ==1 || node[nj].dry_flag2 ==1 ) {
         	for ( int l = 0 ; l < size ; l++ ) {
           		lim[i][l] = 0.0;
         		lim[j][l] = 0.0;
         	}			
          	limbed[i] = 0.0;
          	limbed[j] = 0.0; 
	}		



        double U_i[size]; // node values of conservative vartiables (h,hu,hv)
        double U_j[size];
        double UL_ij[size]; //reconstructed left values on ij interface
        double UR_ij[size]; //reconstructed right values on ij interface
        double u_i[size]; // node values of non conservative vartiables (eta,u,v)
        double u_j[size];
        double uL_ij[size]; //reconstructed left values on ij interface
        double uR_ij[size]; //reconstructed right values on ij interface


        for(int l=0; l<size; l++){
		U_i[l] = node[ni].P[2][l];
		U_j[l] = node[nj].P[2][l];
       		UL_ij[l] = node[ni].P[2][l] + 0.5 * lim[i][l]; 
       		UR_ij[l] = node[nj].P[2][l] - 0.5 * lim[j][l]; 
        }

        P_2_Z(U_i,u_i,element[e].cut_off_ratio);
        P_2_Z(U_j,u_j,element[e].cut_off_ratio);
        P_2_Z(UL_ij,uL_ij,element[e].cut_off_ratio);
        P_2_Z(UR_ij,uR_ij,element[e].cut_off_ratio);
	
	/* Numerical Treatment of artificial velocities */
        double bed_i;
        if ( ( node[ni].dry_flag2 ) && ( ( node[ni].bed[0]+cut_off_H*node[ni].cut_off_ratio ) > max_Htot ) ){
          bed_i = max_Htot;
	}else{
          bed_i = node[ni].bed[2];
	}

       	double bedL_ij;
       	bedL_ij = bed_i + 0.5*limbed[i];

        double bed_j;
        if ( ( node[nj].dry_flag2 ) && ( ( node[nj].bed[0]+cut_off_H*node[nj].cut_off_ratio ) > max_Htot ) ){
          bed_j = max_Htot;
	}else
          bed_j = node[nj].bed[2];

        double bedR_ij;
        bedR_ij = bed_j - 0.5*limbed[j];


        if((U_i[0] > cut_off_H && U_j[0] < cut_off_H && U_i[0] < (node[nj].bed[2] - node[ni].bed[2])) || (U_j[0] > cut_off_H && U_i[0] < cut_off_H && U_j[0] < (node[ni].bed[2] - node[nj].bed[2]))){
          uL_ij[1] = 0.0;
          uL_ij[2] = 0.0;
          uR_ij[1] = 0.0;
          uR_ij[2] = 0.0;
        }

	Z_2_P(uL_ij,UL_ij,element[e].cut_off_ratio);
	Z_2_P(uR_ij,UR_ij,element[e].cut_off_ratio);

        /*************************/
        /******** FLUXES *********/
        /*************************/

        //Centered flux FL_ij
        ad_flux_P( temp_normal, UL_ij, rr ) ;
        //Flux coming from 0.5*g*d/dx (h^2)   Well-Balanced formulation of the shallow-water system
        FLUX[0] += 0.;
        FLUX[1] += 0.5*gm*(UL_ij[0]*UL_ij[0])*temp_normal[0];
        FLUX[2] += 0.5*gm*(UL_ij[0]*UL_ij[0])*temp_normal[1];
        for (int l = 0 ; l < size ; l++ ){
          PHIN_d[i][l] += dt*0.5*FLUX[l]; 
          PHIN_d[j][l] -= dt*0.5*FLUX[l]; 
        }

        //Centered flux FR_ij
        ad_flux_P( temp_normal, UR_ij, rr ) ;
        //Flux coming from 0.5*g*d/dx (h^2)   Well-Balanced formulation of the shallow-water system
        FLUX[0] += 0.;
        FLUX[1] += 0.5*gm*(UR_ij[0]*UR_ij[0])*temp_normal[0];
        FLUX[2] += 0.5*gm*(UR_ij[0]*UR_ij[0])*temp_normal[1];
        for (int l = 0 ; l < size ; l++ ){
          PHIN_d[i][l] += dt*0.5*FLUX[l]; 
          PHIN_d[j][l] -= dt*0.5*FLUX[l]; 
        }

        //Upwinding

        double FV_upw_flux[size][size];
        double FV_upw_source[size][size];
	

//        height0 = sqrt(uL_ij[0]*uR_ij[0]);
//        if (UL_ij[0]==0. || UR_ij[0]==0.)
	height0 = 0.5*(UL_ij[0] + UR_ij[0]);
	if( fabs(UL_ij[0]) >= cut_off_H || fabs(UR_ij[0]) >= cut_off_H ){
		ubar0 = ( sqrt(UL_ij[0])*uL_ij[1] + sqrt(UR_ij[0])*uR_ij[1] ) / ( sqrt(UL_ij[0]) + sqrt(UR_ij[0]) );
		vbar0 = ( sqrt(UL_ij[0])*uL_ij[2] + sqrt(UR_ij[0])*uR_ij[2] ) / ( sqrt(UL_ij[0]) + sqrt(UR_ij[0]) );
	} else{
		ubar0 = 0.0;
		vbar0 = 0.0;
	}
        speed_of_sound0 = sqrt(0.5*gm*(UL_ij[0] + UR_ij[0]));
        sigma_barx = 0.; // Only for ALE formulation
        sigma_bary = 0.; // Only for ALE formulation
	double hh = 1.e-3*speed_of_sound0;
        Eigenvectors(temp_normal);
        Waves(temp_normal);

	for (int l = 0; l < size; l++) {

		for (int m = 0; m < size; m++) {
			FV_upw_flux[l][m] = 0.0;
            		FV_upw_source[l][m] = 0.0;
            			for (int k = 0; k < size; k++){
	      				double abss = fabs(Lambda[k]);
              				if (Lambda[k]>=0.)
                				FV_upw_source[l][m] += Right[l][k] * Left[k][m];
              				else
                				FV_upw_source[l][m] -= Right[l][k] * Left[k][m];
	      
	      				//if (abss < 2.*hh)  //Entropy correction??? which one? spoils the c-property for emerging topography 
					//	abss = hh + 0.25 * (abss*abss) / hh;

	      				FV_upw_flux[l][m] += 0.5 * Right[l][k] * abss * Left[k][m];
            			}
          	}
        }
        
        for ( int l = 0 ; l < size ; l++ ) {
          double diss = 0.0 ;
          for ( int m = 0 ; m < size ; m++ ){
            diss +=FV_upw_flux[l][m]*(UR_ij[m]-UL_ij[m]);
	  }
	  PHIN_d[i][l] -= dt*diss; 
          PHIN_d[j][l] += dt*diss;
        }

	
        /**************************/
        /******** SOURCES *********/
        /**************************/

        double source[size];
        double source_upw[size];
        double source_HO_L[size];
        double source_HO_R[size];

        // Centered part
        source[0] = 0.;
        source[1] = -0.5* gm* (UL_ij[0]+UR_ij[0])*(bedR_ij - bedL_ij)*temp_normal[0];
        source[2] = -0.5* gm* (UL_ij[0]+UR_ij[0])*(bedR_ij - bedL_ij)*temp_normal[1];

        // Upwinding
        for ( int l = 0 ; l < size ; l++ ) {
          double upw = 0.0 ;
          for ( int m = 0 ; m < size ; m++ ){
            upw += FV_upw_source[l][m]*source[m];
          }
          source_upw[l] = upw;
        }
        for (int l = 0 ; l < size ; l++ ) {
          PHIN_d[i][l] -= dt*0.5*(source[l] - source_upw[l]);
          PHIN_d[j][l] -= dt*0.5*(source[l] + source_upw[l]);
        }



        // Higher order reconstruction of the source term L
        source_HO_L[0] = 0.;
        source_HO_L[1] = -0.5* gm* (UL_ij[0]+U_i[0]) *(bedL_ij - bed_i) *temp_normal[0];
        source_HO_L[2] = -0.5* gm* (UL_ij[0]+U_i[0]) *(bedL_ij - bed_i) *temp_normal[1];
        for (int l = 0 ; l < size ; l++ )
          PHIN_d[i][l] -= dt*source_HO_L[l];

        // Higher order reconstruction of the source term R
        source_HO_R[0] = 0.;
        source_HO_R[1] = -0.5* gm* (UR_ij[0]+U_j[0]) *(bedR_ij - bed_j)*temp_normal[0];
        source_HO_R[2] = -0.5* gm* (UR_ij[0]+U_j[0]) *(bedR_ij - bed_j)*temp_normal[1];

        for (int l = 0 ; l < size ; l++ ){
          PHIN_d[j][l] += dt*source_HO_R[l];
	}

        /*******************************/
        /****** Boundary flux term *****/
        /*******************************/

        // Left 
        ad_flux_P( temp_normal, U_i, rr );    
        FLUX[0] += 0.; 
        FLUX[1] += 0.5*gm*(U_i[0]*U_i[0])*temp_normal[0];
        FLUX[2] += 0.5*gm*(U_i[0]*U_i[0])*temp_normal[1];

        for (int l = 0 ; l < size ; l++ )			
          PHIN_d[i][l] -= dt*FLUX[l];


        // Right
        ad_flux_P( temp_normal, U_j, rr );    
        FLUX[0] += 0.; 
        FLUX[1] += 0.5*gm*(U_j[0]*U_j[0])*temp_normal[0];
        FLUX[2] += 0.5*gm*(U_j[0]*U_j[0])*temp_normal[1]; 	      

        for (int l = 0 ; l < size ; l++ ){			
          PHIN_d[j][l] += dt*FLUX[l];
	}

      }
    }
  }
	
  /* Explicit friction contribution */
	if (1 == numerics_friction_explicit){
		for (int v = 0 ; v < vertex ; v++) {
			int vv = element[e].node[v];
			for (int l = 0 ; l < size ; l++)
				PHIN_d[v][l] += dt*element[e].volume/3.*node[vv].friction[2][l];
		}
	}
	
	// Wave Generator
	if (75 == initial_state || 78 == initial_state || 81 == initial_state || 82 == initial_state || 88 == initial_state) {
	//if (75 == initial_state || 78 == initial_state || 81 == initial_state || 88 == initial_state) {
		for (int v = 0 ; v < vertex ; v++) {
			int vv = element[e].node[v];
			PHIN_d[v][0] += dt *element[e].volume/3.*node[vv].wg ;
		}
  	}
}


void distribute_FV_fluctuation_V2( int e )
{
	int itr = iteration - 1;
	double max_Htot;
	if (itr == 0)
		max_Htot = -10.;
	else
		max_Htot = 0.;
	/* Storing nodal values with WB correction */
	//double W0[vertex][size];
	for (int v = 0 ; v < 3 ; v++ ) {
		int vv = element[e].node[v];
		for (int i = 0 ; i < size ; i++ ) {
			W[v][i] = node[vv].P[2][i];
			//W0[v][i] = node[vv].P[2][i];
			if (itr > 0)
				W[v][i] = 0.5 * node[vv].P[0][i] + 0.5 * node[vv].P[2][i];
			PHIN_d[v][i] = 0.0;
		}
		if ( !node[vv].dry_flag2 )
			if ( ( node[vv].P[2][0] + node[vv].bed[2] ) >  max_Htot ){
				max_Htot = node[vv].P[2][0] + node[vv].bed[2];
			}
	}
	// Well-Balanced correction at shoreline: cancelling artificial fluxes	
	for (int v = 0; v < 3; v++) {
		int vv = element[e].node[v];
		if ((node[vv].dry_flag2) && ((node[vv].bed[2] + cut_off_H * node[vv].cut_off_ratio) > max_Htot))
			W[v][0] += max_Htot;
		else
			W[v][0] +=  node[vv].bed[2];

		//if (  ((node[vv].dry_flag0) && ((node[vv].bed[0] + cut_off_H * node[vv].cut_off_ratio) > max_Htot))
		//    ||((node[vv].dry_flag2) && ((node[vv].bed[2] + cut_off_H * node[vv].cut_off_ratio) > max_Htot)) )
		//	 	W0[v][0]=W[v][0]; 
	}
	
	/* FV scheme with Roe approximate solver */
	int k = 0 ;
	double lim[vertex][size], L[vertex][size], limbed[vertex] ;
	for (int i = 0 ; i < (vertex-1) ; i++ ) {
		for (int j = i+1 ; j < vertex ; j++ ) {	
	      		temp_normal[0] = element[e].FVnormal[k][0] ;
		      	temp_normal[1] = element[e].FVnormal[k][1] ;		
		      	int ni = element[e].node[i] ;	
	      		int nj = element[e].node[j] ;
		      	double lengthx = node[nj].coordinate[0] - node[ni].coordinate[0] ; // The length of the egde ij
		      	double lengthy = node[nj].coordinate[1] - node[ni].coordinate[1] ;     	

	      		// Slope Limiter for reconstructing gradients at interface	
		      	for ( int l = 0 ; l < size ; l++ ) {
				double cc = node[nj].P[2][l] - node[ni].P[2][l];
				if (blending == 2) {// second order reconstruction with limiter:
//					double aa = 2.*( node[ni].dxP[0][l]*lengthx + node[ni].dyP[0][l]*lengthy ) - cc;
//					double bb = 2.*( node[nj].dxP[0][l]*lengthx + node[nj].dyP[0][l]*lengthy ) - cc;
//					lim[i][l] = limiter( aa , cc );
//					lim[j][l] = limiter( bb , cc );

					// second order reconstruction without limiter:
					double aa = node[ni].dxP[0][l]*lengthx + node[ni].dyP[0][l]*lengthy ;
					double bb = node[nj].dxP[0][l]*lengthx + node[nj].dyP[0][l]*lengthy ;
					lim[i][l] = aa;
					lim[j][l] = bb;

				} else {// third order reconstruction with limiter:  ATTENTION! the function limiter2() is not doing anything for the moment!!
					double aa = 2.*( node[ni].dxP[0][l]*lengthx + node[ni].dyP[0][l]*lengthy ) - cc ;
					double bb = 2.*( node[nj].dxP[0][l]*lengthx + node[nj].dyP[0][l]*lengthy ) - cc ;
					if (model_dispersion==0 || nht_bb[ni]==1 || nht_bb[nj]==1){ // if SW or one of the two nodes is flagged as breaking node, apply the limiter
						lim[i][l] = limiter2( aa , cc )*(1./3. *aa + 2./3.*cc) ; 
						lim[j][l] = limiter2( bb , cc )*(1./3. *bb + 2./3.*cc) ;		
					} else {
						double aa = node[ni].dxP[0][l]*lengthx + node[ni].dyP[0][l]*lengthy ;
						double bb = node[nj].dxP[0][l]*lengthx + node[nj].dyP[0][l]*lengthy ;
//						double hh = node[ni].dxxP2[0][l]*lengthx*lengthx + node[ni].dyyP2[0][l]*lengthy*lengthy +
//								2.*node[ni].dxyP2[0][l]*lengthx*lengthy;
//						double kk = node[nj].dxxP2[0][l]*lengthx*lengthx + node[nj].dyyP2[0][l]*lengthy*lengthy +
//								2.*node[nj].dxyP2[0][l]*lengthx*lengthy;
						lim[i][l] = (2./3.*aa + 1./3.*cc);
						lim[j][l] = (2./3.*bb + 1./3.*cc);		
//						lim[i][l] = aa + 1./4.*hh;
//						lim[j][l] = bb - 1./4.*kk;		
					}
				}
				L[i][l] = 0.5*lim[i][l];
				L[j][l] = 0.5*lim[j][l]; 
			}

			double cc = node[nj].bed[2] - node[ni].bed[2] ;
			if (blending == 2) {// second order reconstruction with limiter:
				double aa = 2.*( node[ni].dxbed[0]*lengthx + node[ni].dybed[0]*lengthy ) - cc;
				double bb = 2.*( node[nj].dxbed[0]*lengthx + node[nj].dybed[0]*lengthy ) - cc;
				limbed[i] = limiter( aa , cc );   //second order reconstruction for the bed
				limbed[j] = limiter( bb , cc ); 
				
				// second order reconstruction without limiter:
//				double aa = node[ni].dxbed[0]*lengthx + node[ni].dybed[0]*lengthy ;
//				double bb = node[nj].dxbed[0]*lengthx + node[nj].dybed[0]*lengthy ;
//				limbed[i] = aa;
//				limbed[j] = bb;

			} else { //third order reconstruction with limiter
				double aa = 2.*( node[ni].dxbed[0]*lengthx + node[ni].dybed[0]*lengthy ) - cc ;
				double bb = 2.*( node[nj].dxbed[0]*lengthx + node[nj].dybed[0]*lengthy ) - cc ;
				if (model_dispersion==0 || nht_bb[ni]==1 || nht_bb[nj]==1){ // if SW or one of the two nodes is flagged as breaking node, apply the limiter
					limbed[i] = limiter2( aa , cc )*(1./3. *aa + 2./3.*cc) ; 
					limbed[j] = limiter2( bb , cc )*(1./3. *bb + 2./3.*cc) ;		
				} else {
					aa = node[ni].dxbed[0]*lengthx + node[ni].dybed[0]*lengthy ;
					bb = node[nj].dxbed[0]*lengthx + node[nj].dybed[0]*lengthy ;
					limbed[i] = (2./3.*aa + 1./3.*cc);
					limbed[j] = (2./3.*bb + 1./3.*cc);		
				}
	
			}
			L[i][0] += 0.5*limbed[i];  
			L[j][0] += 0.5*limbed[j];

			// No vars reconstruction for dry-wet edges
			if ( node[ni].dry_flag2 || node[nj].dry_flag2 ) {
				for ( int l = 0 ; l < size ; l++ ) {
					lim[i][l] = 0.0;
					lim[j][l] = 0.0;
					L[i][l] = 0.0;
					L[j][l] = 0.0;
			   	}
			  	limbed[i] = 0.0;
		  		limbed[j] = 0.0; 
			}
		
			/* Numerical Treatment of artificial velocities */
			double bed2_i, bed2_j;
			if ( ( node[ni].dry_flag2 ) && ( ( node[ni].bed[0]+cut_off_H*node[ni].cut_off_ratio ) > max_Htot ) ) { 
				bed2_i = max_Htot; 
				//if ( node[ni].bed[2] < node[ni].bed[0] )
				//	if ( node[ni].bed[2] < max_Htot0  ) 	 
				//		node[ni].bed[0] = max_Htot0 ; // WB-ALE CORRECTION (Treating node passing D-->W)
				//	else
				//		node[ni].bed[0] = node[ni].bed[2] ;
			} else { 
				bed2_i = node[ni].bed[2]; 
			}

  	
			if ( ( node[nj].dry_flag2 ) && ( ( node[nj].bed[0]+cut_off_H*node[nj].cut_off_ratio ) > max_Htot ) ) { 
				bed2_j = max_Htot; 
				//if ( node[nj].bed[2] < node[nj].bed[0] ) 
				//	if ( node[nj].bed[2] < max_Htot0  ) 
				//		node[nj].bed[0] = max_Htot0 ; // WB-ALE CORRECTION (Treating node passing D-->W)
				//	else
				//		node[nj].bed[0] = node[nj].bed[2] ;

			} else { 
				bed2_j = node[nj].bed[2] ; 
			}	

			/* Centered Flux F_i */
			double bed_i = bed2_i + 0.5*limbed[i];
			double eta_node_i = node[ni].P[2][0] + bed2_i;
			double eta_i = eta_node_i + 0.5*lim[i][0] + 0.5*limbed[i] ;
			double bed_j = bed2_j - 0.5*limbed[j];
			double eta_node_j = node[nj].P[2][0] + bed2_j;
			double eta_j = eta_node_j - 0.5*lim[j][0] - 0.5*limbed[j] ;


			for (int l = 0 ; l < size ; l++ ){
				work_vector0[l] = node[ni].P[2][l] ;
				work_vector1[l] = node[ni].P[2][l] + 0.5*lim[i][l] ; 
			} // reconstructed variables 
		

			double rr = sqrt( temp_normal[0]*temp_normal[0] + temp_normal[1]*temp_normal[1] )/gridsize ;
			ad_flux_P( temp_normal, work_vector1, rr ) ;
			double pressure = 0.5*gm*( eta_i*eta_i - 2.0*eta_i*bed_i ) ;		

			FLUX[0] += 0.; 
			FLUX[1] += pressure*temp_normal[0];
			FLUX[2] += pressure*temp_normal[1]; 	       

			double source[size];	 	
			source[0] = 0.;
			source[1] = 0.5* gm* (eta_i+eta_j) *bed_i*temp_normal[0];// can be imprived for high order well-balancedness
			source[2] = 0.5* gm* (eta_i+eta_j) *bed_i*temp_normal[1];

			for (int l = 0 ; l < size ; l++ ) {
			  	PHIN_d[i][l] += dt*0.5*( FLUX[l] - source[l] );
			  	PHIN_d[j][l] -= dt*0.5*( FLUX[l] + source[l] );
			}

			/* Higher order reconstruction of the source term + Boundary flux term*/
			source[0] = 0.;
			source[1] = 0.5* gm* (eta_i+eta_node_i) *(bed_i - bed2_i) *temp_normal[0];// can be imprived for high order well-balancedness
			source[2] = 0.5* gm* (eta_i+eta_node_i) *(bed_i - bed2_i) *temp_normal[1];

			ad_flux_P( temp_normal, work_vector0, rr );
			work_vector0[0] += bed2_i;
			pressure = 0.5*gm*( work_vector0[0]*work_vector0[0] - 2.0*work_vector0[0]*bed2_i );
			FLUX[0] += 0.; 
			FLUX[1] += pressure*temp_normal[0];
			FLUX[2] += pressure*temp_normal[1]; 	      

			for (int l = 0 ; l < size ; l++ ) 			
		  		PHIN_d[i][l] -= dt*2.0*0.5* (FLUX[l] - source[l]);

			/* Centered Flux F_j */
			for (int l = 0 ; l < size ; l++ ) {
				work_vector0[l] = node[nj].P[2][l] ;
				work_vector1[l] = node[nj].P[2][l] - 0.5*lim[j][l] ; 
			}
		
			ad_flux_P( temp_normal, work_vector1, rr ) ;
		
			pressure = 0.5*gm*( eta_j*eta_j - 2.0*eta_j*bed_j ) ;		
			FLUX[0] += 0. ; 
			FLUX[1] += pressure*temp_normal[0] ;
			FLUX[2] += pressure*temp_normal[1] ;

			source[0] = 0. ;
			source[1] = 0.5* gm* (eta_i+eta_j) *bed_j*temp_normal[0] ;
			source[2] = 0.5* gm* (eta_i+eta_j) *bed_j*temp_normal[1] ;

			for (int l = 0 ; l < size ; l++ ) {
			  	PHIN_d[i][l] += dt*0.5*( FLUX[l] + source[l] ) ; 
			  	PHIN_d[j][l] -= dt*0.5*( FLUX[l] - source[l] ) ;
			}

			/* Higher order reconstruction of the source term + Boundary flux term*/
			source[0] = 0.;
			source[1] = 0.5* gm* (eta_j+eta_node_j) *(bed_j - bed2_j) *temp_normal[0];// can be imprived for high order well-balancedness
			source[2] = 0.5* gm* (eta_j+eta_node_j) *(bed_j - bed2_j) *temp_normal[1];

			ad_flux_P( temp_normal, work_vector0, rr ) ;

			work_vector0[0] += bed2_j ;
			pressure = 0.5*gm*( work_vector0[0]*work_vector0[0] - 2.0*work_vector0[0]*bed2_j ) ;		

			FLUX[0] += 0. ; 
			FLUX[1] += pressure*temp_normal[0] ;
			FLUX[2] += pressure*temp_normal[1] ; 	      

			for (int l = 0 ; l < size ; l++ ) 			
				PHIN_d[j][l] += dt*2.0*0.5*(FLUX[l] - source[l]);

			/* Dissipation */
			for ( int l = 0 ; l < size ; l++ ) {
				double diss = 0.0 ;
				for ( int m = 0 ; m < size ; m++ )
					diss += K_i_p1[k][l][m]*( W[j][m]-L[j][m]-W[i][m]-L[i][m] );
				PHIN_d[i][l] -= dt*diss; 
				PHIN_d[j][l] += dt*diss;
			}
			k+= 1 ; // Counter on the elemental interfaces
		}
	}
	
	/* Explicit friction contribution */
	if (1 == numerics_friction_explicit){
		for (int v = 0 ; v < vertex ; v++) {
			int vv = element[e].node[v];
			for (int l = 0 ; l < size ; l++)
				PHIN_d[v][l] += dt*element[e].volume/3.*node[vv].friction[2][l];
		}
	}
	
	// Wave Generator
	if (75 == initial_state || 78 == initial_state || 81 == initial_state || 82 == initial_state || 88 == initial_state) {
		for (int v = 0 ; v < vertex ; v++) {
			int vv = element[e].node[v];
			PHIN_d[v][0] += dt *element[e].volume/3.*node[vv].wg ;
		}
	}
}

