/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/


	
/***************************************************************************
                                 utilities.c
                                 -----------
  This is utilities: here are programmed all the utility functions used in
   the code, including small algebraic functions for matrix-matrix and
 matrix vector multiplications, matrix-matrix and vector-vector summation
                             -------------------
***************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <string.h>
#include "common.h"
#include "my_malloc.h"

int zero_nodes, iteration, ee1, ee2, ee3, ee4, ee5, ee6, ee7, ee8, ee9, ee10, ee16, ee22;
int nn[47] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
char scaled_mesh_name[MAX_CHAR];

extern char grid_file_name[MAX_CHAR];
extern const int size, vertex, time_discretization, time_step;
extern int Nprobes, NN, NNX, NE, NBF, scheme, convergence_variable, initial_state, numerics_friction_explicit, model_dispersion, volume_q_pts, grad_reco;
double residual_norm, steady_norm, ref_norm, ubar0, vbar0, height0, speed_of_sound0, shoreline_flux;
double ivm[3][3], a1[3], a2[3], a3[3], sigma_barx, sigma_bary, hubar0, hvbar0, max_height;
struct probe_struct *probes;
int Nprobes;
extern double **Right, **Left, *phi_node, gm, *work_vector;
extern double cut_off_H, cut_off_U, dt, Time;
extern double *FV_ubar0, *FV_vbar0, *FV_height0, *FV_speed_of_sound0, *FV_sigma_barx, *FV_sigma_bary;
extern double UUinf, VVinf, L_ref, gridsize;
extern double B[3], **W, **Z, **K;
extern struct node_struct *node;
extern struct element_struct *element;
extern struct boundary_nodes_struct *b_nodes;
extern struct boundary_struct *boundary;
extern struct probe_struct *probes;
extern struct numerical_int_struct numerical_int;
extern char grid_file[MAX_CHAR];
double *max_grad;
extern double Area_inlet_do, Area_inlet_ga;
int tidal_DIM, per_DIM;
double *tidal_time, *tidal_elev, TIDE;
double *gaugeIN_time, *gaugeIN_elev, gaugeIN;
double *gaugeOUT_time, *gaugeOUT_elev, gaugeOUT;
double *gaugeIN_vel, gauge_velIN;
double *gaugeOUT_vel, gauge_velOUT;
extern void (*gradient_reconstruction) (int, int, int);
extern double *INTrec;

void A_times_v(double **, double *, double *);
void Eigenvectors(double *);	/* normals, variables, RIGHT EIGENVECTORS */
void initvec(double *);
//int dgetrf_(integer *, integer *, doublereal *, integer *, integer *, integer * ) ;
//int dgetri_(integer *, doublereal *, integer *, integer *, doublereal *, integer *, integer * ) ;
void P_2_Z(double *, double *, double);
void Z_2_K(double *, double *, double, double);
void source_wavegen(int);
void analytic_ini (double, double, double, double *);



double analytic_sol(double x, double y,double t ){

	double xc = x;
	double xs = 35. ;
	double h0 = 1.0 ;
	double alpha = 0.2;
	double d2 = alpha+h0;
	double c = sqrt( 9.81 *d2 ) ;
        double k = 0.5*sqrt(3.*alpha/(d2*pow(h0,2)));
	double xx=(xc-xs-c*t)*k;
	double sech = 1./(cosh(xx) ) ;
	double etai = alpha *sech*sech;
	double hex = h0+etai;
	double uex = c*(1.-h0/ hex);

        double hex_x = -2.*alpha*k*tanh(xx)*sech*sech;
        double hex_t =  2.*alpha*k*c*tanh(xx)*sech*sech;
        double uex_x = c*(h0*hex_x/(hex*hex));
	double uex_t = c*(h0*hex_t/(hex*hex));

        double	solb = hex*uex_t +hex_t*uex +9.81*hex*hex_x +hex_x*uex*uex + 2.*hex*uex*uex_x;
	return solb;
}

void compute_error( double *Error , int eq, double t)
{
double *INTrec2;
INTrec2=(double*) MA_vector(NN, sizeof(double) );
double *INTp;
INTp=(double*) MA_vector(NN, sizeof(double) );
int flag=7;

	if (NULL != gradient_reconstruction)
		gradient_reconstruction(size, 1, 0);

        double INTf = 0., INTEGRAL_L1 = 0., INTEGRAL_L2 = 0., INTEGRAL_Maria = 0.;
	for(int n = 0; n < NN; n++){
		INTrec[n] =0.;
		INTrec2[n] =0.;
		INTp[n] =0.;
	}

        for ( int e = 0 ; e < NE ; e ++ ){
                double xg = element[e].cg[0];
                double yg = element[e].cg[1];
                for (int i = 0 ; i < (vertex-1) ; i++ ) {
                        int ni = element[e].node[i];
                        double xi = node[ni].coordinate[0];
                        double yi = node[ni].coordinate[1];
                        for (int j = i+1 ; j < vertex ; j++ ) {
                                int nj = element[e].node[j];
                                double xj = node[nj].coordinate[0];
                                double yj = node[nj].coordinate[1];
                                double xm = element[e].mp[i+j-1][0];
                                double ym = element[e].mp[i+j-1][1];

                                for (int q = 0; q < volume_q_pts; q++){
                                      	double xq,yq,lengthx,lengthy,Pq,fq;
                                        double XQP[2] = {0., 0.};
                                        xq = xi*numerical_int.volume_coordinate[q][0] + xg*numerical_int.volume_coordinate[q][1] + xm*numerical_int.volume_coordinate[q][2];
                                        yq = yi*numerical_int.volume_coordinate[q][0] + yg*numerical_int.volume_coordinate[q][1] + ym*numerical_int.volume_coordinate[q][2];
					XQP[0] = xq;
					XQP[1] = yq;
 					//Pq = reconstructed_fun(XQP, ni, eq);

                                        double VAR[3] = {0., 0., 0.};
					analytic_ini(xq, yq, t, VAR); //returns eta and u for solitary
                                        //fq = VAR[eq];
                                        fq = 0.;
					if(eq == 0){
   						fq = VAR[eq] +1.;
					}else if(eq == 1){
   						fq = (VAR[0]+1)*VAR[1];
					}else if(eq ==2){
   						fq = (VAR[0]+1)*VAR[2];
					}

                                        INTrec[ni] += element[e].volume/6.*numerical_int.volume_weight[q]*fabs(Pq - fq);
                                        INTrec2[ni] += element[e].volume/6.*numerical_int.volume_weight[q]*fabs(Pq - fq)*fabs(Pq - fq);
                                        INTEGRAL_L1 += element[e].volume/6.*numerical_int.volume_weight[q]*fabs(Pq - fq);
                                        INTEGRAL_L2 += element[e].volume/6.*numerical_int.volume_weight[q]*fabs(pow( Pq - fq, 2));
                                        INTEGRAL_Maria += element[e].volume/6.*numerical_int.volume_weight[q]*fabs(fq*fq);
                                        INTf += element[e].volume/6.*numerical_int.volume_weight[q]*fq;
                                        //INTp[ni] += element[e].volume/(6.*node[ni].volume)*numerical_int.volume_weight[q]*Pq;
                                        INTp[ni] += (element[e].volume/6.)*numerical_int.volume_weight[q]*fq;

                                        xq = xj*numerical_int.volume_coordinate[q][0] + xg*numerical_int.volume_coordinate[q][1] + xm*numerical_int.volume_coordinate[q][2];
                                        yq = yj*numerical_int.volume_coordinate[q][0] + yg*numerical_int.volume_coordinate[q][1] + ym*numerical_int.volume_coordinate[q][2];
					XQP[0] = xq;
					XQP[1] = yq;
 					//Pq = reconstructed_fun(XQP, nj, eq);


                                        for (int i = 0; i<3; i++)
                                                VAR[i] = 0;
					analytic_ini(xq, yq, t, VAR);
                                        //fq = VAR[eq];
                                        fq = 0.;
					if(eq == 0){
   						fq = VAR[eq]+1.;
					}else if(eq == 1){
   						fq = (VAR[0]+1)*VAR[1];
					}else if(eq ==2){
   						fq = (VAR[0]+1)*VAR[2];
					}

                                        INTrec[nj] += element[e].volume/6.*numerical_int.volume_weight[q]*fabs(Pq - fq);
                                        INTrec2[nj] += element[e].volume/6.*numerical_int.volume_weight[q]*fabs(Pq - fq)*fabs(Pq - fq);
                                        INTEGRAL_L1 += element[e].volume/6.*numerical_int.volume_weight[q]*fabs(Pq - fq);
                                        INTEGRAL_L2 += element[e].volume/6.*numerical_int.volume_weight[q]*fabs(pow( Pq - fq, 2));
                                        INTEGRAL_Maria += element[e].volume/6.*numerical_int.volume_weight[q]*fabs(fq*fq);
                                        INTf += element[e].volume/6.*numerical_int.volume_weight[q]*fq;
                                        //INTp[nj] += element[e].volume/(6.*node[nj].volume)*numerical_int.volume_weight[q]*fq;
                                        INTp[nj] += (element[e].volume/6.)*numerical_int.volume_weight[q]*fq;


                                }
                        }
                }
        }
        /* Correction for boundaries */
        for (int f=0; f<NBF; f++){
                int n = b_nodes[f].node;
                node[n].flag = 0;
        }

        for (int f = 0; f < NBF; f++){
                int n = b_nodes[f].node;
                if (b_nodes[f].type==10 && node[n].flag==0){
                        int mate = node[n].mate;
                        INTrec[n] += INTrec[mate];
                        INTrec[mate] = INTrec[n];
                        INTp[n] += INTp[mate];
                        INTp[mate] = INTp[n];
                        INTrec2[n] += INTrec2[mate];
                        INTrec2[mate] = INTrec2[n];
                        node[n].flag = 1;
                        node[mate].flag = 1;
                }      
        }

              //printf("\n");
              //printf("INT f: %le INT P: %le\n",INTf,INTp);
              //printf("diff: %le\n",fabs(INTf-INTp));
              //printf("\n");

        //      write_solution();

		INTEGRAL_L1=0.;
		INTEGRAL_L2=0.;
		//INTEGRAL_Maria=0.;
                double SUM = 0.;
		double XP[2]={0., 0.};
                double VAR[3] = {0., 0., 0.};
		double ee=0.;
		int icount=0;
                for (int n = 0; n < NN; n++){
			//if(sqrt(pow((node[n].coordinate[0]-0.5-6.0*0.0083),2) + pow((node[n].coordinate[1]-0.5-6.0*0.0083),2))<0.2) {
			//if(node[n].coordinate[0]>=20. && node[n].coordinate[0]<=40.) {
				icount=icount+1;
                		//double y = node[n].coordinate[1];
                		//if (y>0.1 && y<0.9)
                		//SUM += INTrec[n];
				//if(SUM<INTrec[n]) SUM=INTrec[n];
				XP[0]=node[n].DcC[0];//coordinate[0];
				XP[1]=node[n].DcC[1];//coordinate[1];
				//XP[0]=node[n].coordinate[0];
				//XP[1]=node[n].coordinate[1];
				analytic_ini(XP[0], XP[1], t, VAR);
				//ee+=fabs(reconstructed_fun(XP, n, 0) - (VAR[0]+1.));
				//ee+=fabs(reconstructed_fun(XP, n, 0) - (VAR[0]))* fabs(reconstructed_fun(XP, n, 0) - (VAR[0]));
				ee+=fabs(node[n].P[2][0] - (VAR[0]+1.));
			//	//ee+=fabs(INTp[n] - (node[n].volume*(VAR[0]+1.)));
			//	//INTrec[n]=INTp[n];//fabs(INTp[n] - (node[n].volume*VAR[0]));
				//ee+=fabs(node[n].P[2][0] - (VAR[0]));
				//ee+=fabs(node[n].P[2][0] - INTp[n]);
				//if(SUM<ee) SUM=ee;
			
				INTEGRAL_L1 +=INTrec[n];
				INTEGRAL_L2 +=INTrec2[n];
 			//}
			//INTEGRAL_L2 += fabs(INTp[n]-node[n].P[2][0])*fabs(INTp[n]-node[n].P[2][0]);
			//INTEGRAL_Maria += fabs(INTp[n])*fabs(INTp[n]);
			
                }
         

        INTEGRAL_L1 = INTEGRAL_L1;
        INTEGRAL_L2 = sqrt(INTEGRAL_L2);//sqrt(INTEGRAL_L2/NN); //sqrt(INTEGRAL_L2);
        //INTEGRAL_Maria = sqrt(INTEGRAL_L2/INTEGRAL_Maria);
        Error[0] = INTEGRAL_L1;
        Error[1] = INTEGRAL_L2;
        //Error[2] = ee/icount;//NN;//icount;//INTEGRAL_Maria;
        Error[2] = INTEGRAL_Maria;

	MA_free(INTrec2);
	MA_free(INTp);
}
// Average solution over the dual cell
void integration_dualcell(void)
{
	double t=0.;
	int flag=6;
	for (int e = 0; e < NE; e++) {
		double xg = element[e].cg[0];
		double yg = element[e].cg[1];
		for (int i = 0; i < vertex-1; i++) {
			int ni = element[e].node[i];
			double xi = node[ni].coordinate[0];
			double yi = node[ni].coordinate[1];
			for (int j = i+1; j < vertex; j++) {
				int nj = element[e].node[j];
				double xj = node[nj].coordinate[0];
				double yj = node[nj].coordinate[1];
				double xm = element[e].mp[i+j-1][0];
				double ym = element[e].mp[i+j-1][1];
				for (int q = 0; q < volume_q_pts; q++) {
					double VAR[3] = {0., 0., 0.};
					double xq = xi*numerical_int.volume_coordinate[q][0] + xg*numerical_int.volume_coordinate[q][1] + xm*numerical_int.volume_coordinate[q][2];
					double yq = yi*numerical_int.volume_coordinate[q][0] + yg*numerical_int.volume_coordinate[q][1] + ym*numerical_int.volume_coordinate[q][2];
					analytic_ini(xq, yq, t, VAR);

					for (int l = 0; l < size; l++)
						node[ni].P[2][l] += (element[e].volume / 6.0 * numerical_int.volume_weight[q] * VAR[l])/node[ni].ss_volume;

					xq = xj*numerical_int.volume_coordinate[q][0] + xg*numerical_int.volume_coordinate[q][1] + xm*numerical_int.volume_coordinate[q][2];
					yq = yj*numerical_int.volume_coordinate[q][0] + yg*numerical_int.volume_coordinate[q][1] + ym*numerical_int.volume_coordinate[q][2];
					analytic_ini(xq, yq, t, VAR);

					for (int l = 0; l < size; l++)
						node[nj].P[2][l] += (element[e].volume / 6.0 * numerical_int.volume_weight[q] * VAR[l])/node[nj].ss_volume;


				}
			}
		}
	}
// Correction for periodic b.c

	for ( int f = 0 ; f < NBF ; f ++ ) {
		int n1 = b_nodes[f].node;
		node[n1].flag = 0.;
	}
	for ( int f = 0 ; f < NBF ; f ++ ) {
		int n1 = b_nodes[f].node;
		if ( b_nodes[f].type == 10 && node[n1].flag == 0.) {
			int mate = node[n1].mate;
			for (int l=0; l<size; l++){
				node[n1].P[2][l] += node[mate].P[2][l];
				node[mate].P[2][l] = node[n1].P[2][l];
			}
			node[n1].flag = 1.;
			node[mate].flag = 1.;
		 }
	}

	
}
void integration(int flag, double *integral, double t)
{
//for flag 3 4 5 when node on the boundary change the volume
	for (int n = 0; n < NN; n++){
		integral[n] =0.;
	}
	for (int e = 0; e < NE; e++) {
		double xg = element[e].cg[0];
		double yg = element[e].cg[1];
		for (int i = 0; i < vertex-1; i++) {
			int ni = element[e].node[i];
			double xi = node[ni].coordinate[0];
			double yi = node[ni].coordinate[1];
			for (int j = i+1; j < vertex; j++) {
				int nj = element[e].node[j];
				double xj = node[nj].coordinate[0];
				double yj = node[nj].coordinate[1];
				double xm = element[e].mp[i+j-1][0];
				double ym = element[e].mp[i+j-1][1];
				for (int q = 0; q < volume_q_pts; q++) {
					double VAR = 0.;
					double xq = xi*numerical_int.volume_coordinate[q][0] + xg*numerical_int.volume_coordinate[q][1] + xm*numerical_int.volume_coordinate[q][2];
					double yq = yi*numerical_int.volume_coordinate[q][0] + yg*numerical_int.volume_coordinate[q][1] + ym*numerical_int.volume_coordinate[q][2];

					double XQP[2] = {0., 0.};
					XQP[0] = xq;
					XQP[1] = yq;
					if (flag == 1){
						VAR = analytic_sol(xq,yq,t);
					}
					integral[ni]  += (element[e].volume / 6.0 * numerical_int.volume_weight[q] * VAR);

				       if (ni!=node[ni].mate && node[ni].flag_boundary[2]==10){
						int mate = node[ni].mate;
						double dx = node[mate].coordinate[0] - node[ni].coordinate[0];
						double dy = node[mate].coordinate[1] - node[ni].coordinate[1];
						if (fabs(dx)<0.0000001){
							yq = (yi+dy)*numerical_int.volume_coordinate[q][0] + (yg+dy)*numerical_int.volume_coordinate[q][1] + (ym+dy)*numerical_int.volume_coordinate[q][2];
							XQP[0] = xq;
							XQP[1] = yq;
							if (flag == 1 ){
							VAR = 0.;//don't contribute on the boundary it will be done in the boundary routine //analytic_sol(xq,yq,t);
							}
							integral[mate]  += (element[e].volume / 6.0 * numerical_int.volume_weight[q] * VAR);
						} else if (fabs(dy)<0.0000001){
							xq = (xi+dx)*numerical_int.volume_coordinate[q][0] + (xg+dx)*numerical_int.volume_coordinate[q][1] + (xm+dx)*numerical_int.volume_coordinate[q][2];
							XQP[0] = xq;
							XQP[1] = yq;
							if (flag == 1 ){
								VAR = 0;//analytic_sol(xq,yq,t);
							}
							integral[mate]  += (element[e].volume / 6.0 * numerical_int.volume_weight[q] * VAR);
						}
					}


					xq = xj*numerical_int.volume_coordinate[q][0] + xg*numerical_int.volume_coordinate[q][1] + xm*numerical_int.volume_coordinate[q][2];
					yq = yj*numerical_int.volume_coordinate[q][0] + yg*numerical_int.volume_coordinate[q][1] + ym*numerical_int.volume_coordinate[q][2];

					XQP[0] = xq;
					XQP[1] = yq;
					if( flag == 1 ) {
						VAR = analytic_sol(xq,yq,t);
					}

					integral[nj] += (element[e].volume / 6.0 * numerical_int.volume_weight[q] * VAR);

				        if (nj!=node[nj].mate && node[nj].flag_boundary[2]==10){
						int mate = node[nj].mate;
						double dx = node[mate].coordinate[0] - node[nj].coordinate[0];
						double dy = node[mate].coordinate[1] - node[nj].coordinate[1];
						if (fabs(dx)<0.0000001){
							yq = (yj+dy)*numerical_int.volume_coordinate[q][0] + (yg+dy)*numerical_int.volume_coordinate[q][1] + (ym+dy)*numerical_int.volume_coordinate[q][2];
							XQP[0] = xq;
							XQP[1] = yq;
							if( flag == 1 ) {
								VAR = 0.;//analytic_sol(xq,yq,t);
							}
							integral[mate]  += (element[e].volume / 6.0 * numerical_int.volume_weight[q] * VAR);
						} else if (fabs(dy)<0.0000001){
							xq = (xj+dx)*numerical_int.volume_coordinate[q][0] + (xg+dx)*numerical_int.volume_coordinate[q][1] + (xm+dx)*numerical_int.volume_coordinate[q][2];
							XQP[0] = xq;
							XQP[1] = yq;
							if( flag == 1 ) {
								VAR = 0.;//analytic_sol(xq,yq,t);
							}
							integral[mate]  += (element[e].volume / 6.0 * numerical_int.volume_weight[q] * VAR);
						}
					}
				}
			
			}
		}
	}
}
/* Initialization of the solution */
void initialize_solution(void)
{
#pragma omp parallel for
	for (int n = 0; n < NN; n++) {
		for (int l = 0; l < size; l++) {
			node[n].P[0][l] = node[n].P[2][l];
			node[n].Z[0][l] = node[n].Z[2][l];
			node[n].K[0][l] = node[n].K[2][l];

			node[n].P[1][l] = node[n].P[2][l];
			node[n].Z[1][l] = node[n].Z[2][l];
			node[n].K[1][l] = node[n].K[2][l];
		}
	}

/*        if(  3 == time_discretization ) {
		for (int n = 0; n < NN; n++) { //keep the pevious info for ebdf3
			for (int i = 0; i < size; i++) {
				node[n].Res2[i] = node[n].Res1[i];
				node[n].Res1[i] = node[n].Res[i];
				node[n].P2[i] =node[n].P1[i];
				node[n].P1[i] =node[n].P[2][i];
			}
		}
        	if(  time_step ==1 ) {
			for (int n = 0; n < NN; n++) { //keep the pevious info for ebdf3
				for (int i = 0; i < size; i++) {
					node[n].Res2[i] = 0.;
					node[n].Res1[i] = 0.;
					node[n].P2[i] = 0.;
					node[n].P1[i] = 0.;
				}
			}	
		}
	}
*/
	
	if (1 == numerics_friction_explicit) {
#pragma omp parallel for
		for (int n = 0; n < NN; n++) {
			for (int l = 0; l < size; l++) {
				node[n].friction[0][l] = node[n].friction[2][l];
				node[n].friction[1][l] = node[n].friction[2][l];
			}
		}
	}

#pragma omp parallel for
	for (int n = 0; n < NN; n++) {
		node[n].bed[0] = node[n].bed[2];
		node[n].bed[1] = node[n].bed[2];
	}

	// Wave Generator
	if (75 == initial_state || 78 == initial_state || 81 == initial_state || 82 == initial_state || 88 == initial_state) {
#pragma omp parallel for
		for(int n = 0; n < NN; n++)
			source_wavegen(n);
	}
	if (75 == initial_state){
#pragma omp parallel for
		for(int n = 0; n < NN; n++){
			node[n].u_ave += node[n].P[2][1];
			node[n].v_ave += node[n].P[2][2];
		}
	}
		
}

/* consistent variables */
void consistent_vars(int e)
{
	int itr = iteration - 1;
	for (int j = 0; j < 3; j++) {
		int vv = element[e].node[j];

		if (iteration == 1)
			B[j] = node[vv].bed[0];
		else
			B[j] = node[vv].bed[2];

		for (int i = 0; i < size; i++)
			W[j][i] = a1[itr] * node[vv].P[0][i] + a2[itr] * node[vv].P[1][i] + a3[itr] * node[vv].P[2][i];

		P_2_Z(W[j], Z[j], element[e].cut_off_ratio);

		if (scheme > 0) {
			Z_2_K(Z[j], K[j], B[j], element[e].cut_off_ratio);
		} else {
			for (int i = 0; i < size; i++)
				K[j][i] = Z[j][i];
		}
	}
}

/* Modified bathy values at the shore */
void compute_max_Htot0(void)
{
	// Well-Balanced correction at shoreline: computing corrections for moving grids at shoreline
	for (int e = 0; e < NE; e++) {
		if (  (!node[element[e].node[0]].dry_flag0)
			||(!node[element[e].node[1]].dry_flag0)
			||(!node[element[e].node[2]].dry_flag0) ) {

			int i0 = element[e].node[0];
			int i1 = element[e].node[1];
			int i2 = element[e].node[2];

			// local correction
			double max_Htot0 = -10.;
			if ( (!node[i0].dry_flag0) && (node[i0].P[0][0] + node[i0].bed[0] > max_Htot0) )
				max_Htot0 = node[i0].P[0][0] + node[i0].bed[0];

			if ( (!node[i1].dry_flag0) && (node[i1].P[0][0] + node[i1].bed[0] > max_Htot0) )
				max_Htot0 = node[i1].P[0][0] + node[i1].bed[0];

			if ( (!node[i2].dry_flag0) && (node[i2].P[0][0] + node[i2].bed[0] > max_Htot0) )
				max_Htot0 = node[i2].P[0][0] + node[i2].bed[0];

			// global correction
			if ((node[i0].dry_flag0) && ((node[i0].bed[0] + cut_off_H * node[i0].cut_off_ratio) > max_Htot0)) {
				if (node[i0].bed[2] < node[i0].bed[0]) {
					if (max_Htot0 > node[i0].max_Htot0)
						node[i0].max_Htot0 = max_Htot0;
				} else {
					node[i0].max_Htot0 = node[i0].bed[0];
				}
			} else {
				node[i0].max_Htot0 = node[i0].bed[0];
			}

			if ( (node[i1].dry_flag0) && ((node[i1].bed[0] + cut_off_H * node[i1].cut_off_ratio) > max_Htot0) ) {
				if (node[i1].bed[2] < node[i1].bed[0]) {
					if (max_Htot0 > node[i1].max_Htot0)
						node[i1].max_Htot0 = max_Htot0;
				} else {
					node[i1].max_Htot0 = node[i1].bed[0];
				}
			} else {
				node[i1].max_Htot0 = node[i1].bed[0];
			}

			if ( (node[i2].dry_flag0) && ((node[i2].bed[0] + cut_off_H * node[i2].cut_off_ratio) > max_Htot0) ) {
				if (node[i2].bed[2] < node[i2].bed[0]) {
					if (max_Htot0 > node[i2].max_Htot0)
						node[i2].max_Htot0 = max_Htot0;
				} else {
					node[i2].max_Htot0 = node[i2].bed[0];
				}
			} else {
				node[i2].max_Htot0 = node[i2].bed[0];
			}
		}
	}
}

/* Computed shoreline artificial flux */
void compute_RD_shoreflux(int e)
{
	double div = 0.;
	for (int v = 0; v < 3; v++) {
		int vv = element[e].node[v];
		div += 0.5 * (node[vv].vel[0] * element[e].normal[v][0] + node[vv].vel[1] * element[e].normal[v][1]);
	}
	for (int v = 0; v < 3; v++) {
		int vv = element[e].node[v];
		if (node[vv].max_Htot0 != -10.)
			shoreline_flux += 1.0 / 3.0 * dt * div * (node[vv].max_Htot0 + node[vv].P[0][0]);
	}
}

void compute_FV_shoreflux(int e)
{
	int k = 0;
	for (int i = 0; i < 2; i++) {
		for (int j = i + 1; j < 3; j++) {
			int ni = element[e].node[i];
			int nj = element[e].node[j];

			// Quadrature formula for interface velocity  \vi^{K}_{ij} = \int \sigma \cdot n^{K}_{ij}
			double ivel = 0.0;

			for (int m = 0; m < 3; m++)
				ivel += ivm[k][m] * (node[element[e].node[m]].vel[0] * element[e].FVnormal[k][0]
				                     + node[element[e].node[m]].vel[1] * element[e].FVnormal[k][1]);

			if (node[ni].max_Htot0 != -10.)
				shoreline_flux += dt * ivel * (node[ni].max_Htot0 + node[ni].P[0][0]);
			if (node[nj].max_Htot0 != -10.)
				shoreline_flux -= dt * ivel * (node[nj].max_Htot0 + node[nj].P[0][0]);
			k += 1;
		}
	}
}

/* RD locally averaged variables */
void RD_local_average(int e, int step)
{
	int i1 = element[e].node[0];
	int i2 = element[e].node[1];
	int i3 = element[e].node[2];
	zero_nodes = 0;
	max_height = 0.;

    
    double fluxlim = 0.;
    for (int v = 0; v < 3; v++)
    {
        int nn = element[e].node[v];
        if ( node[nn].u_norm > fluxlim ) fluxlim = node[nn].u_norm ;
    }
    
	for (int i = 0; i < 3; i++)
		if (fabs(node[element[e].node[i]].P[step][0]) <= cut_off_H * node[element[e].node[i]].cut_off_ratio)
			zero_nodes++;

	for (int i = 0; i < size; i++)
		work_vector[i] = (node[i1].Z[step][i] + node[i2].Z[step][i] + node[i3].Z[step][i]) / 3.0;

	height0 = work_vector[0];
	 ubar0 = work_vector[1] ;
	vbar0 = work_vector[2] ;
 
	speed_of_sound0 = sqrt(gm * height0);

	ubar0 += UUinf * gridsize * element[e].cut_off_ratio / L_ref;
	vbar0 += VVinf * gridsize * element[e].cut_off_ratio / L_ref;
    
    double velnorm = sqrt( ubar0*ubar0 + vbar0*vbar0 ) ;
    double vel_ratio = velnorm/fluxlim ;
    
    if ( vel_ratio > 1. )
    {
        ubar0 /= vel_ratio ;
        vbar0 /= vel_ratio ;
    }
    
    hubar0 = ubar0*height0 ;
    hvbar0 = vbar0*height0 ;

	sigma_barx = (node[i1].vel[0] + node[i2].vel[0] + node[i3].vel[0]) / 3.0;
	sigma_bary = (node[i1].vel[1] + node[i2].vel[1] + node[i3].vel[1]) / 3.0;
}

/* FV locally averaged variables */
void FV_local_average(int e, int step)
{
	int i1 = element[e].node[0];
	int i2 = element[e].node[1];
	int i3 = element[e].node[2];

	double h1 = node[i1].P[step][0];
	double h2 = node[i2].P[step][0];
	double h3 = node[i3].P[step][0];

	zero_nodes = 0;
	max_height = 0.;

	for (int i = 0; i < 3; i++)
		if (fabs(node[element[e].node[i]].P[step][0]) <= cut_off_H * node[element[e].node[i]].cut_off_ratio)
			zero_nodes++;

	/* edge 1: nodes 1 and 2 */
	work_vector[0] = (h1 + h2) / 2.0;
	FV_height0[0] = work_vector[0];

	if ((sqrt(h1) + sqrt(h2)) > cut_off_U * element[e].cut_off_ratio / 3.0) {
		for (int i = 1; i < size; i++)
			work_vector[i] = (sqrt(h1) * node[i1].Z[step][i] + sqrt(h2) * node[i2].Z[step][i]) / (sqrt(h1) + sqrt(h2));

		FV_ubar0[0] = work_vector[1];
		FV_vbar0[0] = work_vector[2];
	} else {
		FV_ubar0[0] = 0.;
		FV_vbar0[0] = 0.;
		//height0 = 0. ;
		//zero_nodes = 3;
	}

	FV_speed_of_sound0[0] = sqrt(gm * FV_height0[0]);

	FV_ubar0[0] += UUinf * gridsize * element[e].cut_off_ratio / L_ref;
	FV_vbar0[0] += VVinf * gridsize * element[e].cut_off_ratio / L_ref;

	FV_sigma_barx[0] = ivm[0][0] * node[i1].vel[0] + ivm[0][1] * node[i2].vel[0] + ivm[0][2] * node[i3].vel[0];
	FV_sigma_bary[0] = ivm[0][0] * node[i1].vel[1] + ivm[0][1] * node[i2].vel[1] + ivm[0][2] * node[i3].vel[1];

	/* edge 2: nodes 1 and 3 */
	work_vector[0] = (h1 + h3) / 2.0;
	FV_height0[1] = work_vector[0];

	if ((sqrt(h1) + sqrt(h3)) > cut_off_U * element[e].cut_off_ratio / 3.0) {
		for (int i = 1; i < size; i++)
			work_vector[i] = (sqrt(h1) * node[i1].Z[step][i] + sqrt(h3) * node[i3].Z[step][i]) / (sqrt(h1) + sqrt(h3));

		FV_ubar0[1] = work_vector[1];
		FV_vbar0[1] = work_vector[2];
	} else {
		FV_ubar0[1] = 0.;
		FV_vbar0[1] = 0.;
		//height0 = 0. ;
		//zero_nodes = 3;
	}

	FV_speed_of_sound0[1] = sqrt(gm * FV_height0[1]);

	FV_ubar0[1] += UUinf * gridsize * element[e].cut_off_ratio / L_ref;
	FV_vbar0[1] += VVinf * gridsize * element[e].cut_off_ratio / L_ref;

	FV_sigma_barx[1] = ivm[1][0] * node[i1].vel[0] + ivm[1][1] * node[i2].vel[0] + ivm[1][2] * node[i3].vel[0];
	FV_sigma_bary[1] = ivm[1][0] * node[i1].vel[1] + ivm[1][1] * node[i2].vel[1] + ivm[1][2] * node[i3].vel[1];

	/* edge 3: nodes 2 and 3 */
	work_vector[0] = (h2 + h3) / 2.0;
	FV_height0[2] = work_vector[0];

	if ((sqrt(h2) + sqrt(h3)) > cut_off_U * element[e].cut_off_ratio / 3.0) {
		for (int i = 1; i < size; i++)
			work_vector[i] = (sqrt(h2) * node[i2].Z[step][i] + sqrt(h3) * node[i3].Z[step][i]) / (sqrt(h2) + sqrt(h3));

		FV_ubar0[2] = work_vector[1];
		FV_vbar0[2] = work_vector[2];
	} else {
		FV_ubar0[2] = 0.;
		FV_vbar0[2] = 0.;
		//height0 = 0. ;
		//zero_nodes = 3;
	}

	FV_speed_of_sound0[2] = sqrt(gm * FV_height0[2]);

	FV_ubar0[2] += UUinf * gridsize * element[e].cut_off_ratio / L_ref;
	FV_vbar0[2] += VVinf * gridsize * element[e].cut_off_ratio / L_ref;

	FV_sigma_barx[2] = ivm[2][0] * node[i1].vel[0] + ivm[2][1] * node[i2].vel[0] + ivm[2][2] * node[i3].vel[0];
	FV_sigma_bary[2] = ivm[2][0] * node[i1].vel[1] + ivm[2][1] * node[i2].vel[1] + ivm[2][2] * node[i3].vel[1];
}

/* residual norm */
void compute_norm(void)
{
	double residual = 0.0;
	int m = convergence_variable;
	steady_norm = 0.0;

	if (m == -1)
		for (int n = 0; n < NN; n++)
			for (int s = 0; s < size; s++){
				residual += fabs(node[n].Res[s]) * 1.0 / (size * NN);
				if(node[n].Res[s]!=node[n].Res[s])
					printf("n=%d, Res = %le\n", n, node[n].Res[s]);
			}
	else
		for (int n = 0; n < NN; n++)
			residual += fabs(node[n].Res[m]) * 1.0 / NN;

	residual_norm = log10(MAX(residual, DBL_EPSILON));

	//if(iteration==2)   printf("Residual oo: %le\n", residual);
	//residual_norm = log10( residual ) ;

	steady_norm = residual;
	if (iteration == 1)
		ref_norm = residual_norm;
}


void compute_rhs_ex(double t)
{
// Computs the volume integral in each element for the manufactured solution 
double *integral;
integral=(double*) MA_vector(NN, sizeof(double) );

	for(int i=0; i<NN; i++){
		node[i].rhs[0]=0.0;
		node[i].rhs[1]=0.0;
	}
	

/*	for (int e = 0; e < NE; e++) {
                        for (int i = 0; i < vertex; i++) {
                                int i0 = element[e].node[i];
                                for (int jj = 1; jj < vertex; jj++) {
                                        int j = (i + jj) % 3;
                                        int k = (i + (3 - jj)) % 3;
                                        int i1 = element[e].node[j];
                                        int i2 = element[e].node[k];

                                        for (int q = 0; q < volume_q_pts; q++) {
                                                double xq =  numerical_int.volume_coordinate[q][0] * node[i0].coordinate[0]
                                                           + numerical_int.volume_coordinate[q][1] * node[i1].coordinate[0]
                                                           + numerical_int.volume_coordinate[q][2] * node[i2].coordinate[0];
                                                double yq =  numerical_int.volume_coordinate[q][0] * node[i0].coordinate[1]
                                                           + numerical_int.volume_coordinate[q][1] * node[i1].coordinate[1]
                                                           + numerical_int.volume_coordinate[q][2] * node[i2].coordinate[1];
                                                double bq = analytic_sol(xq,yq,t);
                                                        //node[i0].rhs[0] += 0.5*(element[e].new_volume / 6.0 * numerical_int.volume_weight[q] * bq)
                                                        integral[i0] += (element[e].new_volume / 6.0 * numerical_int.volume_weight[q] * bq)
                                                                  /node[i0].mod_volume;
                                                        //node[i0].rhs[1] += 0.0;
                                        }
                                }
                        }
                }
*/
        integration(1, integral,t);
	//if(grad_reco==0){
		for(int i=0; i<NN; i++){
			node[i].rhs[0] = integral[i]/node[i].volume;	
			node[i].rhs[1] = 0.0;	
		}
	//}else if (grad_reco==1) {
	//	for(int i=0; i<NN; i++){
	//		node[i].rhs[0] = integral[i]/node[i].ss_volume;	
	//		node[i].rhs[1] = 0.0;	
	//	}
	//}
//	exit(0);
/*       for ( int f = 0 ; f < NBF ; f ++ ) {
                int n1 = b_nodes[f].node;
                node[n1].flag = 0.;
        }
        for ( int f = 0 ; f < NBF ; f ++ ) {
                int n1 = b_nodes[f].node;
                if ( b_nodes[f].type == 10 && node[n1].flag == 0.) {
                        int mate = node[n1].mate;
                        node[n1].rhs[0] += node[mate].rhs[0];
                        node[mate].rhs[0] = node[n1].rhs[0];
                        node[n1].flag = 1.;
                        node[mate].flag = 1.;
                }
        }
*/
MA_free(integral);
}


/* initialize matrix */
void initmat(double **A)
{
	for (int i = 0; i < size; i++)
		for (int j = 0; j < size; j++)
			A[i][j] = 0.0;
}

/* initialize vector */
void initvec(double *v)
{
	for (int i = 0; i < size; i++)
		v[i] = 0.0;
}

/* add a matrix to an existing one */
void add2mat(double **A, double **B)
{
	for (int i = 0; i < size; i++)
		for (int j = 0; j < size; j++)
			A[i][j] += B[i][j];
}

/* add a vector to an existing one */
void add2vec(double *v, double *w)
{
	for (int i = 0; i < size; i++)
		v[i] += w[i];
}

/* matrix-vector product */
void A_times_v(double **A, double *v, double *w)
{
	for (int i = 0; i < size; i++) {
		w[i] = 0.0;
		for (int j = 0; j < size; j++)
			w[i] += A[i][j] * v[j];
	}
}

/* matrix-matrix product */
void A_times_B(double **A, double **B, double **C)
{
	for (int i = 0; i < size; i++)
		for (int j = 0; j < size; j++) {
			C[i][j] = 0.0;
			for (int k = 0; k < size; k++)
				C[i][j] += A[i][k] * B[k][j];
		}
}

/* update the local residual: no transformation */
void residual_update(int n)
{
	for (int i = 0; i < size; i++)
		node[n].Res[i] += phi_node[i];
}

/* decompose a residual in scalar waves */
void decompose(double *v, double *phiN_i, double *phi_i)
{
	Eigenvectors(v);
	A_times_v(Left, phiN_i, phi_i);
}

/* Scale the domain	*/
void scale_grid(void)
{
	int scaling = 0;
	double translate[2], scale_factor[2];
        double x,y;
	if (2 == initial_state) {
		translate[0] = 0.;
		translate[1] = 0.;
		scale_factor[0] = 1.25;
		scale_factor[1] = 1.25;
		scaling = 1;
	} else if (3 == initial_state) {
		translate[0] = 0.;
		translate[1] = 0.;
		scale_factor[0] = 100.;
		scale_factor[1] = 100.;
		scaling = 1;
	} else if (5 == initial_state) {
		translate[0] = 1.;
		translate[1] = 1.;
		scale_factor[0] = 50.;
		scale_factor[1] = 50.;
		scaling = 1;
	} else if (6 == initial_state) {
		translate[0] = 1.;
		translate[1] = 1.0;
		scale_factor[0] = 1.;
		scale_factor[1] = 1.;
		scaling = 1;
	}else if (8 == initial_state) {
		if (!strcmp(grid_file, "vort_t1")) {
			translate[0] = 0.;
			translate[1] = 0.;
			scale_factor[0] = 25;
			scale_factor[1] = 30.;
			scaling = 1;
		}
		if (!strcmp(grid_file, "vort_t1-2")) {
			translate[0] = 0.;
			translate[1] = 0.;
			scale_factor[0] = 25;
			scale_factor[1] = 30.;
			scaling = 1;
		}
		if (!strcmp(grid_file, "vort_t1-3")) {
			translate[0] = 0.;
			translate[1] = 0.;
			scale_factor[0] = 25;
			scale_factor[1] = 30.;
			scaling = 1;
		}

		if (!strcmp(grid_file, "vort_t1-4")) {
			translate[0] = 0.;
			translate[1] = 0.;
			scale_factor[0] = 25;
			scale_factor[1] = 30.;
			scaling = 1;
		}

		if (!strcmp(grid_file, "vort_t1-5")) {
			translate[0] = 0.;
			translate[1] = 0.;
			scale_factor[0] = 25;
			scale_factor[1] = 30.;
			scaling = 1;
		}
		if (!strcmp(grid_file, "briggs_oneref")) {
			translate[0] = 0.;
			translate[1] = 0.;
			scale_factor[0] = 25;
			scale_factor[1] = 30.;
			scaling = 1;
		}
	} else if (9 == initial_state) {
		translate[0] = 1.;
		translate[1] = 1.;
		scale_factor[0] = 0.5;
		scale_factor[1] = 0.5;
		scaling = 1;
	} else if (10 == initial_state) {
		translate[0] = 0.;
		translate[1] = 0.;
		scale_factor[0] = 0.1;
		scale_factor[1] = 0.1;
		scaling = 1;
	} else if (11 == initial_state) {
		translate[0] = 1.;
		translate[1] = 1.;
		scale_factor[0] = 1.;
		scale_factor[1] = 1.;
		scaling = 1;
	} else if (15 == initial_state) {
		translate[0] = 0.;
		translate[1] = 0.;
		scale_factor[0] = 1.2;
		scale_factor[1] = 1.2;

		if ((!strcmp(grid_file, "vort_t1")) || (!strcmp(grid_file, "vort_t1-2")) || (!strcmp(grid_file, "vort_t1-3"))
		    || (!strcmp(grid_file, "vort_t1-4"))) {
			translate[0] = -0.5;
			translate[1] = -0.5;
			scale_factor[0] = 4;
			scale_factor[1] = 4;
		}

		scaling = 1;
	} else if (20 == initial_state) {
		// kappa = 0.;
		translate[0] = translate[1] = 1.;
		scale_factor[0] = scale_factor[1] = 500.;
		scaling = 1;
	} else if (22 == initial_state) {
		if ((!strcmp(grid_file, "vort_t1-3"))) {
			scaling = 1;
			translate[0] = -0.5;
			translate[1] = -0.5;
			scale_factor[0] = 50000;
			scale_factor[1] = 50000;
		}
		if ((!strcmp(grid_file, "vort_t1-4"))) {
			scaling = 1;
			translate[0] = -0.5;
			translate[1] = -0.5;
			scale_factor[0] = 50000;
			scale_factor[1] = 50000;
		}
	} else if (71 == initial_state) {
		translate[0] = 0.;
		translate[1] = 0.;
		scale_factor[0] = 1. / 50000.;
		scale_factor[1] = 1. / 50000.;
		scaling = 1;
	} else if (83 == initial_state) {
		translate[0] = 0.;
		translate[1] = 0.;
		scale_factor[0] = 1.;
		scale_factor[1] = 1.;
		scaling = 1;
	} else if (185 == initial_state) {
        if ((!strcmp(grid_file, "vort_t1-2"))) {
            scaling = 1;
            translate[0] = -.25 ;
            translate[1] = 0. ;
            scale_factor[0] = 40. ;
            scale_factor[1] = 20. ;
        }
        if ((!strcmp(grid_file, "vort_t1-3"))) {
            scaling = 1;
            translate[0] = -0.25 ;
            translate[1] = 0. ;
            scale_factor[0] = 40. ;
            scale_factor[1] = 20. ;
        }
	} /*else if (78 == initial_state) {
		translate[0] = 0.;
		translate[1] = 0.;
		scale_factor[0] = 1.;
		scale_factor[1] = 1.;
		scaling = 1;
	}*/
	if (scaling) {
		for (int n = 0; n < NN; n++) {
			node[n].coordinate[0] += translate[0];
			node[n].coordinate[1] += translate[1];
			node[n].coordinate[0] *= scale_factor[0];
//
/*			if (78 == initial_state){
				double tmpx =node[n].coordinate[0];
				double tmpy =node[n].coordinate[1];

				node[n].coordinate[0] = tmpx*cos(pi/2.0) -tmpy*sin(pi/2.0);
				node[n].coordinate[1] = tmpx*sin(pi/2.0) +tmpy*cos(pi/2.0);
			}
*/
			if (85 == initial_state)
				node[n].coordinate[1] *= scale_factor[1]*exp(-node[0].coordinate[0]);
			else
				node[n].coordinate[1] *= scale_factor[1];


		/*	if (81 == initial_state){
				//for(int n=0; n<NN; n++){
					x = node[n].coordinate[0];
                        		y = node[n].coordinate[1];
                        		node[n].coordinate[0] = y;  
                        		node[n].coordinate[1] = -x;
 				//printf("%le %le %le %le\n",x,y,node[n].coordinate[0],node[n].coordinate[1]);
				//}
			}*/

			node[n].coordinate_new[0] = node[n].coordinate[0];	
			node[n].coordinate_new[1] = node[n].coordinate[1];
		}

		if (1 == model_dispersion){ // We need to rewrite the mesh .grd file with the scaled variables in order to be read by the NonHydro library
			sprintf(scaled_mesh_name, "../input/ScaledMesh.grd");

			FILE *ScaledGrid;
			ScaledGrid = fopen(scaled_mesh_name, "w");
			fprintf( ScaledGrid, "2 %d %d %d \n", NE, NN, NBF ) ;
			fprintf( ScaledGrid, "\n" ) ;
			for ( int e = 0 ; e < NE ; e ++ )
				fprintf( ScaledGrid, "%d %d %d\n", element[e].node[0],element[e].node[1],element[e].node[2]);
			fprintf( ScaledGrid, "\n" ) ;
			for ( int n = 0 ; n < NN ; n ++ )
				fprintf( ScaledGrid, "%le %le %d\n", node[n].coordinate[0],node[n].coordinate[1],node[n].mate);
			fprintf( ScaledGrid, "\n" ) ;
			for ( int f = 0 ; f < NBF ; f++ )
				fprintf( ScaledGrid, "%d %d %d %d %d\n", boundary[f].node[0], boundary[f].node[1], boundary[f].types[0], boundary[f].types[1], boundary[f].type );
			fclose(ScaledGrid);
		}

/*		if (83 == initial_state){
			double *disp = MA_double_vector(2*NN);
			srand(10);
			int n0 = boundary[0].node[0];
			int n1 = boundary[0].node[1];
			double dx = node[n1].coordinate[0] - node[n0].coordinate[1];
			for (int n = 0; n < NN; n++){
			double a = rand() % 100;
			double b = rand() % 100;
			disp[n] = a / 100 * dx/2 - dx/4;
			disp[n+NN] = b / 100 * dx/2 - dx/4;
		}
		for (int f = 0; f < NBF; f++){
			int n = b_nodes[f].node;
			disp[n] = 0.;	
			disp[n+NN] = 0.;
		}
		for (int n = 0; n < NN; n++){
			node[n].coordinate[0] += disp[n];
			node[n].coordinate[1] += disp[n+NN];
			node[n].coordinate_new[0] = node[n].coordinate[0];
			node[n].coordinate_new[1] = node[n].coordinate[1];
		}
		free(disp);
		}
*/	} else
		if (1 == model_dispersion)
			sprintf(scaled_mesh_name, grid_file_name);
}

void install_gauge(void)
{
	if (8 == initial_state) {
		int flag1 = 0, flag6 = 0, flag9 = 0, flag16 = 0, flag22 = 0;
		double xg1, xg6, yg1, yg6, xg9, yg9, xg16, yg16, xg22, yg22, det12, det23, det31;
		for (int e = 0; e < NE; e++) {
			double x1 = node[element[e].node[0]].coordinate_new[0];
			double y1 = node[element[e].node[0]].coordinate_new[1];
			double x2 = node[element[e].node[1]].coordinate_new[0];
			double y2 = node[element[e].node[1]].coordinate_new[1];
			double x3 = node[element[e].node[2]].coordinate_new[0];
			double y3 = node[element[e].node[2]].coordinate_new[1];

			// Gauge1
			if (flag1 == 0) {
				xg1 = 6.82;
				yg1 = 13.05;
				det12 = (x2 - xg1) * (y1 - yg1) - (y2 - yg1) * (x1 - xg1);
				det23 = (x3 - xg1) * (y2 - yg1) - (y3 - yg1) * (x2 - xg1);
				det31 = (x1 - xg1) * (y3 - yg1) - (y1 - yg1) * (x3 - xg1);
				if ((det12 <= 0.) && (det23 <= 0.) && (det31 <= 0.)) {
					ee1 = e;
					flag1 = 1;
				}
			}
			// Gauge6
			if (flag6 == 0) {
				xg6 = 12.96 - 3.6;
				yg6 = 13.8;
				det12 = (x2 - xg6) * (y1 - yg6) - (y2 - yg6) * (x1 - xg6);
				det23 = (x3 - xg6) * (y2 - yg6) - (y3 - yg6) * (x2 - xg6);
				det31 = (x1 - xg6) * (y3 - yg6) - (y1 - yg6) * (x3 - xg6);
				if ((det12 <= 0.) && (det23 <= 0.) && (det31 <= 0.)) {
					ee6 = e;
					flag6 = 1;
				}
			}
			// Gauge9
			if (flag9 == 0) {
				xg9 = 12.96 - 2.6;
				yg9 = 13.8;
				det12 = (x2 - xg9) * (y1 - yg9) - (y2 - yg9) * (x1 - xg9);
				det23 = (x3 - xg9) * (y2 - yg9) - (y3 - yg9) * (x2 - xg9);
				det31 = (x1 - xg9) * (y3 - yg9) - (y1 - yg9) * (x3 - xg9);
				if ((det12 <= 0.) && (det23 <= 0.) && (det31 <= 0.)) {
					ee9 = e;
					flag9 = 1;
				}
			}
			// Gauge16
			if (flag16 == 0) {
				xg16 = 12.96;
				yg16 = 13.8 - 2.58;
				det12 = (x2 - xg16) * (y1 - yg16) - (y2 - yg16) * (x1 - xg16);
				det23 = (x3 - xg16) * (y2 - yg16) - (y3 - yg16) * (x2 - xg16);
				det31 = (x1 - xg16) * (y3 - yg16) - (y1 - yg16) * (x3 - xg16);
				if ((det12 <= 0.) && (det23 <= 0.) && (det31 <= 0.)) {
					ee16 = e;
					flag16 = 1;
				}
			}
			// Gauge22
			if (flag22 == 0) {
				xg22 = 12.96 + 2.6;
				yg22 = 13.8;
				det12 = (x2 - xg22) * (y1 - yg22) - (y2 - yg22) * (x1 - xg22);
				det23 = (x3 - xg22) * (y2 - yg22) - (y3 - yg22) * (x2 - xg22);
				det31 = (x1 - xg22) * (y3 - yg22) - (y1 - yg22) * (x3 - xg22);
				if ((det12 <= 0.) && (det23 <= 0.) && (det31 <= 0.)) {
					ee22 = e;
					flag22 = 1;
				}
			}
		}
	} else if (24 == initial_state) {
		int flag1 = 0, flag2 = 0, flag3 = 0, flag4 = 0, flag5 = 0, flag6 = 0, flag7 = 0, flag8 = 0, flag9 = 0, flag10 = 0;
		double xg1, yg1, xg2, yg2, xg3, yg3, xg4, yg4, xg5, yg5, xg6, yg6, xg7, yg7, xg8, yg8, xg9, yg9, xg10, yg10, det12, det23, det31;
		for (int e = 0; e < NE; e++) {
			double x1 = node[element[e].node[0]].coordinate_new[0];
			double y1 = node[element[e].node[0]].coordinate_new[1];
			double x2 = node[element[e].node[1]].coordinate_new[0];
			double y2 = node[element[e].node[1]].coordinate_new[1];
			double x3 = node[element[e].node[2]].coordinate_new[0];
			double y3 = node[element[e].node[2]].coordinate_new[1];
			// Gauge1
			if (flag1 == 0) {
				xg1 = 7.5;
				yg1 = 0.0;
				det12 = (x2 - xg1) * (y1 - yg1) - (y2 - yg1) * (x1 - xg1);
				det23 = (x3 - xg1) * (y2 - yg1) - (y3 - yg1) * (x2 - xg1);
				det31 = (x1 - xg1) * (y3 - yg1) - (y1 - yg1) * (x3 - xg1);
				if ((det12 <= 0.) && (det23 <= 0.) && (det31 <= 0.)) {
					ee1 = e;
					flag1 = 1;
				}
			}
			// Gauge2 (ADV1)
			if (flag2 == 0) {
				xg2 = 13.0;
				yg2 = 0.0;
				det12 = (x2 - xg2) * (y1 - yg2) - (y2 - yg2) * (x1 - xg2);
				det23 = (x3 - xg2) * (y2 - yg2) - (y3 - yg2) * (x2 - xg2);
				det31 = (x1 - xg2) * (y3 - yg2) - (y1 - yg2) * (x3 - xg2);
				if ((det12 <= 0.) && (det23 <= 0.) && (det31 <= 0.)) {
					ee2 = e;
					flag2 = 1;
				}
			}
			// Gauge3 (ADV2)
			if (flag3 == 0) {
				xg3 = 21.0;
				yg3 = 0.0;
				det12 = (x2 - xg3) * (y1 - yg3) - (y2 - yg3) * (x1 - xg3);
				det23 = (x3 - xg3) * (y2 - yg3) - (y3 - yg3) * (x2 - xg3);
				det31 = (x1 - xg3) * (y3 - yg3) - (y1 - yg3) * (x3 - xg3);
				if ((det12 <= 0.) && (det23 <= 0.) && (det31 <= 0.)) {
					ee3 = e;
					flag3 = 1;
				}
			}
			// Gauge4
			if (flag4 == 0) {
				xg4 = 7.5;
				yg4 = 5.0;
				det12 = (x2 - xg4) * (y1 - yg4) - (y2 - yg4) * (x1 - xg4);
				det23 = (x3 - xg4) * (y2 - yg4) - (y3 - yg4) * (x2 - xg4);
				det31 = (x1 - xg4) * (y3 - yg4) - (y1 - yg4) * (x3 - xg4);
				if ((det12 <= 0.) && (det23 <= 0.) && (det31 <= 0.)) {
					ee4 = e;
					flag4 = 1;
				}
			}
			// Gauge5
			if (flag5 == 0) {
				xg5 = 13.0;
				yg5 = 5.0;
				det12 = (x2 - xg5) * (y1 - yg5) - (y2 - yg5) * (x1 - xg5);
				det23 = (x3 - xg5) * (y2 - yg5) - (y3 - yg5) * (x2 - xg5);
				det31 = (x1 - xg5) * (y3 - yg5) - (y1 - yg5) * (x3 - xg5);
				if ((det12 <= 0.) && (det23 <= 0.) && (det31 <= 0.)) {
					ee5 = e;
					flag5 = 1;
				}
			}
			// Gauge6
			if (flag6 == 0) {
				xg6 = 21.0;
				yg6 = 5.0;
				det12 = (x2 - xg6) * (y1 - yg6) - (y2 - yg6) * (x1 - xg6);
				det23 = (x3 - xg6) * (y2 - yg6) - (y3 - yg6) * (x2 - xg6);
				det31 = (x1 - xg6) * (y3 - yg6) - (y1 - yg6) * (x3 - xg6);
				if ((det12 <= 0.) && (det23 <= 0.) && (det31 <= 0.)) {
					ee6 = e;
					flag6 = 1;
				}
			}
			// Gauge7
			if (flag7 == 0) {
				xg7 = 25.0;
				yg7 = 0.0;
				det12 = (x2 - xg7) * (y1 - yg7) - (y2 - yg7) * (x1 - xg7);
				det23 = (x3 - xg7) * (y2 - yg7) - (y3 - yg7) * (x2 - xg7);
				det31 = (x1 - xg7) * (y3 - yg7) - (y1 - yg7) * (x3 - xg7);
				if ((det12 <= 0.) && (det23 <= 0.) && (det31 <= 0.)) {
					ee7 = e;
					flag7 = 1;
				}
			}
			// Gauge8
			if (flag8 == 0) {
				xg8 = 25.0;
				yg8 = 5.0;
				det12 = (x2 - xg8) * (y1 - yg8) - (y2 - yg8) * (x1 - xg8);
				det23 = (x3 - xg8) * (y2 - yg8) - (y3 - yg8) * (x2 - xg8);
				det31 = (x1 - xg8) * (y3 - yg8) - (y1 - yg8) * (x3 - xg8);
				if ((det12 <= 0.) && (det23 <= 0.) && (det31 <= 0.)) {
					ee8 = e;
					flag8 = 1;
				}
			}
			//  Gauge9 
			if (flag9 == 0) {
				xg9 = 25.0;
				yg9 = 10.0;
				det12 = (x2 - xg9) * (y1 - yg9) - (y2 - yg9) * (x1 - xg9);
				det23 = (x3 - xg9) * (y2 - yg9) - (y3 - yg9) * (x2 - xg9);
				det31 = (x1 - xg9) * (y3 - yg9) - (y1 - yg9) * (x3 - xg9);
				if ((det12 <= 0.) && (det23 <= 0.) && (det31 <= 0.)) {
					ee9 = e;
					flag9 = 1;
				}
			}
			// Gauge10 (ADV3)
			if (flag10 == 0) {
				xg10 = 21.0;
				yg10 = -5.0;
				det12 = (x2 - xg10) * (y1 - yg10) - (y2 - yg10) * (x1 - xg10);
				det23 = (x3 - xg10) * (y2 - yg10) - (y3 - yg10) * (x2 - xg10);
				det31 = (x1 - xg10) * (y3 - yg10) - (y1 - yg10) * (x3 - xg10);
				if ((det12 <= 0.) && (det23 <= 0.) && (det31 <= 0.)) {
					ee10 = e;
					flag10 = 1;
				}
			}
		}
	} else if (25 == initial_state) {
		int flag1 = 0, flag6 = 0, flag9 = 0, flag16 = 0;
		double xg1, yg1, xg6, yg6, xg9, yg9, xg16, yg16, det12, det23, det31;
		for (int e = 0; e < NE; e++) {
			double x1 = node[element[e].node[0]].coordinate_new[0];
			double y1 = node[element[e].node[0]].coordinate_new[1];
			double x2 = node[element[e].node[1]].coordinate_new[0];
			double y2 = node[element[e].node[1]].coordinate_new[1];
			double x3 = node[element[e].node[2]].coordinate_new[0];
			double y3 = node[element[e].node[2]].coordinate_new[1];

			// Gauge1
			if (flag1 == 0) {
				xg1 = 4.521;
				yg1 = 1.196;
				det12 = (x2 - xg1) * (y1 - yg1) - (y2 - yg1) * (x1 - xg1);
				det23 = (x3 - xg1) * (y2 - yg1) - (y3 - yg1) * (x2 - xg1);
				det31 = (x1 - xg1) * (y3 - yg1) - (y1 - yg1) * (x3 - xg1);
				if ((det12 <= 0.) && (det23 <= 0.) && (det31 <= 0.)) {
					ee1 = e;
					flag1 = 1;
				}
			}
			// Gauge6
			if (flag6 == 0) {
				xg6 = 4.521;
				yg6 = 1.696;
				det12 = (x2 - xg6) * (y1 - yg6) - (y2 - yg6) * (x1 - xg6);
				det23 = (x3 - xg6) * (y2 - yg6) - (y3 - yg6) * (x2 - xg6);
				det31 = (x1 - xg6) * (y3 - yg6) - (y1 - yg6) * (x3 - xg6);
				if ((det12 <= 0.) && (det23 <= 0.) && (det31 <= 0.)) {
					ee6 = e;
					flag6 = 1;
				}
			}
			// Gauge9
			if (flag9 == 0) {
				xg9 = 4.521;
				yg9 = 2.196;
				det12 = (x2 - xg9) * (y1 - yg9) - (y2 - yg9) * (x1 - xg9);
				det23 = (x3 - xg9) * (y2 - yg9) - (y3 - yg9) * (x2 - xg9);
				det31 = (x1 - xg9) * (y3 - yg9) - (y1 - yg9) * (x3 - xg9);
				if ((det12 <= 0.) && (det23 <= 0.) && (det31 <= 0.)) {
					ee9 = e;
					flag9 = 1;
				}
			}
			// Gauge16
			if (flag16 == 0) {
				xg16 = 5.05;
				yg16 = 1.900;
				det12 = (x2 - xg16) * (y1 - yg16) - (y2 - yg16) * (x1 - xg16);
				det23 = (x3 - xg16) * (y2 - yg16) - (y3 - yg16) * (x2 - xg16);
				det31 = (x1 - xg16) * (y3 - yg16) - (y1 - yg16) * (x3 - xg16);
				if ((det12 <= 0.) && (det23 <= 0.) && (det31 <= 0.)) {
					ee16 = e;
					flag16 = 1;
				}
			}
		}
	} else if (71 == initial_state) { // Convergent channel
		double dists[11] = { 10., 10., 10., 10., 10., 10., 10., 10., 10., 10., 10. };// 10. is a starting big value in order to find the node with minimum distance from the gauge location.
		double offset[] = { 0, 0.3, 0.6, 0.9, 1.2, 1.5, 1.8, 2.1, 2.4, 2.7, 3.0 }; //gauge location  along the x axis
		for (int n = 0; n < NN; n++) {
			for (int l = 0; l < 11; l++) {
				double dist =
				    (node[n].coordinate[0] - offset[l]) * (node[n].coordinate[0] - offset[l]) + (node[n].coordinate[1] - 0.) * (node[n].coordinate[1] - 0.);
				if (dist < dists[l]) {
					dists[l] = dist;
					nn[l] = n;
				}
			}
		}
		for (int l = 0; l < 11; l++) {
			printf("Installing gauge %d:\n", l);
			printf("node %d, x = %le, y = %le\n", nn[l], node[nn[l]].coordinate[0], node[nn[l]].coordinate[1]);
		}
	} else if (85 == initial_state) { // Convergent channel zoom
		// Installing gauges along simmetry axis at x = iL/8 ; i=0,8           
		double dists[10] = { 10., 10., 10., 10., 10., 10., 10., 10., 10., 10. };// 10. is a starting big value in order to find the node with minimum distance from the gauge location.
		double offset[] = { 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8 }; //gauge location  along the x axis
		for (int n = 0; n < NN; n++) {
			for (int l = 0; l < 10; l++) {
				double dist =
				    (node[n].coordinate[0] - offset[l]) * (node[n].coordinate[0] - offset[l]) + (node[n].coordinate[1] - 0.) * (node[n].coordinate[1] - 0.);
				if (dist < dists[l]) {
					dists[l] = dist;
					nn[l] = n;
				}
			}
		}
		for (int l = 0; l < 10; l++) {
			printf("Installing gauge %d:\n", l);
			printf("node %d, x = %le, y = %le\n", nn[l], node[nn[l]].coordinate[0], node[nn[l]].coordinate[1]);
		}
	} else if (76 == initial_state) { // synolakis-shoaling test case set-up
		// Installing gauges along simmetry axis         
		double dists[10] = { 10., 10., 10., 10., 10., 10., 10., 10., 10., 10. };
		double offset[] = {21.8, 33.22, 33.42, 33.92, 34.22, 34.42, 34.62, 34.86, 35.15, 35.4};//gauge location along the symmetry axis
		for (int n = 0; n < NN; n++) {
			for (int l = 0; l < 10; l++) {
				double dist =
				    (node[n].coordinate[0] - offset[l]) * (node[n].coordinate[0] - offset[l]) + (node[n].coordinate[1] - 0.1) * (node[n].coordinate[1] - 0.1);
				if (dist < dists[l]) {
					dists[l] = dist;
					nn[l] = n;
				}
			}
		}
		for (int l = 0; l < 10; l++) {
			printf("Installing gauge %d:\n", l);
			printf("node %d, x = %le, y = %le\n", nn[l], node[nn[l]].coordinate[0], node[nn[l]].coordinate[1]);
		}
	} else if (78 == initial_state) { // wave over a bar
		// Installing gauges along simmetry axis at x = iL/8 ; i=0,8           
		double dists[21] = { 10., 10., 10., 10., 10., 10., 10., 10., 10., 10., 10.,10., 10., 10., 10., 10., 10., 10., 10., 10., 10. };// 10. is a starting big value in order to find the node with minimum distance from the gauge location.
		double offset[] = { -2, 0, 2.0, 4.0, 5.7, 6.0, 10., 10.4, 11.0, 12., 12.5, 13., 13.5, 14., 14.5, 15., 15.5, 17., 17.3, 19., 21. }; //gauge location  along the x axis
		for (int n = 0; n < NN; n++) {
			for (int l = 0; l < 21; l++) {
				double dist =
				    (node[n].coordinate[0] - offset[l]) * (node[n].coordinate[0] - offset[l]) + (node[n].coordinate[1] - 0.21) * (node[n].coordinate[1] - 0.21);
				    //(node[n].coordinate[1] - offset[l]) * (node[n].coordinate[1] - offset[l]) + (node[n].coordinate[0] - 0.21) * (node[n].coordinate[0] - 0.21);
				if (dist < dists[l]) {
					dists[l] = dist;
					nn[l] = n;
				}
			}
		}
		for (int l = 0; l < 21; l++) {
			printf("Installing gauge %d:\n", l);
			printf("node %d, x = %le, y = %le\n", nn[l], node[nn[l]].coordinate[0], node[nn[l]].coordinate[1]);
		}
        } else if (79 == initial_state) { // gironde-garonne-dordogne
                // Installing gauge at the estuary mouth at node 267
                nn[0] = 267; // Verdon
                printf("Installing gauge %d:\n", 0);
                printf("node %d, x = %le, y = %le\n", nn[0], node[nn[0]].coordinate[0], node[nn[0]].coordinate[1]);
                nn[1] = 9692; // Paiullac
                printf("Installing gauge %d:\n", 0);
                printf("node %d, x = %le, y = %le\n", nn[0], node[nn[0]].coordinate[0], node[nn[0]].coordinate[1]);
                nn[2] = 54020; // Bordeaux
                printf("Installing gauge %d:\n", 0);
                printf("node %d, x = %le, y = %le\n", nn[0], node[nn[0]].coordinate[0], node[nn[0]].coordinate[1]);
                nn[3] = 48630; // Podensac
                printf("Installing gauge %d:\n", 0);
                printf("node %d, x = %le, y = %le\n", nn[0], node[nn[0]].coordinate[0], node[nn[0]].coordinate[1]);
                nn[4] = 47330; // Langon
                printf("Installing gauge %d:\n", 0);
                printf("node %d, x = %le, y = %le\n", nn[0], node[nn[0]].coordinate[0], node[nn[0]].coordinate[1]);
                nn[5] = 47950; // La Reole
                printf("Installing gauge %d:\n", 0);
                printf("node %d, x = %le, y = %le\n", nn[0], node[nn[0]].coordinate[0], node[nn[0]].coordinate[1]);
                nn[6] = 37505; // Libourne
                printf("Installing gauge %d:\n", 0);
                printf("node %d, x = %le, y = %le\n", nn[0], node[nn[0]].coordinate[0], node[nn[0]].coordinate[1]);
                nn[7] = 36965; // Saint-Jean
                printf("Installing gauge %d:\n", 0);
                printf("node %d, x = %le, y = %le\n", nn[0], node[nn[0]].coordinate[0], node[nn[0]].coordinate[1]);
                nn[8] = 36560; // Pessac
                printf("Installing gauge %d:\n", 0);
                printf("node %d, x = %le, y = %le\n", nn[0], node[nn[0]].coordinate[0], node[nn[0]].coordinate[1]);
        } else if (92 == initial_state) { // garonne_ex4
                // Installing gauge at the estuary mouth at node 267
                nn[0] = 380; // Bordeaux
                printf("Installing gauge %d:\n", 0);
                printf("node %d, x = %le, y = %le\n", nn[0], node[nn[0]].coordinate[0], node[nn[0]].coordinate[1]);
                nn[1] = 26148; // Podensac
                printf("Installing gauge %d:\n", 1);
                printf("node %d, x = %le, y = %le\n", nn[1], node[nn[1]].coordinate[0], node[nn[1]].coordinate[1]);
                nn[2] = 39556; // Langon
                printf("Installing gauge %d:\n", 2);
                printf("node %d, x = %le, y = %le\n", nn[2], node[nn[2]].coordinate[0], node[nn[2]].coordinate[1]);
                nn[3] = 34714; // Castets-en-Dorthe
                printf("Installing gauge %d:\n", 3);
                printf("node %d, x = %le, y = %le\n", nn[3], node[nn[3]].coordinate[0], node[nn[3]].coordinate[1]);
	} else if (80 == initial_state) { // wave over a bar
		// Installing gauges along simmetry axis at x = iL/8 ; i=0,8           
		double dists[6] = { 10., 10., 10., 10., 10., 10.};
		double offset[] = { 15.04, 17.22, 19.4, 20.86, 22.33, 22.80}; //gauge location  along the x axis
		for (int n = 0; n < NN; n++) {
			for (int l = 0; l < 6; l++) {
				double dist =
				    (node[n].coordinate[0] - offset[l]) * (node[n].coordinate[0] - offset[l]) + (node[n].coordinate[1] - 0.5*0.5646) * (node[n].coordinate[1] - 0.5*0.5646);
				if (dist < dists[l]) {
					dists[l] = dist;
					nn[l] = n;
				}
			}
		}
		for (int l = 0; l < 6; l++) {
			printf("Installing gauge %d:\n", l);
			printf("node %d, x = %le, y = %le\n", nn[l], node[nn[l]].coordinate[0], node[nn[l]].coordinate[1]);
		}
	} else if (77==initial_state){
		// Installing gauges along simmetry axis for the reef test case           
		double dists[14] = { 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3};
		double offset[] = { 17.643, 28.604, 35.906, 40.578, 44.253, 46.093, 48.233, 50.373, 54.406, 58.050, 61.7, 65.380, 72.72, 80.03}; //gauge location  along the x axis
		for (int n = 0; n < NN; n++) {
			for (int l = 0; l < 14; l++) {
				double dist =
				    (node[n].coordinate[0] - offset[l]) * (node[n].coordinate[0] - offset[l]) + (node[n].coordinate[1] - 0.3) * (node[n].coordinate[1] - 0.3);
				if (dist < dists[l]) {
					dists[l] = dist;
					nn[l] = n;
				}
			}
		}
		for (int l = 0; l < 14; l++) {
			printf("Installing gauge %d:\n", l);
			printf("node %d, x = %le, y = %le\n", nn[l], node[nn[l]].coordinate[0], node[nn[l]].coordinate[1]);
			//printf("node %d, bed = %le\n", nn[l], node[nn[l]].bed[0]);
		}
	} else if (89==initial_state){ // Grilli shoaling of a solitary wave 
		// Installing gauges along simmetry axis for the reef test case           
		double dists[10] = { 10., 10., 10., 10., 10., 10., 10., 10., 10., 10.};
		double offset[] = { -2.2, 9.22, 9.42, 9.92, 10.22, 10.42, 10.62, 10.86, 11.15, 11.4}; //gauge location  along the x axis
		for (int n = 0; n < NN; n++) {
			for (int l = 0; l < 10; l++) {
				double dist = (node[n].coordinate[0] - offset[l]) * (node[n].coordinate[0] - offset[l]) + (node[n].coordinate[1] - 0.) * (node[n].coordinate[1] - 0.);
				if (dist < dists[l]) {
					dists[l] = dist;
					nn[l] = n;
				}
			}
		}
		for (int l = 0; l < 10; l++) {
			printf("Installing gauge %d:\n", l);
			printf("node %d, x = %le, y = %le\n", nn[l], node[nn[l]].coordinate[0], node[nn[l]].coordinate[1]);
			//printf("node %d, bed = %le\n", nn[l], node[nn[l]].bed[0]);
		}
	} else if (84 == initial_state) { // Oregon seastate
		double dists[47] ;// 10. is a starting big value in order to find the node with minimum distance from the gauge location.
                for(int i=0; i<47; i++)
			dists[i]=10.;

		double offset[47]; //gauge location  along the x axis
		double offsety[47]; //gauge location  along the y axis
                
        	FILE *gaugeP,*gaugeB;
		gaugeP = fopen("../input/Positions_jauges_Exp.dat","r");
                for(int i=0; i<35; i++)
        		fscanf(gaugeP,"%le %le\n",&offset[i],&offsety[i]);
                fclose(gaugeP);

		gaugeB = fopen("../input/Positions_jauges_Bat.dat","r");
                for(int i=35; i<47; i++)
        		fscanf(gaugeB,"%lf %lf\n",&offset[i],&offsety[i]);
                fclose(gaugeB);

		for (int n = 0; n < NN; n++) {
			for (int l = 0; l < 47 ; l++) {
				double dist =
				    (node[n].coordinate[0] - offset[l]) * (node[n].coordinate[0] - offset[l]) + (node[n].coordinate[1] - offsety[l]) * (node[n].coordinate[1] - offsety[l]);
				if (dist < dists[l]) {
					dists[l] = dist;
					nn[l] = n;
				}
			}
		}
		for (int l = 0; l < 47; l++) {
			printf("Installing gauge %d:\n", l);
			printf("node %d, x = %le, y = %le\n", nn[l], node[nn[l]].coordinate[0], node[nn[l]].coordinate[1]);
		}
	}
}

/* reassemble a system residual from scalar components */
void recompose(double *v, double *phi_i, double *phiP_i)
{
	Eigenvectors(v);
	A_times_v(Right, phi_i, phiP_i);
}

/* Periodic BCs */
void periodicity(int m)
{
	int n = b_nodes[m].node;
	int mate = node[n].mate;
	if (node[n].flag == 0) {
		for (int i = 0; i < size; i++) {
			double dummy = node[n].Res[i];
			node[n].Res[i] += node[mate].Res[i];
			node[mate].Res[i] += dummy;
		}
		/*
		   dummy = node[n].dtau ;
		   node[n].dtau += node[mate].dtau ;
		   node[mate].dtau += dummy ;
		 */

		node[n].dtau = MIN(node[n].dtau, node[mate].dtau);
		node[n].flag = 1;
		node[mate].dtau = node[n].dtau;
		node[mate].flag = 1;
	}
}

/* Standard supeersonic Inlet BCs */
void supersonic_residual_std(int v)
{
	int n = b_nodes[v].node;
	for (int i = 0; i < size; i++)
		node[n].Res[i] = 0.0;
	node[n].flag = 1;
}

int inversion3(double **A)
{
	unsigned ret = 0;
	const double a = A[0][0];
	const double b = A[0][1];
	const double c = A[0][2];
	const double d = A[1][0];
	const double e = A[1][1];
	const double f = A[1][2];
	const double g = A[2][0];
	const double h = A[2][1];
	const double i = A[2][2];
	double D = -a * e * i + a * f * h + b * d * i - b * f * g - c * d * h + c * e * g;

	if (fabs(D) < 1.e-30) {
		ret = 1;
		D = 1;
	}

	A[0][0] = (-e * i + f * h) / D;
	A[0][1] = (b * i - c * h) / D;
	A[0][2] = (-b * f + c * e) / D;
	A[1][0] = (d * i - f * g) / D;
	A[1][1] = (-a * i + c * g) / D;
	A[1][2] = (a * f - c * d) / D;
	A[2][0] = (-d * h + e * g) / D;
	A[2][1] = (a * h - b * g) / D;
	A[2][2] = (-a * e + b * d) / D;

	return ret;
}

void invertmat(double **A, double **A_1)
{
	for (int i = 0; i < size; i++)
		for (int j = 0; j < size; j++)
			A_1[i][j] = A[i][j];
	inversion3(A_1);
}

/* Max gradient reconstruction for tidal estuary test case */
void max_grad_rec(void)
{
	// Max gradient through Cent. FD scheme along the simmetry axis up to x'=3.0 
	double gradx = fabs((node[5].P[2][0] - node[0].P[2][0]) / (node[5].coordinate[0] - node[0].coordinate[0]));
	if (gradx > max_grad[0])
		max_grad[0] = gradx;

	for (int n = 1; n < (NNX - 1); n++) {
		gradx = fabs((node[n + 5].P[2][0] - node[n + 3].P[2][0]) / (node[n + 5].coordinate[0] - node[n + 3].coordinate[0]));
		if (gradx > max_grad[n])
			max_grad[n] = gradx;
	}

	gradx = fabs((node[1].P[2][0] - node[NNX + 2].P[2][0]) / (node[1].coordinate[0] - node[NNX + 2].coordinate[0]));
	if (gradx > max_grad[NNX - 1])
		max_grad[NNX - 1] = gradx;
}

/* Dissipation estimate */
void dissipation_estimate(double *tot_diss)
{
	tot_diss[0] = 0.0;
	tot_diss[1] = 0.0;
	tot_diss[2] = 0.0;
	tot_diss[3] = 0.0;

	for (int n = 0; n < NN; n++)
		node[n].diss = 0.0;

	for (int e = 0; e < NE; e++) {
		// 1D Dissipation model: D_{b0}=/Int^{x_i+1/2}_{x_i-1/2} (dE/dt + F_{E}) dx 
		double h1 = -10.;
		double h0 = 100000.0;
		//double length = 0.0;
		double xG = 0.0;
		int flag = 0;

		for (int j = 0; j < vertex; j++) {
			int n = element[e].node[j];

			if (node[n].P[2][0] > h1)
				h1 = node[n].P[2][0];
			if (node[n].P[2][0] < h0)
				h0 = node[n].P[2][0];

			xG += node[n].coordinate[0] / 3.0;

			if (node[n].coordinate[1] < 1.e-6)
				flag += 1;
		}

		double diss_ele = sqrt((h1 + h0) / (2.0 * h1 * h0)) * pow(h1 - h0, 3.0) / 4.0;

		// Different x'_{E} are considered
		if ( (xG < 2.1) && (flag == 2) )
			tot_diss[0] += diss_ele;
		if ( (xG < 2.4) && (flag == 2) )
			tot_diss[1] += diss_ele;
		if ( (xG < 2.7) && (flag == 2) )
			tot_diss[2] += diss_ele;
		if ( (xG < 3.488372093) && (flag == 2) )
			tot_diss[3] += diss_ele;

		// This is for 3D tecplot 
		for (int j = 0; j < vertex; j++) {
			int n = element[e].node[j];
			node[n].diss += diss_ele * element[e].volume;
		}
	}

	for (int n = 0; n < NN; n++)
		node[n].diss /= node[n].volume * 3.0;
}

void compute_section_area_garonne(int m)
{
	int n = b_nodes[m].node;
	b_nodes[m].area = 0.;

	// computation of the crossectional area referred to the inlet/outlet nodes
	if (74 == initial_state){
		if ((fabs(node[n].coordinate[0] - 3.725900e+05) < 0.000001) && (fabs(node[n].coordinate[1] - 3.017240e+05) < 0.000001)) // node 0
			b_nodes[m].area = b_nodes[m].blength * node[n].P[2][0];
		else if ((fabs(node[n].coordinate[0] - 3.734110e+05) < 0.000001) && (fabs(node[n].coordinate[1] - 3.017930e+05) < 0.000001)) // node 222
			b_nodes[m].area = b_nodes[m].blength * node[n].P[2][0];
		else 
			b_nodes[m].area = b_nodes[m].blength * node[n].P[2][0];
		
	} else if (92 == initial_state){
		if ((fabs(node[n].coordinate[0] - 4.487940e+05) < 0.000001) && (fabs(node[n].coordinate[1] - 6.390253e+06) < 0.000001)) // node 34266
			b_nodes[m].area = b_nodes[m].blength * node[n].P[2][0];
		else if ((fabs(node[n].coordinate[0] - 4.487440e+05) < 0.000001) && (fabs(node[n].coordinate[1] - 6.390023e+06) < 0.000001)) // node 34948
			b_nodes[m].area = b_nodes[m].blength * node[n].P[2][0];
		else 
			b_nodes[m].area = b_nodes[m].blength * node[n].P[2][0];
		
	}

	//printf("garonne %d %d %d %le %le\n",n,n1,n2,b_nodes[m].area, b_nodes[m].blength);
	Area_inlet_ga += b_nodes[m].area;
}

void compute_section_area_dordogne(int m)
{
	int n = b_nodes[m].node;
	b_nodes[m].area = 0.;

	// computation of the crossectional area referred to the inlet/outlet nodes
	if ((fabs(node[n].coordinate[0] - 3.736990e+05) < 0.000001) && (fabs(node[n].coordinate[1] - 3.059840e+05) < 0.000001)) // node 0
		b_nodes[m].area = b_nodes[m].blength * node[n].P[2][0];
	else if ((fabs(node[n].coordinate[0] - 3.744460e+05) < 0.000001) && (fabs(node[n].coordinate[1] - 3.066120e+05) < 0.000001)) // node 8
		b_nodes[m].area = b_nodes[m].blength * node[n].P[2][0];
	else 
		b_nodes[m].area = b_nodes[m].blength * node[n].P[2][0];

//	printf("dordogne %d %le\n",n,b_nodes[m].area);
	Area_inlet_do += b_nodes[m].area;
}

void compute_section_area_gironde(int m)
{
	int n = b_nodes[m].node;
	double y = node[n].coordinate[1];
	b_nodes[m].area = 0.;
	
	// computation of the crossectional area referred to the inlet/outlet nodes
	if (((fabs(node[n].coordinate[0] - 3.734110e+05) < 0.000001) && (fabs(node[n].coordinate[1] - 3.017930e+05) < 0.000001))  // node 26714
	    || ((fabs(node[n].coordinate[0] - 3.744460e+05) < 0.000001) && (fabs(node[n].coordinate[1] - 3.066120e+05) < 0.000001))) // node 26816
		b_nodes[m].area = b_nodes[m].blength * node[n].P[2][0];
	else if (((fabs(node[n].coordinate[0] - 3.725900e+05) < 0.000001) && (fabs(node[n].coordinate[1] - 3.017240e+05) < 0.000001)) // node 26488
		   || ((fabs(node[n].coordinate[0] - 3.736990e+05) < 0.000001) && (fabs(node[n].coordinate[1] - 3.059840e+05) < 0.000001))) // node 26754
		b_nodes[m].area = b_nodes[m].blength * node[n].P[2][0];
	else 
		b_nodes[m].area = b_nodes[m].blength * node[n].P[2][0];
	

	if (y > 304500.) // Dordogne
		Area_inlet_do += b_nodes[m].area;
	else // Garonne
		Area_inlet_ga += b_nodes[m].area;
	
}

void read_tide_amplitude( void )
{
        FILE *tide;
        double dummy1,dummy2;
		if (79 == initial_state)
			tide = fopen("../input/tide.dat","r");
		else if (92 == initial_state)
			tide = fopen("../input/tide_bordeaux.dat","r");

        if (!tide)
                printf("ERROR: External file tide.dat was not found !\n");

        fscanf(tide,"%lf %lf\n",&dummy1,&dummy2);
        tidal_DIM = (int)floor(dummy1);
        printf("DIM %d\n",tidal_DIM);
        tidal_time = MA_vector( tidal_DIM, sizeof( double ) ) ;
        tidal_elev = MA_vector( tidal_DIM, sizeof( double ) ) ;
        int k = 0;
        while(fscanf(tide,"%le %le\n",&tidal_time[k],&tidal_elev[k]) == 2)
                k++;

        fclose(tide);
}

void read_channel_gauge( void )
{
        FILE *entry,*out;
        double dummy1,dummy2,dummy3;

	// Count the lines of the file:
        entry = fopen("../input/mascaret_gauge4.dat","r");
        if (!entry)
                printf("ERROR: External file mascaret_gauge4.dat was not found !\n");
        int count = 0;
        while(fscanf(entry,"%le %le %le\n",&dummy1,&dummy2,&dummy1) == 3)
                count++;
        fclose(entry);

	// Load gauge signal:		
        entry = fopen("../input/mascaret_gauge4.dat","r");
        if (!entry)
                printf("ERROR: External file mascaret_gauge4.dat was not found !\n");

	per_DIM = count - 367035;
        gaugeIN_time = MA_vector( per_DIM, sizeof( double ) ) ;
      	gaugeIN_elev = MA_vector( per_DIM, sizeof( double ) ) ;
      	gaugeIN_vel = MA_vector( per_DIM, sizeof( double ) ) ;
        int k = 0;
        while(fscanf(entry,"%le %le %le\n",&dummy1,&dummy2,&dummy3) == 3){
		if (k>=367035) {
			gaugeIN_time[k-367035] = dummy1;
			gaugeIN_elev[k-367035] = dummy2;
			gaugeIN_vel[k-367035] = dummy3;
		}
		k++;
	}
        fclose(entry);

	/********/
        out = fopen("../input/mascaret_gauge7.dat","r");
        if (!out)
                printf("ERROR: External file mascaret_gauge7.dat was not found !\n");

        gaugeOUT_time = MA_vector( per_DIM, sizeof( double ) ) ;
      	gaugeOUT_elev = MA_vector( per_DIM, sizeof( double ) ) ;
      	gaugeOUT_vel = MA_vector( per_DIM, sizeof( double ) ) ;
        k = 0;
        while(fscanf(out,"%le %le %le\n",&dummy1,&dummy2,&dummy3) == 3){
		if (k>=367035) {
			gaugeOUT_time[k-367035] = dummy1;
			gaugeOUT_elev[k-367035] = dummy2;
			gaugeOUT_vel[k-367035] = dummy3;
		}
		k++;
	}
        fclose(out);

//	printf("IN %le %le OUT %le %le\n",gaugeIN_time[0],gaugeIN_time[1],gaugeOUT_time[0],gaugeOUT_time[1]);	
	for (int i = 0; i<per_DIM; i++){
//		gaugeIN_time[i] *= 1./(2.*3.14); 
//		gaugeIN_time[i] -= 2-0.005289; 
		gaugeIN_time[i] -= 12.52683; 
//		gaugeOUT_time[i] *= 1./(2.*3.14); 
//		gaugeOUT_time[i] -= 2-0.005289; 
		gaugeOUT_time[i] -= 12.52683; 
	}
//	gaugeIN_time[0] = 0.;
//	gaugeOUT_time[0] = 0.;
//	printf("IN %le %le OUT %le %le\n",gaugeIN_time[0],gaugeIN_time[1],gaugeOUT_time[0],gaugeOUT_time[1]);	

//	exit(0);

}

void compute_tide_amplitude()
{
	double loc_time;
	if (79 == initial_state)
		loc_time = Time/(24*3600); //from seconds to days
	else if (92 == initial_state)
		loc_time = Time;
	int flag = 0;
	for (int n=0; n<tidal_DIM; n++) {
		if ( loc_time>=tidal_time[n] && loc_time<tidal_time[n+1]){
			double slope = (tidal_elev[n+1] - tidal_elev[n])/(tidal_time[n+1] - tidal_time[n]);
			TIDE = tidal_elev[n] + slope*(loc_time - tidal_time[n]);
			flag += 1;
			//printf("time %le n %le n+1 %le\n",loc_time,tidal_time[n],tidal_time[n+1]);
			//printf("TIDE %le n %le n+1 %le\n",TIDE,tidal_elev[n],tidal_elev[n+1]);
		}
	}

	if (flag!=1){
		printf("ERROR!! The value of the flag is %d in time %le\n", flag, loc_time);
		exit(0);
	}
}

void compute_INOUT_elev()
{
	double loc_time = Time;
	if (loc_time >= 1.)
		loc_time -= 1;
	if (loc_time >= 2.)
		loc_time -= 2;

	int flag = 0;
	for (int n=0; n<per_DIM; n++) {
		if ( loc_time>=gaugeIN_time[n] && loc_time<gaugeIN_time[n+1]){
                        double slopeIN = (gaugeIN_elev[n+1] - gaugeIN_elev[n])/(gaugeIN_time[n+1] - gaugeIN_time[n]);
                        double slopeOUT = (gaugeOUT_elev[n+1] - gaugeOUT_elev[n])/(gaugeOUT_time[n+1] - gaugeOUT_time[n]);
                        gaugeIN = gaugeIN_elev[n] + slopeIN*(loc_time - gaugeIN_time[n]);
                        gaugeOUT = gaugeOUT_elev[n] + slopeOUT*(loc_time - gaugeOUT_time[n]);
                        double velIN = (gaugeIN_vel[n+1] - gaugeIN_vel[n])/(gaugeIN_time[n+1] - gaugeIN_time[n]);
                        double velOUT = (gaugeOUT_vel[n+1] - gaugeOUT_vel[n])/(gaugeOUT_time[n+1] - gaugeOUT_time[n]);
                        gauge_velIN = gaugeIN_vel[n] + velIN*(loc_time - gaugeIN_time[n]);
                        gauge_velOUT = gaugeOUT_vel[n] + velOUT*(loc_time - gaugeOUT_time[n]);
                        flag += 1;
//                      printf("time %le n %le n+1 %le\n",loc_time,gaugeIN_time[n],gaugeIN_time[n+1]);
//                      printf("TIDE %le n %le n+1 %le\n",gaugeIN,gaugeIN_elev[n],gaugeIN_elev[n+1]);
//                      printf("time %le n %le n+1 %le\n",loc_time,gaugeOUT_time[n],gaugeOUT_time[n+1]);
//                      printf("TIDE %le n %le n+1 %le\n",gaugeOUT,gaugeOUT_elev[n],gaugeOUT_elev[n+1]);
                }
        }
        if (flag!=1){
                printf("ERROR!! The value of the flag is %d in time %le\n", flag, loc_time);
                exit(0);
        }
}

void geom_preprocessing()
{
	//int n3, m3, mate, newmate, j, neigh_counter1, neigh_counter2;
	int e, n1, n2, n, v;
	double x1, y1;//, nx1, ny1, nx2, ny2;
	double x2, y2;

	//double x_max, x_min, y_max, y_min;

	//double dB;

	for (n = 0; n < NN; n++)
		node[n].volume = 0.;

	/*******************************************/
	/**    Computation of the nodal nomals    **/
	/*******************************************/

	for (e = 0; e < NE; e++) {

		n1 = element[e].node[1];
		n2 = element[e].node[2];

		x1 = node[n1].coordinate[0];
		y1 = node[n1].coordinate[1];

		x2 = node[n2].coordinate[0];
		y2 = node[n2].coordinate[1];

		element[e].normal[0][0] = y1 - y2;
		element[e].normal[0][1] = x2 - x1;

		n1 = element[e].node[2];
		n2 = element[e].node[0];

		x1 = node[n1].coordinate[0];
		y1 = node[n1].coordinate[1];

		x2 = node[n2].coordinate[0];
		y2 = node[n2].coordinate[1];

		element[e].normal[1][0] = y1 - y2;
		element[e].normal[1][1] = x2 - x1;

		n1 = element[e].node[0];
		n2 = element[e].node[1];

		x1 = node[n1].coordinate[0];
		y1 = node[n1].coordinate[1];

		x2 = node[n2].coordinate[0];
		y2 = node[n2].coordinate[1];

		element[e].normal[2][0] = y1 - y2;
		element[e].normal[2][1] = x2 - x1;

		/**************************************************/
		/**   Computation of the volume of the element   **/
		/**************************************************/

		element[e].volume = 0.5 * fabs(element[e].normal[2][0] * element[e].normal[1][1] -
					       element[e].normal[1][0] * element[e].normal[2][1]);

		for (v = 0; v < 3; v++) {
			node[element[e].node[v]].volume += element[e].volume / 3.;
		}

	}			/* End loop over elements */

	/**********************************************/
	/*******      Boundary Normals          *******/
	/**********************************************/

/*	m = 0;

	for (f = 0; f < NBF; f++)
	{
		n1 = boundary[f].node[0];
		n2 = boundary[f].node[1];

		x1 = node[n1].coordinate[0];
		y1 = node[n1].coordinate[1];

		x2 = node[n2].coordinate[0];
		y2 = node[n2].coordinate[1];

		nx1 = y1 - y2;
		ny1 = x2 - x1;

		if (f == 0) {
			b_nodes[m].node = n1;
			b_nodes[m].mate = node[n1].mate;

			m1 = boundary[NBF - 1].node[0];
			m2 = boundary[NBF - 1].node[1];

			x1 = node[m1].coordinate[0];
			y1 = node[m1].coordinate[1];

			x2 = node[m2].coordinate[0];
			y2 = node[m2].coordinate[1];

			nx2 = y1 - y2;
			ny2 = x2 - x1;

			b_nodes[m].normal[0] = 0.5 * (nx1 + nx2);
			b_nodes[m].normal[1] = 0.5 * (ny1 + ny2);

			b_nodes[m].type = boundary[f].types[0];

			if ((initial_state == 7) && fabs(node[n1].coordinate[0] - 75.) < 1.e-6) {
				b_nodes[m].normal[0] = nx1;
				b_nodes[m].normal[1] = ny1;
			}

			if ((initial_state == 7) && fabs(node[n1].coordinate[0] + 200.) < 1.e-6) {
				b_nodes[m].normal[0] = nx1;
				b_nodes[m].normal[1] = ny1;
			}
		} else {
			b_nodes[m].node = n1;
			b_nodes[m].mate = node[n1].mate;

			m1 = boundary[f - 1].node[0];
			m2 = boundary[f - 1].node[1];

			x1 = node[m1].coordinate[0];
			y1 = node[m1].coordinate[1];

			x2 = node[m2].coordinate[0];
			y2 = node[m2].coordinate[1];

			nx2 = y1 - y2;
			ny2 = x2 - x1;

			b_nodes[m].normal[0] = 0.5 * (nx1 + nx2);
			b_nodes[m].normal[1] = 0.5 * (ny1 + ny2);

			b_nodes[m].type = boundary[f].types[0];

			if ((initial_state == 7) && fabs(node[n1].coordinate[0] + 200.) < 1.e-6) {
				b_nodes[m].normal[0] = nx1;
				b_nodes[m].normal[1] = ny1;
			}
		}

		m += 1;
	}

	if (m != NBF)
		printf("ERROR in the Boundary preprocessing !!!!!!!!");
*/
}

void install_probes(void)
{

	if (75 == initial_state){// rip current
		Nprobes = 220;
		probes = MA_vector( Nprobes, sizeof(struct probe_struct) );
		for (int i = 0; i < Nprobes; i++)
			probes[i].coordinate = MA_vector( 2, sizeof(double) );

		int k = 0;
		for (int i = 0; i < 110; i++) {
			probes[i].coordinate[0] = 3.0 + 0.2*k;
			probes[i].coordinate[1] = 1.9875;
			k = k + 1;
		}
		k = 0;
		for (int i = 110; i < Nprobes; i++) {
			probes[i].coordinate[0] = 3.0 + 0.2*k;
			probes[i].coordinate[1] = 14.9625;
			k = k + 1;
		}
		
		for (int n = 0; n < Nprobes; n++) {
			probes[n].node = -1;
			probes[n].dist = 10.;
		}

		for (int m = 0; m < Nprobes; m++) {
			double xp = probes[m].coordinate[0];
			double yp = probes[m].coordinate[1];

			for (int e = 0; e < NE; e++) {
				int n = element[e].node[0];
				double nx = element[e].normal[0][0];
				double ny = element[e].normal[0][1];
				double x = node[n].coordinate[0];
				double y = node[n].coordinate[1];
				double phi1 = 1. + 0.5 * (nx * (xp - x) + ny * (yp - y)) / element[e].volume;

				n = element[e].node[1];
				nx = element[e].normal[1][0];
				ny = element[e].normal[1][1];
				x = node[n].coordinate[0];
				y = node[n].coordinate[1];
				double phi2 = 1. + 0.5 * (nx * (xp - x) + ny * (yp - y)) / element[e].volume;

				n = element[e].node[2];
				nx = element[e].normal[2][0];
				ny = element[e].normal[2][1];
				x = node[n].coordinate[0];
				y = node[n].coordinate[1];
				double phi3 = 1. + 0.5 * (nx * (xp - x) + ny * (yp - y)) / element[e].volume;

				if (((phi1 >= 0.) && (phi2 >= 0.)) && (phi3 >= 0.)) {
//                                      printf("fis %le %le %le\n",phi1,phi2,phi3) ;
					probes[m].node = element[e].node[0];

					if (phi2 > phi1) {
						probes[m].node = element[e].node[1];
						if (phi3 > phi2)
							probes[m].node = element[e].node[2];
					} else if (phi3 > phi1)
						probes[m].node = element[e].node[2];
				}
			}	/* end loop over elements */
		}		/* end loop over probes */
		
		for (int n = 0; n < Nprobes; n++) {
			if (probes[n].node < 0)
				printf("WARNING: trouble installing probe%d !!!!\n", n);
			else {
				int m = probes[n].node;
				double x = node[m].coordinate[0];
				double y = node[m].coordinate[1];

				if (fabs(x - probes[n].coordinate[0]) > 1.e-6)
					node[m].coordinate[0] = probes[n].coordinate[0];
				if (fabs(y - probes[n].coordinate[1]) > 1.e-6)
					node[m].coordinate[1] = probes[n].coordinate[1];
			}
		}

		geom_preprocessing();


	} else if (82 == initial_state){// circ shoal
		Nprobes = 43;
		probes = MA_vector( Nprobes, sizeof(struct probe_struct) );
		for (int i = 0; i < Nprobes; i++)
			probes[i].coordinate = MA_vector( 2, sizeof(double) );

		probes[0].coordinate[0] = 0.;
		probes[0].coordinate[1] = 0.5 * 6.096;

		for (int i = 1; i < Nprobes; i++) {
			probes[i].coordinate[1] = 0.5 * 6.096;
			if (probes[i - 1].coordinate[0] >= 15.+10)
				probes[i].coordinate[0] = probes[i - 1].coordinate[0] + 0.5;
			else {
				if (probes[i - 1].coordinate[0] >= 9.+10)
					probes[i].coordinate[0] = probes[i - 1].coordinate[0] + 1.0;
				else
					probes[i].coordinate[0] = probes[i - 1].coordinate[0] + 1.5;
			}
		}

		for (int n = 0; n < Nprobes; n++) {
			probes[n].node = -1;
			probes[n].dist = 10.;
		}

		for (int m = 0; m < Nprobes; m++) {
			double xp = probes[m].coordinate[0];
			double yp = probes[m].coordinate[1];

			for (int e = 0; e < NE; e++) {
				int n = element[e].node[0];
				double nx = element[e].normal[0][0];
				double ny = element[e].normal[0][1];
				double x = node[n].coordinate[0];
				double y = node[n].coordinate[1];
				double phi1 = 1. + 0.5 * (nx * (xp - x) + ny * (yp - y)) / element[e].volume;

				n = element[e].node[1];
				nx = element[e].normal[1][0];
				ny = element[e].normal[1][1];
				x = node[n].coordinate[0];
				y = node[n].coordinate[1];
				double phi2 = 1. + 0.5 * (nx * (xp - x) + ny * (yp - y)) / element[e].volume;

				n = element[e].node[2];
				nx = element[e].normal[2][0];
				ny = element[e].normal[2][1];
				x = node[n].coordinate[0];
				y = node[n].coordinate[1];
				double phi3 = 1. + 0.5 * (nx * (xp - x) + ny * (yp - y)) / element[e].volume;

				if (((phi1 >= 0.) && (phi2 >= 0.)) && (phi3 >= 0.)) {
//                                      printf("fis %le %le %le\n",phi1,phi2,phi3) ;
					probes[m].node = element[e].node[0];

					if (phi2 > phi1) {
						probes[m].node = element[e].node[1];
						if (phi3 > phi2)
							probes[m].node = element[e].node[2];
					} else if (phi3 > phi1)
						probes[m].node = element[e].node[2];
				}
			}	/* end loop over elements */
		}		/* end loop over probes */

		for (int n = 0; n < Nprobes; n++) {
			if (probes[n].node < 0)
				printf("WARNING: trouble installing probe%d !!!!\n", n);
			else {
				int m = probes[n].node;
				double x = node[m].coordinate[0];
				double y = node[m].coordinate[1];

				if (fabs(x - probes[n].coordinate[0]) > 1.e-6)
					node[m].coordinate[0] = probes[n].coordinate[0];
				if (fabs(y - probes[n].coordinate[1]) > 1.e-6)
					node[m].coordinate[1] = probes[n].coordinate[1];
			}
		}

		geom_preprocessing();

	} else if (81 == initial_state){// elliptic shoal
		Nprobes = 401;
		probes = MA_vector( Nprobes, sizeof(struct probe_struct) );
		for (int i = 0; i < Nprobes; i++)
			probes[i].coordinate = MA_vector( 2, sizeof(double) );

		/* Section 1: y = 1 ; -5 < x < 5 */

		for (int i = 0; i < 50; i++) {
			probes[i].coordinate[0] = -4.8 + 0.2 * i;
			probes[i].coordinate[1] = 1.;
			//probes[i].coordinate[1] = -4.8 + 0.2 * i;
			//probes[i].coordinate[0] = 1.;
		}

		/* Section 2: y = 3 ; -5 < x < 5 */

		for (int i = 50; i < 100; i++) {
			probes[i].coordinate[0] = -4.8 + 0.2 * (i - 50);
			probes[i].coordinate[1] = 3.;
			//probes[i].coordinate[1] = -4.8 + 0.2 * (i - 50);
			//probes[i].coordinate[0] = 3.;
		}

		/* Section 3: y = 5 ; -5 < x < 5 */

		for (int i = 100; i < 150; i++) {
			probes[i].coordinate[0] = -4.8 + 0.2 * (i - 100);
			probes[i].coordinate[1] = 5.;
			//probes[i].coordinate[1] = -4.8 + 0.2 * (i - 100);
			//probes[i].coordinate[0] = 5.;
		}

		/* Section 4: y = 7 ; -5 < x < 5 */

		for (int i = 150; i < 200; i++) {
			probes[i].coordinate[0] = -4.8 + 0.2 * (i - 150);
			probes[i].coordinate[1] = 7.;
			//probes[i].coordinate[1] = -4.8 + 0.2 * (i - 150);
			//probes[i].coordinate[0] = 7.;
		}

		/* Section 5: y = 9 ; -5 < x < 5 */

		for (int i = 200; i < 250; i++) {
			probes[i].coordinate[0] = -4.8 + 0.2 * (i - 200);
			probes[i].coordinate[1] = 9.;
			//probes[i].coordinate[1] = -4.8 + 0.2 * (i - 200);
			//probes[i].coordinate[0] = 9.;
		}

		/* Section 6: x = -2 ; 0 < y < 10 */

		for (int i = 250; i < 300; i++) {
			probes[i].coordinate[0] = -2.;
			probes[i].coordinate[1] = 0.2 + 0.2 * (i - 250);
			//probes[i].coordinate[1] = -2.;
			//probes[i].coordinate[0] = 0.2 + 0.2 * (i - 250);
		}

		/* Section 7: x = 0 ; 0 < y < 10 */

		for (int i = 300; i < 350; i++) {
			probes[i].coordinate[0] = 0.;
			probes[i].coordinate[1] = 0.2 + 0.2 * (i - 300);
			//probes[i].coordinate[1] = 0.;
			//probes[i].coordinate[0] = 0.2 + 0.2 * (i - 300);
		}

		/* Section 8: x = 2 ; 0 < y < 10 */

		for (int i = 350; i < 400; i++) {
			probes[i].coordinate[0] = 2.;
			probes[i].coordinate[1] = 0.2 + 0.2 * (i - 350);
			//probes[i].coordinate[1] = 2.;
			//probes[i].coordinate[0] = 0.2 + 0.2 * (i - 350);
		}

		probes[400].coordinate[0] = 0.;
		probes[400].coordinate[1] = -9.6;
		//probes[400].coordinate[1] = 0.;
		//probes[400].coordinate[0] = -9.6;

		for (int n = 0; n < Nprobes; n++) {
			probes[n].node = -1;
			probes[n].dist = 10.;
		}

		for (int m = 0; m < Nprobes; m++) {
			double xp = probes[m].coordinate[0];
			double yp = probes[m].coordinate[1];

			for (int e = 0; e < NE; e++) {
				int n = element[e].node[0];
				double nx = element[e].normal[0][0];
				double ny = element[e].normal[0][1];
				double x = node[n].coordinate[0];
				double y = node[n].coordinate[1];
				double phi1 = 1. + 0.5 * (nx * (xp - x) + ny * (yp - y)) / element[e].volume;

				n = element[e].node[1];
				nx = element[e].normal[1][0];
				ny = element[e].normal[1][1];
				x = node[n].coordinate[0];
				y = node[n].coordinate[1];
				double phi2 = 1. + 0.5 * (nx * (xp - x) + ny * (yp - y)) / element[e].volume;

				n = element[e].node[2];
				nx = element[e].normal[2][0];
				ny = element[e].normal[2][1];
				x = node[n].coordinate[0];
				y = node[n].coordinate[1];
				double phi3 = 1. + 0.5 * (nx * (xp - x) + ny * (yp - y)) / element[e].volume;

				if (((phi1 >= 0.) && (phi2 >= 0.)) && (phi3 >= 0.)) {
					probes[m].node = element[e].node[0];

					if (phi2 > phi1) {
						probes[m].node = element[e].node[1];
						if (phi3 > phi2)
							probes[m].node = element[e].node[2];
					} else if (phi3 > phi1)
						probes[m].node = element[e].node[2];
				}
			}	/* end loop over elements */
		}		/* end loop over probes */

		for (int n = 0; n < Nprobes; n++) {
			if (probes[n].node < 0)
				printf("WARNING: trouble installing probe%d !!!!\n", n);
			else {
				int m = probes[n].node;
				double x = node[m].coordinate[0];
				double y = node[m].coordinate[1];

				if (fabs(x - probes[n].coordinate[0]) > 1.e-6)
					node[m].coordinate[0] = probes[n].coordinate[0];
				if (fabs(y - probes[n].coordinate[1]) > 1.e-6)
					node[m].coordinate[1] = probes[n].coordinate[1];
			}
		}

		geom_preprocessing();

	} else if (93 == initial_state){// cylinder
		Nprobes = 6;
		probes = MA_vector( Nprobes, sizeof(struct probe_struct) );
		for (int i = 0; i < Nprobes; i++)
			probes[i].coordinate = MA_vector( 2, sizeof(double) );

		probes[0].coordinate[0] = 8.4;
		probes[0].coordinate[1] = 0.275;
		probes[1].coordinate[0] = 8.5;
		probes[1].coordinate[1] = 0.170;
		probes[2].coordinate[0] = 8.5;
		probes[2].coordinate[1] = 0.045;
		probes[3].coordinate[0] = 8.6;
		probes[3].coordinate[1] = 0.275;
		probes[4].coordinate[0] = 8.975;
		probes[4].coordinate[1] = 0.275;
		probes[5].coordinate[0] = 9.375;
		probes[5].coordinate[1] = 0.275;



		for (int n = 0; n < Nprobes; n++) {
			probes[n].node = -1;
			probes[n].dist = 10.;
		}

		for (int m = 0; m < Nprobes; m++) {
			double xp = probes[m].coordinate[0];
			double yp = probes[m].coordinate[1];

			for (int e = 0; e < NE; e++) {
				int n = element[e].node[0];
				double nx = element[e].normal[0][0];
				double ny = element[e].normal[0][1];
				double x = node[n].coordinate[0];
				double y = node[n].coordinate[1];
				double phi1 = 1. + 0.5 * (nx * (xp - x) + ny * (yp - y)) / element[e].volume;

				n = element[e].node[1];
				nx = element[e].normal[1][0];
				ny = element[e].normal[1][1];
				x = node[n].coordinate[0];
				y = node[n].coordinate[1];
				double phi2 = 1. + 0.5 * (nx * (xp - x) + ny * (yp - y)) / element[e].volume;

				n = element[e].node[2];
				nx = element[e].normal[2][0];
				ny = element[e].normal[2][1];
				x = node[n].coordinate[0];
				y = node[n].coordinate[1];
				double phi3 = 1. + 0.5 * (nx * (xp - x) + ny * (yp - y)) / element[e].volume;

				if (((phi1 >= 0.) && (phi2 >= 0.)) && (phi3 >= 0.)) {
//                                      printf("fis %le %le %le\n",phi1,phi2,phi3) ;
					probes[m].node = element[e].node[0];

					if (phi2 > phi1) {
						probes[m].node = element[e].node[1];
						if (phi3 > phi2)
							probes[m].node = element[e].node[2];
					} else if (phi3 > phi1)
						probes[m].node = element[e].node[2];
				}
			}	/* end loop over elements */
		}		/* end loop over probes */

		for (int n = 0; n < Nprobes; n++) {
			if (probes[n].node < 0)
				printf("WARNING: trouble installing probe%d !!!!\n", n);
			else {
				int m = probes[n].node;
				double x = node[m].coordinate[0];
				double y = node[m].coordinate[1];

				if (fabs(x - probes[n].coordinate[0]) > 1.e-6)
					node[m].coordinate[0] = probes[n].coordinate[0];
				if (fabs(y - probes[n].coordinate[1]) > 1.e-6)
					node[m].coordinate[1] = probes[n].coordinate[1];
			}
		}

		geom_preprocessing();
	}
}

void rotation(double *p0, double *p1, double *p2, double *symmetric){
//given 3 points (two vectors with a common start p0=(x0,y0) ) find the symmetric point of n1 in respec to n0,n2 line

	double length02 = sqrt(pow(p2[0] - p0[0],2) + pow(p2[1] - p0[1],2));
	double length01 = sqrt(pow(p1[0] - p0[0],2) + pow(p1[1] - p0[1],2));
	double dot = (p1[0] -p0[0])*(p2[0] -p0[0]) + (p1[1] -p0[1])*(p2[1] -p0[1]);
	double theta = acos(dot/(length01*length02));
	//if(fabs(p0[0]-46.2)<0.0001 && fabs(p0[1]-0.0)<0.0001) printf("%le %le %le\n",theta,p2[0],p2[1]);
	if(fabs(p2[1]-p0[1])<0.00001){ //boundary on the x axis
		if(p0[1]> p1[1] ) {
			theta = 2.0*theta;
		} else if (p0[1]<=p1[1] ){
			theta = -2.0*theta;
		}
	}else if(fabs(p2[0]-p0[0])<0.00001){ //boundary on the y axis
		if(p0[0]< p1[0]) {
			theta = 2.0*theta;
		} else if ( p0[0]>=p1[0]){
			theta = -2.0*theta;
		}

	}
	symmetric[0] = cos(theta)*(p1[0]-p0[0]) - sin(theta)*(p1[1]-p0[1]) + p0[0];
	symmetric[1] = sin(theta)*(p1[0]-p0[0]) + cos(theta)*(p1[1]-p0[1]) + p0[1];
}
double distance(double x0, double y0, double x1, double y1, double x2, double y2 ){
	// distance of the point x0,y0 from the line crossing the point (x1,y1) (x2,x2)
	
       double tmp  = fabs((y2-y1)*x0 -(x2-x1)*y0 +x2*y1-y2*x1)/sqrt((y2-y1)*(y2-y1) + (x2-x1)*(x2-x1)); //distance of point n0  to the line c2 c0
       return tmp;	       
}
