/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/
 


/***************************************************************************
                        LF_PSI_distribution.c
 ----------------
 This is LF_PSI_distribution: here the distribution of the cell fluctuation is
 performed through the Lax-Friedrich scheme distribution function
 with additional PSI-Limiting and additional dissipation
 -------------------
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "common.h"

extern const int size;
extern int iteration, zero_nodes, blending, Mscaling, numerics_friction_explicit, initial_state;
extern double u_ref, L_ref, HHref, UUinf, VVinf, gridsize, dt, ubar0, vbar0, hubar0, hvbar0;
double alpha_LF;
extern double sigma_barx, sigma_bary, height0, gm, cut_off_H, cut_off_U, gridsize, coarse_cut_off ;
extern double *work_vector0, *work_vector1, *work_vector2, *phi_a_w1, *phi_w, vel[2], B[3];
extern double **PHI_d, **PHIN_d, **PHIN_scal, **Left, **Right, **dU, **W, **sum_K, **sum_K_1;
extern double ***K_i, ***K_i_p1;
extern struct node_struct *node;
extern struct element_struct *element;

void initmat(double **);
void add2mat(double **, double **);
void invertmat(double **, double **);
void Eigenvectors(double *);
void velocity(double);

/* distribute shallow water fluctuation */
void distribute_RD_fluctuation(int e)
{
	double betaP, arg, delta, pos, HHmin, HHmax;
	double h_min, phi_abs, x1, eta_min, bedmax, bed0;
	double DDV[3][3], DV[3][3], diss[3], V[3], E, factor, M1[3][3], M2[3][3];
	double eta1, eta2, eta3;

	double vel_x, vel_y, ale_corr_fact;

	int d1 = 0 ;
	int d2 = 0 ;
	int nd1 = 0 ;
	int nd2 = 0 ;
	int w1 = 0 ;
	int w2 = 0 ;
	int nw1 = 0 ;
	int nw2 = 0 ;
	int ndry = 0 ;
	int n1 = 0 ;
	int n2 = 0 ;
	int n3 = 0 ;
    
	int itr = iteration - 1;
	
	double fluxlim = 0.;

	for ( int v = 0; v < 3; v++) {
                int nn = element[e].node[v] ;
        
		for (int i = 0; i < size; i++) {
			W[v][i] = node[nn].P[0][i];
			if (itr > 0)
				W[v][i] = 0.5 * node[nn].P[0][i] + 0.5 * node[nn].P[2][i];
		}

		B[v] = node[nn].bed[0];
		if (itr > 0)
			B[v] = 0.5 * node[nn].bed[0] + 0.5 * node[nn].bed[2];

        if ( node[nn].u_norm > fluxlim ) fluxlim = node[nn].u_norm ;
	}


	double length = dt * element[e].volume;
	length = pow(length, 1. / 3.);

	// computing the divergence div(sigma)
	double div = 0.;
	for (int v = 0; v < 3; v++) {
		vel_x = node[element[e].node[v]].vel[0];
		vel_y = node[element[e].node[v]].vel[1];
		div += 0.5 * (vel_x * element[e].normal[v][0] + vel_y * element[e].normal[v][1]);
	}

	// building SUPG distribution matrix 
	if (Mscaling) {
		initmat(sum_K);

		for (int v = 0; v < 3; v++)
			add2mat(sum_K, K_i_p1[v]);

		invertmat(sum_K, sum_K_1);
	}

	// building modified Galerkin mass-matrix 
	ale_corr_fact = (1. + dt / (2. * element[e].volume) * div);

	for (int j = 0; j < size; j++) {
		// Lump Galerkin term
		DDV[0][j] = -ale_corr_fact * dU[0][j];
		DDV[1][j] = -ale_corr_fact * dU[1][j];
		DDV[2][j] = -ale_corr_fact * dU[2][j];
		// F2 diss term	
		DV[0][j] = ale_corr_fact * (2. * dU[0][j] - dU[1][j] - dU[2][j]) / 12.;
		DV[1][j] = ale_corr_fact * (2. * dU[1][j] - dU[0][j] - dU[2][j]) / 12.;
		DV[2][j] = ale_corr_fact * (2. * dU[2][j] - dU[1][j] - dU[0][j]) / 12.;
	}

	// Reference quantities over the element        
	u_ref = fabs(ubar0);
	if (fabs(vbar0) > u_ref)
		u_ref = fabs(vbar0);

	h_min = height0;
	HHmax = height0;

	bed0 = 0.0;
	for (int l = 0; l < 3; l++) {
		int n = element[e].node[l];
		bed0 += node[n].bed[0] / 3.0;
	}
	eta_min = height0 + bed0;
	bedmax = bed0;

	for (int l = 0; l < 3; l++) {
		int n = element[e].node[l];

		if (node[n].P[0][0] < h_min)
			h_min = node[n].P[0][0];
		if (node[n].P[2][0] < h_min)
			h_min = node[n].P[2][0];

		if ((node[n].P[0][0] + node[n].bed[0]) < eta_min)
			eta_min = node[n].P[0][0] + node[n].bed[0];
		if ((node[n].P[2][0] + node[n].bed[2]) < eta_min)
			eta_min = node[n].P[2][0] + node[n].bed[2];

		if (node[n].bed[0] > bedmax)
			bedmax = node[n].bed[0];
		if (node[n].bed[2] > bedmax)
			bedmax = node[n].bed[2];

		if (node[n].P[0][0] > HHmax)
			HHmax = node[n].P[0][0];
		if (node[n].P[2][0] > HHmax)
			HHmax = node[n].P[2][0];


        
		if (fabs(node[n].Z[2][2]) > u_ref)
			u_ref = fabs(node[n].Z[2][2]) ;
		if (fabs(node[n].Z[0][2])  > u_ref)
			u_ref = fabs(node[n].Z[0][2]) ;
		if (fabs(node[n].Z[2][1])  > u_ref)
			u_ref = fabs(node[n].Z[2][1]) ;
		if (fabs(node[n].Z[0][1]) > u_ref)
			u_ref = fabs(node[n].Z[0][1]) ;
        
        if ( u_ref > fluxlim ) u_ref = fluxlim ;
	}

	HHmin = h_min;

	u_ref += 0.5 * (UUinf + VVinf) * gridsize * element[e].cut_off_ratio / L_ref;

	E = HHmax * (gm * HHmax + u_ref * u_ref * 0.5);

	V[0] = gm * HHmax - 0.5 * u_ref * u_ref;
	V[1] = u_ref;
	V[2] = u_ref;

	// Worst estimate for minimal height
	double cbar = sqrt(gm * height0) + sqrt(cut_off_U * element[e].cut_off_ratio);
	double Fr = sqrt(ubar0 * ubar0 + vbar0 * vbar0) / cbar;
	double plim = 0.5 * (fabs(1 - Fr) + (1 + Fr)) * fabs(phi_w[0]) + (fabs(phi_w[1]) + fabs(phi_w[2])) / cbar;
	x1 = eta_min - plim * 3. / (element[e].volume * ale_corr_fact);
	x1 -= bedmax;

	// Matrices for the limiting LxF distribution
	if ((x1 > cut_off_U * element[e].cut_off_ratio) && (zero_nodes == 0)) {
		velocity(element[e].cut_off_ratio);
		Eigenvectors(vel);

		for (int i = 0; i < size; i++)
			for (int j = 0; j < size; j++) {
				M1[i][j] = Left[i][j];
				M2[i][j] = Right[i][j];
			}

		factor = 0.5 * (fabs(hubar0) + fabs(hvbar0) + sqrt(gm * height0) * height0 + gridsize * element[e].cut_off_ratio * (fabs(UUinf) + fabs(VVinf)) / L_ref);
		                                                                            //gridsize*height0*( fabs( UUinf ) + fabs( VVinf ) )/L_ref ) ;
		factor = height0 * length / factor;	//SUPG scaling
	} else {
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				M1[i][j] = 0.0;
				M2[i][j] = 0.0;
			}
			M1[i][i] = 1.0;
			M2[i][i] = 1.0;
		}
		factor = 0.;
	}

	// Smoothness sensor using approximate entropy residual
	phi_abs = 0.;
	for (int j = 0; j < size; j++)
        phi_abs += V[j]  * (phi_w[j]);// + element[e].phi[j])* 0.5;
	phi_abs = fabs(phi_abs) + 1.e-15;

	delta = dt * element[e].volume * E * u_ref / (L_ref * phi_abs);
	if (delta >= 1.0)
		delta = 1.0;

	pos = (HHref -  cut_off_U * element[e].cut_off_ratio) / (MAX(cut_off_H * element[e].cut_off_ratio, HHmin - cut_off_U * element[e].cut_off_ratio));
	pos *= 0.1 * gridsize * element[e].cut_off_ratio / L_ref;
	pos = -pos * pos;
	if (pos < log(1.e-15))
		pos = log(1.e-15);

	delta = exp(pos) * delta;
	//delta = 1.0; //-> SUPG scheme    For testing the convergence, otherwise comment!!!!!!
        //delta = 0.0; //-> LLxF scheme

	node[element[e].node[0]].tt += (1. / 3.) * element[e].volume * delta / node[element[e].node[0]].volume;
	node[element[e].node[1]].tt += (1. / 3.) * element[e].volume * delta / node[element[e].node[1]].volume;
	node[element[e].node[2]].tt += (1. / 3.) * element[e].volume * delta / node[element[e].node[2]].volume;

	// LxF scheme
	for (int v = 0; v < 3; v++) {
		int v1 = v;
		int v2 = (v + 1) % 3;
		int v3 = (v + 2) % 3;

		eta1 = W[v1][0] + B[v1];
		eta2 = W[v2][0] + B[v2];
		eta3 = W[v3][0] + B[v3];

		for (int j = 1; j < size; j++)
			diss[j] = dt * alpha_LF * (2. * W[v1][j] - W[v2][j] - W[v3][j]) / 3.;

		// LxF Dissipation in tot.depth var.
		if ((zero_nodes == 0) && (x1 > cut_off_U * element[e].cut_off_ratio)) {
			diss[0] = 2. * eta1 - eta2 - eta3;
			diss[0] *= dt * alpha_LF / 3.;
		} else {
			if ((W[v1][0] - W[v2][0]) * (eta1 - eta2) >= 0.)
				diss[0] = eta1 - eta2;
			else
				diss[0] = W[v1][0] - W[v2][0];

			if ((W[v1][0] - W[v3][0]) * (eta1 - eta3) >= 0.)
				diss[0] += eta1 - eta3;
			else
				diss[0] += W[v1][0] - W[v3][0];

			diss[0] *= dt * alpha_LF / 3.;
		}
		
		for (int j = 0; j < size; j++)
			PHI_d[v][j] = dU[v][j] + dt * phi_a_w1[j] / 3. + diss[j];
		
		// Addition of friction term
		if (1 == numerics_friction_explicit) {
			int n1 = element[e].node[v] ;
			for (int j = 0 ; j < size ; j ++ )
				PHI_d[v][j] += dt*element[e].volume*( node[n1].friction[2][j] )/3. ;
		}
		if (83 == initial_state) {
			int n1 = element[e].node[v] ;
			for (int j = 0 ; j < size ; j ++ )
				PHI_d[v][j] -= dt*element[e].volume*( node[n1].PHI_bar[2][j] )/3. ;
		}
	}

	// Residual in characteristics variables
	for (int k = 0; k < size; k++) {
		work_vector0[k] = 0.;
		PHIN_scal[0][k] = 0.;
		PHIN_scal[1][k] = 0.;
		PHIN_scal[2][k] = 0.;

		for (int l = 0; l < size; l++) {
			work_vector0[k] += M1[k][l] * phi_w[l];
			PHIN_scal[0][k] += M1[k][l] * PHI_d[0][l];
			PHIN_scal[1][k] += M1[k][l] * PHI_d[1][l];
			PHIN_scal[2][k] += M1[k][l] * PHI_d[2][l];
		}
	}
	
	// SUPG scheme          
	if (Mscaling) {
		for (int j = 0; j < size; j++) {
			work_vector2[j] = 0.;
			for (int k = 0; k < size; k++)
				work_vector2[j] += sum_K_1[j][k] * phi_w[k];
		}
	}

	// Limiting Lax-Friederich scheme       
	for (int v = 0; v < 3; v++) {
		for (int j = 0; j < size; j++) {
			work_vector1[j] = 0.;
			if (fabs(work_vector0[j]) > 1.e-15) {
				arg = PHIN_scal[0][j] / work_vector0[j];
				if (arg > 0.)
					betaP = arg + 1.e-15;
				else
					betaP = 1.e-15;

				arg = PHIN_scal[1][j] / work_vector0[j];
				if (arg > 0.)
					betaP += arg + 1.e-15;
				else
					betaP += 1.e-15;

				arg = PHIN_scal[2][j] / work_vector0[j];
				if (arg > 0.)
					betaP += arg + 1.e-15;
				else
					betaP += 1.e-15;

				arg = PHIN_scal[v][j] / work_vector0[j];
				if (arg > 0.)
					betaP = (arg + 1.e-15) / betaP;
				else
					betaP = 1.e-15 / betaP;

				work_vector1[j] = betaP * work_vector0[j];
			}
		}
	
		// Blending LLxF-SUPG
		for (int k = 0; k < size; k++) {
			// This goes to formulation 2 in smooth regions. To use F2 everywhere: set delta=1. To use F1 everywhere: set delta=0
			PHIN_d[v][k] = delta * DV[v][k];
			
			//projection, mass matrix and SUPG stabilization
			if (blending) {
				//Bc
				PHIN_d[v][k] += DDV[v][k] + delta * phi_w[k] / 3.;	//Bc

				for (int l = 0; l < size; l++) {
					// Bc
					if (Mscaling)
						PHIN_d[v][k] += (1. - delta) * M2[k][l] * work_vector1[l] + delta * K_i[v][k][l] * work_vector2[l];	// Bc  +pos - matrix scaling
					else
						PHIN_d[v][k] += (1. - delta) * M2[k][l] * work_vector1[l] + factor * delta * K_i[v][k][l] * phi_w[l] / element[e].volume;	//Bc + pos
				}
			} else {
				//LLF
				PHIN_d[v][k] += DDV[v][k];	// LLFs
				//LLF

				for (int l = 0; l < size; l++) {
					//LLF
					if (Mscaling)
						PHIN_d[v][k] += M2[k][l] * work_vector1[l] + delta * K_i[v][k][l] * work_vector2[l]; // LLFs +pos - matrix scaling
					else
						PHIN_d[v][k] += M2[k][l] * work_vector1[l] + factor * delta * K_i[v][k][l] * phi_w[l] / element[e].volume; // LLFs + pos
					// LLF
				}
			}
		}
	}
    
    /*  post-processing to avoid spurious runup */
    
	n1 = element[e].node[0] ;
	n2 = element[e].node[1] ;
	n3 = element[e].node[2] ;
    
	ndry = 0 ;
	if ( node[n1].P[0][0] < coarse_cut_off ) ndry +=1 ;
	if ( node[n2].P[0][0] < coarse_cut_off ) ndry +=1 ;
	if ( node[n3].P[0][0] < coarse_cut_off ) ndry +=1 ;
    
	if (  ( ndry < 3 ) && ( ndry > 0 ) ){
		if ( ndry == 1 ){

			if ( node[n1].P[0][0]  < coarse_cut_off ){
				nd1 = n1 ; d1 = 0 ;
				nw1 = n2 ; w1 = 1 ;
				nw2 = n3 ; w2 = 2 ;
			} else if ( node[n2].P[0][0]  < coarse_cut_off ){
				nd1 = n2 ; d1 = 1 ;
				nw1 = n1 ; w1 = 0 ;
				nw2 = n3 ; w2 = 2 ;
			} else if ( node[n3].P[0][0]  < coarse_cut_off ){
				nd1 = n3 ; d1 = 2 ;
				nw1 = n1 ; w1 = 0 ;
				nw2 = n2 ; w2 = 1 ;
			}
            
			if ( ( node[nd1].bed[2]  + coarse_cut_off > node[nw1].P[0][0] +  node[nw1].bed[2] ) && ( node[nd1].bed[2] + coarse_cut_off > node[nw2].P[0][0] +  node[nw2].bed[2] ) ){
				for ( int l = 0 ; l < size ; l ++ ){
					PHIN_d[w1][l] += 0.5*PHIN_d[d1][l] ;
					PHIN_d[w2][l] += 0.5*PHIN_d[d1][l] ;
					PHIN_d[d1][l] = 0.0 ;
				}
            		}
		}
        	else if ( ndry == 2 ){
			if ( node[n1].P[0][0]  > coarse_cut_off ){
				nw1 = n1 ; w1 = 0 ;
                		nd1 = n2 ; d1 = 1 ;
               	 		nd2 = n3 ; d2 = 2 ;
            		}else if ( node[n2].P[0][0]  > coarse_cut_off ){
                		nw1 = n2 ; w1 = 1 ;
                		nd1 = n1 ; d1 = 0 ;
                		nd2 = n3 ; d2 = 2 ;
            		} else if ( node[n3].P[0][0]  > coarse_cut_off ){
                		nw1 = n3 ; w1 = 2 ;
                		nd1 = n1 ; d1 = 0 ;
                		nd2 = n2 ; d2 = 1 ;
            		}
            
            		if (  node[nd1].bed[2]+ coarse_cut_off > node[nw1].P[0][0] +  node[nw1].bed[2]  ){
                		for ( int l = 0 ; l < size ; l ++ ){
                    			PHIN_d[w1][l] += PHIN_d[d1][l] ;
                    			PHIN_d[d1][l] = 0.0 ;
                		}
            		}
            		if (  node[nd2].bed[2]  + coarse_cut_off > node[nw1].P[0][0] +  node[nw1].bed[2] ){
                		for ( int l = 0 ; l < size ; l ++ ){
                    			PHIN_d[w1][l] += PHIN_d[d2][l] ;
                    			PHIN_d[d2][l] = 0.0 ;
                		}
            		}
        	} else{
            		printf("WARNING WARNING: post-treatment inconsistency !!!!!!  %d\n",ndry) ;
        	}
        
	}
}
