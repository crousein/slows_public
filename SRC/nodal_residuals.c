/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/
	


/***************************************************************************
nodal_residuals.c
-----------------
nodal_residuals computes the nodal resusuals in pseudo time by looping over
all the elements of the spatial mesh
-------------------
***************************************************************************/
#include <stdio.h>
#include <math.h>
#include "common.h"
#include "my_malloc.h"
extern const int size;
extern int NE, NN, NBF, zero_nodes, blending;
extern struct element_struct *element;
extern struct node_struct *node;
extern struct node_struct *node;
extern struct boundary_nodes_struct *b_nodes;
extern struct boundary_struct *boundary;
extern double **PHIN_d, *phi_node;
extern double *phi_w;
extern double shoreline_flux, temp_scalar;
extern void (*compute_shoreflux) (int);
extern void (*STfluctuation) (int);
extern void (*distribute_fluctuation) (int);
extern void (*compute_upwind_Keys) (int);
extern void (*local_average) (int, int);

void initvec(double *);
void compute_max_Htot0(void);
void residual_update(int);

void nodal_residuals(void)
{
  	/********************************************/
  	/** initialization of flags and residuals  **/
 	/********************************************/

	// \int_{DRY} div(b\sigma) = -\int_{SHORE} b\sigma n_{shore}  
	shoreline_flux = 0.0;
	for (int n = 0; n < NN; n++) {
		node[n].flag = 0;
		node[n].dtau = infinity;
		node[n].negative = 0;
		node[n].sum_alpha = 0;
		initvec(node[n].Res);
		node[n].tt = 0.;
		node[n].fully_dry = 1;
		node[n].max_Htot0 = -10.;
	}
	//printf("all zero \n") ;

	/* main loop over the elements */
	compute_max_Htot0();

  	/********************************************/
	/**       main loop over the elements      **/
	/********************************************/

	for (int e = 0; e < NE; e++) {

		/* Initialize distribution */
		for (int v = 0; v < 3; v++)
			for (int i = 0; i < size; i++)
				PHIN_d[v][i] = 0.;

		local_average(e, 2);

		if (3 == zero_nodes) { /* Dry-cell treatment */
			// cell residual = 0 
			for (int v = 0; v < 3; v++) {
				for (int i = 0; i < size; i++) {
					PHIN_d[v][i] = 0.;
					phi_w[i] = 0.;
				}
			}
			// shoreline flux correction to mantain an analogy with a conservative scheme
			compute_shoreflux(e);
		} else { /* Compute & Distribute fluctuation */
			temp_scalar = 0.0;
			for (int v = 0; v < 3; v++)
				node[element[e].node[v]].fully_dry = 0;

			if (blending<2) 
				compute_upwind_Keys(e); // Jacobian matrix for the upwinding

			if (NULL != STfluctuation)
				STfluctuation(e); // computation of the residuals (RD)
			
			if (blending < 2) {
				if (fabs(phi_w[0]) + fabs(phi_w[1]) + fabs(phi_w[2]) > 1.e-15)
					distribute_fluctuation(e); // (RD case) distributing the residuals computed in STfluctuation( e )
			} else {
				distribute_fluctuation(e); // (FV case) computation of the fluxes: distribute_FV_fluctuation(e)
			}

			shoreline_flux += temp_scalar;
		}

		
		for (int v = 0; v < 3; v++) {
			for (int i = 0; i < size; i++){
				phi_node[i] = PHIN_d[v][i];
			}
			residual_update(element[e].node[v]); // (both schemes) summation of the fluxes on the cells which contain node "v"
		}
		


		for ( int i = 0 ; i < size ; i++ ) 
			element[e].phi[i] = phi_w[i];

	}

}
