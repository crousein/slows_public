/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

/***************************************************************************
                               elliptic_interface.c
                                  -------
This is a file which contains the interface between the hyperbolic and the elliptic parts.
***************************************************************************/
#ifdef WITH_NHT
#include <stdio.h>
#include <math.h> // sqrt, cosh, tanh
#include "common.h"
#include "nht.h"
#include "sparse.h"
#include "my_malloc.h"
extern int NE, NN, NBF, NNZ, blending, time_step, initial_state, model_breaking, volume_q_pts, size, grad_reco;
extern double Time, dt, gm, cut_off_H;
extern struct node_struct *node;
extern struct element_struct *element;
extern struct boundary_nodes_struct *b_nodes;
extern struct numerical_int_struct numerical_int;
extern double brk_param[5];
extern double cut_off_H, cut_off_U, coarse_cut_off;
extern const int vertex;
extern char out_file[MAX_CHAR];
extern double *nht_h; // used to pass h to the non hydro lib
extern double *nht_u; // used to pass u to the non hydro lib
extern double *nht_v; // used to pass v to the non hydro lib
extern double *nht_eta; // used to pass eta to the non hydro lib
extern double *nht_bed; // used to exchange nht with the non hydro lib
extern double *nht_dxh; // used to pass h_x to the non hydro lib
extern double *nht_dxhu; // used to pass (hu)_x to the non hydro lib
extern double *nht_dxhv; // used to pass (hv)_x to the non hydro lib
extern double *nht_dxeta; // used to pass eta_x to the non hydro lib
extern double *nht_dxbed; 
extern double *nht_dyh; // used to pass h_y to the non hydro lib
extern double *nht_dyhu; // used to pass (hu)_y to the non hydro lib
extern double *nht_dyhv; // used to pass (hv)_y to the non hydro lib
extern double *nht_dyeta; // used to pass eta_y to the non hydro lib
extern double *nht_dybed; 
extern double *nht_phi; // nonHydro->output
extern double *nht_k;
extern double *nht_e;
extern int *nht_bb;
extern double *FVMM;
extern int *IFVMM;
extern int *JFVMM;


//gradient reconstruction for the computation of the term gradient of the divergence of the momentum, for the Turbulence eddy viscosity 
void tur_gradient_reconstruction()
{
	double dxtur[NN],dytur[NN];
        for (int n = 0; n < NN; n++){
                node[n].flag = 0;
		dxtur[n]=0;
		dytur[n]=0;
        }

        for (int e = 0; e < NE; e++) {
                double max_Htot = -10.;
                int zero_nodes = 0;

                for (int i = 0; i < vertex; i++) {
                        int m = element[e].node[i];
                        if (node[m].P[2][0]!=0.) {
                                if (node[m].P[2][0] > max_Htot)
                                        max_Htot = node[m].P[2][0];
                        } else {
                                zero_nodes++;
                        }
                }

                if(3 != zero_nodes) {
                        // we reconstruct the gradient of the quantity w on the element
                        double dxw1 = 0., dyw2 = 0.;
                        for (int v = 0; v < vertex; v++) {
                                double nx = element[e].normal[v][0];
                                double ny = element[e].normal[v][1];
                                int n = element[e].node[v];
                                dxw1 += node[n].tur[0] * nx * 0.5 / element[e].volume;
                                dyw2 += node[n].tur[1] * ny * 0.5 / element[e].volume;
                        }

                        for (int v = 0; v < vertex; v++) {
                                int n = element[e].node[v];
                                double kdt = 1. / node[n].volume;
                                dxtur[n] += kdt * element[e].volume / 3. * dxw1;
                                dytur[n] += kdt * element[e].volume / 3. * dyw2;
                        }
                }
        }
        for (int f = 0; f < NBF; f++) {
                int n0 = b_nodes[f].node;
                if(b_nodes[f].type==3){//Correction for wall b.c.
                        double length = sqrt(pow(b_nodes[f].normal[0], 2) + pow(b_nodes[f].normal[1], 2));
                        dxtur[n0] -= (dxtur[n0] * b_nodes[f].normal[0] + dytur[n0] * b_nodes[f].normal[1]) * b_nodes[f].normal[0] / pow(length, 2);
                        dytur[n0] -= (dxtur[n0] * b_nodes[f].normal[0] + dytur[n0] * b_nodes[f].normal[1]) * b_nodes[f].normal[1] / pow(length, 2);
                }else if (b_nodes[f].type == 10) {//Correction for periodic b.c.
                        if(node[n0].flag == 0) {
                                int mate = node[n0].mate ;
                                dxtur[n0] += dxtur[mate];
                                dxtur[mate] = dxtur[n0];
                                dytur[n0] += dytur[mate];
                                dytur[mate] = dytur[n0];
                                node[mate].flag = 1;
                                node[n0].flag = 1;
                        }
                }
        }
	
	for(int n=0; n<NN; n++){
		node[n].tur[0]=dxtur[n];
		node[n].tur[1]=dytur[n];
	}
		
}

// Manufactured solution for a GN solitary wave in a SW code
double rest_msSW(double x, int itr)
{
	double time = 0.;
	if (itr == 0)
		time = 0.;
	else if (itr == 1)
		time = Time-dt;
	else if (itr == 2)
		time = Time;
	else if (itr == 3)
		time = Time-dt/2.0;

	double a = .2;
	double d = 1.;
	double g = 9.81;
	double k = sqrt(3.*a)/(2.*d*sqrt(d + a));
	double c = sqrt(g*(d + a));
	double x0 = 35.;
	double X = x - x0 - c*time;
	double sech = 1/cosh(k*X);
	double eta = a*sech*sech;
	double eta_x = -2*a*k*sech*sech*tanh(k*X);
	double h = d + eta;
	double h_x = eta_x;
	double q = c*eta;
	double q_x = c*eta_x;
	double u = q/h;
	double u_x = q_x/h - q*h_x/(h*h);
	double q_t = - c*q_x;
	double PHI = q_t + u*q_x + u_x*q + g*h*eta_x;
	return PHI;
}

void pass_the_values( void )
{
#pragma omp parallel for
	for (int n = 0; n < NN; n++) {
		nht_h[ n ] = node[ n ].Z[ 2 ][ 0 ]; 
		nht_u[ n ] = node[ n ].Z[ 2 ][ 1 ];
		nht_v[ n ] = node[ n ].Z[ 2 ][ 2 ]; 
		
		nht_eta[ n ] = node[ n ].Z[ 2 ][ 0 ] + node[ n ].bed[ 2 ];

		nht_dxh[ n ] = node[ n ].dxP[ 0 ][ 0 ]; 
		nht_dxhu[ n ] = node[ n ].dxP[ 0 ][ 1 ];
		nht_dxhv[ n ] = node[ n ].dxP[ 0 ][ 2 ];
		nht_dyh[ n ] = node[ n ].dyP[ 0 ][ 0 ]; 
		nht_dyhu[ n ] = node[ n ].dyP[ 0 ][ 1 ];
		nht_dyhv[ n ] = node[ n ].dyP[ 0 ][ 2 ];

		nht_dxeta[ n ] = node[n].dxeta; //node[ n ].dxP[ 0 ][ 0 ] + node[ n ].dxbed[0];  //for the moment it is h
		nht_dyeta[ n ] = node[n].dyeta; //node[ n ].dyP[ 0 ][ 0 ] + node[ n ].dybed[0];  //for the moment it is h
		//nht_dxeta[ n ] = node[ n ].dxP[ 0 ][ 0 ] + node[ n ].dxbed[0];  //for the moment it is h
		//nht_dyeta[ n ] = node[ n ].dyP[ 0 ][ 0 ] + node[ n ].dybed[0];  //for the moment it is h

		if( nht_h[ n ] <= 0. ){ 
			nht_eta[ n ] = 0.0;
			nht_h[ n ] = 0.0;
			nht_u[n]=0.0;
			nht_v[n]=0.0;
		}
	}
}

void get_PHI_bar( void )
{
	// Compute PHI_{corr} for GN to add to GNTD code
	double tmp_PHI[2*NN];
	//double tmpin[2*NN];
	//double tmpout[2*NN];
	int flagr;
	int flag=8;

#pragma omp parallel for
	for (int n = 0; n < NN; n++) {
		//if( (model_breaking == 1 && nht_bb[n] == 1) || node[n].dry_flag2==1 || node[n].dry_flag3==1 || node[n].dry_flag4==1) {
		//if( (model_breaking == 1 && nht_bb[n] == 1) ) {
		if( (model_breaking == 1 && nht_bb[n] == 1) || node[n].dry_flag2==1 || node[n].dry_flag3==1 ) {
			tmp_PHI[n] = 0.0;
			tmp_PHI[n+NN] =0.0;
		}else{
			tmp_PHI[n] = nht_phi[ n ]; //nonHydro.output[n];
			tmp_PHI[n+NN] = nht_phi[ n + NN ]; //nonHydro.output[n+NN];
		}

		if (78 == initial_state){
 			 double delta=0.5;
			 double li = 2.1;
			 double wsize= delta*li/2.0;
			 double x=node[n].coordinate[0];
			 double Xs=22.0;
                	//if(x>=Xs-wsize && x<= Xs+wsize ){
			//	nht_phi[n+NN] = 0.;
			//}
		}
//		if (78 ==initial_state && node[n].coordinate[0]<=5){ //wave over a bar
//			tmp_PHI[n] = nht_phi[ n ]; // nonHydro.output[n];  //Do not activate breaking inside the wave generation region
//			tmp_PHI[n+NN] = nht_phi[ n + NN ]; // nonHydro.output[n+NN];
//			nht_bb[ n ] = 0;
//		}
//                if(Time < 0.1) {
//                      tmp_PHI[n]=0.0;
//                      tmp_PHI[n+NN]=0.0;
//		}
	}
#pragma omp parallel for
	for (int n = 0; n < NN; n++) {
		node[n].PHI_bar[0][0] = 0.; // for the continuity equation
		node[n].PHI_bar[0][1] = tmp_PHI[n];
		node[n].PHI_bar[0][2] = tmp_PHI[n+NN];
		node[n].PHI_bar[2][0] = 0.; // for the continuity equation
		node[n].PHI_bar[2][1] = 0.;
		node[n].PHI_bar[2][2] = 0.;
	}
	
#pragma omp parallel for
	for (int e = 0; e < NE; e++) {
		int n0 = element[e].node[0];
		int n1 = element[e].node[1];
		int n2 = element[e].node[2];
		double PHIg[2] = {0., 0.};
		PHIg[0] = 1./3.*(tmp_PHI[n0] + tmp_PHI[n1] + tmp_PHI[n2]);
		PHIg[1] = 1./3.*(tmp_PHI[n0+NN] + tmp_PHI[n1+NN] + tmp_PHI[n2+NN]);
		for (int i = 0; i < vertex-1; i++) {
			int ni = element[e].node[i];
			double PHIi[2] = {0., 0.};
			PHIi[0] = tmp_PHI[ni];
			PHIi[1] = tmp_PHI[ni+NN];
			for (int j = i+1; j < vertex; j++) {
				int nj = element[e].node[j];
				double PHIj[2] = {0., 0.};
				PHIj[0] = tmp_PHI[nj];
				PHIj[1] = tmp_PHI[nj+NN];
				double PHIm[2] = {0., 0.};
				PHIm[0] = 1./2.*(tmp_PHI[ni] + tmp_PHI[nj]);
				PHIm[1] = 1./2.*(tmp_PHI[ni+NN] + tmp_PHI[nj+NN]);
				for (int q = 0; q < volume_q_pts; q++) {
					double PHIq[2] = {0., 0.};
					PHIq[0] = PHIi[0]*numerical_int.volume_coordinate[q][0] + PHIg[0]*numerical_int.volume_coordinate[q][1] + PHIm[0]*numerical_int.volume_coordinate[q][2];
					PHIq[1] = PHIi[1]*numerical_int.volume_coordinate[q][0] + PHIg[1]*numerical_int.volume_coordinate[q][1] + PHIm[1]*numerical_int.volume_coordinate[q][2];
					for (int l = 1; l < size; l++) {
						double result = (element[e].volume / 6.0 * numerical_int.volume_weight[q] * PHIq[l-1])/node[ni].volume;
						if(grad_reco==1 && node[ni].flag_boundary[2]==20)
							result = (element[e].volume / 6.0 * numerical_int.volume_weight[q] * PHIq[l-1])/(2.0*node[ni].volume);
#pragma omp atomic
						node[ni].PHI_bar[2][l] += result;
					}

					PHIq[0] = PHIj[0]*numerical_int.volume_coordinate[q][0] + PHIg[0]*numerical_int.volume_coordinate[q][1] + PHIm[0]*numerical_int.volume_coordinate[q][2];
					PHIq[1] = PHIj[1]*numerical_int.volume_coordinate[q][0] + PHIg[1]*numerical_int.volume_coordinate[q][1] + PHIm[1]*numerical_int.volume_coordinate[q][2];
					for (int l = 1; l < size; l++){
						double result = (element[e].volume / 6.0 * numerical_int.volume_weight[q] * PHIq[l-1])/node[nj].volume;
						if(grad_reco==1 && node[nj].flag_boundary[2]==20)
							result = (element[e].volume / 6.0 * numerical_int.volume_weight[q] * PHIq[l-1])/(2.0*node[nj].volume);
#pragma omp atomic
						node[nj].PHI_bar[2][l] += result;
					}
				}
			}
		}
	}
//        double *tmpin = MA_vector(2*NN, sizeof(double));
//        double *tmpout = MA_vector(2*NN, sizeof(double));
//
//	for (int n = 0; n < NN; n++) {
//		tmpin[n]=node[n].PHI_bar[2][1];
//		tmpin[n+NN]=node[n].PHI_bar[2][2];
//	}
//	matrixvec_prod_coo( 2*NN, 2*NNZ, FVMM, IFVMM, JFVMM, tmpin, tmpout);

#pragma omp parallel for
	for (int n = 0; n < NN; n++) {
		node[n].PHI_bar[1][0] = 0.;
		node[n].PHI_bar[1][1] = dt*node[n].PHI_bar[2][1];
		node[n].PHI_bar[1][2] = dt*node[n].PHI_bar[2][2];
//		node[n].PHI_bar[1][1] = dt*tmpout[n];
//		node[n].PHI_bar[1][2] = dt*tmpout[n+NN];
//		node[n].PHI_bar[1][1] = dt*tmp_PHI[n];
//		node[n].PHI_bar[1][2] = dt*tmp_PHI[n+NN];
		if (71 == initial_state){
			if ( node[n].coordinate[0]<0.91 || node[n].coordinate[0]>1.79){
				node[n].PHI_bar[1][0] = 0.;
				node[n].PHI_bar[1][1] = 0.;
				node[n].PHI_bar[1][2] = 0.;
			}
		}
		if (24 == initial_state){
			if ( node[n].coordinate[0]>40.0 ){
				node[n].PHI_bar[1][0] = 0.;
				node[n].PHI_bar[1][1] = 0.;
				node[n].PHI_bar[1][2] = 0.;
			}
		}
		if (78 == initial_state){
 			 double delta=0.5;
			 double li = 2.1;
			 double wsize= delta*li/2.0;
			 double x=node[n].coordinate[0];
			 double Xs=22.0;
                	//if(x>=Xs-wsize && x<= Xs+wsize ){
		//		node[n].PHI_bar[1][2] = 0.;
		 //               node[n].PHI_bar[2][2] = 0.;
			//}
		}
		if(82==initial_state){
			double li = 1.4941;;
			double Xs=6.0; //generation zone position
			double x = node[n].coordinate[0] ;
			double delta = 0.8;// generation zone semi-amplitude
			double wsize= delta*li/2.0;
			if(x>=Xs-wsize && x<= Xs+wsize ) {
			//	node[n].PHI_bar[1][2] = 0.0;

			}
		}
		

	}
//	MA_free(tmpin);
//	MA_free(tmpout);

        /*if (84 == initial_state){
                for (int e = 0; e < NE; e++) {
                        int n1 = element[e].node[0];
                        int n2 = element[e].node[1];
                        int n3 = element[e].node[2];
                        if ( node[n1].P[2][0] < cut_off_H || node[n2].P[2][0] < cut_off_H || node[n3].P[2][0]< cut_off_H){
                                node[n1].PHI_bar[1][0] = 0.;
                                node[n1].PHI_bar[1][1] = 0.;
                                node[n1].PHI_bar[1][2] = 0.;
                                node[n2].PHI_bar[1][0] = 0.;
                                node[n2].PHI_bar[1][1] = 0.;
                                node[n2].PHI_bar[1][2] = 0.;
                                node[n3].PHI_bar[1][0] = 0.;
                                node[n3].PHI_bar[1][1] = 0.;
                                node[n3].PHI_bar[1][2] = 0.;
                        }
                }

        }*/
        if (84 == initial_state){
#pragma omp parallel for
		for( int n = 0; n < NN; n++){
			if(0.-node[n].bed[2] <= 0.) {
				node[n].PHI_bar[1][0]=0.;
				node[n].PHI_bar[1][1]=0.;
				node[n].PHI_bar[1][2]=0.;
				nht_bb[n]=1;
			}				
		}
	}
	

	if (2 == model_breaking){
#pragma omp parallel for
		for(int n=0; n<NN; n++){
			node[n].tur[0]= sqrt( nht_k[n])*brk_param[4]*(node[n].dxP[0][1]+node[n].dyP[0][2]); // v_t * grad . Hu
			node[n].tur[1]= sqrt( nht_k[n])*brk_param[4]*(node[n].dxP[0][1]+node[n].dyP[0][2]);
		}
 		tur_gradient_reconstruction();

#pragma omp parallel for
		for(int n=0; n<NN; n++){
			node[n].PHI_bar[1][0] = 0.;
			node[n].PHI_bar[1][1] += dt*node[n].tur[0];
			node[n].PHI_bar[1][2] += dt*node[n].tur[1];
		}
	}

// Computation of the total energy for the GN equations
	double energy=0.;
#pragma omp parallel for reduction(+: energy)
	for(int e=0; e<NE; e++){
		energy += element[e].volume/3. * (nht_e[element[e].node[0]] + nht_e[element[e].node[1]] + nht_e[element[e].node[2]]);
	}

        char tecplot_file_name[MAX_CHAR];
        sprintf(tecplot_file_name, "../output/%s_energy.dat", out_file);

        FILE* fp = fopen(tecplot_file_name, "a");
	fprintf(fp,"%le %le\n",Time,energy);
	fclose(fp);


}
#endif
