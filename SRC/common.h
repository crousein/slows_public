/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

/***************************************************************************
                                  common.h
                                  -------
This is the common: it defines the data structures used in the computations
                             and some constants.
***************************************************************************/
#define MAX(a,b) ((a)>(b) ? (a) : (b))
#define MIN(a,b) ((a)<(b) ? (a) : (b))

#define pi  3.14159265358979
#define MAX_CHAR 150
#define infinity ((double)(1.e+50))
#define epsilon ((double)(1.e-14))
#define small ((double )(1.e-12))
#define SIGN(a)  ((a) < 0. ? -1. : 1.)
#define MINMOD(a) ( 0.5*( 1.0 + SIGN((a)) )*MIN(1,(a)) )
#define false 0
#define true 1
// The crap above is problematic when included in c++ code.
// Since all that are really needed are the struct definitions,
// they are moved in a "clean" .hpp file to be included in c++ sources 
// while common.h can continue to be used as it has always been in c sources
#include "common.hpp"
