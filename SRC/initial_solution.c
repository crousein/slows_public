/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/
	

/***************************************************************************
 initial_solution.c
 ------------------
 This is initial_solution: depending on the value of initial_state, it reads
 or just sets up the initial solution for the computation
 --------------------
 ***************************************************************************/
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h> // exit
#include "common.h"
#include "my_malloc.h"

extern const int size, vertex;
extern int initial_state, NE, NN, NBF, scheme, time_bc_pts, blending, volume_q_pts;
extern int grad_reco;
extern char initial_solution_file[MAX_CHAR];
double HHref;
extern double gm, cut_off_H, manning, UUinf, VVinf, cut_off_U;
extern double *h_bc, *time_bc, *temp_vector, *work_vector1;
double *a_wg, *t_wg, *l_wg, *erand, *direction;
int freq,angle;
extern struct node_struct *node;
extern struct element_struct *element;
extern struct boundary_struct *boundary;
extern struct boundary_nodes_struct *b_nodes;
extern struct node_struct *node;
extern struct numerical_int_struct numerical_int;
extern void (*bathy_quadrature) (void);

void read_solution_dat(void);
void P_2_Z(double *, double *, double);
void Z_2_P(double *, double *, double);
void Z_2_K(double *, double *, double, double);
void K_2_Z(double *, double *, double, double, double);
void initial_solution();
void friction(int, int);
void integration_dualcell(void);

/* Initializing bathymetry and initial conditions */
void initial_conditions(void)
{
	bathy_quadrature();
	initial_solution();
}

double soliton_GNeta(double x, double a, double d)
{
	//double g = 9.81;
	double k = sqrt(3.*a)/(2.*d*sqrt(d + a));
	double x0 = 35.;
	double X = x - x0;
	double sech = 1./cosh(k*X);
	double eta = a*sech*sech;
	return eta;
}

void analytic_ini(double x, double y, double t, double *VAR)
{
	double hex=0.;
	double uex=0.;
	if(91 == initial_state) {
        	//hex = x;
		double xc = x;
		double yc = y;
		double alpha = 0.2;
		double h0 = 1.0 ;
		double gamma=sqrt(3.0*alpha/(4.0*h0));
		double xs = 35.;//19.85 + (1.0/gamma)*2.178327221030376 + 20.;
		double d2 = alpha+h0;
		double xx=0.5*(xc-xs)*sqrt(3.*alpha/(d2*pow(h0,2)));
		double c = sqrt( 9.81 *d2 ) ;
		double sech = 1./(cosh(xx) ) ;
		double etai = alpha *sech*sech;
		hex = etai+h0;// +h0;
		uex = c*(1.-h0/ hex); 
        	//uex = 0.0;

        	VAR[0] = hex;
        	VAR[1] = uex;
        	VAR[2] = 0.;
	//}else if(24 == initial_state || 83 == initial_state || 70 == initial_state || 93 == initial_state ||82==initial_state){
	}else if(24 == initial_state || 83 == initial_state || 70 == initial_state || 93 == initial_state ){
		double xc = x;
		double yc = y;
		double alpha = 0.2;//0.39;//0.0375;//0.2;
		double h0 = 1.0;//0.78;//0.15;//1.0 ;
		double gamma=sqrt(3.0*alpha/(4.0*h0));
		double xs =35.0;//4.0;//35.;
		double d2 = alpha+h0;
		double c = sqrt( 9.81 *d2 ) ;
		double xx=0.5*(xc-xs-c*t)*sqrt(3.*alpha/(d2*pow(h0,2)));
		double sech = 1./(cosh(xx) ) ;
		double etai = alpha *sech*sech;
		hex = etai;// +h0;
		uex = c*(1.-h0/ (etai+h0));

        	VAR[0] = hex; 
        	VAR[1] = uex;
        	VAR[2] = 0.;
        	//VAR[0] = hex;
        	//VAR[1] = 0.;
        	//VAR[2] = uex;
	}else if( 76 == initial_state){
		double xc = x;
		double yc = y;
		double alpha = 0.28;
		double h0 = 1.0;
		double gamma=sqrt(3.0*alpha/(4.0*h0));
                double xs = 19.85 + (1.0/gamma)*2.178327221030376+ 20.;
		double d2 = alpha+h0;
		double c = sqrt( 9.81 *d2 ) ;
		double xx=0.5*(xc-xs-c*t)*sqrt(3.*alpha/(d2*pow(h0,2)));
		double sech = 1./(cosh(xx) ) ;
		double etai = alpha *sech*sech;
		hex = etai;// +h0;
		uex = -c*(1.-h0/ (etai+h0));

        	VAR[0] = hex; 
        	VAR[1] = uex;
        	VAR[2] = 0.;
	}else if(13 == initial_state || 60 == initial_state){	
        	double xc = x - 0.5 - 6.0*t;
       		double yc = y - 0.5;
        	double r = sqrt(xc * xc + yc * yc);
        	double rho = 4. * pi * r;
        	double omega = 15. * (1. + cos(rho));
        	double pm = 10.;
        	double dp = (225. / (gm * 16. * pi * pi)) * (2. * cos(rho) + 2. * rho * sin(rho) + cos(2. * rho) / 8. + rho * sin(2. * rho) / 4. + rho * rho * 12. / 16.);
        	dp -= (225. / (gm * 16. * pi * pi)) * (2. * cos(pi) + 2. * pi * sin(pi) + cos(2. * pi) / 8. + pi * sin(2. * pi) / 4. + pi * pi * 12. / 16.);

        	if (r >= 0.25) {
                	omega = 0.;
                	dp = 0.;
        	}
	
        	VAR[0] = pm + dp;
        	VAR[1] = (6. - omega * yc) * VAR[0];
        	VAR[2] = (omega * xc) * VAR[0];
	}else if(78 == initial_state || 81 == initial_state){	//Lake at rest
        	VAR[0] = 0.;
        	VAR[1] = 0.;
        	VAR[2] = 0.;

	}

}
double soliton_GNu(double eta, double a, double d)
{
	double g = 9.81;
	double c = sqrt(g*(d + a));
	double h = d + eta;
	double u = c*(1.0-d/h); 
	return u;
}

void soliton_integral(int e, double a, double d)
{
        int n0=element[e].node[0];
        int n1=element[e].node[1];
        int n2=element[e].node[2];
	    
	// First method:
/*	double PHIn0[2] = {0};	
	double PHIn1[2] = {0};	
	double PHIn2[2] = {0};	
	PHIn0[0] = soliton_GNeta(node[n0].coordinate[0],a,d);	   
	PHIn1[0] = soliton_GNeta(node[n1].coordinate[0],a,d);	   
	PHIn2[0] = soliton_GNeta(node[n2].coordinate[0],a,d);	   
	PHIn0[1] = soliton_GNu(PHIn0[0],a,d);	   
	PHIn1[1] = soliton_GNu(PHIn1[0],a,d);	   
	PHIn2[1] = soliton_GNu(PHIn2[0],a,d);	   
            
	double tmp0_eta = (element[e].volume/18.0)*(22*PHIn0[0] + 7.*PHIn1[0] + 7.*PHIn2[0])/6.0;
        double tmp0_u = (element[e].volume/18.0)*(22*PHIn0[1] + 7.*PHIn1[1] + 7.*PHIn2[1])/6.0;
        double tmp1_eta = (element[e].volume/18.0)*(22*PHIn1[0] + 7.*PHIn0[0] + 7.*PHIn2[0])/6.0;
        double tmp1_u = (element[e].volume/18.0)*(22*PHIn1[1] + 7.*PHIn0[1] + 7.*PHIn2[1])/6.0;
	double tmp2_eta = (element[e].volume/18.0)*(22*PHIn2[0] + 7.*PHIn1[0] + 7.*PHIn0[0])/6.0;
        double tmp2_u = (element[e].volume/18.0)*(22*PHIn2[1] + 7.*PHIn1[1] + 7.*PHIn0[1])/6.0;
*/

	// Second method:		
	double xg = element[e].cg[0];
	double xm12 = element[e].mp[2][0];
	double xm01 = element[e].mp[0][0];
	double xm02 = element[e].mp[1][0];
	double x010 = 1./2.*(node[n0].coordinate[0] + xm01);
	double x011 = 1./2.*(node[n1].coordinate[0] + xm01);
	double x020 = 1./2.*(node[n0].coordinate[0] + xm02);
	double x022 = 1./2.*(node[n2].coordinate[0] + xm02);
	double x121 = 1./2.*(node[n1].coordinate[0] + xm12);
	double x122 = 1./2.*(node[n2].coordinate[0] + xm12);
	double x0g = 1./2.*(node[n0].coordinate[0] + xg);
	double x1g = 1./2.*(node[n1].coordinate[0] + xg);
	double x2g = 1./2.*(node[n2].coordinate[0] + xg);
	double x01g = 1./2.*(xm01 + xg);
	double x02g = 1./2.*(xm02 + xg);
	double x12g = 1./2.*(xm12 + xg);
	
	double PHI020[2] = {0};	
	double PHI022[2] = {0};	
	double PHI121[2] = {0};	
	double PHI122[2] = {0};	
	double PHI010[2] = {0};	
	double PHI011[2] = {0};	
	double PHI01g[2] = {0};	
	double PHI0g[2] = {0};	
	double PHI02g[2] = {0};	
	double PHI2g[2] = {0};	
	double PHI12g[2] = {0};	
	double PHI1g[2] = {0};	
	PHI010[0] = soliton_GNeta(x010,a,d);	   
	PHI011[0] = soliton_GNeta(x011,a,d);	   
	PHI020[0] = soliton_GNeta(x020,a,d);	   
	PHI022[0] = soliton_GNeta(x022,a,d);	   
	PHI121[0] = soliton_GNeta(x121,a,d);	   
	PHI122[0] = soliton_GNeta(x122,a,d);	   
	PHI01g[0] = soliton_GNeta(x01g,a,d);	   
	PHI02g[0] = soliton_GNeta(x02g,a,d);	   
	PHI12g[0] = soliton_GNeta(x12g,a,d);	   
	PHI0g[0] = soliton_GNeta(x0g,a,d);	   
	PHI1g[0] = soliton_GNeta(x1g,a,d);	   
	PHI2g[0] = soliton_GNeta(x2g,a,d);	   
	PHI010[1] = soliton_GNu(PHI010[0],a,d);	   
	PHI011[1] = soliton_GNu(PHI011[0],a,d);	   
	PHI020[1] = soliton_GNu(PHI020[0],a,d);	   
	PHI022[1] = soliton_GNu(PHI022[0],a,d);	   
	PHI121[1] = soliton_GNu(PHI121[0],a,d);	   
	PHI122[1] = soliton_GNu(PHI122[0],a,d);	   
	PHI01g[1] = soliton_GNu(PHI01g[0],a,d);	   
	PHI02g[1] = soliton_GNu(PHI02g[0],a,d);	   
	PHI12g[1] = soliton_GNu(PHI12g[0],a,d);	   
	PHI0g[1] = soliton_GNu(PHI0g[0],a,d);	   
	PHI1g[1] = soliton_GNu(PHI1g[0],a,d);	   
	PHI2g[1] = soliton_GNu(PHI2g[0],a,d);	   
        
	double tmp0_eta = (element[e].volume/18.0)*(PHI020[0] + PHI02g[0] + 2*PHI0g[0] + PHI010[0] + PHI01g[0]);
        double tmp0_u = (element[e].volume/18.0)*(PHI020[1] + PHI02g[1] + 2*PHI0g[1] + PHI010[1] + PHI01g[1]);
        double tmp1_eta = (element[e].volume/18.0)*(PHI011[0] + PHI01g[0] + 2*PHI1g[0] + PHI121[0] + PHI12g[0]);
        double tmp1_u = (element[e].volume/18.0)*(PHI011[1] + PHI01g[1] + 2*PHI1g[1] + PHI121[1] + PHI12g[1]);
        double tmp2_eta = (element[e].volume/18.0)*(PHI022[0] + PHI02g[0] + 2*PHI2g[0] + PHI122[0] + PHI12g[0]);
        double tmp2_u = (element[e].volume/18.0)*(PHI022[1] + PHI02g[1] + 2*PHI2g[1] + PHI122[1] + PHI12g[1]);

	node[n0].cell_ave[0] += tmp0_eta/node[n0].mod_volume;
        node[n0].cell_ave[1] += tmp0_u/node[n0].mod_volume;
        node[n1].cell_ave[0] += tmp1_eta/node[n1].mod_volume;
        node[n1].cell_ave[1] += tmp1_u/node[n1].mod_volume;
        node[n2].cell_ave[0] += tmp2_eta/node[n2].mod_volume;
        node[n2].cell_ave[1] += tmp2_u/node[n2].mod_volume;

}

void initial_solution(void)
{
	if (strcmp(initial_solution_file, "NULL")) {
		read_solution_dat();
		for (int n = 0; n < NN; n++) {
			P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio);

			if (scheme > 0)
				Z_2_K(node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio);
			else
				for (int i = 0; i < size; i++)
					node[n].K[2][i] = node[n].Z[2][i];

			friction(n, 2);
		}
	
		if(84 == initial_state) {
				FILE* inletfile = fopen("../input/Forcing.dat", "r");
				for (int i = 0; i < time_bc_pts; i++)
					fscanf(inletfile, "%le  %le\n", &time_bc[i], &h_bc[i]);
				fclose(inletfile);
		}	

	} else
		if (1 == initial_state) { // Wedge
			double p1 = 1.;
			double u1 = 2.74 * sqrt(gm);
			double v1 = 0.0;
			double U0 = p1;
			double U1 = U0 * u1;
			double U2 = U0 * v1;

			for (int n = 0; n < NN; n++) {
				node[n].P[2][0] = U0;
				node[n].P[2][1] = U1;
				node[n].P[2][2] = U2;

				if (node[n].coordinate[0] - 1. > 2. * node[n].coordinate[1])
					node[n].P[2][1] = 0.;

				P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio);

				if (scheme > 0)
					Z_2_K(node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio);
				else
					for (int i = 0; i < size; i++)
						node[n].K[2][i] = node[n].Z[2][i];

				friction(n, 2);
			}
		} else if (2 == initial_state) { // 1D flow draining
			for (int n = 0; n < NN; n++) {
				node[n].P[2][0] = 0.5;
				node[n].P[2][0] -= node[n].bed[2];
				node[n].P[2][1] = 0.;
				node[n].P[2][2] = 0.0;

				P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio);

				if (scheme > 0)
					Z_2_K(node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio);
				else
					for (int i = 0; i < size; i++)
						node[n].K[2][i] = node[n].Z[2][i];

				friction(n, 2);
			}
		} else if (3 == initial_state) { // 1D dam break on dry bed[2]
			for (int n = 0; n < NN; n++) {
				double xc = node[n].coordinate[0];

				if (xc < 1000.)
					node[n].P[2][0] = 10.;
				else
					node[n].P[2][0] = 0.;

				node[n].P[2][1] = 0.0;
				node[n].P[2][2] = 0.0;

				P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio);

				if (scheme > 0)
					Z_2_K(node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio);
				else
					for (int i = 0; i < size; i++)
						node[n].K[2][i] = node[n].Z[2][i];

				friction(n, 2);
			}
		} else if (4 == initial_state) { // perturbation over smooth bed
			for (int n = 0; n < NN; n++) {
				double xc = node[n].coordinate[0];
				//yc = node[n].coordinate[1] ;

				if ((0.05 < xc) && (xc < 0.15))
					node[n].P[2][0] = 1.01 - node[n].bed[2];
				else
					node[n].P[2][0] = MAX(0., 1.0 - node[n].bed[2]);

				node[n].P[2][1] = 0.;
				node[n].P[2][2] = 0.;

				P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio);

				if (scheme > 0)
					Z_2_K(node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio);
				else
					for (int i = 0; i < size; i++)
						node[n].K[2][i] = node[n].Z[2][i];

				friction(n, 2);
			}
		} else if (5 == initial_state) { //circular dam break
			for (int n = 0; n < NN; n++) {
				double xc = node[n].coordinate[0] ;
				double yc = node[n].coordinate[1];
				double r = sqrt((xc * xc) + (yc * yc));
				if (r < 60.)
					node[n].P[2][0] = 10.;
				else
					node[n].P[2][0] = 5;

				node[n].P[2][1] = 0.;
				node[n].P[2][2] = 0.;

				P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio);

				if (scheme > 0)
					Z_2_K(node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio);
				else
					for (int i = 0; i < size; i++)
						node[n].K[2][i] = node[n].Z[2][i];

				friction(n, 2);
			}
		} else if ( (7 == initial_state) || (34 == initial_state) ) {  // asymmetric dam break, double dam break
			for (int n = 0; n < NN; n++) {
				double xc = node[n].coordinate[0];
				//double yc = node[n].coordinate[1];

				if (xc <= 95.)
					node[n].P[2][0] = 10.;
				else
					node[n].P[2][0] = 5.;

				node[n].P[2][1] = 0.;
				node[n].P[2][2] = 0.;
				P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio);

				if (scheme > 0)
					Z_2_K(node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio);
				else
					for (int i = 0; i < size; i++)
						node[n].K[2][i] = node[n].Z[2][i];

				friction(n, 2);
			}
		} else if (8 == initial_state) { // conical island (F. Marche)
			for (int n = 0; n < NN; n++) {
				double xc = node[n].coordinate[0];
				//double yc = node[n].coordinate[1];
				//xs = 0.;
				double h0 = 0.32;
				double alpha = 0.2;
				//c = sqrt(gm * h0 + gm * alpha * h0);
				double beta = sqrt(3. * alpha * h0 / (4. * h0 * h0 * h0)) * (xc);
				double sech = 1. / (cosh(beta));
				double etai = alpha * h0 * sech * sech;
				double hex = h0 + etai;
				double uex = sqrt(gm / h0) * etai;
				// hex = 0.32 ;
				//  uex = 0. ; // VENDREDI 17
				// if ( xc > 3 ) if ( xc < 6 ) hex += 0.2  ; 
				node[n].P[2][0] = MAX(0, hex - node[n].bed[2]);

				if (node[n].P[2][0] > 0.)
					node[n].P[2][1] = uex * node[n].P[2][0];
				else
					node[n].P[2][1] = 0.;

				node[n].P[2][2] = 0.;

				P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio);

				if (scheme > 0)
					Z_2_K(node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio);
				else
					for (int i = 0; i < size; i++)
						node[n].K[2][i] = node[n].Z[2][i];

				friction(n, 2);
			}
		//} else if (9 == initial_state || 84 == initial_state || 88 == initial_state || 92 == initial_state || 78 == initial_state) { // Exact lake at rest
		} else if (9 == initial_state || 84 == initial_state || 88 == initial_state || 92 == initial_state ) { // Exact lake at rest
			if (84 == initial_state) {
				FILE* inletfile = fopen("../input/Forcing.dat", "r");
				for (int i = 0; i < time_bc_pts; i++)
					fscanf(inletfile, "%le  %le\n", &time_bc[i], &h_bc[i]);
				fclose(inletfile);
			}
			if (92 == initial_state) {
				FILE* inletfile = fopen("../input/spectrum1.dat", "r");
				double dummy;
				fscanf(inletfile, "%d  %le %le %le %le\n", &freq, &dummy, &dummy, &dummy, &dummy);
				fscanf(inletfile, "%le  %le %le %le %le\n", &dummy, &dummy, &dummy, &dummy, &dummy);
				freq=24;
				angle=72;
				a_wg = MA_vector( freq*angle, sizeof(double) );
				t_wg = MA_vector( freq*angle, sizeof(double) );
				l_wg = MA_vector( freq*angle, sizeof(double) );
				erand = MA_vector( freq*angle, sizeof(double) );
				direction = MA_vector( freq*angle, sizeof(double) );

				for (int i = 0; i <= freq*angle-1 ; i++){
					fscanf(inletfile, "%le %le %le %le %le\n", &a_wg[i], &t_wg[i],&direction[i],&l_wg[i],&dummy);
                        	}
				fclose(inletfile);
			}
			for (int n = 0; n < NN; n++) {
				node[n].P[2][0] = 1.;
				if (9== initial_state || 84 == initial_state || 88 == initial_state || 92 == initial_state || 78 == initial_state) node[n].P[2][0] = 0.;
				node[n].P[2][1] = 0;
				node[n].P[2][2] = 0.0;
				node[n].P[2][0] -= node[n].bed[2];
				P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio);

				if (scheme > 0)
					Z_2_K(node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio);
				else
					for (int i = 0; i < size; i++)
						node[n].K[2][i] = node[n].Z[2][i];

				friction(n, 2);
			}
		} else if (10 == initial_state) { // wave over 1d hump
			for (int n = 0; n < NN; n++) {
				double xc = node[n].coordinate[0];

				if (xc < 0.4)
					node[n].P[2][0] = MAX(2. - node[n].bed[2], 0.);
				else
					node[n].P[2][0] = MAX(1. - node[n].bed[2], 0.);
				node[n].P[2][1] = 0;
				node[n].P[2][2] = 0.0;

				P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio);

				if (scheme > 0)
					Z_2_K(node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio);
				else
					for (int i = 0; i < size; i++)
						node[n].K[2][i] = node[n].Z[2][i];

				friction(n, 2);
			}
		} else if (11 == initial_state) { // wave over 2d hump
			for (int n = 0; n < NN; n++) {
				double xc = node[n].coordinate[0];

				if (xc < 0.4)
					node[n].P[2][0] = MAX(2. - node[n].bed[2], 0.);
				else
					node[n].P[2][0] = MAX(1. - node[n].bed[2], 0.);
				node[n].P[2][1] = 0;
				node[n].P[2][2] = 0.0;

				P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio);

				if (scheme > 0)
					Z_2_K(node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio);
				else
					for (int i = 0; i < size; i++)
						node[n].K[2][i] = node[n].Z[2][i];

				friction(n, 2);
			}
		} else if (13 == initial_state || 60 == initial_state) { // Jirka's vortex: must set g=0.25 !!!!!
			for (int n = 0; n < NN; n++) {
				if(initial_state==60){
					double xc = node[n].coordinate[0] ;
					double yc = node[n].coordinate[1] ;
					if(yc==0.0) node[n].P[2][0]=10.0;
					if(yc==1.0) node[n].P[2][0]=10.0;
				}
				double xc = node[n].coordinate[0] - 0.5;
				double yc = node[n].coordinate[1] - 0.5;
				double r = sqrt(xc * xc + yc * yc);
				double rho = 4. * pi * r;
				double omega = 15. * (1. + cos(rho));
				double pm = 10.;
				double dp = (225. / (gm * 16. * pi * pi)) * (2. * cos(rho) + 2. * rho * sin(rho) + cos(2. * rho) / 8. + rho * sin(2. * rho) / 4. + rho * rho * 12. / 16.);
				dp -= (225. / (gm * 16. * pi * pi)) * (2. * cos(pi) + 2. * pi * sin(pi) + cos(2. * pi) / 8. + pi * sin(2. * pi) / 4. + pi * pi * 12. / 16.);

				if (r >= 0.25) {
					omega = 0.;
					dp = 0.;
				}


				node[n].Z[2][0] = pm + dp;
				node[n].Z[2][1] = 6. - omega * yc;
				node[n].Z[2][2] = omega * xc;

				Z_2_P(node[n].Z[2], node[n].P[2], node[n].cut_off_ratio);

				if (scheme > 0)
					Z_2_K(node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio);
				else
					for (int i = 0; i < size; i++)
						node[n].K[2][i] = node[n].Z[2][i];

				friction(n, 2);
                		P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio );
                		if ( scheme > 0 )
                        		Z_2_K( node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio ) ;
                		else
                        		for ( int i = 0 ; i < size ; i ++ )
                                		node[n].K[2][i] = node[n].Z[2][i];
                		friction( n, 2 ) ;
			}
        		HHref = 0.;
		} else if (15 == initial_state) { // thacker's curved solution
			for (int n = 0; n < NN; n++) {
				double xc = node[n].coordinate[0];
				double yc = node[n].coordinate[1];
				double tt = 0.;
				double rr = sqrt(xc * xc + yc * yc);
				double parah = 0.1;
				double para = 1.;
				double parar = 0.8;
				double omega = sqrt(8. * gm * parah / (para * para));
				double dp = (para * para - parar * parar) / (para * para + parar * parar);

				node[n].Z[2][0] = -1. + sqrt(1. - dp * dp) / (1. - dp * cos(omega * tt));
				node[n].Z[2][0] += -rr * rr * (-1. + (1. - dp * dp) / ((1. - dp * cos(omega * tt)) * (1. - dp * cos(omega * tt)))) / (para * para);
				node[n].Z[2][0] *= parah;
				node[n].Z[2][0] -= node[n].bed[2] - 1.;
				node[n].Z[2][1] = 0.5 * (omega * xc * dp * sin(omega * tt)) / (1. - dp * cos(omega * tt));
				node[n].Z[2][2] = 0.5 * (omega * yc * dp * sin(omega * tt)) / (1. - dp * cos(omega * tt));

				if (node[n].Z[2][0] <= 0.) {
					node[n].Z[2][0] = 0.;
					node[n].Z[2][1] = 0.;
					node[n].Z[2][2] = 0.;
				}

				Z_2_P(node[n].Z[2], node[n].P[2], node[n].cut_off_ratio);

				if (scheme > 0)
					Z_2_K(node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio);
				else
					for (int i = 0; i < size; i++)
						node[n].K[2][i] = node[n].Z[2][i];
				friction(n, 2);
			}
		} else if (16 == initial_state) { // thacker's planar solution
			for (int n = 0; n < NN; n++) {
				double xc = node[n].coordinate[0];
				double yc = node[n].coordinate[1];
				double tt = 0.;
				//double rr = sqrt(xc * xc + yc * yc);
				double parah = 0.1;
				double para = 1.;
				double parar = 0.5;
				double omega = sqrt(2. * gm * parah / (para * para));

				node[n].Z[2][0] = -parar + 2. * xc * cos(omega * tt) + 2. * yc * sin(omega * tt);
				node[n].Z[2][0] *= parar * parah / (para * para);
				node[n].Z[2][0] -= node[n].bed[2] - 1.;
				node[n].Z[2][1] = -parar * omega * sin(omega * tt);
				node[n].Z[2][2] = parar * omega * cos(omega * tt);

				if (node[n].Z[2][0] <= 0.) {
					node[n].Z[2][0] = 0.;
					node[n].Z[2][1] = 0.;
					node[n].Z[2][2] = 0.;
				}

				Z_2_P(node[n].Z[2], node[n].P[2], node[n].cut_off_ratio);

				if (scheme > 0)
					Z_2_K(node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio);
				else
					for (int i = 0; i < size; i++)
						node[n].K[2][i] = node[n].Z[2][i];
				friction(n, 2);
			}
		} else if (20 == initial_state) { //1D dam break on dry bed
			for (int n = 0; n < NN; n++) {
				node[n].P[2][0] = 10.;	// - node[n].bed[2] ;
				node[n].P[2][1] = 10.;
				node[n].P[2][2] = 0.;

				if (node[n].coordinate[0] > 100.)
					if (node[n].coordinate[0] < 200.)
						node[n].P[2][0] += 5.;

				P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio);

				if (scheme > 0)
					Z_2_K(node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio);
				else
					for (int i = 0; i < size; i++)
						node[n].K[2][i] = node[n].Z[2][i];

				friction(n, 2);
			}
		} else if (22 == initial_state) { // Reef test case
			for (int n = 0; n < NN; n++) {
				double Z0 = 10. - 0.5;
				node[n].P[2][0] = MAX(0, Z0 - node[n].bed[2]);
				node[n].P[2][1] = 0.;
				node[n].P[2][2] = 0.;

				P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio);

				if (scheme > 0)
					Z_2_K(node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio);
				else
					for (int i = 0; i < size; i++)
						node[n].K[2][i] = node[n].Z[2][i];

				friction(n, 2);
			}
		} else if (23 == initial_state) { // Run up test case [Carrier,2003]
			for (int n = 0; n < NN; n++) {
				double xc = node[n].coordinate[1] / 5000.;
				node[n].Z[2][0] = 0. - node[n].bed[2];
				if (node[n].Z[2][0] < 0.)
					node[n].Z[2][0] = 0.;
				node[n].Z[2][1] = 0.;
				node[n].Z[2][2] = 0.;

				double alpha = 0.018;
				double omega = alpha / 3. * exp(-0.4444 * (xc - 4.1209) * (xc - 4.1209)) - alpha * exp(-4.0 * (xc - 1.6384) * (xc - 1.6384));
				omega *= 500.0;

				node[n].Z[2][0] += omega;

				Z_2_P(node[n].Z[2], node[n].P[2], node[n].cut_off_ratio);

				if (scheme > 0)
					Z_2_K(node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio);
				else
					for (int i = 0; i < size; i++)
						node[n].K[2][i] = node[n].Z[2][i];

				friction(n, 2);
			}
		} else if (24 == initial_state) { // Shelf with an island
			for ( int n = 0 ; n < NN ; n ++ ) {
				double xc = node[n].coordinate[0];
				double h0 = 0.78;
				double alpha =0.39;
				double gamma=sqrt(3.0*alpha/(4.0*h0));
               			double xs = 5.0;
				double d2 = alpha+h0;
  				double xx=0.5*(xc-xs)*sqrt(3.*alpha/(d2*pow(h0,2)));
				double c = sqrt( gm *d2 ) ;
				double sech = 1./(cosh(xx) ) ;
				double etai = alpha *sech*sech;
				double hex = h0+etai;
				double uex = c*(1.-h0/ hex);
		
				node[n].P[2][0] = MAX(0,etai - node[n].bed[2]); 
		
		    		if(node[n].P[2][0] > 0.)
					node[n].P[2][1] = uex*node[n].P[2][0];
				else
					node[n].P[2][1] = 0.;
				node[n].P[2][2] = 0.;
				P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio );
				
				if ( scheme > 0 ){
					Z_2_K( node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio ) ;
					Z_2_P( node[n].Z[2], node[n].P[2],  node[n].cut_off_ratio ) ;
				}else
					for ( int i = 0 ; i < size ; i ++ ) 
						node[n].K[2][i] = node[n].Z[2][i];
				friction( n, 2 ) ;
			}
			/*// Nwogu solitary wave (approximate solution of [Wei&Kirby,1995])       
			double h0 = 0.78;
			double a0 = 0.39;
			//double L0 = 45.0;
			//double alpha = -0.390;	// Nwogu optimal alpha
			//double delta = a0 / h0;	// nonlinear parameter
			//double mu = h0 / L0;	// dispersive parameter
			//double C = sqrt(1.49125);	// approximated celerity

			//A = (C * C - 1.0) / (delta * C);
			//double B = sqrt((C * C - 1.0) / (4.0 * mu * mu * ((alpha + 1.0 / 3.0) - alpha * C * C)));
			//double A1 = (C * C - 1.0) / (3.0 * delta * ((alpha + 1.0 / 3.0) - alpha * C * C));
			//double A2 = -(C * C - 1.0) * (C * C - 1.0) / (2.0 * delta * C * C) * ((alpha + 1.0 / 3.0) + 2.0 * alpha * C * C) / ((alpha + 1.0 / 3.0) - alpha * C * C);

			for (int n = 0; n < NN; n++) {
				//Nwogu's Initial solution
				//double xc = node[n].coordinate[0];
				//double beta = B * xc / L0;
				//double sech = 1. / (cosh(beta));
				//double etai = a0 * A1 * sech * sech + a0 * A2 * sech * sech * sech * sech;
				//double uex = sqrt(gm / h0) * delta * sech * sech;

				// GN initial solution
				double xc = node[n].coordinate[0];//[0];
				//double yc = node[n].coordinate[1];
				double xs = 0.0;
				double alpha = a0;
				double d2 = alpha+h0;
  				double xx=0.5*(xc-xs)*sqrt(3.*alpha/(d2*pow(h0,2)));
				double c = sqrt( gm *d2 ) ;
				double sech = 1./(cosh(xx) ) ;
				double etai = alpha *sech*sech;
				double hex = h0+etai;
				double uex = c*(1.-h0/ hex);
				node[n].P[2][0] = MAX(0, etai - node[n].bed[2]);

				if (node[n].P[2][0] > 0.)
					node[n].P[2][1] = uex * node[n].P[2][0];
				else
					node[n].P[2][1] = 0.;

				node[n].P[2][2] = 0.;

				P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio);

				if (scheme > 0)
					Z_2_K(node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio);
				else
					for (int i = 0; i < size; i++)
						node[n].K[2][i] = node[n].Z[2][i];

				friction(n, 2);
				}
		        */
			} else if (25 == initial_state) { // Okushiri experiment
			FILE* inletfile = fopen("../input/Okushiriwave.dat", "r");
			for (int i = 0; i < time_bc_pts; i++)
				fscanf(inletfile, "%le  %le\n", &time_bc[i], &h_bc[i]);
			fclose(inletfile);

			for (int n = 0; n < NN; n++) {
				node[n].P[2][0] = MAX(-node[n].bed[2], 0.);
				node[n].P[2][1] = 0.;
				node[n].P[2][2] = 0.;

				P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio);

				if (scheme > 0)
					Z_2_K(node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio);
				else
					for (int i = 0; i < size; i++)
						node[n].K[2][i] = node[n].Z[2][i];

				friction(n, 2);
			}
		} else if (30 == initial_state) {
			double q0 = UUinf;
			// for bed = 1. - ( 2.*x + y )/( sqrt( 5. ) * 10*VVinf ) ;
			double u0 = pow(q0, 4. / 3.) / (10 * VVinf * manning * manning);
			u0 = pow(u0, 3. / 10.);
			double h0 = q0 / u0;
			printf("\n");
			printf("Constant slope channel flow. Mass flux     = %le\n", q0);
			printf("Constant slope channel flow. Speed         = %le\n", u0);
			printf("Constant slope channel flow. Depth         = %le\n", h0);
			printf("Constant slope channel flow. Froude number = %le\n", u0 / sqrt(gm * h0));
			printf("\n");

			double uu = 2. * u0 / sqrt(5.);
			double vv = u0 / sqrt(5.);
			for (int n = 0; n < NN; n++) {
				// Works for \nabla b = - (2, 1)/( sqrt( 5. )*10*VVinf ) 
				node[n].P[2][0] = h0;
				node[n].P[2][1] = h0 * uu;
				node[n].P[2][2] = h0 * vv;

				//if ( node[n].coordinate[0] > 100. )                    
				//if ( node[n].coordinate[0] < 200. ) node[n].P[2][0] += 5. ; 

				P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio);

				if (scheme > 0)
					Z_2_K(node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio);
				else
					for (int i = 0; i < size; i++)
						node[n].K[2][i] = node[n].Z[2][i];

				friction(n, 2);
			}
		} else if (31 == initial_state) {
			double q0 = UUinf;
			// for bed = 1. - ( 2.*x + y )/( sqrt( 5. ) * 10*VVinf ) ;
			double u0 = pow(q0, 4. / 3.) / (10 * VVinf * manning * manning);
			u0 = pow(u0, 3. / 10.);
			double h0 = q0 / u0;
			double uu = 2. * u0 / sqrt(5.);
			double vv = u0 / sqrt(5.);
			printf("\n");
			printf("Constant slope channel flow. Mass flux     = %le\n", q0);
			printf("Constant slope channel flow. Speed         = %le\n", u0);
			printf("Constant slope channel flow. Depth         = %le\n", h0);
			printf("Constant slope channel flow. Froude number = %le\n", u0 / sqrt(gm * h0));
			printf("\n");

			for (int n = 0; n < NN; n++) {
				// Works for \nabla b = - (2, 1)/( sqrt( 5. )*10*VVinf ) 
				node[n].P[2][0] = h0;
				node[n].P[2][1] = h0 * uu;
				node[n].P[2][2] = h0 * vv;
				double xc = 0.5;
				xc = (node[n].coordinate[0] - xc) * (node[n].coordinate[0] - xc) + (node[n].coordinate[1] - xc) * (node[n].coordinate[1] - xc);
				xc = sqrt(xc);

				if (xc < 0.1)
					node[n].P[2][0] += 0.01;

				P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio);

				if (scheme > 0)
					Z_2_K(node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio);
				else
					for (int i = 0; i < size; i++)
						node[n].K[2][i] = node[n].Z[2][i];

				friction(n, 2);
			}
		} else if ( 89 == initial_state || 90 == initial_state ) { // propagation of GN solitary wave
		//} else if ( 89 == initial_state || 90 == initial_state ||78==initial_state) { // propagation of GN solitary wave
		//} else if (70 == initial_state || 89 == initial_state || 90 == initial_state ) { // propagation of GN solitary wave
			for ( int n = 0 ; n < NN ; n ++ ) {
				double xc = node[n].coordinate[0];
				//double yc = node[n].coordinate[1];
				double xs = 35; //-7.5;//-6.6; //30;// -7.5;//-6.6; //25. ;
				double h0 = 1.0;//0.44 ;
				double alpha = 0.2;//0.088;//0.1;
				double d2 = alpha+h0;
                          
// For rotated domains
/*				double theta=pi/4.0;
				double x1=15.0;
				double y1=0.0;
				double x2=15.0;
				double y2=0.43;
				double xn1=cos(theta)*x1-sin(theta)*y1;
				double yn1=sin(theta)*x1+cos(theta)*y1;
				double xn2=cos(theta)*x2-sin(theta)*y2;
				double yn2=sin(theta)*x2+cos(theta)*y2;
       	       			double l1=(yn2-yn1)/(xn2-xn1); // the coef of the line e1 that passes from xn1,yn1 xn2,yn2
				double b=-1.0;
				double c1=-l1*xn1+yn1;
                		// coeff of the generation line 
				double dis=fabs(l1*xc+b*yc+c1)/sqrt(pow(l1,2)+1); //distance of x,y from the generation line
 
  				double xx=0.5*(dis)*sqrt(3.*alpha/(d2*pow(h0,2)));

*/
  				double xx=0.5*(xc-xs)*sqrt(3.*alpha/(d2*pow(h0,2)));
				double c = sqrt( gm *d2 ) ;
				double sech = 1./(cosh(xx) ) ;
				double etai = alpha *sech*sech;
				double hex = h0+etai;
				double uex = c*(1.-h0/ hex);
				
				node[n].P[2][0] = MAX(0,etai - node[n].bed[2]); 

// For rotated domains
/*			    	if(node[n].P[2][0] > 0.){
					node[n].P[2][1] =  uex*cos(theta)*node[n].P[2][0];
					node[n].P[2][2] =  uex*sin(theta)*node[n].P[2][0];
				}else{
				node[n].P[2][1] = 0.;
				node[n].P[2][2] = 0.;
				}
*/

			    	if(node[n].P[2][0] > 0.)
					node[n].P[2][1] =  uex*node[n].P[2][0];//P[2][1]
					//node[n].P[2][2] =  uex*node[n].P[2][0];//P[2][1]
				else
					node[n].P[2][1] = 0.;//P[2][1]
					//node[n].P[2][2] = 0.;//P[2][1]
				node[n].P[2][2] = 0.;//P[2][2]
				//node[n].P[2][1] = 0.;//P[2][2]

				

				P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio );
				
				if ( scheme > 0 )
					Z_2_K( node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio ) ;
				else
					for ( int i = 0 ; i < size ; i ++ ) 
						node[n].K[2][i] = node[n].Z[2][i];
				
				friction( n, 2 ) ;
			}
		} else if (71 == initial_state)  { // Convergent Channel	
			for (int n = 0 ; n < NN ; n ++ ) {
				if (node[n].coordinate[0]<1.5)
					node[n].P[2][0] = 0. - node[n].bed[2];
				else
					node[n].P[2][0] = - node[n].bed[2];
				node[n].P[2][1] = 0.;
				node[n].P[2][2] = 0.;
				
				P_2_Z( node[n].P[2], node[n].Z[2], node[n].cut_off_ratio );
				
				if ( scheme > 0 )
					Z_2_K( node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio );
				else
					for (int i = 0 ; i < size ; i ++ ) 
						node[n].K[2][i] = node[n].Z[2][i]; 
				
				friction( n, 2 ) ;
			}
		} else if (72 == initial_state) { // Gironde
			FILE *bathy = fopen("../input/bathy_pb.sol","r");
			if (!bathy)
				printf("ERROR: External file bathy.sol was not found !\n");
			int dummy;
			fscanf(bathy,"MeshVersionFormatted %d\n",&dummy);
			fscanf(bathy,"\n");	
			fscanf(bathy,"Dimension %d\n",&dummy);
			fscanf(bathy,"\n");	
			fscanf(bathy,"SolAtVertices\n");
			fscanf(bathy,"%d\n",&dummy);
			fscanf(bathy,"%d %d\n",&dummy, &dummy);
                        for (int n=0; n<NN; n++)
                                fscanf(bathy,"%le\n",&node[n].bed[2]);
                        fclose(bathy);

                        /* Nodal bathymetry value averaged among the neighbours */
/*                      double bed[NN];
                        int count[NN];
			for (int k=1; k<4; k++){ 
				for (int n=0; n<NN; n++){
					bed[n] = 0.;
					count[n] = 0.;
				}
				for (int e = 0; e < NE; e++) {
					double sum = 0.;
					for (int i=0; i<vertex; i++)
						sum += 1./3.*node[element[e].node[i]].bed[2];
					for (int i=0; i<vertex; i++){
						bed[element[e].node[i]] += sum;
						count[element[e].node[i]] += 1;
					}
				}
				for (int n=0; n<NN; n++)
					node[n].bed[2] = bed[n]/count[n];
			} */
                        
			for ( int n = 0 ; n < NN ; n ++ ) {
                                node[n].P[2][0] = 3.29 - node[n].bed[2];
                                node[n].P[2][1] = 0.;
                                node[n].P[2][2] = 0.;

                                P_2_Z( node[n].P[2], node[n].Z[2], node[n].cut_off_ratio );

                                if ( scheme > 0 )
                                        Z_2_K( node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio );
                                else
                                        for ( int i = 0 ; i < size ; i ++ )
                                                node[n].K[2][i] = node[n].Z[2][i];
                                friction( n, 2 );
                        }
		} else if (73 == initial_state) { // Garonne
			FILE *bathy = fopen("../input/bathy_ga.sol","r");
			if (!bathy)
				printf("ERROR: External file bathy.sol was not found !\n");
			int dummy;
			fscanf(bathy,"MeshVersionFormatted %d\n",&dummy);
			fscanf(bathy,"\n");	
			fscanf(bathy,"Dimension %d\n",&dummy);
			fscanf(bathy,"\n");	
			fscanf(bathy,"SolAtVertices\n");
			fscanf(bathy,"%d\n",&dummy);
			fscanf(bathy,"%d %d\n",&dummy, &dummy);
			for ( int n = 0 ; n < NN ; n ++ ) 
				fscanf(bathy,"%le\n",&node[n].bed[2]);
			fclose(bathy);

			/* Nodal bathymetry value averaged among the neighbours */
			double bed[NN];
			int count[NN];
			for (int k=1; k<2; k++){ 
				for (int n=0; n<NN; n++){
					bed[n] = 0.;
					count[n] = 0.;
				}
				for (int e = 0; e < NE; e++) {
					double sum = 0.;
					for (int i=0; i<vertex; i++)
						sum += 1./3.*node[element[e].node[i]].bed[2];
					for (int i=0; i<vertex; i++){
						bed[element[e].node[i]] += sum;
						count[element[e].node[i]] += 1;
					}
				}
				for (int n=0; n<NN; n++)
					node[n].bed[2] = bed[n]/count[n];
			}			
	
			for ( int n = 0 ; n < NN ; n ++ ) {
				node[n].P[2][0] = 3.29 - node[n].bed[2];
//				if ( (node[n].coordinate[0]>400000) && (node[n].coordinate[1]<260000) && (node[n].coordinate[1]>250000) ) // Garonne
//					node[n].P[2][0] = (3.29150 + 6.9055e-4*(node[n].coordinate[0] - 400000)) - node[n].bed[2];

				node[n].P[2][1] = 0.;
				node[n].P[2][2] = 0.;
				
				P_2_Z( node[n].P[2], node[n].Z[2], node[n].cut_off_ratio );
				
				if ( scheme > 0 )
					Z_2_K( node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio );
				else
					for ( int i = 0 ; i < size ; i ++ ) 
						node[n].K[2][i] = node[n].Z[2][i]; 
				friction( n, 2 ) ;
			}
		} else if (74 == initial_state) { // Dordogne
			FILE *bathy = fopen("../input/bathy_do.sol","r");
			if (!bathy)
				printf("ERROR: External file bathy.sol was not found !\n");
			int dummy;
			fscanf(bathy,"MeshVersionFormatted %d\n",&dummy);
			fscanf(bathy,"\n");	
			fscanf(bathy,"Dimension %d\n",&dummy);
			fscanf(bathy,"\n");	
			fscanf(bathy,"SolAtVertices\n");
			fscanf(bathy,"%d\n",&dummy);
			fscanf(bathy,"%d %d\n",&dummy, &dummy);
			for ( int n = 0 ; n < NN ; n ++ )
				fscanf(bathy,"%le\n",&node[n].bed[2]);
			fclose(bathy);

			/* Nodal bathymetry value averaged among the neighbours */
/*                        double bed[NN];
                        int count[NN];
			for (int k=1; k<4; k++){ 
				for (int n=0; n<NN; n++){
					bed[n] = 0.;
					count[n] = 0.;
				}
				for (int e = 0; e < NE; e++) {
					double sum = 0.;
					for (int i=0; i<vertex; i++)
						sum += 1./3.*node[element[e].node[i]].bed[2];
					for (int i=0; i<vertex; i++){
						bed[element[e].node[i]] += sum;
						count[element[e].node[i]] += 1;
					}
				}
				for (int n=0; n<NN; n++)
					node[n].bed[2] = bed[n]/count[n];
			}			
*/
			for ( int n = 0 ; n < NN ; n ++ ) {
				node[n].P[2][0] = 3.29 - node[n].bed[2];
//				if ( (node[n].coordinate[0]>400000) && (node[n].coordinate[1]<295000) && (node[n].coordinate[1]>280000) ) // Dordogne
//					node[n].P[2][0] = (3.292 + 3.756e-4*(node[n].coordinate[0] - 400000)) - node[n].bed[2];

				node[n].P[2][1] = 0.;
				node[n].P[2][2] = 0.;
				
				P_2_Z( node[n].P[2], node[n].Z[2], node[n].cut_off_ratio );
				
				if ( scheme > 0 )
					Z_2_K( node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio );
				else
					for ( int i = 0 ; i < size ; i ++ ) 
						node[n].K[2][i] = node[n].Z[2][i]; 
				friction( n, 2 ) ;
			}
		} else if (75 == initial_state) { // rip channel
			FILE* inletfile = fopen("../input/spectrum.dat", "r");
			double dummy;
			fscanf(inletfile, "%d  %le %le %le %le\n", &freq, &dummy, &dummy, &dummy, &dummy);
			fscanf(inletfile, "%le  %le %le %le %le\n", &dummy, &dummy, &dummy, &dummy, &dummy);
			a_wg = MA_vector( freq, sizeof(double) );
			t_wg = MA_vector( freq, sizeof(double) );
			l_wg = MA_vector( freq, sizeof(double) );
			erand = MA_vector( freq, sizeof(double) );

			for (int i = 0; i <= freq-1 ; i++){
				fscanf(inletfile, "%le %le %le %le %le\n", &a_wg[i], &t_wg[i],&l_wg[i],&dummy,&erand[i]);
                        }
			fclose(inletfile);

                        for (int n = 0 ; n < NN ; n ++ ) {
	                        node[n].P[2][0] = 0. - node[n].bed[2];
                                node[n].P[2][1] = 0.;
                                node[n].P[2][2] = 0.;
			
				node[n].u_ave = 0.;
				node[n].v_ave = 0.;

                                P_2_Z( node[n].P[2], node[n].Z[2], node[n].cut_off_ratio );

                                if ( scheme > 0 )
                                        Z_2_K( node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio );
                                else
                                        for (int i = 0 ; i < size ; i ++ )
                                                node[n].K[2][i] = node[n].Z[2][i];

                                friction( n, 2 ) ;
			}
		/*} else if (76 == initial_state) { // synolakis
                        for ( int n = 0 ; n < NN ; n ++ ) {
                                double xc = node[n].coordinate[0];
                                double alpha = 0.28;//0.04;
                                double h0 = 1.0 ;
          			double gamma=sqrt(3.0*alpha/(4.0*h0));
                                double xs = 19.85 + (1.0/gamma)*2.178327221030376 + 20.;
                                double d2 = alpha+h0;
                                double xx=0.5*(xc-xs)*sqrt(3.*alpha/(d2*pow(h0,2)));
                                double c = sqrt( gm *d2 ) ;
                                double sech = 1./(cosh(xx) ) ;
                                double etai = alpha *sech*sech;
                                double hex = h0+etai;
                                double uex = -c*(1.-h0/ hex);

                                node[n].P[2][0] =  MAX(0,etai - node[n].bed[2]);

		                if(node[n].P[2][0] > 0.)
                                        node[n].P[2][1] =  uex*node[n].P[2][0];
                                else
                                        node[n].P[2][1] = 0.;

                                node[n].P[2][2] = 0.;

                                P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio );


                                if ( scheme > 0 )
                                        Z_2_K( node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio );
                                else
                                        for (int i = 0 ; i < size ; i ++ )
                                                node[n].K[2][i] = node[n].Z[2][i];

                                friction( n, 2 ) ;
			}*/
		} else if (77 == initial_state) { // volker_reef
                        for ( int n = 0 ; n < NN ; n ++ ) {
                                double xc = node[n].coordinate[0];
                                double alpha = 0.75;
                                double h0 = 2.5 ;
                                double xs = 17.64;
                                double d2 = alpha+h0;
                                double xx=0.5*(xc-xs)*sqrt(3.*alpha/(d2*pow(h0,2)));
                                double c = sqrt( gm *d2 ) ;
                                double sech = 1./(cosh(xx) ) ;
                                double etai = alpha *sech*sech;
                                double hex = h0+etai;
                                double uex = c*(1.-h0/ hex);

                                node[n].P[2][0] =  MAX(0,etai - node[n].bed[2]);

		                if(node[n].P[2][0] > 0.)
                                        node[n].P[2][1] =  uex*node[n].P[2][0];
                                else
                                        node[n].P[2][1] = 0.;


                                node[n].P[2][2] = 0.;

                                P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio );
                                
				if ( scheme > 0 )
                                        Z_2_K( node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio );
                                else
                                        for (int i = 0 ; i < size ; i ++ )
                                                node[n].K[2][i] = node[n].Z[2][i];
                                friction( n, 2 ) ;
			}
		} else if (78 == initial_state) { // wave over a bar
                        for ( int n = 0 ; n < NN ; n ++ ) {
                                node[n].P[2][0] = 0.0 - node[n].bed[2];
                                node[n].P[2][1] = 0.;
                                node[n].P[2][2] = 0.;


                                P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio );
                                
				if ( scheme > 0 )
                                        Z_2_K( node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio );
                                else
                                        for (int i = 0 ; i < size ; i ++ )
                                                node[n].K[2][i] = node[n].Z[2][i];
			}
		} else if (79 == initial_state) { // garonne-dordogne-gironde
/*                        int flag[NN];
                        for ( int n = 0 ; n < NN ; n ++ ){
                                flag[n] = 0;
                        }
*/
                        /***********************
                        ***      GIRONDE     ***
                        ***********************/
/*                        FILE *gir = fopen("../input/start_gironde.dat","r");
                        int NN_gir, dummy_i;
                        if (!gir)
                                printf("ERROR: External file start_gironde.dat was not found !\n");
                        fscanf(gir,"TITLE      =  Unstructured grid data\n");
                        fscanf(gir,"VARIABLES  =  X  Y  HT  H  U  V  BED  PHIx  PHIy  Wx  Wy  Rx  Ry  Fx  Fy\n");
                        fscanf(gir,"ZONE    F = FEPOINT    ET = TRIANGLE N  =  %d    E  =  %d\n", &NN_gir, &dummy_i);
                        fscanf(gir,"\n");
                        for ( int n = 0 ; n < NN_gir ; n ++ ){
                                double x,y,h,u,v,bed,dummy_d;
				int flag_gir = 0;
                                fscanf(gir,"%le %le %le %le %le %le %le %le %le %le %le %le %le %le %le\n",
                                        &x,&y,&dummy_d,&h,&u,&v,&bed,&dummy_d,&dummy_d,&dummy_d,&dummy_d,&dummy_d,&dummy_d,&dummy_d,&dummy_d);
                                for ( int m = 0 ; m < NN ; m ++ ){
                                        if ( (fabs(x - node[m].coordinate[0])<=0.00001) && (fabs(y - node[m].coordinate[1])<=0.00001) ){
                                                node[m].P[2][0] = h;
                                                node[m].P[2][1] = h*u;
                                                node[m].P[2][2] = h*v;
                                                node[m].bed[2] = bed;
                                                flag[m] += 1;
                                                flag_gir = 1;
                                        }
                                }
                                if (flag_gir==0)
                                        printf("gironde: I did not find a matching node for n : %d x: %le y: %le\n",n,x,y);
                        }
                        fclose(gir);
*/
                        /***********************
                        ***      GARONNE     ***
                        ***********************/
/*                        FILE *gar = fopen("../input/start_garonne.dat","r");
                        int NN_gar;
                        if (!gar)
                                printf("ERROR: External file start_garonne.dat was not found !\n");
                        fscanf(gar,"TITLE      =  Unstructured grid data\n");
                        fscanf(gar,"VARIABLES  =  X  Y  HT  H  U  V  BED  PHIX  PHIY  bb\n");
                        fscanf(gar,"ZONE    F = FEPOINT    ET = TRIANGLE N  =  %d    E  =  %d\n", &NN_gar, &dummy_i);
                        fscanf(gar,"\n");
                        for ( int n = 0 ; n < NN_gar ; n ++ ){
                                double x,y,h,u,v,bed,dummy_d;
                                //int flag_gar = 0;
                                fscanf(gar,"%le %le %le %le %le %le %le %le %le %le\n",
                                        &x,&y,&dummy_d,&h,&u,&v,&bed,&dummy_d,&dummy_d,&dummy_d);
                                for ( int m = 0 ; m < NN ; m ++ ){
                                        if ( (fabs(x - node[m].coordinate[0])<=0.00001) && (fabs(y - node[m].coordinate[1])<=0.00001) ){
                                                node[m].P[2][0] = h;
                                                node[m].P[2][1] = h*u;
                                                node[m].P[2][2] = h*v;
                                                node[m].bed[2] = bed;
                                                flag[m] += 1;
                                  //              flag_gar = 1;
                                        }
                                }
                                if (n==215){
                                        node[215].P[2][0] = h;
                                        node[215].P[2][1] = h*u;
                                        node[215].P[2][2] = h*v;
                                        node[215].bed[2] = bed;
                                        flag[215] += 1;
                                 //       flag_gar = 1;
                                }
			}
			fclose(gar);
*/
			/***********************
                        ***      DORDOGNE    ***
                        ***********************/
/*                        FILE *dor = fopen("../input/start_dordogne.dat","r");
                        int NN_dor;
                        if (!dor)
                                printf("ERROR: External file start_dordogne.dat was not found !\n");
                        fscanf(dor,"TITLE      =  Unstructured grid data\n");
                        fscanf(dor,"VARIABLES  =  X  Y  HT  H  U  V  BED  PHIX  PHIY bb\n");
                        fscanf(dor,"ZONE    F = FEPOINT    ET = TRIANGLE N  =  %d    E  =  %d\n", &NN_dor, &dummy_i);
                        fscanf(dor,"\n");
                        for ( int n = 0 ; n < NN_dor ; n ++ ){
                                double x,y,h,u,v,bed,dummy_d;
                                int flag_dor = 0;
                                fscanf(dor,"%le %le %le %le %le %le %le %le %le %le\n",
                                        &x,&y,&dummy_d,&h,&u,&v,&bed,&dummy_d,&dummy_d,&dummy_d);
                                for ( int m = 0 ; m < NN ; m ++ ){
                                        if ( (fabs(x - node[m].coordinate[0])<=0.00001) && (fabs(y - node[m].coordinate[1])<=0.00001) ){
                                                node[m].P[2][0] = h;
                                                node[m].P[2][1] = h*u;
                                                node[m].P[2][2] = h*v;
                                                node[m].bed[2] = bed;
                                                flag[m] += 1;
                                                flag_dor = 1;
                                        }
                                }
                                if (flag_dor==0)
                                        printf("dordogne: I did not find a matching node for n : %d x: %le y: %le\n",n,x,y);
                        }
                        fclose(dor);

			int count = 0;
                        for ( int m = 0 ; m < NN ; m ++ ){
                                if (flag[m]==0){
                                        count += 1;
                                        printf("%d The node %d has no values\n",count,m);
                                } else if (flag[m]==2)
                                        printf("The node %d has been counted two times\n",m);
                        }
                        printf("%d %d %d %d %d\n",NN,NN_gir,NN_gar,NN_dor,NN_gir+NN_gar+NN_dor);

                        for ( int n = 0 ; n < NN ; n ++ ){
                                P_2_Z( node[n].P[2], node[n].Z[2], node[n].cut_off_ratio );

                                if ( scheme > 0 )
                                        Z_2_K( node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio );
                                else
                                        for ( int i = 0 ; i < size ; i ++ )
                                                node[n].K[2][i] = node[n].Z[2][i];
                                friction( n, 2 );
                        }
*/
			FILE *bathy = fopen("../input/bathy_pb.sol","r");
                        if (!bathy)
                                printf("ERROR: External file bathy.sol was not found !\n");
                        int dummy;
                        fscanf(bathy,"MeshVersionFormatted %d\n",&dummy);
                        fscanf(bathy,"\n");
                        fscanf(bathy,"Dimension %d\n",&dummy);
                        fscanf(bathy,"\n");
                        fscanf(bathy,"SolAtVertices\n");
                        fscanf(bathy,"%d\n",&dummy);
                        fscanf(bathy,"%d %d\n",&dummy, &dummy);
                        for ( int n = 0 ; n < NN ; n ++ )
                                fscanf(bathy,"%le\n",&node[n].bed[2]);
                        fclose(bathy);

                        /* Nodal bathymetry value averaged among the neighbours */
/*                        double bed[NN];
                        int count[NN];
			for (int k=1; k<4; k++){ 
				for (int n=0; n<NN; n++){
					bed[n] = 0.;
					count[n] = 0.;
				}
				for (int e = 0; e < NE; e++) {
					for (int i=0; i<vertex; i++){
						bed[element[e].node[i]] += sum;
						count[element[e].node[i]] += 1;
					}
				}
				for (int n=0; n<NN; n++)
					node[n].bed[2] = bed[n]/count[n];
			} */
			for ( int n = 0 ; n < NN ; n ++ ){
                                node[n].P[2][0] = 3.29 - node[n].bed[2];
                                node[n].P[2][1] = 0.;
                                node[n].P[2][2] = 0.;

                                P_2_Z( node[n].P[2], node[n].Z[2], node[n].cut_off_ratio );

                                if ( scheme > 0 )
                                        Z_2_K( node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio );
                                else
                                        for ( int i = 0 ; i < size ; i ++ )
                                                node[n].K[2][i] = node[n].Z[2][i];
                                friction( n, 2 );
                        }
                } else if (80 == initial_state) { // shoaling_lannes
                        for ( int n = 0 ; n < NN ; n ++ ) {
                                double xc = node[n].coordinate[0];
                                double h0 = 0.218 ;
                                double alpha = 0.264*h0;
                                //double gamma=sqrt(3.0*alpha/(4.0*h0));
                                double xs = 0.;
                                double d2 = alpha+h0;
                                double xx=0.5*(xc-xs)*sqrt(3.*alpha/(d2*pow(h0,2)));
                                double c = sqrt( gm *d2 ) ;
                                double sech = 1./(cosh(xx) ) ;
                                double etai = alpha *sech*sech;
                                double hex = h0+etai;
                                double uex = c*(1.-h0/ hex);

                                node[n].P[2][0] =  MAX(0,etai - node[n].bed[2]);

                                if(node[n].P[2][0] > 0.)
                                        node[n].P[2][1] =  uex*node[n].P[2][0];
                                else
                                        node[n].P[2][1] = 0.;

                                node[n].P[2][2] = 0.;

                                P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio );

                                if ( scheme > 0 )
                                        Z_2_K( node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio );
                                else
                                        for (int i = 0 ; i < size ; i ++ )
                                                node[n].K[2][i] = node[n].Z[2][i];
                                friction( n, 2 ) ;
                        }
		//} else if (81 == initial_state || 82 == initial_state){ /* Wave diffraction over an elliptical shoal (with iwg) */
		} else if (82 == initial_state){ /* Wave diffraction over an elliptical shoal (with iwg) */
                        						/* Wave diffraction over a semi-circular shoal (with iwg) */
			for (int n = 0; n < NN; n++) {

                                node[n].P[2][0] = - node[n].bed[2];
                                node[n].P[2][1] = 0.;
                                node[n].P[2][2] = 0.;

                                P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio );

                                if ( scheme > 0 )
                                        Z_2_K( node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio );
                                else
                                        for (int i = 0 ; i < size ; i ++ )
                                                node[n].K[2][i] = node[n].Z[2][i];
                                friction( n, 2 ) ;
                        }
		} else if ( 83 == initial_state || 70 == initial_state || 78 == initial_state || 81 == initial_state || 93 == initial_state || 76 == initial_state ){ // manufactured solution & lake at rest
		//} else if (83 == initial_state){ // manufactured solution 
			//if (2>= blending || grad_reco==0) {
			if (grad_reco==0) {
				for ( int n = 0 ; n < NN ; n ++ ) {
					if(83 == initial_state || 70==initial_state || 93==initial_state || 76 == initial_state){
						double xc = node[n].coordinate[0];
						//double xc = node[n].coordinate[1];
						//double xc = node[n].DcC[0];
						//double xs = 6.0;////0.0;//7.3;//35. ;
						double h0 = 1.0;//0.4572;//0.78;//0.15;//1.0;//0.15;//1.0 ;
						double alpha =0.28;// 0.1783;//0.39;//0.0375;//0.28;//0.0375; //0.2;
						double gamma=sqrt(3.0*alpha/(4.0*h0));
                				double xs = 19.85 + (1.0/gamma)*2.178327221030376;//+ 20.; //35;
						double d2 = alpha+h0;
  						double xx=0.5*(xc-xs)*sqrt(3.*alpha/(d2*pow(h0,2)));
						double c = sqrt( gm *d2 ) ;
						double sech = 1./(cosh(xx) ) ;
						double etai = alpha *sech*sech;
						double hex = h0+etai;
						double uex = c*(1.-h0/ hex);
				
						node[n].P[2][0] = MAX(0,etai - node[n].bed[2]); 
				
			    			if(node[n].P[2][0] > 0.)
							node[n].P[2][1] = -uex*node[n].P[2][0];
						else
							node[n].P[2][1] = 0.;
						node[n].P[2][2] = 0.;
					        

			    			/*if(node[n].P[2][0] > 0.)
							node[n].P[2][2] =  uex*node[n].P[2][0];
						else
							node[n].P[2][2] = 0.;
						node[n].P[2][1] = 0.;
						*/
						//node[n].P[2][0] = MAX(0,node[n].P[2][0] - node[n].bed[2]); 
						//node[n].P[2][1] = 0.;
						//node[n].P[2][2] = 0.;
					}else{
						node[n].P[2][0] = MAX(0,node[n].P[2][0] - node[n].bed[2]); 
						node[n].P[2][1] = 0.;
						node[n].P[2][2] = 0.;
					}
					
					P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio );
					
					if ( scheme > 0 ){
						Z_2_K( node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio ) ;
						Z_2_P( node[n].Z[2], node[n].P[2],  node[n].cut_off_ratio ) ;
					}else
						for ( int i = 0 ; i < size ; i ++ ) 
							node[n].K[2][i] = node[n].Z[2][i];
					friction( n, 2 ) ;
				}		
			} else {
				/*double a = 0.045;
				double d = 0.45;
				for ( int e = 0 ; e < NE ; e ++ ) {
					soliton_integral(e,a,d);	
				}
				// Correction for period boundaries:
				for (int f=0; f<NBF; f++){
					int n = b_nodes[f].node;
					node[n].flag = 0;
				}
				for ( int f = 0 ; f < NBF ; f ++) {
        				if(b_nodes[f].type == 10) {
				            	int n = b_nodes[f].node;
						if(node[n].flag == 0) {		
							int mate = node[n].mate;
							node[n].cell_ave[0] += node[mate].cell_ave[0];
							node[n].cell_ave[1] += node[mate].cell_ave[1];
							node[mate].cell_ave[0] = node[n].cell_ave[0];
							node[mate].cell_ave[1] = node[n].cell_ave[1];
							node[n].flag = 1;
							node[mate].flag = 1;
						}
					}	
				}*/

				// Average solution over the dual cell
				integration_dualcell(); // returns eta and u

				for ( int n = 0; n < NN; n++ ){
					node[n].P[2][0] = MAX(0,node[n].P[2][0] - node[n].bed[2]); 
			    		if(node[n].P[2][0] > 0.)
						node[n].P[2][1] =  node[n].P[2][1]*node[n].P[2][0];
					else
						node[n].P[2][1] = 0.;
					node[n].P[2][2] = 0.;
					//
			    		/*if(node[n].P[2][0] > 0.)
						node[n].P[2][2] =  node[n].P[2][2]*node[n].P[2][0];
					else
						node[n].P[2][2] = 0.;
					node[n].P[2][1] = 0.;
						*/
					// lake at rest
					//node[n].P[2][1] = 0.;
					//node[n].P[2][2] = 0.;
	
					P_2_Z(node[n].P[2], node[n].Z[2], node[n].cut_off_ratio );
					
					if ( scheme > 0 ){
						Z_2_K( node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio ) ;
						Z_2_P( node[n].Z[2], node[n].P[2],  node[n].cut_off_ratio ) ;
					}else
						for ( int i = 0 ; i < size ; i ++ ) 
							node[n].K[2][i] = node[n].Z[2][i];
					friction( n, 2 ) ;
				}
			}
		} else if (86 == initial_state)  { // Tissier ondular bore	
			double a = 2.;
			double ha = 1.;
			double ua = 0.;
			double Fr = 1.4;
			double cb = Fr*sqrt(gm*ha);
			double hb1 = 0.5*( -ha + sqrt(ha*ha + 8.*ha*cb*cb/gm) );
			double hb2 = 0.5*( -ha - sqrt(ha*ha + 8.*ha*cb*cb/gm) );
			double hb = 0.;
			if ( (hb1 <= 0.) && (hb2 > 0.) )
				hb = hb2;
			else if ( (hb2 <= 0.) && (hb1 > 0.) )
				hb = hb1;
			else {
				printf("Problem in the computation of hb via the jump relations!!!");
				exit(0);
			}
			double ub = cb*(1. - ha/hb);

			for ( int n = 0 ; n < NN ; n ++ ) {
/*				double xc = node[n].coordinate[0];//[0];
				//double yc = node[n].coordinate[1];
				double xs = 25. ;
				double h0 = 1. ;
				double alpha = 0.1;
				double d2 = alpha+h0;
  				double xx=0.5*(xc-xs)*sqrt(3.*alpha/(d2*pow(h0,2)));
				double c = sqrt( gm *d2 ) ;
				double sech = 1./(cosh(xx) ) ;
				double etai = alpha *sech*sech;
				double hex = h0+etai;
				double uex = c*(1.-h0/ hex);
				
				node[n].P[2][0] = MAX(0,etai - node[n].bed[2]); 
			    	if(node[n].P[2][0] > 0.)
					node[n].P[2][1] =  uex*node[n].P[2][0];//P[2][1]
				else
					node[n].P[2][1] = 0.;//P[2][1]
				node[n].P[2][2] = 0.;//P[2][2]
*/
				double x = node[n].coordinate[0];
				double h = 0.5*(hb - ha)*(1. - tanh(x/a)) + ha;
				double u = 0.5*(ub - ua)*(1. - tanh(x/a)) + ua;
			
				node[n].P[2][0] = h;
				node[n].P[2][1] = u*h;
				node[n].P[2][2] = 0.;
				
				P_2_Z( node[n].P[2], node[n].Z[2], node[n].cut_off_ratio );
				
				if ( scheme > 0 )
					Z_2_K( node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio );
				else
					for (int i = 0 ; i < size ; i ++ ) 
						node[n].K[2][i] = node[n].Z[2][i]; 
				
				friction( n, 2 ) ;
			}
		} else if (91 == initial_state)  { // Polynomial reconstruction

			// Average solution over the dual cell
			integration_dualcell();
 
			//for(int n = 0; n < NN; n++){
                        //        node[n].P[2][0]=node[n].coordinate[0];
                         //       node[n].P[2][1]=0.0;
                        //        node[n].P[2][2]=0.0;
			//}

			for(int n = 0; n < NN; n++){
				P_2_Z( node[n].P[2], node[n].Z[2], node[n].cut_off_ratio );
				
				if ( scheme > 0 )
					Z_2_K( node[n].Z[2], node[n].K[2], node[n].bed[2], node[n].cut_off_ratio );
				else
					for (int i = 0 ; i < size ; i ++ ) 
						node[n].K[2][i] = node[n].Z[2][i]; 
				
				friction( n, 2 ) ;
			}
		} else {
			printf("ERROR: Wrong problem type given!\n");
			printf("1: Wedge\n");
			printf("2: 1D-flow\n");
			printf("3: 1D dam break on dry bed\n");
			printf("ERROR: Wrong problem type given!\n");
			printf("1: Wedge\n");
			printf("2: 1D-flow\n");
			printf("3: 1D dam break on dry bed\n");
			printf("4: pertubation over smooth bed\n");
			printf("5: Circular dam break\n");
			printf("6: 2D with dry cylinder\n");
			printf("7: Asymmetric Dam Break\n");
			printf("8: Conical Island\n");
			printf("9: Lake at rest\n");
		}

	HHref = 0.;
	for (int n = 0; n < NN; n++) {
		node[n].h1 = 0.;
		node[n].h2 = 10000.;

		if (node[n].P[2][0] > HHref)
			HHref = node[n].P[2][0];

		if (!(node[n].P[2][0] > cut_off_H * node[n].cut_off_ratio)) {
			node[n].dry_flag4 = 1;
			node[n].dry_flag3 = 1;
			node[n].dry_flag2 = 1;
			node[n].dry_flag1 = 1;
			node[n].dry_flag0 = 1;

			node[n].P[2][0] = 0.;
			node[n].P[2][1] = 0.;
			node[n].P[2][2] = 0.;

			node[n].Z[2][0] = 0.;
			node[n].Z[2][1] = 0.;
			node[n].Z[2][2] = 0.;

			node[n].K[2][0] = 0.;
			node[n].K[2][1] = 0.;
			node[n].K[2][2] = 0.;
		} else {
			node[n].dry_flag4 = 0;
			node[n].dry_flag3 = 0;
			node[n].dry_flag2 = 0;
			node[n].dry_flag1 = 0;
			node[n].dry_flag0 = 0;
		}

		if(node[n].dry_flag2==1){ // if a node is dry put to his neighbors dry_flag3=1
			for(int i=0; i<node[n].num_neigh; i++){
				int m=node[n].neigh[i];
				node[m].dry_flag3=1;
			}
		}
	}



	for (int n = 0; n < NN; n++) {
		if(node[n].dry_flag3==1){
			for(int i=0; i<node[n].num_neigh; i++){
				int m=node[n].neigh[i];
				node[m].dry_flag4=1;
			}
		}

		node[n].bed[0] = node[n].bed[2];
		node[n].bed[1] = node[n].bed[2];
		for (int i = 0; i < size; i++) {
			node[n].friction[0][i] = node[n].friction[2][i];
			node[n].friction[1][i] = node[n].friction[2][i];
		}
	}
	FILE* bathymetry = fopen("../output/bathymetry.dat", "w");
	fprintf(bathymetry, "MeshVersionFormatted %d\n", 2);
	fprintf(bathymetry, "\n");
	fprintf(bathymetry, "Dimension %d\n", 2);
	fprintf(bathymetry, "\n");
	fprintf(bathymetry, "SolAtVertices\n");
	fprintf(bathymetry, "%d\n", NN);
	fprintf(bathymetry, "%d %d\n", 1, 1);
	for (int n = 0; n < NN; n++)
		fprintf(bathymetry, "%le\n", node[n].bed[2]);
	fprintf(bathymetry, "\n");
	fprintf(bathymetry, "End");
	fclose(bathymetry);
}
