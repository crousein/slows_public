/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/
	

/***************************************************************************
                                 read_write.c
------------
            This is read_write: it contains every function reading
	or writing the solution
-------------------
***************************************************************************/
#include <stdio.h>
#include <math.h>
#include "common.h"
#include "my_malloc.h"
#include <stdlib.h>

int output_step;
extern int NN, NE, Nprobes, movie, time_step, initial_state, write_time, freq,angle, blending;
extern double gm, cut_off_U, Time, gridsize, manning, UUinf, VVinf;
extern double *temp_vector, *work_vector1;
extern struct node_struct *node;
extern struct probe_struct *probes;
extern struct element_struct *element;
extern int model_dispersion;
extern double *nht_phi; // nonHydro->output
extern int *nht_bb;
extern double *nht_k;
extern double *nht_e;
extern double *a_wg, *t_wg, *l_wg, *erand, *direction;
extern char out_file[MAX_CHAR];
extern char initial_solution_file[MAX_CHAR];
double *node_href;
extern double *INTrec;

void compute_error( double * , int, double );
void K_2_Z(double *, double *, double, double, double);
void integration_dualcell(double, double, double *);
double analytic_sol( double, double, double);

double interpolation(double x, double *f, int m, double *pos, int DIM)
{
	//definition of the coordinates of the 4 points of the stencil used for interpolation:
	double S;
	if (x == pos[0]) {
		S = f[0];
	} else if (x == pos[DIM-1]) {
		S = f[DIM-1];
	} else {
		double x1, x2, x3, x4, f1, f2, f3, f4;
		if (x==pos[DIM-2]) {
 			x1 = pos[m-1];
			f1 = f[m-1];
			x2 = pos[m];
			f2 = f[m];
			x3 = pos[m+1];
			f3 = f[m+1];
			x4 = pos[m-2];
			f4 = f[m-2];
		} else {
 			x1 = pos[m-1];
			f1 = f[m-1];
			x2 = pos[m];
			f2 = f[m];
			x3 = pos[m+1];
			f3 = f[m+1];
			x4 = pos[m+2];
			f4 = f[m+2];
		}

		//interpolation:
		double W = (x4 - x2)/(x1 - x2);
		double Z = (x3 - x2)/(x1 - x2);
		double A = (pow(x3,2) - pow(x2,2)) + Z*(pow(x2,2) - pow(x1,2));
		double B = W*(pow(x2,2) - pow(x1,2)) + (pow(x4,2) - pow(x2,2));
		double D = f4 - f2 - W*(f1 - f2);
		double E = D - B/A*(f3 - f2 - Z*(f1 - f2));
		double F = (pow(x4,3) - pow(x2,3)) - B/A*(Z*(pow(x2,3) - pow(x1,3)) + (pow(x3,3) - pow(x2,3)));
		double a3 = E/F;
		double a2 = (f3 - f2 - Z*(f1 - f2 + a3*(pow(x2,3) - pow(x1,3))) -  a3*(pow(x3,3) - pow(x2,3)))/A;
		double a1 = (f1 - f2 + a2*(pow(x2,2) - pow(x1,2)) + a3*(pow(x2,3) - pow(x1,3)) ) / (x1 - x2);
		double a0 = f2 - a1*x2 - a2*pow(x2,2) - a3*pow(x2,3);
		S = a0 + a1*x + a2*pow(x,2) + a3*pow(x,3);
	}
	return S;
}

/* 2D shallow water */
void write_solution_dat(void)
{
	if ( initial_state == 91 || initial_state ==83 || initial_state == 70 || initial_state == 13 || initial_state == 60) {
                double Error[3] = {0., 0., 0.}; 
                compute_error( Error , 0, Time); //0 for the first equation -> h
                printf("Error in L1 norm: %le ; L2 norm %le ; L2maria %le ; gridsize %le\n", Error[0],Error[1],Error[2],gridsize);
                printf("\n");

		FILE *fp = fopen( "../output/convergence_info.dat", "a" ) ;
                fprintf(fp," %le %le %le %le\n", Error[0],Error[1],Error[2],gridsize);
		fclose(fp);

	}

	char tecplot_file_name[MAX_CHAR];
	if (time_step == 0) {
		output_step = 0;
		sprintf(tecplot_file_name, "../output/initial_solution.dat");
		printf("          **************************************\n");
		printf("          **      Writing Tecplot file       **\n");
		printf("          **************************************\n");
	} else if (movie) {
		output_step++;
		sprintf(tecplot_file_name, "../output/%s%04d.dat", out_file, output_step);
		if(initial_state==76){ // synolakis
			//if ( (Time == 20.*sqrt(1.0/gm)) || (Time == 26.*sqrt(1.0/gm)) || (Time == 32.*sqrt(1.0/gm)) || (Time == 38.*sqrt(1.0/gm)) || //non breaking case
			//     (Time == 44.*sqrt(1.0/gm)) || (Time == 50.*sqrt(1.0/gm)) || (Time == 56.*sqrt(1.0/gm)) || (Time == 62.*sqrt(1.0/gm)) )
			if ( (Time == 10.*sqrt(1.0/gm)) || (Time == 15.*sqrt(1.0/gm)) || (Time == 20.*sqrt(1.0/gm)) || (Time == 25.*sqrt(1.0/gm)) || //breaking case
			     (Time == 30.*sqrt(1.0/gm)) || (Time == 45.*sqrt(1.0/gm)) || (Time == 55.*sqrt(1.0/gm)) || (Time == 60.*sqrt(1.0/gm)) ||
                             (Time == 70.*sqrt(1.0/gm)) || (Time == 80.*sqrt(1.0/gm)) )
					sprintf(tecplot_file_name, "../output/synolakis_sn%d.dat", output_step);
		}    
		if(initial_state==77){ // Volker reef
		if ((Time == 4.0436) || (Time == 5.053) || (Time == 6.815) || (Time == 7.82) || (Time == 8.94) || 
			(Time == 10.91) || (Time == 13.02) || (Time == 21.8082) || (Time == 28.779) || (Time == 31.6) ||
                        (Time == 32.96) || (Time == 48.96))
					sprintf(tecplot_file_name, "../output/reef_sn%d.dat", output_step);
		}    

	} else {
		sprintf(tecplot_file_name, "../output/%s.dat", out_file);
	}


	printf("\nWrite file %s\n\n", out_file);

	FILE *out = fopen(tecplot_file_name, "w");
	fprintf(out, "TITLE      =  Unstructured grid data \n");
	if (31 == initial_state)
		fprintf(out, "VARIABLES  =  X  Y  HT  H  U  V  BED  N\n");
	else if (75 == initial_state)
		fprintf(out, "VARIABLES  =  X  Y  HT  H  U  V  BED PHIX PHIY bb k Uave Vave\n");
	else if (91 == initial_state)
		fprintf(out, "VARIABLES  =  X  Y  HT Hx  Hy  Hxx Hyy Hxy U Ux Uy Uxx Uyy Uxy\n");
	else{
		if(blending==3){
			if (output_step == 0 || model_dispersion ==0){
				fprintf(out, "VARIABLES  =  X  Y  HT  H  U  V  BED  Hx Hy Hxx Hyy Hxy Ux Uy Uxx Uyy Uxy Vx Vy Vxx Vyy Vxy Error Ansol\n");
			}else{
				fprintf(out, "VARIABLES  =  X  Y  HT  H  U  V  BED Hx Hy Hxx Hyy Hxy Ux Uy Uxx Uyy Uxy Error Ansol PHIX PHIY bb e\n");
			}
		}else{
				fprintf(out, "VARIABLES  =  X  Y  HT  H  U  V  BED \n");
		}
	}
	int TOT_N = NN;
	int TOT_E = NE;
	fprintf(out, "ZONE    F = FEPOINT    ET = TRIANGLE N  =  %d    E  =  %d    \n", TOT_N, TOT_E);
	if (write_time == 1)
		fprintf(out, "SOLUTIONTIME = %le \n", Time);
	fprintf(out, "\n");

	double u, v, Fr, uex, Z1, uu0, h0, eps_0, q_0, FR, m1;
	double xc, yc, omega, dp, pm, rho, r, derr, derr2;
	double tt, rr, para, parah, parar, radius, eta0, xi0;
	double einf = 0.;
	double e1 = 0;
	double u1 = 0.;
	double e2 = 0.;
	double u2 = 0.;
	double e3 = 0.;
	double e4 = 0.;
	double total_area = 0.;
	//KK = 0;
	double ll2 = 0.;
	double ll2h = 0.;
	//lev = 1;
	for (int n = 0; n < TOT_N; n++) {
		//x1 = node[n].coordinate[0];
		//x2 = node[n].coordinate[1];
		double h = node[n].P[2][0];


		/*if (initial_state == 13) {
			xc = node[n].coordinate_new[0] - 0.5;
			yc = node[n].coordinate_new[1] - 0.5;
			r = sqrt(xc * xc + yc * yc);
			rho = 4. * pi * r;
			omega = 15. * (1. + cos(rho));
			pm = 10.;
			dp = (225. / (gm * 16. * pi * pi)) * (2. * cos(rho) + 2. * rho * sin(rho)  + cos(2. * rho) / 8. + rho * sin(2. * rho) / 4. + rho * rho * 12. / 16.);
			dp -= (225. / (gm * 16. * pi * pi)) * (2. * cos(pi) + 2. * pi * sin(pi) + cos(2. * pi) / 8. + pi * sin(2. * pi) / 4. + pi * pi * 12. / 16.);
			if (r >= 0.25) {
				omega = 0.;
				dp = 0.;
			}

			derr = fabs(pm + dp - node[n].P[2][0]);
		}*/

		if (initial_state == 15) {
			xc = node[n].coordinate[0];
			yc = node[n].coordinate[1];
			tt = Time;
			rr = sqrt(xc * xc + yc * yc);
			parah = 0.1;
			para = 1.;
			parar = 0.8;
			omega = sqrt(8. * gm * parah / (para * para));
			dp = (para * para - parar * parar) / (para * para + parar * parar);
			Z1 = -1. + sqrt(1. - dp * dp) / (1. - dp * cos(omega * tt));
			Z1 += -rr * rr * (-1. + (1. - dp * dp) / ((1. - dp * cos(omega * tt)) * (1. - dp * cos(omega * tt)))) / (para * para);
			Z1 *= parah;
			Z1 -= node[n].bed[2] - 1.;

			if (Z1 < 0.)
				Z1 = 0.;

			Z1 += node[n].bed[2];
		}

		if (initial_state == 30) {
			// for bed = 1. - ( 2.*x + y )/( sqrt( 5. ) * 10*VVinf ) ;
			uu0 = pow(UUinf, 4. / 3.) / (10 * VVinf * manning * manning);
			uu0 = pow(uu0, 3. / 10.);
			Z1 = UUinf / uu0;
		}

		if (initial_state == 31) {
			// for bed = 1. - ( 2.*x + y )/( sqrt( 5. ) *10*VVinf ) ;
			uu0 = pow(UUinf, 4. / 3.) / (10 * VVinf * manning * manning);
			uu0 = pow(uu0, 3. / 10.);
			Z1 = UUinf / uu0;
		}

		if (h > cut_off_U * node[n].cut_off_ratio) {
			u = node[n].P[2][1] / h;
			v = node[n].P[2][2] / h;
			Fr = sqrt(u * u + v * v) / sqrt(gm * h);
		} else {
			u = 0;
			v = 0;
			Fr = 0;
		}

		if (fabs(u) <= 1.e-17)
			u = 0.;
		if (fabs(v) <= 1.e-17)
			v = 0.;
		if (Fr <= 1.e-17)
			Fr = 0.;


		if (initial_state == 15) {
			ll2h += fabs(h + node[n].bed[2] - Z1) * fabs(h + node[n].bed[2] - Z1);
			radius = sqrt(node[n].coordinate[0] * node[n].coordinate[0] + node[n].coordinate[1] * node[n].coordinate[1]);
			if (radius <= 0.75)
				ll2 += fabs(h + node[n].bed[2] - Z1) * fabs(h + node[n].bed[2] - Z1);
		}

		if (initial_state == 30)
			ll2h += fabs(h - Z1) * fabs(h - Z1);

		if (initial_state == 31)
			ll2h += fabs(h - Z1) * fabs(h - Z1);

		if (initial_state == 91) {
			fprintf(out, "%le %le %le %le %le %le %le %le %le %le %le %le %le %le\n", node[n].coordinate[0], node[n].coordinate[1], node[n].bed[2] + h, node[n].dxP[0][0] ,node[n].dyP[0][0],node[n].dxxP2[0][0],node[n].dyyP2[0][0],node[n].dxyP2[0][0],u,node[n].dxP[0][1],node[n].dyP[0][1],node[n].dxxP2[0][1],node[n].dyyP2[0][1],node[n].dxyP2[0][1]);
		} else {
			if (initial_state == 31)
				fprintf(out, "%le %le %le %le %le %le %le %d\n", node[n].coordinate[0], node[n].coordinate[1], node[n].bed[2] + h, h, u, v, node[n].bed[2], n);
			else if (initial_state == 75){ // rip channel
				if (time_step==0)
					fprintf(out, "%le %le %le %le %le %le %le %le %le %d %le %le %le\n",
							node[n].coordinate_new[0], node[n].coordinate_new[1], node[n].bed[2] + h, h, u, v, node[n].bed[2],
							(nht_phi?nht_phi[n]:0), (nht_phi?nht_phi[n+NN]:0), (nht_bb ? nht_bb[n]:0), (nht_k?nht_k[n]:0),
							node[n].u_ave, node[n].v_ave);
				else
					fprintf(out, "%le %le %le %le %le %le %le %le %le %d %le %le %le\n",
						node[n].coordinate_new[0], node[n].coordinate_new[1], node[n].bed[2] + h, h, u, v, node[n].bed[2],
						(nht_phi?nht_phi[n]:0), (nht_phi?nht_phi[n+NN]:0), (nht_bb?nht_bb[n]:0), (nht_k?nht_k[n]:0),
						node[n].u_ave/time_step, node[n].v_ave/time_step);
			}else{
				if(blending==3){
			    		if (output_step == 0 || model_dispersion ==0){
						
						fprintf(out, "%le %le %le %le %le %le %le %le %le %le %le %le %le %le %le %le %le %le %le %le %le %le %le %e\n",
							node[n].coordinate_new[0], node[n].coordinate_new[1], node[n].bed[2] + h, h, u, v, node[n].bed[2], node[n].dxP[0][0], node[n].dyP[0][0],node[n].dxxP2[0][0], node[n].dyyP2[0][0], node[n].dxyP2[0][0], node[n].dxP[0][1], node[n].dyP[0][1], node[n].dxxP2[0][1], node[n].dyyP2[0][1], node[n].dxyP2[0][1],node[n].dxP[0][2], node[n].dyP[0][2], node[n].dxxP2[0][2], node[n].dyyP2[0][2], node[n].dxyP2[0][2],INTrec[n],fabs(node[n].rhs[0]));
					}else{
						//fprintf(out, "%le %le %le %le %le %le %le %le %le %d %le\n",
							//node[n].coordinate_new[0], node[n].coordinate_new[1], node[n].bed[2] + h, h, u, v, node[n].bed[2],
							// //node[n].tt, node[n].shore, MAX(node[n].Grad, node[n].Hess), node[n].cut_off_ratio);
							//(nht_phi?nht_phi[n]:0), (nht_phi?nht_phi[n+NN]:0), (nht_bb?nht_bb[n]:0), (nht_e?nht_e[n]:0));
	
						fprintf(out, "%le %le %le %le %le %le %le %le %le %le %le %le %le %le %le %le %le %le %le %le %le %d %le\n",
							node[n].coordinate_new[0], node[n].coordinate_new[1], node[n].bed[2] + h, h, u, v, node[n].bed[2], node[n].dxP[0][0], node[n].dyP[0][0],node[n].dxxP2[0][0], node[n].dyyP2[0][0], node[n].dxyP2[0][0], node[n].dxP[0][1], node[n].dyP[0][1], node[n].dxxP2[0][1], node[n].dyyP2[0][1], node[n].dxyP2[0][1], INTrec[n],node[n].rhs[0], (nht_phi?nht_phi[n]:0), (nht_phi?nht_phi[n+NN]:0), (nht_bb?nht_bb[n]:0), (nht_e?nht_e[n]:0));
					}
				}else{
					fprintf(out, "%le %le %le %le %le %le %le \n", node[n].coordinate_new[0], node[n].coordinate_new[1], node[n].bed[2] + h, h, u, v, node[n].bed[2]); 
				}
			}
		}
	}

	if (initial_state == 13) {
	/*	for (int n = 0; n < NN; n++) {
			//if ( (node[n].coordinate[0]>0.05 && node[n].coordinate[0]<0.95) &&
			//	(node[n].coordinate[1]>0.05 && node[n].coordinate[1]<0.95) ){
				xc = node[n].coordinate_new[0] - 0.5;
				yc = node[n].coordinate_new[1] - 0.5;
				r = sqrt(xc * xc + yc * yc);
				rho = 4. * pi * r;
				omega = 15. * (1. + cos(rho));
				pm = 10.;
				dp = (225. / (gm * 16. * pi * pi)) * (2. * cos(rho) + 2. * rho * sin(rho) + cos(2. * rho) / 8. + rho * sin(2. * rho) / 4. + rho * rho * 12. / 16.);
				dp -= (225. / (gm * 16. * pi * pi)) * (2. * cos(pi) + 2. * pi * sin(pi) + cos(2. * pi) / 8. + pi * sin(2. * pi) / 4. + pi * pi * 12. / 16.);

				if (r >= 0.25) {
					omega = 0.;
					dp = 0.;
				}

				derr = fabs(pm + dp - node[n].P[2][0]);
				uex = fabs(pm + dp);

				if (derr > einf)
					einf = derr;

				e1 += derr * node[n].mod_volume;
				u1 += uex * node[n].mod_volume;
				e2 += derr * derr * node[n].mod_volume;
				u2 += uex * uex * node[n].mod_volume;
				e3 += derr * derr;
				e4 += derr * derr * node[n].mod_volume;
				total_area += node[n].mod_volume;
			}
		//}
		printf("\n");
		printf("\n");
		printf("Error w.r.t. initial solution at time %le\n", Time);
		printf("Error in Linf norm %le\n", log10(einf));
		printf("Error in L1 norm [dim] [adim]: %le %le\n", log10(e1), log10(e1 / u1));
		printf("Error in L2 norm [dim] [adim]: %le %le\n", log10(sqrt(e2)), log10(sqrt(e2) / sqrt(u2)));
		printf("Error in Standard way MARIA: %le \n", log10(sqrt(e3/NN)));
		printf("Error in Weighted way MARIA: %le \n", log10(sqrt(e4/total_area)));
		printf("\n");
		printf("Mesh  %le\n", log10(gridsize));
		printf("\n");
		printf("\n");
		FILE *conv;
		conv = fopen( "../output/convergence_info.dat", "w" );
		fprintf(conv,"\n");
		fprintf(conv,"\n");
		fprintf(conv,"Error w.r.t. initial solution at time %le\n", Time);
		fprintf(conv,"Error in Linf norm %le\n", log10( einf) );
		fprintf(conv,"Error in L1 norm [dim] [adim]: %le %le\n", log10(e1), log10( e1/u1 ));
		fprintf(conv,"Error in L2 norm [dim] [adim]: %le %le\n", log10(sqrt(e2)), log10(sqrt(e2)/sqrt(u2)));
		fprintf(conv,"\n");
		fprintf(conv,"Mesh  %le\n", log10( gridsize ) );
		fprintf(conv,"\n");
		fprintf(conv,"\n");
		fclose(conv);  */
	}

	if (initial_state == 30) {
		ll2h /= 1. * TOT_N;
		ll2h = sqrt(ll2h);
		printf("l2 error on the height =%le\n", log10(ll2h));
	}

	if (initial_state == 15) {
		ll2h /= 1. * TOT_N;
		ll2h = sqrt(ll2h);
		ll2 /= 1. * TOT_N;
		ll2 = sqrt(ll2);
		printf("l2 error on the free surface =%le\n", log10(ll2h));
		printf("local l2 error on the free surface =%le\n", log10(ll2));
	}

	if (initial_state == 31) {
		ll2h /= 1. * TOT_N;
		ll2h = sqrt(ll2h);
		printf("l2 error on the height =%le\n", log10(ll2h));
	}

	if ( initial_state == 70 ) {
                derr=0.;
	        FILE *err = fopen( "../output/error.dat", "w" );
		fprintf( err, "TITLE      =  Unstructured grid data \n");
		fprintf( err, "VARIABLES  =  X  Y  H  U  V  href  err\n");
		fprintf( err, "ZONE    F = FEPOINT    ET = TRIANGLE N  =  %d    E  =  %d    \n", TOT_N, TOT_E ) ;
		fprintf( err, "\n");
		// Error respect to the analytical solution
        	double a = 0.045;
	        double d = 0.45;
        	double g = 9.81;
	        double k = sqrt(3.*a)/(2.*d*sqrt(d + a));
        	double c = sqrt(g*(d + a));
	        double x0 = 35.;
		for (int n = 0 ; n < NN ; n++) {
			if (node[n].coordinate[1]==0.4 && (node[n].coordinate[0]>10 || node[n].coordinate[0]<60)){
				double x = node[n].coordinate[0];
				double X = x - x0 - c*Time;
				double sech = 1/cosh(k*X);
				double eta = a*sech*sech;
				double h = d + eta;
				derr = fabs ( h - node[n].P[2][0] ) ;
				uex = fabs(h);
				if ( derr > einf )
					einf = derr;

			/*	e1 += derr*node[n].volume ;
				u1 += uex*node[n].volume ;

				e2 += derr*derr*node[n].volume ;
				u2 += uex*uex*node[n].volume ; */

				e1 += derr * node[n].mod_volume;
				u1 += uex * node[n].mod_volume;

				e2 += derr * derr * node[n].mod_volume;
				u2 += uex * uex * node[n].mod_volume;

				e3 += derr * derr;
				e4 += derr ;

				total_area += node[n].mod_volume;

				fprintf( err, "%le %le %le %le %le %le %le\n",node[n].coordinate_new[0], node[n].coordinate_new[1], node[n].P[2][0],
			              node[n].P[2][1]/node[n].P[2][0], node[n].P[2][2]/node[n].P[2][0], h, derr) ;
			}
		};
		for (int e = 0; e < NE; e++)
			fprintf(err, "%d %d %d \n", element[e].node[0] + 1, element[e].node[1] + 1, element[e].node[2] + 1);
		fclose(err);

		// Error respect to a reference solution (1D GNTD)
		/* if(iteration == 0) {
			FILE *ref;
			double dummy1,dummy2;

			ref = fopen("reference_sol.dat","r");
			if (!ref)
				printf("ERROR: External file reference_sol.dat was not found !\n");

			fscanf(ref,"%lf %lf\n",&dummy1,&dummy2);
			int DIM = (int)floor(dummy1);

			double *pos = MA_vector( DIM, sizeof( double ) ) ;
			double *href = MA_vector( DIM, sizeof( double ) ) ;
			node_href = MA_vector( NN, sizeof( double ) ) ;

			int k = 0;
			while(fscanf(ref,"%le %le\n",&pos[k],&href[k]) == 2)
				k++;

			fclose(ref);

			for (int n=0; n<NN; n++) {
				double x = node[n].coordinate_new[0];
				for (int m=0; m<DIM; m++) {
					if ( ((x>=pos[m]) && (x<pos[m+1])) || x==pos[DIM-1] )
						node_href[n] = interpolation(x, href, m, pos, DIM);
				}
			}
			MA_free( href );
			MA_free( pos );
		}

		for (int n = 0; n < NN ; n++) {
			h = node_href[n];
			derr = fabs ( h - node[n].P[2][0] ) ;
			uex = fabs(h);
			if ( derr > einf )
				einf = derr ;

   			e1 += derr*node[n].volume ;
			u1 += uex*node[n].volume ;

			e2 += derr*derr*node[n].volume ;
			u2 += uex*uex*node[n].volume ;

			fprintf( err, "%le %le %le %le %le %le %le\n",node[n].coordinate_new[0], node[n].coordinate_new[1], node[n].P[2][0],
				node[n].P[2][1]/node[n].P[2][0], node[n].P[2][2]/node[n].P[2][0], h, derr) ;
		}
			MA_free( node_href );
		for ( int e = 0 ; e < NE ; e++ )
			fprintf( err, "%d %d %d \n", element[e].node[0] + 1,element[e].node[1] + 1,element[e].node[2] + 1 );
		*/
		printf("\n") ;
		printf("\n") ;
		printf("Error w.r.t. initial solution at time %le\n", Time) ;
		printf("Error in Linf norm %le\n", log10( einf) ) ;
		//printf("Error in L1 norm [dim] [adim]: %le %le\n", log10(e1), log10( e1/u1 )) ;
		//printf("Error in L2 norm [dim] [adim]: %le %le\n", log10(sqrt(e2)), log10( sqrt(e2)/sqrt(u2) )) ;
		printf("Error in L1 norm [dim] : %le \n", log10(e4) ) ;
		printf("Error in L2 norm [dim] : %le \n", log10(sqrt(e3)) ) ;
		printf("Error in L1 norm [weigthed]: %le \n",  log10(e1/total_area) ) ;
		printf("Error in L2 norm [weigthed]: %le \n",  log10(sqrt(e2/total_area)) ) ;
		printf("\n") ;
		printf("Mesh  %le\n", log10( gridsize ) );
		printf("\n") ;
		printf("\n") ;

		/*FILE *fp = fopen( "../output/convergence_info.dat", "w" ) ;
		fprintf(fp,"\n") ;
		fprintf(fp,"\n") ;
		fprintf(fp,"Error w.r.t. initial solution at time %le\n", Time) ;
		fprintf(fp,"Error in Linf norm %le\n", log10( einf) ) ;
		//fprintf(fp,"Error in L1 norm [dim] [adim]: %le %le\n", log10(e1), log10( e1/u1 )) ;
		//fprintf(fp,"Error in L2 norm [dim] [adim]: %le %le\n", log10(sqrt(e2)), log10(sqrt(e2)/sqrt(u2))) ;
		fprintf(fp,"Error in L1 norm [dim]: %le\n", log10(e4)) ;
		fprintf(fp,"Error in L2 norm [dim]: %le\n", log10(sqrt(e3))) ;
		fprintf(fp,"Error in L1 norm [/NN]: %le\n", log10(e4/NN)) ;
		fprintf(fp,"Error in L2 norm [/NN]: %le\n", log10(sqrt(e3/NN))) ;
		fprintf(fp,"Error in L1 norm [weighted]: %le\n",  log10(e1/total_area)) ;
		fprintf(fp,"Error in L2 norm [weigthed]: %le\n", log10(sqrt(e2/total_area))) ;
		fprintf(fp,"\n") ;
		fprintf(fp,"Mesh  %le\n", log10( gridsize ) );
		fprintf(fp,"\n") ;
		fprintf(fp,"\n") ;
		fclose(fp);*/
	}

	for (int e = 0; e < NE; e++)
		fprintf(out, "%d %d %d \n", element[e].node[0] + 1, element[e].node[1] + 1, element[e].node[2] + 1);

	/*if ( initial_state == 4 ) {
		einf = -10. ;
		e1 = -10. ;
		for ( n = 0 ; n < NN ; n ++ ) {
			if ( fabs( node[n].P[2][0] + node[n].bed[2] - 1. ) > einf )
				einf =  fabs( node[n].P[2][0] + node[n].bed[2] - 1. );
			if ( fabs( node[n].P[2][1]/node[n].P[2][0] ) > e1 )
				e1 = fabs( node[n].P[2][1] );
			if ( fabs( node[n].P[2][2]/node[n].P[2][0] ) > e1 )
				e1 = fabs( node[n].P[2][2] );
		}
		printf("err \n");
		printf("err on H = %le\n",einf);
		printf("err on v = %le\n",e1);
		printf("err \n");
	} */

	fclose(out);
	if( 24 == initial_state) {
		FILE *out = fopen("bathy_lynett.dat", "w");
		for(int i=0; i<NN; i++)
			fprintf(out,"%le\n",node[i].bed[2]);
		fclose(out);
	}
}

void read_solution_dat(void)
{
	printf("          *************************************\n");
	printf("          **     Reading tecplot file.....   **\n");
	printf("          *************************************\n");
	printf("\n");
	char init_file_name[MAX_CHAR];
	if ( 85 == initial_state ){ // Convergent channel zoom
		sprintf(init_file_name, "../input/%s.dat", initial_solution_file);
	} else
		sprintf(init_file_name, "../output/%s.dat", initial_solution_file);

	int NTOT = NN;
	FILE *fp = fopen(init_file_name, "r");
		
	if ( 85 == initial_state ){ // Convergent channel zoom
		double XX[36001], HH[36001], UU[36001], VV[36001];
		for(int n = 0; n < 36001; n++){
			fscanf(fp,"%le %le %le %le\n",&XX[n],&HH[n],&UU[n],&VV[n]);
		}
		for(int n = 0; n < NN; n++){
			double x = node[n].coordinate[0];
			for (int m = 0; m < 36001; m++){
				if (fabs(x-XX[m])<0.000001){
					node[n].P[2][0] = HH[m];
					node[n].P[2][1] = UU[m]*HH[m];
					node[n].P[2][2] = VV[m]*HH[m];
				}
			}
		}

		fclose(fp);
	} else {
		//fscanf(fp, "TITLE      =  Unstructured grid data \n");
		size_t buf_size = 512;
		char* pipes = (char*)malloc(buf_size * sizeof(char) );
		getline(&pipes, &buf_size, fp);
	
		if( model_dispersion ==0) 
			//fscanf(fp, "VARIABLES  =  X  Y  HT  H  U  V  BED \n");
			//fscanf(fp, "VARIABLES  =  X  Y  HT  H  U  V  BED Hx Hy Hxx Hyy Hxy Ux Uy Uxx Uyy Uxy Error Ansol PHIX PHIY bb e \n");
			getline(&pipes, &buf_size, fp);
		else{
			if(initial_state == 75 ) 
				fscanf(fp, "VARIABLES  =  X  Y  HT  H  U  V  BED PHIX PHIY bb k Uave Vave\n");
			else
				//fscanf(fp, "VARIABLES  =  X  Y  HT  H  U  V  BED Hx Hy Hxx Hyy Hxy Ux Uy Uxx Uyy Uxy Error Ansol PHIX PHIY bb e \n");
				getline(&pipes, &buf_size, fp);
		}

		free(pipes);
		//fscanf( initial, "VARIABLES  =  x  y  h  u  v  Fr  bed  Htot  theta\n"                                               ) ;
		int dummy_i, NN_file;
		fscanf(fp, "ZONE    F = FEPOINT    ET = TRIANGLE N  =  %d    E  =  %d \n", &NN_file, &dummy_i);

		double dummy_d;
		if (write_time == 1)
			fscanf(fp, "SOLUTIONTIME = %le \n", &dummy_d);
		fscanf(fp, "\n");
		if ( 85 != initial_state ){ // convergent channel zoom
			for (int n = 0; n < NTOT; n++) {
				double h, u, v, Fr;
				if( 1 == model_dispersion) {
					if (initial_state == 75)
						fscanf(fp, "%le %le %le %le %le %le %le %le  %le %d %le %le %le \n",
				       			&dummy_d, &dummy_d, &dummy_d, &h, &u, &v, &node[n].bed[2], &dummy_d, &dummy_d, &dummy_i, &dummy_d, &dummy_d, &dummy_d);
					else
						fscanf(fp, "%le %le %le %le %le %le %le %le  %le %le %le %le %le %le %le %le %le %le %le %le %le %d %le  \n",&dummy_d, &dummy_d, &dummy_d, &h, &u, &v, &node[n].bed[2], &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_i, &dummy_d);
				}else{
						fscanf(fp, "%le %le %le %le %le %le %le %le  %le %le %le %le %le %le %le %le %le %le %le %le %le %d %le  \n",&dummy_d, &dummy_d, &dummy_d, &h, &u, &v, &node[n].bed[2], &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_i, &dummy_d);

					//	fscanf(fp, "%le %le %le %le %le %le %le %le  %le %le %le %le %le %le %le %le %le %le %le %le %le %le %le %le \n",&dummy_d, &dummy_d, &dummy_d, &h, &u, &v, &node[n].bed[2], &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d, &dummy_d);
				}
				
				//fscanf( initial, "%le %le %le %le %le %le %le %le %le\n", &dummy_d, &dummy_d, &h, &u, &v, &Fr, &node[n].bed[2], &dummy_d, &dummy_d) ;
	
				node[n].P[2][0] = h;
				node[n].P[2][1] = u * h;
				node[n].P[2][2] = v * h;
			}
			fclose(fp);
		} else {
			double x[NN_file], y[NN_file], h[NN_file], u[NN_file], v[NN_file];
			for (int n = 0; n < NN_file; n++) {
				fscanf(fp, "%le %le %le %le %le %le %le %le  %le %d %le %le %le \n", &x[n], &y[n], &dummy_d, &h[n], &u[n], &v[n], &dummy_d, &dummy_d, &dummy_d, &dummy_i, &dummy_d, &dummy_d, &dummy_d);
			}
			fclose(fp);
	
			// Take nodes on the x axis:
			double XX[3001], HH[3001], UU[3001], VV[3001];
			int i = 0;
			for (int n = 0; n < NN_file; n++){
				if (fabs(y[n]-0.)<= 0.0000001){
					XX[i] = x[n];
					HH[i] = h[n];
					UU[i] = u[n];
					VV[i] = v[n];
					i += 1;
				}
			}

			// Order the vector befor interpolation:
			for (int i = 0; i < 3001; ++i){
	        		for (int j = i + 1; j < 3001; ++j){
            				if (XX[i] > XX[j]){
	                			double a =  XX[i];
		                		double b =  HH[i];
        		        		double c =  UU[i];
                				double d =  VV[i];
                				XX[i] = XX[j];
                				HH[i] = HH[j];
	        	        		UU[i] = UU[j];
		                		VV[i] = VV[j];
         			       		XX[j] = a;
                				HH[j] = b;
                				UU[j] = c;
	                			VV[j] = d;
	        	    		}
       				}
		   	}
		
			printf("%le %le\n",XX[0],XX[3000]);
			double Mass_tot_before = 0.;
			double Flux_tot_before = 0.;
			int count = 0;
			for (int n = 0 ; n<3001; n++){
				if (XX[n]-0.9>=0. && XX[n]-1.8<=0.){
					Mass_tot_before += HH[n]*(XX[2] - XX[1]);
					Flux_tot_before += HH[n]*UU[n]*(XX[2] - XX[1]);
					count += 1;
				}
			}
			printf("Prima %le  %le %d %le\n",Mass_tot_before, Flux_tot_before, count, (XX[2] - XX[1]));

			// Interpolation
/*			for (int n = 0 ; n<NN; n++){
				for (int m = 0; m<3001; m++){
					if (XX[m]-node[n].coordinate[0]<=0.0000001 && XX[m+1]-node[n].coordinate[0]>0.0000001){
						node[n].P[2][0] = HH[m] + (HH[m] - HH[m+1])/(XX[m] - XX[m+1])*(node[n].coordinate[0] - XX[m]);
						node[n].P[2][1] = node[n].P[2][0]*(UU[m] + (UU[m] - UU[m+1])/(XX[m] - XX[m+1])*(node[n].coordinate[0] - XX[m]));
						node[n].P[2][2] = node[n].P[2][2]*(VV[m] + (VV[m] - VV[m+1])/(XX[m] - XX[m+1])*(node[n].coordinate[0] - XX[m]));
					}	
				}
			}
*/			for (int n=0; n<NN; n++) {
				double x = node[n].coordinate[0];
				for (int m=0; m<3001; m++) {
					if (XX[m]-x<=0.0000001 && XX[m+1]-x>0.0000001){
						node[n].P[2][0] = interpolation(x,HH,m,XX,3001);
						node[n].P[2][1] = interpolation(x,UU,m,XX,3001);
						node[n].P[2][2] = interpolation(x,VV,m,XX,3001);
					}
				}
			}
			count = 0;
			double Mass_tot_after = 0.;
			double Flux_tot_after = 0.;
			for (int n = 0 ; n<NN; n++){
				if (fabs(node[n].coordinate[1] - 0.)<=0.000001){
					Mass_tot_after += node[n].P[2][0]*(node[4].coordinate[0] - node[0].coordinate[0]);
					Flux_tot_after += node[n].P[2][1]*(node[4].coordinate[0] - node[0].coordinate[0]);
					count += 1;
				}
			}
			printf("Dopo %le %le %d %le\n",Mass_tot_after, Flux_tot_after, count, (node[4].coordinate[0] - node[0].coordinate[0]));
		}
	}
}

void write_initial_solution_dat(void)
{
	int TOT_N = NN;
	int TOT_E = NE;
	printf("          *************************************\n");
	printf("          **     Writing tecplot file.....   **\n");
	printf("          *************************************\n");
	printf("\n");
	char tecplot_file_name[MAX_CHAR];
	sprintf(tecplot_file_name, "../output/initial_solution.dat");

	FILE* fp = fopen(tecplot_file_name, "w");
	fprintf(fp, "TITLE      =  Unstructured grid data \n");
	fprintf(fp, "VARIABLES  =  x  y  h  u  v  Fr  bed  Htot\n");
	fprintf(fp, "ZONE    N  =  %d    E  =  %d    F = FEPOINT    ET = TRIANGLE \n", TOT_N, TOT_E);
	fprintf(fp, "\n");

	for (int n = 0; n < TOT_N; n++) {
		double h = node[n].P[2][0];
		double u = node[n].P[2][1] / h;
		double v = node[n].P[2][2] / h;
		double Fr = sqrt(u * u + v * v) / sqrt(gm * h);
		fprintf(fp, "%le %le %le %le %le %le %le %le\n",
		             node[n].coordinate[0], node[n].coordinate[1], h, u, v, Fr, node[n].bed[2], node[n].bed[2] + h);
	}

	for (int e = 0; e < NE; e++)
		fprintf(fp, "%d %d %d \n", element[e].node[0] + 1, element[e].node[1] + 1, element[e].node[2] + 1);

	fclose(fp);
}

void sample_probes(void)
{
	char series_file_name[MAX_CHAR];
	FILE* timeseries1; 

	if (75 == initial_state) { // rip current
		double t_out = 0.;
		if (Time >= t_out) {
			for (int n = 0; n < Nprobes; n++) {
			//for (int n = 0; n < Nprobes/2; n++) {
				int m = probes[n].node;
				sprintf(series_file_name, "../output/%s_probe%d.dat", out_file, n);
				timeseries1 = fopen(series_file_name, "a");
				fprintf(timeseries1, "%le %le %le %le\n", Time, node[m].coordinate[0], node[m].P[2][0], node[m].bed[2]);
				fclose(timeseries1);
			}
		}
	}
	if (initial_state == 81) { // elliptic_shoal

		double t_out = 5.;
		if (Time >= t_out) {
			/* Section 1: y = 1 ; -5 < x < 5 */
			sprintf(series_file_name, "../output/%s_section1.dat", out_file);
			timeseries1 = fopen(series_file_name, "a");
			for (int n = 0; n < 50; n++) {
				int m = probes[n].node;
				fprintf(timeseries1, "%le %le %le %le\n", Time, node[m].coordinate[0], node[m].P[2][0], node[m].bed[2]);
			}
			fclose(timeseries1);

			/* Section 2: y = 3 ; -5 < x < 5 */
			sprintf(series_file_name, "../output/%s_section2.dat", out_file);
			timeseries1 = fopen(series_file_name, "a");
			for (int n = 50; n < 100; n++) {
				int m = probes[n].node;
				fprintf(timeseries1, "%le %le %le %le\n", Time, node[m].coordinate[0], node[m].P[2][0], node[m].bed[2]);
			}
			fclose(timeseries1);

			/* Section 3: y = 5 ; -5 < x < 5 */
			sprintf(series_file_name, "../output/%s_section3.dat", out_file);
			timeseries1 = fopen(series_file_name, "a");
			for (int n = 100; n < 150; n++) {
				int m = probes[n].node;
				fprintf(timeseries1, "%le %le %le %le\n", Time, node[m].coordinate[0], node[m].P[2][0], node[m].bed[2]);
			}
			fclose(timeseries1);

			/* Section 4: y = 7 ; -5 < x < 5 */
			sprintf(series_file_name, "../output/%s_section4.dat", out_file);
			timeseries1 = fopen(series_file_name, "a");
			for (int n = 150; n < 200; n++) {
				int m = probes[n].node;
				fprintf(timeseries1, "%le %le %le %le\n", Time, node[m].coordinate[0], node[m].P[2][0], node[m].bed[2]);
			}
			fclose(timeseries1);

			/* Section 5: y = 9 ; -5 < x < 5 */
			sprintf(series_file_name, "../output/%s_section5.dat", out_file);
			timeseries1 = fopen(series_file_name, "a");
			for (int n = 200; n < 250; n++) {
				int m = probes[n].node;
				fprintf(timeseries1, "%le %le %le %le\n", Time, node[m].coordinate[0], node[m].P[2][0],node[m].bed[2]);
			}
			fclose(timeseries1);

			/* Section 6: x = -2 ; 0 < y < 10 */
			sprintf(series_file_name, "../output/%s_section6.dat", out_file);
			timeseries1 = fopen(series_file_name, "a");
			for (int n = 250; n < 300; n++) {
				int m = probes[n].node;
				fprintf(timeseries1, "%le %le %le %le\n", Time, node[m].coordinate[1], node[m].P[2][0],node[m].bed[2]);
			}
			fclose(timeseries1);

			/* Section 7: x = 0 ; 0 < y < 10 */
			sprintf(series_file_name, "../output/%s_section7.dat", out_file);
			timeseries1 = fopen(series_file_name, "a");
			for (int n = 300; n < 350; n++) {
				int m = probes[n].node;
				fprintf(timeseries1, "%le %le %le %le\n", Time, node[m].coordinate[1], node[m].P[2][0],node[m].bed[2]);
			}
			fclose(timeseries1);

			/* Section 8: x = 2 ; 0 < y < 10 */
			sprintf(series_file_name, "../output/%s_section8.dat", out_file);
			timeseries1 = fopen(series_file_name, "a");
			for (int n = 350; n < 400; n++) {
				int m = probes[n].node;
				fprintf(timeseries1, "%le %le %le %le\n", Time, node[m].coordinate[1], node[m].P[2][0],node[m].bed[2]);
			}
			fclose(timeseries1);

		}
		/* inlet region */
		sprintf(series_file_name, "../output/%s_incoming3.dat", out_file);
		timeseries1 = fopen(series_file_name, "a");
		int m = probes[400].node;
		fprintf(timeseries1, "%le %le %le\n", Time, node[m].coordinate[0], node[m].P[2][0]);
		fclose(timeseries1);
	}

	if (82 == initial_state) { // circ shoal
		double t_out = 0.;
		if (Time >= t_out) {
			for (int n = 0; n < Nprobes; n++) {
				int m = probes[n].node;
				sprintf(series_file_name, "../output/%s_probe%d.dat", out_file, n);
				timeseries1 = fopen(series_file_name, "a");
				fprintf(timeseries1, "%le %le %le\n", Time, node[m].P[2][0], node[m].coordinate[0]);
				fclose(timeseries1);
			}
		}
	}
	if (93 == initial_state) { // solitary and cylinder
		double t_out = 0.0;
		if (Time >= t_out) {
			for (int n = 0; n < 6; n++) {
				int m = probes[n].node;
				sprintf(series_file_name, "../output/%s_probe%d.dat", out_file, n);
				timeseries1 = fopen(series_file_name, "a");
				fprintf(timeseries1, "%le %le %le %le\n", Time, node[m].P[2][0], node[m].coordinate[0],node[m].bed[2]);
				fclose(timeseries1);
			}
		}
	}
}

void read_spectrum(void){
	FILE* inletfile = fopen("../input/spectrum.dat", "r");
	double dummy;
	fscanf(inletfile, "%d  %le %le %le %le\n", &freq, &dummy, &dummy, &dummy, &dummy);
	fscanf(inletfile, "%le  %le %le %le %le\n", &dummy, &dummy, &dummy, &dummy, &dummy);
	freq = 24;
	angle =72;

                        for (int i = 0; i <= freq*angle-1 ; i++){
                                fscanf(inletfile, "%le %le %le %le %le\n", &a_wg[i], &t_wg[i],&direction[i],&l_wg[i],&dummy);
                        }
                        fclose(inletfile);

}
