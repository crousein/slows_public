/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/
	


/***************************************************************************
                              select_function.c
                              -----------------
          This is select_function: function pointers are assigned here
                             -------------------
***************************************************************************/
#include <stdio.h>
#include "common.h"

int kmax;
extern int problem, initial_state, blending, time_discretization, scheme;

void (*supersonic_residual) (int);	/* node global index  */

void supersonic_residual_std(int);

/* INITIAL STATE dependent function */
double (*source) (double, double);

double source_wedge(double, double);
double source_1d(double, double);
double source_1d_hump(double, double);
double source_2d_hump(double, double);
double source_lake(double, double);
double source_2d_smooth(double, double);
double source_dam(double, double);
double source_conical_island(double, double);
double source_thacker(double, double);
double source_shelf(double, double);
double source_steady_exner(double, double);
double source_okushiri(double, double);
double source_slope(double, double);
double source_reef(double, double);
double source_run_up(double, double);
double source_solitary(double, double);
double source_convergent_channel(double, double);
double source_gironde(double, double);
double source_rip_channel(double, double);
double source_synolakis(double, double);
double source_volker_reef(double, double);
double source_bar(double, double);
double source_shoaling(double, double);
double source_elliptic_shoal(double, double);
double source_circ_shoal(double, double);
double source_oregon(double, double);
double source_shelf_1d(double, double);
double source_hansen(double, double);
double source_grilli(double, double);
double source_biarritz(double, double);
double source_cylinder(double, double);

void (*char_bc) (int);
void (*char_bc_4prj) (int);

void char_bc_2d_smooth(int);
void char_bc_wedge(int);
void char_bc_1d(int);
void char_bc_dam(int);
void char_bc_lake(int);
void char_bc_1d_dam(int);
void char_bc_solitary(int);
void char_bc_conical_island(int);
void char_bc_conical_island_4prj(int);
void char_bc_hump(int);
void char_bc_hump_2d(int);
void char_bc_thacker(int);
void char_bc_steady_exner(int);
void char_bc_shelf(int);
void char_bc_okushiri(int);
void char_bc_okushiri_4prj(int);
void char_bc_slope(int);
void char_bc_reef(int);
void char_bc_run_up(int);
void char_bc_convergent_channel(int);
void char_bc_gigado(int);
void char_bc_gironde(int);
void char_bc_garonne(int);
void char_bc_dordogne(int);
void char_bc_rip_channel(int);
void char_bc_synolakis(int);
void char_bc_volker_reef(int);
void char_bc_bar(int);
void char_bc_shoaling(int);
void char_bc_elliptic_shoal(int);
void char_bc_circ_shoal(int);
void char_bc_oregon(int);

/* MESH MOVEMENT dependent functions */

void (*adaptive_mesh_refinement) (void);
void (*initial_mesh) (void);
void (*initialize_step) (void);
void (*project_vars) (void);
void (*preparing_timestepper) (void);
void (*move_the_mesh) (void);
void (*param_adaptation) (void);
void (*distribute_ht_projection) (int, double, double *);
void (*distribute_full_projection) (int);


/* SCHEME dependent functions */
void (*STfluctuation) (int);
void (*set_numerical_integration) (void);
void (*compute_shoreflux) (int);
void (*local_average) (int, int);
void (*distribute_fluctuation) (int);
void (*compute_upwind_Keys) (int);
void (*gradient_reconstruction) (int, int, int);
void (*bathy_quadrature) (void);
void (*read_solution) (void);
void (*write_solution) (void);

//void compute_upwind_Keys_LF_D(struct element_struct*);
void STfluctuation_CN(int);
void set_RD_numerical_integration(void);
void set_FV_numerical_integration(void);
void compute_RD_shoreflux(int);
void compute_FV_shoreflux(int);
void RD_local_average(int, int);
void FV_local_average(int, int);
void distribute_RD_fluctuation(int);
void distribute_FV_fluctuation(int);
void distribute_FV_fluctuation_V2(int);
void compute_RD_upwind_Keys(int);
void compute_FV_upwind_Keys(int);
void vars_reconstruction(int, int, int);
void RD_bathy_quadrature(void);
void FV_bathy_quadrature(void);
void read_solution_dat(void);
void write_solution_dat(void);

void select_function(void)
{
	if (0 == blending) {
		set_numerical_integration = set_RD_numerical_integration;
		STfluctuation = STfluctuation_CN;
		local_average = RD_local_average;
		compute_upwind_Keys = compute_RD_upwind_Keys;
		distribute_fluctuation = distribute_RD_fluctuation;
		gradient_reconstruction = NULL;
		bathy_quadrature = RD_bathy_quadrature;
		compute_shoreflux = compute_RD_shoreflux;
	} else if (1 == blending) {
		set_numerical_integration = set_RD_numerical_integration;
		STfluctuation = STfluctuation_CN;
		local_average = RD_local_average;
		compute_upwind_Keys = compute_RD_upwind_Keys;
		distribute_fluctuation = distribute_RD_fluctuation;
		gradient_reconstruction = NULL;
		bathy_quadrature = RD_bathy_quadrature;
		compute_shoreflux = compute_RD_shoreflux;
	} else if (2 <= blending) {
		set_numerical_integration = set_FV_numerical_integration;
		STfluctuation = NULL;
		local_average = FV_local_average;
		compute_upwind_Keys = compute_FV_upwind_Keys;
		if(2 == blending) 
			distribute_fluctuation = distribute_FV_fluctuation;
		else if (3==blending)
			distribute_fluctuation = distribute_FV_fluctuation;
		gradient_reconstruction = vars_reconstruction;
		bathy_quadrature = FV_bathy_quadrature;
		compute_shoreflux = compute_FV_shoreflux;
	}

	/* INITIAL STATE DEPENDENT SETTINGS */
	if (1 == initial_state) {
		source = source_wedge;
		char_bc = char_bc_wedge;
	} else if (2 == initial_state) { //1D drain
		source = source_1d;
		char_bc = char_bc_1d;
	} else if (3 == initial_state) { // 1d dam break
		source = source_dam;
		char_bc = char_bc_1d_dam;
	} else if (4 == initial_state) { // pertubation over smooth bed
		source = source_2d_smooth;
		char_bc = char_bc_2d_smooth;
	} else if (5 == initial_state) { //circular dam break
		source = source_dam;
		char_bc = char_bc_dam;
	} else if (60 == initial_state) { //vortex test case with wall on the bounary (0,2 domain)
		source = source_solitary;
		char_bc = char_bc_solitary;
	} else if (7 == initial_state) { //asymmetric dambreak
		source = source_dam;
		char_bc = char_bc_dam;
	} else if (8 == initial_state) { //conical island [Bri:95]
		source = source_conical_island;
		char_bc = char_bc_conical_island;
		char_bc_4prj = char_bc_conical_island_4prj;
	} else if (9 == initial_state) {
		source = source_lake;
		char_bc = char_bc_lake;
	} else if (10 == initial_state) {
		source = source_1d_hump;
		char_bc = char_bc_hump;
	} else if (11 == initial_state) {
		source = source_2d_hump;
		char_bc = char_bc_hump_2d;
	} else if (13 == initial_state) {
		source = source_wedge;
		char_bc = char_bc_wedge;
	} else if (15 == initial_state) { //parabolic oscillation 
		source = source_thacker;
		char_bc = char_bc_thacker;
	} else if (16 == initial_state) { //parabolic oscillation
		source = source_thacker;
		char_bc = char_bc_thacker;
	} else if (20 == initial_state) {
		source = source_steady_exner;
		char_bc = char_bc_steady_exner;
	} else if (22 == initial_state) {
		source = source_reef;
		char_bc = char_bc_reef;
	} else if (23 == initial_state) { //1D runup [Carriere:03]
		source = source_run_up;
		char_bc = char_bc_run_up;
	} else if (24 == initial_state) { //shelf with island
		source = source_shelf;
		char_bc = char_bc_shelf;
	} else if (25 == initial_state) { //okushiri
		source = source_okushiri;
		char_bc = char_bc_okushiri;
		char_bc_4prj = char_bc_okushiri_4prj;
	} else if (30 == initial_state) {
		source = source_slope;
		char_bc = char_bc_slope;
	} else if (31 == initial_state) {
		source = source_slope;
		char_bc = char_bc_slope;
	} else if (34 == initial_state) { //double dambreak
		source = source_dam;
		char_bc = char_bc_dam;
	} else if (70 == initial_state) { //gn solitary wave propagation
		source = source_solitary;
		char_bc = char_bc_solitary;
	} else if ( (71 == initial_state) || (85 == initial_state) || (86 == initial_state) ) { //convergent channel // Tissier ondular bore
		source = source_convergent_channel;
		char_bc = char_bc_convergent_channel;
	} else if (72 == initial_state) { // gironde
		source = source_gironde;
		char_bc = char_bc_gironde; 
	} else if (73 == initial_state) { // garonne
		source = source_gironde ;
		char_bc = char_bc_garonne ; 
	} else if (74 == initial_state) { // dordogne
		source = source_gironde ;
		char_bc = char_bc_dordogne ; 
	} else if ( 75 == initial_state)  { // rip channel
		source = source_rip_channel;
		char_bc = char_bc_rip_channel; 
	} else if ( 76 == initial_state)  { // synolakis
		source = source_synolakis;
		char_bc = char_bc_synolakis; 
	} else if ( 77 == initial_state)  { // volker reef
		source = source_volker_reef;
		char_bc = char_bc_volker_reef; 
	} else if ( 78 == initial_state)  { // wave over a bar
		source = source_bar;
		char_bc = char_bc_bar; 
	} else if ( 79 == initial_state)  { // gironde-garonne-dordogne
		source = source_gironde;
		char_bc = char_bc_gigado; 
	} else if ( 80 == initial_state)  { // gironde-garonne-dordogne
		source = source_shoaling;
		char_bc = char_bc_shoaling; 
	} else if ( 81 == initial_state)  { // elliptic shoal
		source = source_elliptic_shoal;
		char_bc = char_bc_elliptic_shoal; 
	} else if ( 82 == initial_state)  { // circ shoal
		source = source_circ_shoal;
		char_bc = char_bc_circ_shoal; 
	} else if ( 83 == initial_state) { // manufactured solution
		source = source_solitary;
		char_bc = char_bc_solitary;
	} else if ( 84 == initial_state) { // Oregon seaside
                source = source_oregon;
		char_bc = char_bc_oregon;
	} else if ( 88 == initial_state) { // Hansen and Svendsen, regular waves breaking on a sloping beach
                source = source_hansen;
		char_bc = char_bc_solitary;
	} else if ( 89 == initial_state) { // Grilli, solitary wave shoaling over a beach
                source = source_grilli;
		char_bc = char_bc_solitary;
	} else if ( 90 == initial_state) { // Solitary propagation over a 1D shelf
                source = source_shelf_1d;
		char_bc = char_bc_solitary;
	} else if ( 91 == initial_state) { // Polynomial reconstruction 
                source = source_solitary;
		char_bc = char_bc_solitary;
	} else if ( 92 == initial_state) { // Biarritz
                source = source_biarritz;
		char_bc = char_bc_synolakis;
	} else if ( 93 == initial_state) { // solitary and a cylinder
                source = source_cylinder;
		char_bc = char_bc_solitary;
	}


	// PROBLEM DEPENDENT SETTINGS 
	if (initial_state > 3)
		supersonic_residual = char_bc;
	else
		supersonic_residual = supersonic_residual_std;

	read_solution = read_solution_dat;
	write_solution = write_solution_dat;
}
