/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

/***************************************************************************
			conservative_fluctuation.c
--------------------------
       This is conservative_fluctuation: here the integral of
       the (space-time) fluxes is performed:

	\int_K ( du/dt + div(F) + \sigma grad(u) + gh grad b ) dx	
		 -----   -----    ------------     --------
 		 time	  adv         ale           source 
-------------------
***************************************************************************/
#include <math.h>
#include "common.h"
extern const int size, face_q_pts, vertex;
extern int iteration, numerics_friction_explicit, initial_state;
double temp_scalar, int_x, int_y, local_bathymetry, Dx_local_bathymetry, Dy_local_bathymetry;
extern double gm, dt, cut_off_H, cut_off_U;
extern double sigma_barx, sigma_bary, gridsize;
extern double *work_vector, *temp_vector, temp_normal[2+1], *phi_w, *phi_a_w1, *FLUX;
extern double **dU;
extern void (*fluctuation2) (int);
extern struct numerical_int_struct numerical_int;
extern struct node_struct *node;
extern struct element_struct *element;

void ad_flux_P(double *, double *, double);
void fluctuationRK(int);

/* Residual for shallow water system */
void STfluctuation_CN(int e)
{
	/* Time: du/dt */
	double V_T = element[e].volume / 3.0;
	int itr = iteration - 1;
	for (int v = 0; v < 3; v++) {
		int vv = element[e].node[v];
		// terms for mass equation in total depth var
		double h0 = node[vv].P[0][0] + node[vv].max_Htot0;
		double h1 = node[vv].P[2][0] + node[vv].bed[2];

		if (itr > 0) {
			if (!node[vv].dry_flag0 && node[vv].dry_flag2 && (node[vv].bed[2] + cut_off_H * node[vv].cut_off_ratio > h0))
				h1 = h0;
			if (node[vv].dry_flag0 && !node[vv].dry_flag2 && (node[vv].max_Htot0 + cut_off_H * node[vv].cut_off_ratio > h1))
				h0 = h1;
			if (node[vv].dry_flag0 && node[vv].dry_flag2)
				h1 = h0;
			//dU[v][0] = 2.0*V_T * (h1 - h0);
			dU[v][0] = V_T * (h1 - h0);
		} else {
			dU[v][0] = 0.;
		}

		// terms for momentum equation in standard vars
		for (int i = 1; i < size; i++) {
			if (itr > 0){
				//dU[v][i] = 2.0*V_T * (node[vv].P[2][i] - node[vv].P[0][i]);
				dU[v][i] = V_T * (node[vv].P[2][i] - node[vv].P[0][i]);
			}else
				dU[v][i] = 0.;
		}
	}

	for (int i = 0; i < size; i++) {
		phi_w[i] = dU[0][i] + dU[1][i] + dU[2][i];
		/* Explicit friction contribution */
		if (1 == numerics_friction_explicit) {
			for (int v = 0 ; v < 3 ; v ++ ) {
				int vv = element[e].node[v] ;
				phi_w[i] += dt*V_T*(node[vv].friction[2][i]) ;
			}
		}

		if (83 == initial_state) {
			for (int v = 0 ; v < 3 ; v ++ ) {
				int vv = element[e].node[v] ;
				phi_w[i] -= dt*V_T*(node[vv].PHI_bar[2][i]) ;
			}
		}
	}

	// Wave Generator
	if (75 == initial_state || 82 == initial_state || 88 == initial_state) {
		int i1 = element[e].node[0];
		int i2 = element[e].node[1];
		int i3 = element[e].node[2];
		phi_w[0] += dt *V_T*( node[i1].wg + node[i2].wg + node[i3].wg ) ;
	}

	fluctuationRK(e);

	for (int i = 0; i < size; i++)
		phi_w[i] += dt * phi_a_w1[i];
}

static void fluctuation(int e, int time_lev, double *phi)
{
	double max_Htot = -10.;

	int dry[3] = {0, 0, 0};
	for (int v = 0; v < vertex; v++){
		int n = element[e].node[v];
		if (0 == time_lev)
			dry[v] = node[n].dry_flag0;
		else if (2 == time_lev)
			dry[v] = node[n].dry_flag2;
	}

	for (int v = 0; v < vertex; v++){
		int n = element[e].node[v];
		if (!dry[v] && (node[n].P[time_lev][0] + node[n].bed[time_lev] > max_Htot) )
			max_Htot = node[n].P[time_lev][0] + node[n].bed[time_lev];
	}

	double h[3];
	for (int v = 0; v < vertex; v++){
		int n = element[e].node[v];
		h[v] = node[n].P[time_lev][0];
	}

	double hh = (h[0] + h[1] + h[2]) / 3.;

	for (int v = 0; v < vertex; v++){
		int n = element[e].node[v];
		if (dry[v] && (node[n].bed[time_lev] > max_Htot))
			h[v] = max_Htot;
		else
			h[v] += node[n].bed[time_lev];
	}
	
	double phi_bed[3] = {0., 0., 0.};

	phi_bed[1] = h[0] * element[e].normal[0][0] + h[1] * element[e].normal[1][0] + h[2] * element[e].normal[2][0];
	phi_bed[1] *= -0.5 * gm * hh;

	phi_bed[2] = h[0] * element[e].normal[0][1] + h[1] * element[e].normal[1][1] + h[2] * element[e].normal[2][1];
	phi_bed[2] *= -0.5 * gm * hh;

	for (int l = 0; l < size; l++) 
		phi[l] = -phi_bed[l];
    
    
    

	for (int i = 0; i < vertex-1; i++) {
		int ni = element[e].node[i];
		for (int j = i+1; j < vertex; j++) {
			int nj = element[e].node[j];
			int k = 3 - j - i;
			double temp_normal[2] = {0., 0.};
			temp_normal[0] = -element[e].normal[k][0];
			temp_normal[1] = -element[e].normal[k][1];
			double rr = sqrt(temp_normal[0] * temp_normal[0] + temp_normal[1] * temp_normal[1]) / gridsize;


            		double fluxlim = 0. ;  //added for the velocity limmiting
            		if ( node[ni].u_norm > fluxlim ) fluxlim = node[ni].u_norm ;
            		if ( node[nj].u_norm > fluxlim ) fluxlim = node[nj].u_norm ;
            
            
            		for (int l = 0; l < size; l++) work_vector[l] = 0 ;
				for (int f = 0; f < face_q_pts; f++){
					for (int l = 0; l < size; l++)
						temp_vector[l] = numerical_int.face_coordinate[f][0] * node[ni].P[time_lev][l] + numerical_int.face_coordinate[f][1] * node[nj].P[time_lev][l];
                
                 			/* TO be re-tested*/
                			// Computation and limiting of the velocity in the quadrature points
                			double uux = temp_vector[1]/temp_vector[0] ; if ( temp_vector[0] < cut_off_U*rr ) uux = 0. ;
                			double vvy = temp_vector[2]/temp_vector[0] ;if ( temp_vector[0] < cut_off_U*rr ) vvy = 0. ;
                			double velnorm = sqrt( uux*uux + vvy*vvy ) ;
                			double vel_ratio = velnorm/fluxlim ;
                
                			if ( vel_ratio > 1. ){
                    				temp_vector[1] /= vel_ratio ;
                    				temp_vector[2] /= vel_ratio ;
                			}
               				 /* TO be re-tested*/

					ad_flux_P(temp_normal, temp_vector, rr);

					for (int l = 0; l < size; l++)
						work_vector[l] += numerical_int.face_weight[f] * FLUX[l];
				}
            
             			/* TO be tested*/
           			/* if ( ( ( node[ni].Z[2][0] < coarse_cut_off ) || ( node[ni].Z[2][0] < coarse_cut_off ) ) && ( fabs(work_vector[0]) > coarse_cut_off ) ){
                			double velnorm = sqrt( work_vector[2]*work_vector[2] + work_vector[1]*work_vector[1] )/abs(work_vector[0]) ;
                			double speed_coeff = 1. ; if ( velnorm > fluxlim ) speed_coeff = fluxlim/velnorm ;
                
                			work_vector[1] *= speed_coeff ;
                			work_vector[2] *= speed_coeff ;
            			}*/ /* TO be tested*/
            
           			 for (int l = 0; l < size; l++)
                			phi[l] += work_vector[l] ;

		}
	}

	/**********************************************/
	/**            ALE : u\div(\sigma)           **/
	/**********************************************/

/*	double dtV;
	dtV = (element[e].volume[2]-element[e].volume[0])/dt;
	phi[0] += ( h[0] + h[1] + h[2] )*dtV/3.0;
	for (int l = 1; l < size; l++)
		for (int v = 0; v < vertex; v++){
			int n = element[e].node[v];
			phi[l] += node[n].P[time_lev][l]*dtV/3.0;
		}
*/
}

void fluctuationRK(int e)
{
	if (1 == iteration) {

		double phi[3] = {0., 0., 0.};
		for (int l = 0; l < size; l++)
			phi_a_w1[l] = 0.;

		fluctuation(e, 0, phi);
		
		for (int l = 0; l < size; l++)
			phi_a_w1[l] += phi[l];

	} else if (2 == iteration) {
		
		double phi[3] = {0., 0., 0.};
		for (int l = 0; l < size; l++)
			phi_a_w1[l] = 0.;

		fluctuation(e, 0, phi);
		
		for (int l = 0; l < size; l++){
			phi_a_w1[l] += 0.5 * phi[l];
			phi[l] = 0.;
		}
		
		fluctuation(e, 2, phi);

		for (int l = 0; l < size; l++)
			phi_a_w1[l] += 0.5 * phi[l];
	}
}

