/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

	


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "my_malloc.h"

/** Alocate a vector of any type (calloc wrapper) */
void* MA_vector( int icount, long type_size )
{
	void* mem = calloc( icount, type_size);
	if ( NULL == mem ) {
		printf( "Memory allocation error \n" );
		exit( EXIT_FAILURE );
	}
	return mem;
}

/** Allocate a 2D matrix */
void** MA_matrix( int icount, int jcount, long type_size )
{
	assert ( (icount > 0) && (jcount > 0) && "Negative dimension requested" );
	void** iret = malloc( icount * sizeof(void**) + icount * jcount * type_size );
	if ( NULL == iret ) {
		printf( "Memory allocation error \n" );
		exit( EXIT_FAILURE );
	}
	char* jret = (char*)(iret + icount);
	for ( int i = 0; i < icount; i++ )
		iret[i] = &jret[i * jcount * type_size];
	return iret;
}

/** Allocate a tensor (3D matrix) */
void*** MA_tensor( int icount, int jcount, int kcount, long type_size )
{
	assert ( (icount > 0) && (jcount > 0) && (kcount > 0) && "Negative dimension requested" );
	void*** iret = malloc( icount * sizeof(void***) + icount * jcount * sizeof(void** ) + icount * jcount * kcount * type_size );
	if ( NULL == iret ) {
		printf( "Memory allocation error \n" );
		exit( EXIT_FAILURE );
	}
	void** jret = (void**)(iret + icount);
	char* kret = (char*)(jret + icount * jcount);
	for ( int i = 0; i < icount; i++ )
		iret[i] = &jret[i * jcount];
	for ( int i = 0; i < icount; i++ )
		for ( int j = 0; j < jcount; j++ )
			jret[i * jcount + j] = &kret[i * jcount * kcount * type_size + j * kcount * type_size];
	return iret;
}

/** Free allocated memory */
void MA_free( void *ptr )
{
	free( ptr );
}

