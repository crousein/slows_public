/* Copyright (C) INRIA BSO - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by M.Ricchiuto  <mario.ricchiuto@inria.fr>
 *            M. Kazolea   <maria.kazolea@inria.fr>
 *            A. Filippini <a.filippini@brgm.fr>
 *            L. Arpaia    <luca.arpaia@brgm.fr>
 *            N.Pattakos   <pattakosn@fastmail.com>, June 2021
 */
#ifndef NHT_H
#define NHT_H
#include <stdlib.h> // size_t

#ifdef __cplusplus
extern "C" {
#endif

/** Non hydrostatic part library
 *    Computes the non hydrostatic terms of the GN equations
 */

/**
 */
int nht_new_from_file( char *gridname,
                       double alpha,
                       double gravity,
                       double mu,
                       double href,
                       double cut_off[2],
                       int model_Tmatrix,
                       int model_breaking,
                       double brk_param[ 5 ],
                       int massIn,
                       double *bed,
		       double *dxbed,
		       double *dybed,
                       size_t NN );

/**
 */
int nht_new( double alpha,
             double gravity,
             double mu,
             double href,
             double cut_off[2],
             int model_Tmatrix,
             int model_breaking,
             double brk_param[ 5 ],
             int massIn,
             double *bed,
             double *dxbed,
             double *dybed,
             size_t NN,
	     int **el_node,
             double **no_coord,
             int *no_mate,
	     int **bnode,
             int **btypes,
             int *btype,
             double **bnormal );

/**
 */
int nht_phi_bb_e( double *h,
                  double *u,
                  double *v,
                  double *eta,
		  double *dxh,
                  double *dxhu,
                  double *dxhv,
                  double *dxeta,
		  double *dyh,
                  double *dyhu,
                  double *dyhv,
                  double *dyeta,
                  size_t NN,
                  double time,
                  double *phi,
                  int *bb,                  /** accounts for the breaking nodes. 1 if the node is breaking 0 if it is not */
                  double *e );                 /** energy for the GN equations */

/**
 */
int nht_phi_bb_k_e( double *h,
                    double *u,
                    double *v,
                    double *eta,
		    double *dxh,
                    double *dxhu,
                    double *dxhv,
                    double *dxeta,
		    double *dyh,
                    double *dyhu,
                    double *dyhv,
                    double *dyeta,
                    size_t NN,
                    double time,
                    double *phi,
                    int *bb,                 /** accounts for the breaking nodes. 1 if the node is breaking 0 if it is not */
                    double *e,                /** energy for the GN equations */
                    double *k );                 /** kinetic energy for the breaking model */

/** Deallocate memory
 */
void nht_delete( void );
#ifdef __cplusplus
}
#endif
#endif
