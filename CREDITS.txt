Slows is a C code for simulating hydrostatic free surface
flows on unstructured grids. 

The main features are:
  - The equations solved are the shallow water equations 
    with friction (Manning or Chezy). 
  - Both a high order MUSCL finite volume method and a 
    high order residual distribution approach are 
	available to solve the equations.
  - Time stepping is performed with first, second or
    third order SSP Runge-Kutta method
  - arbitrary bathymetries can be used (including xy-z
    tables) 


Slows has been developed at INRIA Bordeaux Sud-Ouest.
The authors of the current version of the code are:
  - Luca ARPAIA 
  - Andrea FILIPPINI
  - Maria KAZOLEA
  - Nikolaos PATTAKOS
  - Mario RICCHIUTO
