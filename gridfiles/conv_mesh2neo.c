#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void main()
{
  FILE *dplgrid, *neogrid ;

  int dimension, NX, NY, NE, NN, NBF, boundaries, bnodes,mate, NF ;
  int e, n, f, dummy, i , j, d0, d1, d2, d3, n0, n1, n2, m ;
  double fl0, fl1, fl2, fl3, fl4, x, y ;
  int type[12], Ve, Vj, Fe ;

  int *element_node0 ;
  int *element_node1 ;
  int *element_node2 ;
  int *element_neigh0 ;
  int *element_neigh1 ;
  int *element_neigh2 ;
  int *element_neigh_f0 ;
  int *element_neigh_f1 ;
  int *element_neigh_f2 ;
  int *element_flag0 ;
  int *element_flag1 ;
  int *element_flag2 ;
  double *node_x, *node_y ;
  int *f_type ;
  int *node_mate ; /* TO PROVIDE EXTERNALLY!! IT IS SIMULATION-DEPENDENT !!!!!! */
  int *node_flag ;
  int *face_nodeL ;
  int *face_nodeR ;
  double *face_normal0 ;
  double *face_normal1 ;
  int *face_type ;/* TO PROVIDE EXTERNALLY!! IT IS SIMULATION-DEPENDENT !!!!!! */
  int *face_element ; 
  int *boundary_node ;
  int *boundary_type ;
  double *boundary_normal0 ;
  double *boundary_normal1 ;

  type[0] = 1 ;
  type[1] = 10 ;
  type[2] = 1 ;
  type[3] = 10 ;
  type[4] = 1 ;
  type[5] = 1 ;
  type[6] = 1 ;
  type[7] = 1 ;

  dplgrid = fopen( "Output.mesh", "r" ) ;

  /*********************************************************/
  /********* READING THE DPL GRIDFILE HEADER ***************/
  /*********************************************************/

  fscanf( dplgrid," MeshVersionFormatted 0\n");
  fscanf( dplgrid," Dimension\n");
  fscanf( dplgrid, "%d\n", &dummy ) ;
  fscanf( dplgrid," Vertices\n");
  fscanf( dplgrid, "%d\n", &NN ) ;


  node_x = ( double * ) calloc( NN , sizeof( double ) ) ;
  node_y = ( double * ) calloc( NN , sizeof( double ) ) ;

  node_mate = ( int * ) calloc( NN , sizeof( int ) ) ;
  node_flag = ( int * ) calloc( NN , sizeof( int ) ) ;

  f_type = ( int * ) calloc( NN , sizeof( int ) ) ;
  
  for ( n = 0 ; n < NN ; n++ ) 
    f_type[n]=0; 

  for ( n = 0 ; n < NN ; n++ )
  {
    node_flag[n] = 0 ;

    fscanf( dplgrid, "%le %le %d\n", &node_x[n], &node_y[n], &dummy);

    /*********************************************************/
    /****     Boundary Flag for each boundary nodes     ******/
    /*********************************************************/

    // Convergent Channel flag

    f_type[n] = 1  ; 

  }

  fscanf( dplgrid," Edges\n");
  fscanf( dplgrid,"%d\n",&NF);

  face_nodeL   = ( int * ) calloc( NF , sizeof( int ) ) ;
  face_nodeR   = ( int * ) calloc( NF , sizeof( int ) ) ;
  face_type    = ( int * ) calloc( NF , sizeof( int ) ) ;

  /*********************************************************/
  /****                 Writing Edges                 ******/
  /*********************************************************/


  NBF = 0 ;

  for ( i = 0 ; i < NF ; i ++ )
  {
    fscanf( dplgrid, "%d %d %d\n", &d0, &d1, &d3 ) ;

    face_nodeR[NBF] = d1 - 1 ;  
    face_nodeL[NBF] = d0 - 1 ;

    NBF += 1 ;

  }

  fscanf( dplgrid," Triangles\n");
  fscanf( dplgrid,"%d\n",&NE ) ;


  element_node0 = ( int * ) calloc( NE , sizeof( int ) ) ;
  element_node1 = ( int * ) calloc( NE , sizeof( int ) ) ;
  element_node2 = ( int * ) calloc( NE , sizeof( int ) ) ;

  boundary_node = ( int * ) calloc( 2*NBF , sizeof( int ) ) ;
  boundary_type = ( int * ) calloc( 2*NBF , sizeof( int ) ) ;

  for ( e = 0 ; e < NE ; e ++ ){
    fscanf( dplgrid, "%d %d %d %d \n", &d0, &d1, &d2, &dummy ) ;  

    element_node0[e] = d0 - 1 ;
    element_node1[e] = d1 - 1 ;
    element_node2[e] = d2 - 1 ;

  }


  fclose ( dplgrid ) ; 

  // Count number of nodes on the two axis:
/*  NX = 0;
  NY = 0;
  for ( n = 0 ; n < NN ; n ++ ){
    if (fabs(node_y[n]-0.)<0.00000001)
      NX += 1;
    else if (fabs(node_x[n]-0.)<0.00000001)
      NY += 1;
  }
  printf("NX = %d NY = %d\n",NX,NY);
*/

  // Print the new mesh file:
  neogrid = fopen( "neogrid.grd", "w" ) ;

  dimension = 2 ;

  fprintf( neogrid, "%d %d %d %d \n", dimension, NE, NN, NBF ) ;
  fprintf( neogrid, "\n" ) ;

  for ( e = 0 ; e < NE ; e ++ )
    fprintf( neogrid, "%d %d %d\n", element_node0[e],element_node1[e],element_node2[e]);

  fprintf( neogrid, "\n" ) ;

  for ( n = 0 ; n < NN ; n ++ ){
     /**********************************
     ***  Periodic b.c. (left-right) ***
     **********************************/
      /*double XL = 0.;
      double XR = 2.;
      if ( fabs(node_x[n]-XL) < 1.E-6 ){
       int match = 0;
       for ( m = 0 ; m < NN ; m ++ ){
            if ( ( fabs(node_x[m]-XR) < 1.E-6 ) && ( fabs(node_y[n] - node_y[m]) < 1.E-6 ) ){
                node_y[n] = node_y[m];
                fprintf( neogrid, "%le %le %d\n", node_x[n],node_y[n],m ) ;
                match = 1;
                break;
            }
       }
       if (match == 0){
         printf("ERROR: No match found for node %d x %le y %le \n",n,node_x[n],node_y[n]);
         exit(0);
       }
    } else if ( fabs(node_x[n]-XR) < 1.E-6){
       int match = 0;
       for ( m = 0 ; m < NN ; m ++ ){
            if ( ( fabs(node_x[m]-XL)<1.E-6 ) && ( fabs(node_y[n] - node_y[m]) < 1.E-6 ) ){
                node_y[n] = node_y[m];
                fprintf( neogrid, "%le %le %d\n", node_x[n],node_y[n],m ) ;
                match = 1;
                break;
            }
       }
       if (match == 0){
         printf("ERROR: No match found for node %d x %le y %le \n",n,node_x[n],node_y[n]);
         exit(0);
       }
    } else */ 

     /**********************************
     ***  Periodic b.c. (top-bottom) ***
     **********************************/
 /*   double YB = 0.;
    double YT = 0.43;
    if ( fabs(node_y[n]-YB)<1.E-6 ){
       int match = 0;
       for ( m = 0 ; m < NN ; m ++ ){
            if ( ( fabs(node_y[m]-YT) < 1.E-6 ) && ( fabs(node_x[n] - node_x[m]) < 1.E-6 ) ){
                node_x[n] = node_x[m];
                fprintf( neogrid, "%le %le %d\n", node_x[n],node_y[n],m ) ;
                match = 1;
                break;
            }
       }
       if (match == 0){
         printf("ERROR: No match found for node %d x %le y %le \n",n,node_x[n],node_y[n]);
         exit(0);
       }
    } else if ( fabs(node_y[n]-YT) < 1.E-6 ){
       int match = 0;
       for ( m = 0 ; m < NN ; m ++ ){
            if ( ( fabs(node_y[m]-YB)<1.E-6 ) && ( fabs(node_x[n] - node_x[m]) < 1.E-6 ) ){
                node_x[n] = node_x[m];
                fprintf( neogrid, "%le %le %d\n", node_x[n],node_y[n],m ) ;
                match = 1;
                break;
            }
       }
       if (match == 0){
         printf("ERROR: No match found for node %d x %le y %le \n",n,node_x[n],node_y[n]);
         exit(0);
       }
    } else 
*/
    /***********************************
     ***           Wall               ***
     ***********************************/
    fprintf( neogrid, "%le %le %d\n", node_x[n],node_y[n],n ) ;
  }
  fprintf( neogrid, "\n" ) ;

  for ( f = 0 ; f < NBF ; f++ ){
    /***********************************
     ***  Periodic b.b. (left-right) ***
     **********************************/
    /*double XL = 0.;
    double XR = 2.;
    int fl = face_nodeL[f];
    int fr = face_nodeR[f];
    if ( ( fabs(node_x[fl]-XL) < 1.E-6 ) && ( fabs(node_x[fr] - XL) < 1.E-6 ) )
      fprintf( neogrid, "%d %d %d %d %d\n", face_nodeL[f], face_nodeR[f],10,10,10 ) ;
    else if ( ( fabs(node_x[fl] - XR) < 1.E-6 ) && ( fabs(node_x[fr] - XR) < 1.E-6 ) )
      fprintf( neogrid, "%d %d %d %d %d\n", face_nodeL[f], face_nodeR[f],10,10,10 ) ;
    else
*/
    /***********************************
     ***  Periodic b.b. (top-bottom) ***
     **********************************/
/*    double YB = 0.;
    double YT = 0.43;
    int fl = face_nodeL[f];
    int fr = face_nodeR[f];
    if ( ( fabs(node_y[fl]-YB) < 1.E-6 ) && ( fabs(node_y[fr]-YB) < 1.E-6 ) )
      fprintf( neogrid, "%d %d %d %d %d\n", face_nodeL[f], face_nodeR[f],10,10,10 ) ;
    else if ( ( fabs(node_y[fl]-YT) < 1.E-6 ) && ( fabs(node_y[fr]-YT) < 1.E-6 ) )
      fprintf( neogrid, "%d %d %d %d %d\n", face_nodeL[f], face_nodeR[f],10,10,10 ) ;
    else
*/
    /**************
     ***  Wall  ***
     **************/

      fprintf( neogrid, "%d %d %d %d %d\n", face_nodeL[f], face_nodeR[f],3,3,3 ) ;
//      fprintf( neogrid, "%d %d %d %d %d\n", face_nodeL[f], face_nodeR[f],20,20,20 ) ;
}

  /**********************************************************************************/
  /******************* FINISHED .......   BYE BYE !!!!!!!! **************************/
  /**********************************************************************************/

  fclose ( neogrid ) ;
}







