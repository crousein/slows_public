/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

	
#ifndef INTEGRAL_HPP
#define INTEGRAL_HPP
#include <cstddef>

/* provides the Gaussian points and weights for the Gaussian quadrature formula
 * of order 0,1,2 and 3 for the standard triangle
 *    Gauss quadrature formula:
 *       qp  : number of quadrature points
 *       phi : quadrature points baricentric coordinates
 *       w   : weights
 */

enum class quadrature_order : char { FIRST, SECOND, THIRD, FOURTH };

template < quadrature_order A>
struct QaussQuad_s {
};

template <>
struct QaussQuad_s< quadrature_order::FIRST > {
	const double phi[ 1 ][ 3 ] = { { 1./3., 1./3., 1./3. } };
	const double w[ 1 ] = { 1.0 };
	const size_t qp = 1;
};

template <>
struct QaussQuad_s< quadrature_order::SECOND > {
	const double phi[ 3 ][ 3 ] = { { 0.5, 0.5, 0.0 }, { 0.5, 0.0, 0.5 }, { 0.0, 0.5, 0.5 } };
	const double w[ 3 ] = { 1.0/3.0, 1.0/3.0, 1.0/3.0 };
	const size_t qp = 3;
};

template <>
struct QaussQuad_s< quadrature_order::THIRD > {
	const double phi[ 4 ][ 3 ] = { { 1./3., 1./3., 1./3. }, { 0.6, 0.2, 0.2 }, { 0.2, 0.6, 0.2 }, { 0.2, 0.2, 0.6 } };
	const double w[ 4 ] = { -27.0/48.0, 25.0/48.0, 25.0/48.0, 25.0/48.0 };
	const size_t qp = 4;
};

template <>
struct QaussQuad_s< quadrature_order::FOURTH > {
	const double phi[ 6 ][ 3 ] = { { 1. - 0.44594849091597 - 0.44594849091597, 0.44594849091597, 0.44594849091597 },
	                               { 1. - 0.44594849091597 - 0.10810301816807, 0.44594849091597, 0.10810301816807 },
	                               { 1. - 0.10810301816807 - 0.44594849091597, 0.10810301816807, 0.44594849091597 },
	                               { 1. - 0.09157621350977 - 0.09157621350977, 0.09157621350977, 0.09157621350977 },
        	                       { 1. - 0.09157621350977 - 0.81684757298046, 0.09157621350977, 0.81684757298046 },
	                               { 1. - 0.81684757298046 - 0.09157621350977, 0.81684757298046, 0.09157621350977 } };
	const double w[ 6 ] = { 0.22338158967801, 0.22338158967801, 0.22338158967801, 0.10995174365532, 0.10995174365532, 0.10995174365532 };
	const size_t qp = 6;
};

// This is useful as the last function call for the recursion
static double phi( const double * const& )
{
	return 1;
}
template <typename First, typename... Rest>
static double phi( const double * const F, const First& first, const Rest... rest )
{
//TODO:  static_assert( first < sizeof(F) );
	return F[ first ] * phi( F, rest... );
}
template < quadrature_order A, typename first, typename... Ints >
double int_F ( first arg1, Ints... arg2 )
{
  QaussQuad_s< A> gq;

	double sum = 0.;
	for ( size_t q = 0; q < gq.qp; q++ )
		sum += gq.w[q] * phi( &gq.phi[q][0], arg1, arg2... );

	return sum;
}
#endif
