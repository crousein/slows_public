/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

    
#ifndef SPARSE_H
#define SPARSE_H
#include "common.h"
#include <stdlib.h>
#ifdef __cplusplus
extern "C" {
#endif
void Tblock_assembly( int block_i, int block_j, int block_n, size_t NE, size_t NNZ, size_t NP,
                      double **grad_h, double **grad_b, double ***grad_phi,
                      node_s *node,
                      element_s *element,
                      boundary_nodes_s *b_nodes );
void TSPDblock_assembly( int block_i, int block_j, int block_n, size_t NE, size_t NNZ, size_t NP,
                         double **grad_h, double **grad_b, double ***grad_phi,
                         node_s *node,
                         element_s *element,
                         boundary_nodes_s *b_nodes );
void TNCFblock_assembly( int block_i, int block_j, int block_n, size_t NE, size_t NNZ, size_t NP,
                         double **grad_h, double **grad_b, double ***grad_phi,
                         node_s *node,
                         element_s *element,
                         boundary_nodes_s *b_nodes );
void sparse_initialize( size_t NE, size_t NNZ, size_t NP, node_s *node, element_s *element );
void GMM_assembly( size_t NN, size_t NE, size_t NNZ, size_t NP, node_s *node, element_s *element, boundary_nodes_s *b_nodes );
void T_assembly( size_t NN, size_t NE, size_t NNZ, size_t NP, node_s *node,
                 element_s *element, boundary_nodes_s *b_nodes,
                 void (*block_func) (int, int, int, size_t, size_t, size_t,
                 double **, double **, double ***,
                 node_s *, element_s *,
                 boundary_nodes_s * ) );
void Tdiag_assembly( double mu, size_t NN, size_t NE, size_t NNZ, size_t NP, double alpha,
                    node_s *node,
                    element_s *element,
                    boundary_nodes_s *b_nodes );
void HGMM_assembly( size_t NN, size_t NE, size_t NNZ, size_t NP, node_s *node, element_s *element, boundary_nodes_s *b_nodes );
#ifdef __cplusplus
}
#endif
#endif
