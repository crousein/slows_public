/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/
	


#ifdef WITH_MUMPS
#include "lin_solver_mumps.h"
#include <stdio.h>
#include <stdlib.h>
#include <cassert>

lin_solver_mumps::lin_solver_mumps(int N, int nnz, int* ia, int* ja, double* A) {
	init_factorize(N, nnz, ia, ja, A);
}

lin_solver_mumps::~lin_solver_mumps() {
	release_handler();
}

//void MUMPS_solver_destroy(DMUMPS_STRUC_C * id)
void lin_solver_mumps::release_handler() {
	id.job = -2;  // clean items
	dmumps_c(&id); // update the mumps items
	id = DMUMPS_STRUC_C{};
}

//void MUMPS_solver_init(DMUMPS_STRUC_C * id)
//void MUMPS_solver_facto(int N, int NZ, int *irn, int *jcn, double *A, DMUMPS_STRUC_C * id)
void lin_solver_mumps::init_factorize( int N, int nnz, int *ia, int *ja, double *A)
{
	if( id.job != 0 )
		release_handler();

	id.job = -1;		// initialize items
	id.par = 1;		// no parallel
	id.sym = 0;		// no symmetrical (it is but easier not say it)
	dmumps_c(&id);		// update the mumps items

// macro s.t. indices match documentation
#define ICNTL(I) icntl[(I)-1]
	// No output
	id.ICNTL(1) = 0;
	id.ICNTL(2) = 0;
	id.ICNTL(3) = 0;
	id.ICNTL(4) = 0;

	//id.write_problem  = matrixout;
	// Define the problem on the host
	id.n = N; // size of no sparse matrix
	id.nz = nnz; // number of contributions for the sparse matrix (no only the nonzero elements)

	//id.ICNTL(11)=1;
	//id->ICNTL(10)=10;
	id.ICNTL(10) = 0;

	// id.ICNTL(6)=5;
	id.irn = ia; // matrix i indices
	id.jcn = ja; // matrix j indices
	id.a = A; // matrix values

	// Call MUMPS
	id.job = 4; // facto
	dmumps_c(&id);
#undef ICNTL
}


//void MUMPS_solver_solve(double *RHS, DMUMPS_STRUC_C * id) {
void lin_solver_mumps::solve( double *b, int ) {
	assert(id != DMUMPS_STRUC_C{} && "Apparently you forgot to initialize MUMPS handler");

	id.rhs = b;
	id.job = 3; // facto+solve
	dmumps_c(&id); // update the mumps items
	if ( id.info[0] == -10) {
		printf(" job= %i, info=%d infog=%d \nERROR: Sparse Matrix is singular -exit\n", id.job, id.info[0], id.infog[0]);
		exit(EXIT_FAILURE);
	}
}
#endif
