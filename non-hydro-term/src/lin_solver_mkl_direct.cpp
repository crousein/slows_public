/*  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                         M. Kazolea   <maria.kazolea@inria.fr>
                         A. Filippini <a.filippini@brgm.fr>
                         L. Arpaia    <luca.arpaia@brgm.fr>
                         N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.*/


#include "lin_solver_mkl_direct.h"
#ifdef WITH_SPARSE_MKL
#include <cstdio>
#include <cassert>
#include <algorithm>

#define CHECK_MKL( command ) do {                                                                   \
	sparse_status_t ret_err;                                                                    \
	ret_err = command;                                                                          \
	if( ret_err != SPARSE_STATUS_SUCCESS )                                                      \
		fprintf(stderr, "\n\t Sparse MKL error (file: %s line: %d): ", __FILE__, __LINE__); \
	if (ret_err == SPARSE_STATUS_NOT_INITIALIZED ) {                                            \
		fprintf(stderr, "Empty handle or matrix array\n");                                  \
		abort();                                                                            \
	} else if (ret_err == SPARSE_STATUS_ALLOC_FAILED ) {                                        \
		fprintf(stderr, "Internal memory allocation failed\n");                             \
		abort();                                                                            \
	} else if (ret_err == SPARSE_STATUS_INVALID_VALUE ) {                                       \
		fprintf(stderr, "The input parameters contain an invalid value\n");                 \
		abort();                                                                            \
	} else if (ret_err == SPARSE_STATUS_EXECUTION_FAILED ) {                                    \
		fprintf(stderr, "Execution failed\n");                                              \
		abort();                                                                            \
	} else if (ret_err == SPARSE_STATUS_INTERNAL_ERROR ) {                                      \
		fprintf(stderr, "An error in algorithm implementation occurred\n");                 \
		abort();                                                                            \
	} else if (ret_err == SPARSE_STATUS_NOT_SUPPORTED ) {                                       \
		fprintf(stderr,  "The requested operation is not supported\n");                     \
		abort();                                                                            \
	}                                                                                           \
} while(0)

lin_solver_mkl_direct::lin_solver_mkl_direct() {
	pardiso_init();
}

lin_solver_mkl_direct::lin_solver_mkl_direct(int rowsIN, int nnz, int* ia, int* ja, double* a) {
	pardiso_init();
	init_factorize(rowsIN, nnz, ia, ja, a);
}

lin_solver_mkl_direct::~lin_solver_mkl_direct() {
	release_a_csr();
	release_pt();
}

void lin_solver_mkl_direct::release_a_csr() {
	if(a_csr_h != 0)
		CHECK_MKL( mkl_sparse_destroy(a_csr_h) );
	a_csr_h = 0;
}

void lin_solver_mkl_direct::init_factorize(int rowsIN, int nnz, int* ia, int* ja, double* a) {
	rows = rowsIN;

	sparse_matrix_t a_coo = 0;
	CHECK_MKL( mkl_sparse_d_create_coo( &a_coo, SPARSE_INDEX_BASE_ONE, rows, rows, nnz, ia, ja, a) );

	release_a_csr();
	const sparse_operation_t operation = SPARSE_OPERATION_NON_TRANSPOSE;
	CHECK_MKL( mkl_sparse_convert_csr( a_coo, operation, &a_csr_h) );

	sparse_index_base_t indexing;
	MKL_INT export_rows;
	MKL_INT export_cols;
	MKL_INT *rows_start = nullptr;
	MKL_INT *rows_end = nullptr;
	CHECK_MKL( mkl_sparse_d_export_csr(a_csr_h, &indexing, &export_rows, &export_cols, &rows_start, &rows_end, &a_csr3_ja, &a_csr3 ) );
	assert(export_cols == export_rows == rows);
	assert(indexing == SPARSE_INDEX_BASE_ONE );

	a_csr3_ia.resize(rows+1);
	std::copy_n(rows_start, rows, std::back_inserter(a_csr3_ia));
	for ( int i = 0; i < rows; ++i )
		a_csr3_ia[i] = rows_start[i];
	a_csr3_ia[rows] = rows_end[rows-1];
	CHECK_MKL( mkl_sparse_destroy(a_coo) );

	MKL_INT error;
	MKL_INT idum;
	double ddum;
	const MKL_INT phase = 12;
	pardiso (pt, &maxfct, &mnum, &mtype, &phase, &rows, a_csr3, a_csr3_ia.data(), a_csr3_ja, &idum, &nrhs, iparm, &msglvl, &ddum, &ddum, &error);
	if( error ) {
		fprintf(stderr, "\n\t Sparse MKL error (file: %s line: %d): ", __FILE__, __LINE__);
		fprintf(stderr, "error during symbolic factorization or numerical factorization: %d", error);
		abort();
	}
	//TODO printf ("\nReordering, symbolic factorization, numerical factorization completed. ");
	//TODO printf ("Number of nonzeros in factors = %d .", iparm[17]);
	//TODO printf ("Number of factorization MFLOPS = %d\n", iparm[18]);
}

void lin_solver_mkl_direct::solve(double *b, int N) {
	const MKL_INT phase = 33;
	MKL_INT error;
	MKL_INT perm;
	assert(N == rows);
	x.resize(N);
	pardiso (pt, &maxfct, &mnum, &mtype, &phase, &rows, a_csr3, a_csr3_ia.data(), a_csr3_ja, &perm, &nrhs, iparm, &msglvl, b, x.data(), &error);
	if( error ) {
		fprintf(stderr, "\n\t Sparse MKL error (file: %s line: %d): ", __FILE__, __LINE__);
		fprintf(stderr, "error during symbolic factorization or numerical factorization: %d", error);
		abort();
	}
	for( MKL_INT i = 0; i < N; ++i )
		b[i] = x[i];
}

void lin_solver_mkl_direct::pardiso_init() {
	printf("Initializing MKL's PARallel DIRect SOlver (PARDISO)\n");
	for ( size_t i = 0; i < 64; ++i )
		pt[i] = 0;
	iparm[0] = 0;
	pardisoinit( pt, &mtype, iparm );
}

void lin_solver_mkl_direct::release_pt() {
	MKL_INT phase = -1;
	double ddum;
	MKL_INT idum;
	MKL_INT error;
	pardiso(pt, &maxfct, &mnum, &mtype, &phase, &rows, &ddum, a_csr3_ia.data(), a_csr3_ja, &idum, &nrhs, iparm, &msglvl, &ddum, &ddum, &error);
}
#endif
