------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
  Copyright (C) 2021   M.Ricchiuto  <mario.ricchiuto@inria.fr>
                       M. Kazolea   <maria.kazolea@inria.fr>
                       A. Filippini <a.filippini@brgm.fr>
                       L. Arpaia    <luca.arpaia@brgm.fr>
                       N.Pattakos   <pattakosn@fastmail.com> 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
    
SLOWs is a code for the simulation of  free surface flows  on unstructured meshes using depth averaged models.
Both hydrostatic (shallow water) and non-hydrostatic (Green-Naghdi) models are available. The spatial discretizations available are based on residual distribution or finite volumes. The elliptic equations  providing the dispersive non-hydrostatic correction are solved useind a continuous finite element method. 
Exmaples of computations performed with SLOWs can be found  

here -> https://team.inria.fr/cardamom/slows-shallow-water-flows/

here-> https://team.inria.fr/cardamom/research-themes/gallery_coastal_modelling/


For more information you can check the following references.

M. Kazolea, A.G. Filippini, M. Ricchiuto, A low dispersion finite volume/element method for nonlinear wave propagation, breaking and runup on unstructured meshes, https://hal.inria.fr/hal-03402701

A.G. Filippini, M. Kazolea, M. Ricchiuto, A flexible genuinely nonlinear approach for nonlinear wave propagation, breaking and runup, https://hal.inria.fr/hal-01266424

M. Ricchiuto. An explicit residual based approach for shallow water flows, 
https://hal.inria.fr/hal-01087940

M. Ricchiuto, A.G. Filippini. Upwind residual discretization of enhanced Boussinesq equations for wave propagation over complex bathymetries. https://hal.inria.fr/hal-00934578

------------------------------------------------------------------------------------------
1 ] General information / outline

Slows uses the cmake build system to build and install.
The usual steps for a project using cmake are:
   * go to the project directory
   * create a directory to use for the build
   * configure the build
   * compile
   * install


2 ] Dependencies

* A compiler that supports C++11
   gcc   >=  4.6
   clang >=  3.0
   MSVC  >= 17.0
   icc   >= 13.0

* The CMake tool. On GNU Debian / Ubuntu systems install the following packages
  " cmake-qt-qui cmake-curses-gui ". There are also precompiled binaries for
  Windows and OSX on cmake's web page: https://cmake.org/download/

* Building the non hydro part requires the MUMPS sparse solver. On GNU Debian / Ubuntu
  systems installing package " libmumps-seq-dev " is enough. If you are using a custom
  installation of MUMPS (eg one installed in ~/opt or ~/mumps etc) set
    MUMPS_INCLUDE_DIR
  to the location of the include/dmumps_c.h header file


  More information on using cmake can be found on https://cmake.org/runningcmake

3 ]  The sources are in the directories (main in driver.c)

    $TOP_LEVEL_DIR/SRC 

and

    $TOP_LEVEL_DIR/SRC/non-hydro-term/src/

Running the executable expects this input file:

    $TOP_LEVEL_DIR/textinput/inputfile-ex-ex-loose.txt

to be available. Depending on the requested case a mesh file is also
expected to be found in the directory

    $TOP_LEVEL_DIR/input/

Output files will be written in:

    $TOP_LEVEL_DIR/output/
    
4 ] Some basic documentation can be found in 

    $TOP_LEVEL_DIR/user_manual/

