\documentclass[11pt]{report}
%\documentclass{llncs}
%\linespread{1.15} % for more than single spacing

% % take more advantage of the size of paper
 \addtolength{\topmargin}{-1.8cm}
 \addtolength{\textheight}{3cm}
 \addtolength{\evensidemargin}{-2.4cm}
 \addtolength{\oddsidemargin}{-2.4cm}
 \addtolength{\textwidth}{3.8cm}
% 
% % some standard packages
 \usepackage{times}
 \usepackage{graphics}
 \usepackage{graphicx}
 \usepackage{subfigure, epsfig}
 \usepackage{rotate}
 \usepackage[toc,page]{appendix}
 \usepackage{mathtools}
\usepackage{pifont,natbib,geometry}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{hyperref}

 %\usepackage{pstricks}

% AMS, keywords, etc
 \newenvironment{AMS}{\small\textit{AMS subject classification:}}{}
 \newenvironment{keywords}{\small\textit{Key words:}}{}


\def\red#1{{\color{red}#1}}
\def\orange#1{{\color{orange}#1}}
\def\green#1{{\color{green}#1}}
\def\purple#1{{\color{purple}#1}}



\begin{document}
\begin{titlepage}

\begin{center}

\vspace{10.5cm}
\LARGE{ \textbf{ SLOWS - Shallow water fLOWS }} \\
\vspace{0.5cm}
\LARGE{USER'S MANUAL} \\
\vspace{0.5cm}
\Large{Contributors:} \large{Mario Ricchiuto, Luca Arpaia, Andrea Filippini, Maria Kazolea, Nikos Pattakos}
\vspace{0.5cm}
\vfill
\includegraphics[height=10cm, width=14cm]{figures/t2_setup.jpg}
\end{center}
\end{titlepage}

\pagenumbering{arabic}
\tableofcontents

\chapter{Introduction}

SLOWS (Shallow-water fLOWS) is a  C-platform, developed at INRIA Bordeaux Sud-Ouest, allowing the simulation of free surface shallow water flows with friction. It can be used to simulate near shore hydrodynamics, wave transformations processes, etc. The kernel of the CODE is a shallow water solver based on second order residual distribution or second and third order finite volume schemes. Three different approaches are available to march in time, based on conditionally depth-positivity preserving implicit schemes, or on conditionally depth-positivity preserving genuinely explicit discretisation, or on an unconditionally depth-positivity preserving space-time approach.

From the theoretical point of view the code solves the Nonlinear Shallow Water Equations (NSWE) with friction (Manning or Chezy) or/and the fully non-linear weakly dispersive equations of Green-Naghdi.  

From the numerical point of view SLOWS solves the NSWE using a high order residual distribution scheme (RD) or a high order MUSCL finite volume (FV) scheme. The Non-Hydrostatic (NHT) part is solved using a standard $C^0$ Galerkin finite element method.  Time stepping is performed with first, second or third order Strong stability preserving  (SSP) Runge-Kutta method. For the NSWE part Dynamic mesh adaptation is allowed via a nonlinear ALE deformation technique. Elliptic mesh equations
are solved with a finite element method and Jacobi iterations, and the coupling with the flow is obtained either via and ALE remap (plus a static mesh flow time step), or via a full ALE coupling (flow and mesh equations solved at once with explicit iterations). Arbitrary bathymetries can be used (including x-y-z tables)

\chapter{Files}
\begin{itemize}
\item The folder SRC/ contains all the source files:
\item The folder input/ contains the grid files (name.grd) and the bathymetry files (name.dat) 
\item textinput/
\item The output/ folder is initially empty but is the folder were all the output files will be written.
\end{itemize}

\chapter{Code Installation}
\section{Dependencies}  
For the development of the code its necessary the installation of the following software:
\begin{itemize}
	\item cmake
	\item a c/c++ compiler
	\item intel-MKL (or/and)
	\item MUMPS 
\end{itemize}
For visualization of the results  Tecplot 360 is necessary. Intel's MKL can be installed using spack on a linux environment or it can be downloaded from :\\ \url{https://software.intel.com/content/www/us/en/develop/tools/math-kernel-library.html}

%Developer Subversion Access via DAV: Only project developers can access the Subversion tree via this method. Enter your site password when prompted.\\
%
%\textbf{svn checkout --username [username] https://scm.gforge.inria.fr/authscm/[username]/svn/slows/}\\
%Subversion Repository Browser: Browsing the Subversion tree gives you a view into the current status of this project's code. You may also view the complete histories of any file in the repository. 

\section{Compile SLOWS} 

Once the environment is set then we can compile SLOWS. First we have to select the compiling options:\\

\fbox{\begin{minipage}{10em}
mkdir slows/build \\
cd slows/build \\
ccmake .. 
\end{minipage}
} \\

Its a user friendly environment that presents the user different options to assist compilation. For example: \\
\begin{center}
\includegraphics[height=5cm, width=9.cm]{figures/cmake.jpg}  \\
\end{center}


If one wants to solve only the hyperbolic (and NOT include in the calculation the non hydrostatic term), then options NHT\_HYDRO\_TERM is to be set OFF. Open MP is only implemented in the library NHT. Setting NHT\_HYDRO\_TERM to ON and pressing configure will present additional options controlling which linear solver to use. \\

\begin{center}
\includegraphics[height=7cm, width=9cm]{figures/cmake2.jpg}  \\  
\end{center}
If we choose  Intel's MKL then the library should be pre-installed in your system. If we choose MUMPS then there are two options: MUMPS\_PREBUILD means that mumps should be pre-installed in your system but if MUMPS\_SELF\_BUILD is selected then the chosen version is downloaded and compiled in build. Table \ref{options_table} summarizes the different options for compiling.  \\
\begin{center}
\begin{tabular}{|p{3.5cm}||p{3.5cm}|p{3.5cm}|p{3.7cm}| }
	\hline
	\multicolumn{4}{|c|}{Options} \\
	\hline
	CMAKE BUILD TYPE   & {\color{magenta}Release}: faster, to run a test case & {\color{magenta} Debug}: For a developer & {\color{magenta} MinisizeRel}\\
	\hline
	EXTRA COMPILER WARNINGS & {\color{green}ON}: printing them in compile    & {\color{red} OFF} &  \\
	\hline
	IPO &  {\color{green}ON}   & {\color{red} OFF}  &  \\
	\hline
	NON HYDRO TERM & {\color{green}ON}: NHT library will be used & {\color{red} OFF} &   \\
	\hline
	OpenMP&   {\color{green}ON}: used only in the NHT library  &{\color{red} OFF} & \\
	\hline
	SINITIZER \_*& {\color{green}ON}  & {\color{red} OFF}  & gcc tools for debugging for more info see: https://github.com/google /sanitizers/wiki/AddressSanitizer\\
	\hline
\end{tabular}
\label{options_table}
\end{center}

If	NON HYDRO TERM  is set to {\color{green}ON} and the first configure performer ([c]) then we have to choose the library for the linear solver and some more options need to be specified. 
\begin{center}
	\begin{tabular}{|p{3.5cm}||p{3.5cm}|p{3.5cm}|p{3.7cm}| }
		\hline
		\multicolumn{4}{|c|}{Options for NHT} \\
		\hline
		GFORGE user name   & repository's login to download the NHT &  & \\
		\hline
		INTEL MKL & {\color{green}ON}: use of MKL solver    & {\color{red} OFF} &  \\
		\hline
		INTEL MKL PARDISO &  {\color{green}ON}: parallelisation on MKL solver (INTEL MKL should be ON)   & {\color{red} OFF}  &  \\
		\hline
		MUMPS PREBUILD & {\color{green}ON}: MUMPS library should be pre-installed & {\color{red} OFF} &   \\
		\hline
		MUMPS SELF BUILD    & {\color{magenta} none}: when MUMPS is not used   & {\color{magenta}v.5.3.*} different versions of MUMPS&  \\
		\hline
	\end{tabular}
\end{center}
After selecting the options for NHT compiling it should be re-configured [(c)] and generated [(g)]. Then in build folder type \\

\fbox{\begin{minipage}{10em}
		make
	\end{minipage}
} \\
which generates the executable \textbf{slows}.


\chapter{Prepossessing}
The prepossessing state contains the construction of the mesh files and off all the possible additional files needed as an input for the code. 

 The mesh file is a grd file. The structure of a grd file for a 2D problem is as follows:
 
 
 \begin{verbatim}
2 3440 1720 138 //Mesh type, Number of Elements, Number of Nodes, Boundary Nodes

61707 166361 24541 //	Conectivity---Nodes forming the first element...
16691 57827 165387 //			    Nodes forming the second element
62516 171652 24734 // 			     etc....
169099 54070 65656

...........
67916 172054 172081
60077 172389 172100

2.070000e+00 -6.010000e+00 0 //x and y coordinates of the 1st node, Node ID
3.000000e+01 -6.010000e+00 1
4.369000e+01 -6.010000e+00 2


...........
3.682174e+01 2.501767e+00 172850
4.196030e+01 7.764932e+00 172851

0 6 11 3 11 // the two boundary nodes forming a boundary face, 
		3 boundary ids for nodes and the face
6 7 3 3 3
7 8 3 3 3
............
1383 1384 11 11 11

 \end{verbatim}

\section{Create a grid file using Gmsh}
\begin{itemize}
\item Create/modify a geometry file named *.geo
\item  Open Gmesh and make the mesh.  It produces *.msh file
\item Run gmsh2DtoMesh  *.msh. It produces Output.mesh
\item Run run conv\_mesh2neo. It produces neorgid.grd
\end{itemize}
In order to obtain the desired boundary conditions you should either modify the file  conv\_mesh2neo.c or modify the boundary nodes in the produced file neogrid.grd


\section{Create a grid file using an equilateral mesh generator}
You should modify the lines 21-29, inside the file equil\_mesh\_gen.c. More precisely  VL is the vertical length of the desired domain, HL the horizontal one, dx is the elemet's size on x direction, 
dy is the elements size on y direction. x,y is the down left point of the domain. 

You should also modify the lines 465-490 in order to obtain the desired boundary conditions. The boundary conditions are noted as:
\begin{itemize}
\item ID 3: Wall boundary conditions
\item ID 10: Periodic boundary conditions
\item ID 11: Inflow/outflow boundary conditions
\end{itemize}
\red{Attention:} The file equil\_mesh\_gen.c creates only rectancular domains with equilateral triangles.  

\section{Extra files}
Some extra files may be needed to use slows , depending on the test case.  All of them should be included in the folder input. 



\chapter{Using SLOWS}
The file that defines the user's interface is inside the folder textinput and is named  "inputfile-ex-ex-loose.txt". For example:
\begin{verbatim}
***************************************************
***************************************************
***************************************************
***************************************************
***************************************************
**                  PHYSICS                      **
***************************************************
    Initial state (1-10)                        84
    Initial solution file (or NULL)	     NULL
    Problem                                     0
    Gamma                                      9.81 
    U infinity                                 1. 
    V infinity                                 0.
    p coefficient                              0.4
    A coefficient                              1.
    m coefficient                              1.
    Friction coefficient                       0.01
    Exner flux 0 (exponential), 1 (mass flux)  0
***************************************************
**                  MODEL                        **
***************************************************
   Enable dispersion terms (Green-Naghdi)      0 
   T-Matrix:   0 (full) 1 (const diagonal)
	       2 (non conservative form)        0
   SPD Format: 0 (None) 1 (standard) 2 (SPD)    1
   Breaking: 0 (None) 1 (Hybrid) 2 (Turbulence) 0
   Hybrid Param:
	- gamma (surface variation crit)	0.6
	- phi (slope angle crit)		0.37
   Turbulence Param:
	- sigma (empirical const)		0.2
	- cd (empirical const)			0.01
	- lt (turbulence length)		1.5
***************************************************
**                  GEOMETRY                     **
***************************************************
   External grid file                       oregon
***************************************************
**                  NUMERICS                     **
***************************************************
   Blending: 0 (LLFs), 1 (Bc), 2 (MUSCL2), 3 (MUSCL3)  3
   Scaling: 0 (scalar tau), 1 (matrix tau)     1 
   Interpolate:  0 (std),1 (kinetic)           0
   Linear bathymetry (0) linear, (1) exact     0
   Time discretization:0 (RK1),1 (RK2),2 (RK3) 2
   Explicit friction treatment: 0(off) 1(on)    1
   Time CFL                                    0.5
   Zero_H				       1.e-6
   Zero_U			              -1.e-6
***************************************************
**                  ALGEBRA                      **
***************************************************
   Convergence treshold			     -3.
   Convergence limit                         -7.
   Check Steady State                         0
   Convergence variable (0->Neq.s-1)         -1
***************************************************
**                   STOP                        **
***************************************************
   Final time		                        40.0
   Number of additional output steps     	0
   Output time step size			1.0
   Maximum number of time steps     	      100000000
   Stop					        0	
***************************************************
**                  OUTPUT                       **
***************************************************
   Movie (0/1)		       1
   Steps to save	       200
   Information Level (0/1)	       	0
   Output file	                        name_of_file
   Write solution time                     0 
***************************************************
***************************************************
                     8576
***************************************************
***************************************************
***************************************************
\end{verbatim}

\begin{enumerate}

\item Physics section
\begin{enumerate}
\item The initial state variable defines the test case used. A list of the test cases already introduced in the code can be found in section \ref{old_cases}.
\item  Initial solution file (or NULL): Its the name of the initial solution file name.dat. The format of the file should be a tecplot format. If there is no initial solution file and the initial conditions are defined inside the code you must put NULL.
\item Problem:  $\; 0$
\item Gravity:   The gravity constant
\item U infinity: $\; 1$ 
\item V infinity: $\; 0$                            
\item p coefficient $\; 0.4$                              
\item A coefficient $\; 1$                             
\item m coefficient $\; 1$                             
\item   Friction coefficient : Manning number - Friction is implemented using the manning formula
\item   Exner flux $\; 0$ 
\end{enumerate}     

\item  Model section
\begin{enumerate}
\item  Enable dispersion terms (Green-Naghdi) : give 0 to run shallow water equations and give 1 to run the Green-Naghdi (GN) equations      
\item T-Matrix:   give 0 (for the full matrix ) and 1 (for the constal diagonal form). These are two different forms of the GN  equations. See the corresponding papers for details.
\item \quad \quad \quad  \quad  -give 2 for the non conservative form of the matrix    or   0 otherwise.
\item  \quad \quad \quad  \quad -SPD Format: 0 for None,  1  for standard format  and 2 for Symmetric Positive Definite format.    
\item   Breaking:     If the GN equations are used a breaking closure is needed. Give 0 for none (when SW are used or there is no breaking in the case), give 1 for the  Hybrid wave breaking model and  2 for the Turbulence model.
\item   Hybrid Param:  The above are the parameters for the triggering of the breaking procedure. But be used in both breaking models
\item \quad \quad \quad  \quad	- gamma (for the surface variation criterion) between 0.3-0.6 
\item  \quad \quad \quad  \quad	- phi (slope angle criterion) between 0.37-0.57
\item  Turbulence Param: (Only given when the Breaking is 2) 
\item \quad \quad \quad  \quad	- sigma (empirical const)		case dependent usually between 0.2-0.8
\item \quad \quad \quad  \quad	- cd (empirical const)		case dependent usually between 0.01-0.08
\item \quad \quad \quad  \quad	- lt (turbulence length)	case dependent
\end{enumerate}
   
 \item Geometry
 \begin{enumerate}
\item  External grid file : The name of the mesh file without the extention grd 
 \end{enumerate}  
   
\item  Numerics  
\begin{enumerate}                
\item   Blending:  Defines the numerical scheme. Give 0 for LLFs, 1 for Bc, 2 for MUSCL2 , 3 for MUSCL3  
\item   Scaling: 0 (scalar tau), 1 (matrix tau) : 1
\item   Interpolate:  0 (std),1 (kinetic) : 0
\item   Linear bathymetry (0) linear, (1) exact : 0    
\item   Time discretization: Defines the discretization scheme in time. Give  0 for Runge Kutta (RK)1 ,  Give 1 for RK2 , Give 2 for RK3.
\item   Explicit friction treatment:  The way friction is treated. Give 1 for an  explicit treatment and 0 for an implicit treatment.
\item   Time CFL : CFL condition                                 
\item    $Zero_H$  Threshold for the wet/dry treatment, for H.				      
\item    $Zero_U$  Threshold for the wet/dry treatment, for U.			             
 \end{enumerate}                     
 
 
 \item Algebra
 \begin{enumerate}
 	\item Convergence treshold:  $\; -3.$
    \item Convergence limit:     $\; -7.$
    \item Check Steady State     $\; 0 $
    \item Convergence variable (0$->$Neq.s-1)         $\; -1 $
\end{enumerate}
  
  
 \item Stop
 \begin{enumerate}
 \item  Final time	: Finial time of the simulation	                       
 \item  Number of additional output steps:    	
 \item  Output time step size:   Give the time steps for the output files.
 \item  Maximum number of time steps     	     
 \item  Stop
 \end{enumerate}
   
 \item Output
 \begin{enumerate}
 \item  Movie: Output files for a movie  no-0   yes-1		       
 \item   Steps to save: 	   The number of time steps to save
 \item   Information Level (0/1) 	       	
\item    Output file: The name of the output tecplot files
\item    Write solution time                   
 \end{enumerate}
                      
\end{enumerate}


\section{Run a test case already introduced in the code}
\label{old_cases}
There are a number of well known test cases already introduced in the code. Each one has a unique numbering and a unique grid file. Below there is a list of this test cases: \\
\textbf{Note}: In the input file  the parameter External grid file  must be set to DEFAULT
\begin{itemize}
%\item   Test id [1]. \red{Wedge}
%\item   Test id [2]. Initial state
%\item   Test id [3]. Dam break.  The name of the grid file used is: dambreak_*.grd
%\item   Test id [4]. Perturbation over a smooth test bed. The name of the grid file used is :... See \cite{arpaia2016r} for details of the problem. 
\item  Test id [5]. Circular dam break. See \cite{arpaia2016r} for details of the problem.    
%\item   Test id [6]. 2D with dry cylinder.
%\item   Test id [7]. Asymmetric dam break. See \cite{arpaia2016r} for details of the problem. 
%\item   Test id [8]. Solitary wave over a conical island. See \cite{arpaia2016r} for details of the problem. 
\item   Test id [9]. Lake at rest test case. 
\item   Test id [13]. Vortex
%\item   Test id [15]. Thacker's test case
%\item   Test id [16]. Thacker's test case \red{whats the difference with 15?}
%\item   Test id [23]. 1D runup. Carrier and Greenspan (1958) test case. See \cite{kazolea2013well} for the set up of the case. 
\item   Test id [24]. Shelf with island. See \cite{arpaia2016r} for the set up of the test case.
\item   Test id [25]. Okushiri test case. Tsunami over the beach. For the set up of the test case see \cite{arpaia2016r}.
 \red{Attention:} Extra files are used which are  placed in the folder input. These are:
 \begin{enumerate}
 	\item Okushiriwave.dat
 \end{enumerate}
%\item   Test id [34]. Double dam break. 
%\item   Test id [35]. Dambreak with bathymetry.
\item   Test id [70]. Solitary wave propagation over a flat bottom. 
%\item   Test id [71]-[85]-[86]. Convergent channel and propagation of an ondular bore. See  for the set up.   
%\item   Test id [72]. Gironde
%\item   Test id [72]. Garonne
%\item   Test id [72]. Dordonge
\item   Test id [75]. Rip Current (Hamm test case). See ...for the set up.
\item   Test id [76]. Solitary wave over a sloping beach. See \cite{filippini2016flexible} for the set up.
\item   Test id [77]. Solitary wave over a reef.  \cite{filippini2016flexible} for the set up.  
\item   Test id [78]. Wave over a bar. See also  \cite{filippini2016flexible} for the set up of the test case. 
%\item   Test id [79]. Gironde - Garonne-Dordogne
%\item   Test id [80]. Gironde - Garonne-Dordogne \red{whats the difference with 79}
\item   Test id [81]. Elliptic Shoal. See \cite{ricchiuto2014upwind} for the set up of the test case. 
%\item   Test id [82]. Circular Shoal
%\item   Test id [83]. Manufactured Solution \red{should we put this??}
\item   Test id [84]. Seaside, Oregon. Tsunami inundation modeling in constructed environments. See \cite{park2013tsunami}  \red{Attention:} Extra files are used which are  placed in the folder input. These are:
\begin{enumerate}
\item Forcing.dat: contains the wave series that serves as an input for the model. This boundary condition is interpolated inside the code, in the correct time.
\item Positions\_jauges\_Bat.dat: contains the position of the wave gauges
\item Positions\_jauges\_Exp.dat: the same
\item bathy\_oregon.dat: contains the bathymetry of the test cases which is interpolated inside the code in order to be attributed to the position of the nodes.   
\end{enumerate} 
\end{itemize}


\section{Set up a new test case}

\begin{itemize}
\item Modify the "textinput/inputfile-ex-ex-loose.txt" file according to your test case.
\item If you want to include the bathymetry form a file modify initialize.c  and add the name of the file in geometry.c (linked with the id number of the problem named initial\_state).
\item If you have an analytic function for the topography, create the function in source.c named double source\_name()
\item In select\_function.c write the initial state dependent settings.  In here you should place the name of the source function and the boundary function linked with the id number of the 
problem (initial state).

\item if you want to instal wave gauges/probes you should do it inside utilities.c 
\item Boundary conditions are computed in boundary\_residual.c inside the function  void boundary\_conditions(void) 

\end{itemize}

\section{Postprocessing}
For the post-processing of the data you will need \textbf{Tecplot 360}. The standard output files are written in tecplot ascii format  named *.dat.

\chapter{Error Messages}
ERROR: Wrong problem type given! \\
Reason: The test case number in the input file, is not corresponding in an existing test case or the initial conditions of the new test case have not been entered in initial\_solution.c \\
Solution: Choose an existing problem from the list described in  section \ref{old_cases} or include the initial conditions in the file initial\_solution.c   \\

\noindent ERROR: inputfile-ex-ex-loose.txt not found! \\
Reason: The input file is not inside the folder textinput.\\
Solution: Create the inputfile-ex-ex-loose.txt as described in chapter Using Slows. \\

\noindent ERROR: inputfile was not read correctly!! \\
Reason: The input file is not exactly as described in section Using Slows.\\
Solution: Set the file as expected. \\
 
\noindent ERROR: Memory allocation error. \\
Reason: Not enough memory on you system in order to allocate memory. \\
Solution: Check the memory requirements of your system. \\

\noindent ERROR: External spatial grid file * was not found !\\
Reason: The name of the grid file that you placed in the input file is not corresponding to a grid file in input/ \\
Solution: Check the name of the grid file or put the grid file *.grd inside the forder input/ \\

\chapter{Examples}
%\section{Solitary wave over a conical island}

%first case using only the shallow water part
%\section{Rip current}
%second case using the library


\bibliography{manual_ref}{}
\bibliographystyle{plain}

\end{document}
