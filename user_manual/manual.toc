\contentsline {chapter}{\numberline {1}Introduction}{2}{chapter.1}%
\contentsline {chapter}{\numberline {2}Files}{3}{chapter.2}%
\contentsline {chapter}{\numberline {3}Code Installation}{4}{chapter.3}%
\contentsline {section}{\numberline {3.1}Dependencies}{4}{section.3.1}%
\contentsline {section}{\numberline {3.2}Compile SLOWS}{4}{section.3.2}%
\contentsline {chapter}{\numberline {4}Prepossessing}{8}{chapter.4}%
\contentsline {section}{\numberline {4.1}Create a grid file using Gmsh}{9}{section.4.1}%
\contentsline {section}{\numberline {4.2}Create a grid file using an equilateral mesh generator}{9}{section.4.2}%
\contentsline {section}{\numberline {4.3}Extra files}{9}{section.4.3}%
\contentsline {chapter}{\numberline {5}Using SLOWS}{10}{chapter.5}%
\contentsline {section}{\numberline {5.1}Run a test case already introduced in the code}{14}{section.5.1}%
\contentsline {section}{\numberline {5.2}Set up a new test case}{15}{section.5.2}%
\contentsline {section}{\numberline {5.3}Postprocessing}{15}{section.5.3}%
\contentsline {chapter}{\numberline {6}Error Messages}{16}{chapter.6}%
\contentsline {chapter}{\numberline {7}Examples}{17}{chapter.7}%
